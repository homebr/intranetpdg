﻿using System;
using FrameWork.DataObject;
using FrameWork.WebControls;

namespace ClassLibrary
{

    [ClassData("ID", false )]
    public class LinkProgramas : ObjectData
    {

        /// <summary>
        /// ID CHAVE PRIMARIA
        /// </summary>
        [FieldData()]
        public int ID;

        /// <summary>
        /// ID Nome
        /// </summary>
        [FieldData(Length = 150)]
        public string Nome;

        [FieldData(Length = 150)]
        public string Descricao;

       


        [FieldData(Length = 150)]
        public string Link;

    
        [FieldData()]
        public int Sequencia;

        [FieldData(Length = 150)]
        public string Class;

        [FieldData(Length = 150)]
        public string Logo;

      

        [FieldData()]
        public Boolean Ativo;

 
       


    }


}