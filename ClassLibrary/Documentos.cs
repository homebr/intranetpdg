﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using FrameWork.WebControls;
using FrameWork.DataObject;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Runtime.InteropServices;

namespace ClassLibrary
{
    [ClassData("ID", true)]
    public class Documentos : ObjectData
    {

        [FieldData()]
        public int ID;

        [FieldData(Length =30)]
        public string CPF;

        [FieldData(Length = 100)]
        public string Arquivo;


        [FieldData(Length = 300)]
        public string Descricao;

        [FieldData(Length = 100)]
        public string Email;


        [FieldData(Length = 150)]
        public string Tipo;

        [FieldData()]
        public int IDusuario;

        [FieldData(DefaultValue = "0")]
        public Boolean Ativo;

        [FieldData()]
        public int Area;

        [FieldData(Length = 150)]
        public string Setor;

        [FieldData()]
        public DateTime DataInclusao = DateTime.Now;

        [FieldData()]
        public DateTime DataAlteracao;


        [FieldData()]
        public DateTime Data;
    }
}
