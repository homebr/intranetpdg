﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using FrameWork.WebControls;
using FrameWork.DataObject;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Runtime.InteropServices;
using ClassLibrary;

namespace ClassLibrary
{
    [ClassData("ImagemID", true)]
    public class ImovelImagens : FrameWork.DataObject.ObjectData
    {

        [FieldData(), TableLayout()]
        public int ImagemID;

        [FieldData]
        public int CodEmpreendimento;

        [FieldData]
        public int Posicao;

        [FieldData]
        public bool Principal;

        [FieldData]
        public int Categoria;

        [FieldData(), FieldControl(Width = 350), FieldTitle("Descrição:", Width = 100)]
        public string Descricao;

        [FieldData(), FieldTitle("Arquivo (*.jpg):"), FieldControl(Type = FieldControlTypes.FileUpload, Execute = "SaveImage", Options = ".jpg .jpeg")]
        public string FileName;

        [FieldData(), FieldTitle("Arquivo (*.jpg):"), FieldControl(Type = FieldControlTypes.FileUpload, Execute = "SaveImage", Options = ".jpg .jpeg")]
        public string FileName_600x600;

        [FieldData()]
        public string FileNameOriginal;

        [FieldData]
        public string URLExterna;

        [FieldData]
        public DateTime Periodo;

        [FieldData]
        public DateTime DataInclusao;

        [FieldData()]
        public DateTime DataAlteracao;

        /// <summary>
        /// Usuario da ultima alteração do registro
        /// </summary>
        [FieldData(), FieldControl(Type = FieldControlTypes.Label),
         //FieldColumn(Header = "Usuário", Width=100),
         FieldTitle("Usuário: ")]
        public string Usuario;

        //[FieldControl(Load = "LoadGrid"), CellLayout(ColSpan = 2)]
        //public DataGrid dtgFotos;

        //public override void Delete()
        //{
        //    if (this.Parent != null)
        //    {
        //        ControlLayout cl = (ControlLayout)this.Parent;
        //        Geral.SaveFile(cl.Page, "Foto", ".jpg", this.ImagemID, null);
        //    }
        //    base.Delete();
        //    //if (this.Home && (this.LoadWhere("ImovelID=" + Convert.ToString(this.ImovelID), new ParameterItem[0]) == 1))
        //    //{
        //    //    this.Home = true;
        //    //    this.Save();
        //    //}
        //}

        public override void Save()
        {
            //if (this.Parent != null)
            //{
            //    this.DataSource.Execute("UPDATE Fotos SET Home=0 WHERE ImovelID=" + Convert.ToString(this.ImovelID));

            //}
            base.Save();
            //if ((this.Parent != null) && this.Home)
            //{
            //    this.DataSource.Execute("UPDATE Veiculo SET FotoID=" + Convert.ToString(this.ImagemID) + " WHERE ImovelID=" + Convert.ToString(this.ImovelID));
            //}
        }

        //public void SaveImage(object sender, string cFileName, string cFileExt, byte[] data)
        //{
        //    Button btn = (Button)sender;
        //    //DirectoryInfo di = new DirectoryInfo((cPath));
        //    FileInfo fi;

        //    try
        //    {

        //        if (this.TotalLoad == 0)
        //        {
        //            this.Posicao = (int)this.DataSource.Execute("SELECT ISNULL(MAX(Posicao)+1,1) FROM Fotos where ImovelID=" + Geral.intImovelID);
        //            this.ImovelID = Convert.ToInt32(Geral.intImovelID);
        //            this.FileName = ImovelID + "_" + this.Posicao + ".jpg";
        //            this.Save();
        //        }

        //        //ImagemSaveDelegate btnND = this.onSaveEvent;
        //        String strFoto = Geral.strPathFotosFS + @"\" + this.FileName;
        //        fi = new FileInfo(strFoto);
        //        if (fi.Exists)
        //        {
        //            throw new Exception("Arquivo j\x00e1 existe: " + fi.Name);
        //        }
        //        FileStream fs = fi.Create();
        //        fs.Write(data, 0, data.Length);
        //        fs.Close();

        //    }
        //    catch (Exception exception1)
        //    {
        //        //ProjectData.SetProjectError(exception1);
        //        Exception ex = exception1;
        //        if (btn != null)
        //        {
        //            ((PageLayout)btn.Page).DisplayMessage = ex.Message;
        //        }
        //        //ProjectData.ClearProjectError();
        //    }

        //}

        ////// Properties
        ////public Noticias Noticia
        ////{
        ////    get
        ////    {
        ////        Noticias oNoticia = new Noticias();
        ////        oNoticia.DataSource = this.DataSource;
        ////        oNoticia.Load(this.ImovelID);
        ////        return oNoticia;
        ////    }
        ////}

        //public void LoadGrid(DataGrid dtg)
        //{
        //    dtg.AutoGenerateColumns = false;
        //    dtg.GridLines = GridLines.None;
        //    dtg.ShowHeader = false;
        //    dtg.Width = new Unit("90%");

        //    BoundColumn cID = new BoundColumn();
        //    cID.DataField = "ImagemID";
        //    cID.Visible = false;
        //    dtg.Columns.Add(cID);

        //    HyperLinkColumn cImg = new HyperLinkColumn();
        //    cImg.DataTextField = "ImagemID,Posicao";
        //    cImg.DataTextFormatString = "<img src='" + Geral.strPathFotosFS + "{0}_{1}.jpg' width=150 border=0>";
        //    cImg.DataNavigateUrlField = "ImagemID,Posicao";
        //    cImg.DataNavigateUrlFormatString = Geral.strPathFotosFS + "{0}_{1}.jpg";
        //    cImg.Target = "_blank";
        //    cImg.ItemStyle.Width = new Unit(160);
        //    dtg.Columns.Add(cImg);

        //    ButtonColumn cDel = new ButtonColumn();
        //    cDel.Text = "<img src='Skin/btn_excluir_img.jpg' border=0>";
        //    cDel.ButtonType = ButtonColumnType.LinkButton;
        //    cDel.ItemStyle.Width = new Unit(30);
        //    cDel.Visible = true;//((PageLayout)dtg.Page).CheckUserRight(Modulos_Direitos.Orcamento_Editar);
        //    dtg.Columns.Add(cDel);

        //    dtg.ItemCommand += DeleteImagem;

        //    BindGrid();

        //}

        //public void DeleteImagem(object sender, DataGridCommandEventArgs e)
        //{
        //    string cID = e.Item.Cells[0].Text;
        //    ImovelImagens i = new ImovelImagens();
        //    i.Load(cID);
        //    string cFileName = string.Format(Geral.strPathFotosFS + "{0}_{1}.jpg", i.ImagemID);
        //    i.Delete();
        //    System.IO.FileInfo fi = new System.IO.FileInfo(cFileName);
        //    fi.Delete();
        //    BindGrid();
        //}

        //public void BindGrid()
        //{
        //    string str = "SELECT ImagemID,FileName as Foto FROM ImovelImagens WHERE ImovelID=" + Geral.intImovelID + " ORDER BY Posicao";
        //    dtgFotos.DataSource = this.DataSource.Select(str);
        //    dtgFotos.DataBind();
        //}


    }
}