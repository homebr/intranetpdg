﻿using System;
using FrameWork.DataObject;

namespace ClassLibrary
{
    [ClassData("ID", true) ]
    public class Eventos : ObjectData
    {

        /// <summary>
        /// Código interno de referencia única para relacionamento no banco de dados
        /// </summary>
        [FieldData()]
        public int ID;

        /// <summary>
        /// Título
        /// </summary>
        [FieldData(Length = 140)]
        public string Titulo;

        [FieldData()]
        public int Categoria;

        [FieldData()]
        public DateTime Data;

        [FieldData(Length = 50)]
        public string Autor;

        [FieldData(Length = 50)]
        public string Fonte;

        [FieldData(ColumnType = ColumnTypes.Text)]
        public String Chamada;

        [FieldData(ColumnType=ColumnTypes.Text)] 
        public String Conteudo;

        [FieldData(Length = 255)]
        public string Imagem;

        [FieldData(Length = 255)]
        public string Video;

    

        [FieldData(Length = 255)]
        public string Link;
        

        [FieldData(Length = 255)]
        public string FotoAutor;

        //[FieldData(Length = 100)]
        //public string FotoPequena;

        [FieldData(DefaultValue = "1")]
        public int Tipo;

        [FieldData(DefaultValue = "0")]
        public Boolean Destaque;

        [FieldData()]
        public Boolean Ativo;

        [FieldData()]
        public int Acesso;

        [FieldData(Length = 150)]
        public string sku;

        [FieldData()]
        public DateTime DataInclusao; // = DateTime.Now;

        [FieldData()]
        public DateTime DataAlteracao; // = DateTime.Now;

        /// <summary>
        /// Usuario da ultima alteração do registro
        /// </summary>
        [FieldData()]
        public string Usuario;



    }
}
