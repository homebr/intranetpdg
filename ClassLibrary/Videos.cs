﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using FrameWork.DataObject;
using FrameWork.WebControls;
using System.Web.UI.WebControls;

namespace ClassLibrary
{
    [ClassData("ID", true)]
    public class Videos : ObjectData
    {

        /// <summary>
        /// ID CHAVE PRIMARIA
        /// </summary>
        [FieldData()]
        public int ID;

        [FieldData()]
        public string Titulo;

        [FieldData(ColumnType = ColumnTypes.Text)]
        public string Descricao;


        [FieldData()]
        public string Titulo_en;

        [FieldData(ColumnType = ColumnTypes.Text)]
        public string Descricao_en;


        [FieldData()]
        public string Titulo_de;

        [FieldData(ColumnType = ColumnTypes.Text)]
        public string Descricao_de;



        [FieldData()]
        public string Foto;

        [FieldData(Length = 250),
         FieldTitle("Url: ")]
        public string Url;

        [FieldData(Length = 1),
         FieldTitle("Situação: ")]
        public string Situacao;

        [FieldData()]
        public int IDPagina;

        ///// <summary>
        ///// Sequencia do Item
        ///// </summary>
        //[FieldData(), FieldTitle("Sequência: ")]
        //public int Sequencia;

        /// <summary>
        /// ID DataInclusao
        /// </summary>
        [FieldData()]
        public DateTime DataInclusao;

        [FieldData()]
        public DateTime DataAlteracao;

        [FieldData()]
        public string Usuario;


    }


}


