﻿using System;
using FrameWork.DataObject;
using FrameWork.WebControls;

namespace ClassLibrary
{

    [ClassData("ID", false )]
    public class Parceria : ObjectData
    {

        /// <summary>
        /// ID CHAVE PRIMARIA
        /// </summary>
        [FieldData()]
        public int ID;

        /// <summary>
        /// ID Nome
        /// </summary>
        [FieldData(Length = 150)]
        public string Empresa;



        [FieldData(Length = 150)]
        public string Link;

        [FieldData(Length = 150)]
        public string Tipo;

        [FieldData(Length = 150)]
        public string Cupom;

        [FieldData(Length = 150)]
        public string Desconto;


        [FieldData(ColumnType = ColumnTypes.Text)]
        public string Obs;

        [FieldData()]
        public int Sequencia;

        [FieldData(Length = 150)]
        public string Logo;



        [FieldData()]
        public Boolean Ativo;


        [FieldData(Length = 50)]
        public string Class;


    }


}