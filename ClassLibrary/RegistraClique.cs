﻿using System;
using System.Collections.Generic;
using System.Text;
using FrameWork.DataObject;

namespace ClassLibrary
{
    [ClassData("",false)]
    public class RegistraClique: ObjectData 
    {
        [FieldData()]
        public int IDVisitante;

        [FieldData()]
        public DateTime Data = DateTime.Now;

        [FieldData()]
        public String Pagina;

        [FieldData()]
        public String Objeto;

        [FieldData()]
        public int IDRegistro;

        [FieldData(Length=255)]
        public String URLDestino;

        [FieldData]
        public string IP;
    }
}
