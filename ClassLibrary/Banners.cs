﻿using System;
using FrameWork.DataObject;
using FrameWork.WebControls;

namespace ClassLibrary
{
    [ClassData("IDBanner", true, LogXML = "xmlLOG")]
    public class Banners : ObjectData
    {


        /// <summary>
        /// Código 
        /// </summary>
        [FieldData()]
        public int IDBanner;

        
        /// <summary>
        /// Titulo da Mídia
        /// </summary>
        [FieldData(Length=80)]
        public string Titulo;


        /// <summary>
        /// Frase da Mídia
        /// </summary>
        [FieldData(Length = 250)]
        public string Frase;

        [FieldData(ColumnType = ColumnTypes.Text)]
        public string Descricao;

        /// <summary>
        /// Posição
        /// </summary>
        [FieldData(Length = 1)]
        public int Posicao;

        /// <summary>
        /// Tipo de Link. usados atualmente: T,L,C,H
        /// </summary>
        [FieldData(Length = 1)]
        public string TipoLink;


        /// <summary>
        /// Link
        /// </summary>
        [FieldData(Length = 250)]
        public string Link;


      
        [FieldData(Length = 255)]
        public string Imagem;


        [FieldData(Length = 255)]
        public string Imagem_iPad;

        [FieldData(Length = 255)]
        public string Imagem_Mobile;


        /// <summary>
        /// Data Inicial da campanha
        /// </summary>
        [FieldData()]
        public DateTime DataInicial;

        /// <summary>
        /// Data Final da campanha
        /// </summary>
        [FieldData()]
        public DateTime DataFinal;

        [FieldData(DefaultValue = "0")]
        public Boolean Indeterminado;

        [FieldData()]
        public Boolean Ativo;

        /// <summary>
        /// Data Inclusao do registro
        /// </summary>
        [FieldData()]
        public DateTime DataInclusao = DateTime.Now;

        /// <summary>
        /// Data da ultima alteração do registro
        /// </summary>
        [FieldData()]
        public DateTime DataAlteracao;

        /// <summary>
        /// Usuario da ultima alteração do registro
        /// </summary>
        [FieldData()]
        public string Usuario;

       
        [FieldData(ColumnType = ColumnTypes.Text)]
        public String xmlLOG;




    }
}
