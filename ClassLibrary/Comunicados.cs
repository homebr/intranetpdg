﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using FrameWork.DataObject;
using FrameWork.WebControls;
using System.Data;

namespace ClassLibrary
{
    [ClassData("ComunicadoID", true),
     ClassControl(Title = "Comunicados"),
     //ClassRight(101, "C,E,I,X", "Cadastro de Comunicados", Group = "Conteudo")
    ]
    public class Comunicados : ObjectData
    {

#region campos
        /// <summary>
        /// Código interno de referencia única para relacionamento no banco de dados
        /// </summary>
        [FieldData(), TableLayout(), FieldColumn("ComunicadoID"),
         FieldControl(Type = FieldControlTypes.Label)]
        public int ComunicadoID;


        /// <summary>
        /// Título
        /// </summary>
        [FieldData(Length = 80),
         FieldControl(MaxLength = 80, Width = 200, Validate = "!", ValidateError = "Favor inserir o título!"),
         FieldColumn("NoticiaID", Header = "Título"),
         FieldTitle("Título: ")
        ]
        public string Titulo;


        [FieldData(), FieldTitle("Data", Width = 80), FieldColumn(Width = 120), FieldFilter(FieldFilterTypes.DataSelect, Text = "Data: "), FieldControl(Format = "{0:dd/MM/yy HH:mm}", Category = 1, Width = 150, MaxLength = 17, Mask = "00/00/00 00:00:00")]
        public DateTime Data = System.DateTime.Now;

        //[FieldData(), FieldControl(Width = 200, Category = 1)]
        //public string Autor;

        [FieldData()]
        public string ImoveisIDs;

        [FieldData(ColumnType = ColumnTypes.Text)]
        public string Descricao;


        [FieldData()]
        public string Arquivo;

        //[FieldData(), FieldControl(Width = 30, Category = 1)]
        //public int Posicao;

        [ FieldData(Length = 1)]
        public string Situacao;

        [FieldData()]
        public Boolean Urgente;

        //[FieldData(Length = 150)]
        //public string pagina;

        [FieldData()]
        public Boolean Visualizado;

        [
         FieldData(),
         FieldControl(Type = FieldControlTypes.Label),
         FieldTitle("Data de Inclusão: ")
        ]
        public DateTime DataInclusao; // = DateTime.Now;

        [
         FieldData(),
         FieldControl(Type = FieldControlTypes.Label),
         FieldTitle("Data de Alteração: ")
        ]
        public DateTime DataAlteracao; // = DateTime.Now;

        /// <summary>
        /// Usuario da ultima alteração do registro
        /// </summary>
        [
         FieldData(),
         FieldControl(Type = FieldControlTypes.Label),
         FieldTitle("Usuário: ")
        ]
        public string Usuario;

 #endregion



    }
}
