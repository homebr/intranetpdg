﻿using System;
using FrameWork.DataObject;
using FrameWork.WebControls;

namespace ClassLibrary
{
    [ClassData("ID", true, LogXML = "xmlLOG")]
    public class Cases : ObjectData
    {

        /// <summary>
        /// ID CHAVE PRIMARIA
        /// </summary>
        [FieldData()]
        public int ID;

        [FieldData()]
        public int IDCliente;

        [FieldData()]
        public int IDTipo;

        [FieldData(Length = 150)]
        public string Titulo;

        [FieldData(ColumnType = ColumnTypes.Text)]
        public string Descricao;

        [FieldData()]
        public DateTime Data;

        [FieldData(Length = 50)]
        public string Local;

        [FieldData(Length = 50)]
        public string Segmento;

        [FieldData(Length = 30)]
        public string Espaco;

        [FieldData(Length = 150)]
        public string Foto;

        [FieldData(Length = 250)]
        public string Video;


        [FieldData(DefaultValue = "0")]
        public Boolean Ativo;

        [FieldData(DefaultValue = "0")]
        public Boolean Destaque;

        /// <summary>
        /// Sequencia do Item
        /// </summary>
        [FieldData()]
        public int Sequencia;

        [FieldData(Length = 150)]
        public string sku;

        [FieldData(Length = 50)]
        public string Relato_Nome;

        [FieldData(Length = 50)]
        public string Relato_Cargo;

        [FieldData(ColumnType = ColumnTypes.Text)]
        public string Relato_Descricao;

        [FieldData(Length = 50)]
        public string Relato_foto;

        /// <summary>
        /// ID DataInclusao
        /// </summary>
        [FieldData()]
        public DateTime DataInclusao;

        [FieldData()]
        public DateTime DataAlteracao;

        [FieldData()]
        public string Usuario;

        [FieldData(ColumnType = ColumnTypes.Text), FieldControl(Type = FieldControlTypes.LogDisplay), FieldTitle(null)]
        public string xmlLOG;

        public static string getCliente(int id)
        {
            return FrameWork.Util.GetString(FrameWork.DataObject.DataSource.DefaulDataSource.Execute("Select Nome From Clientes Where ID=" + id));
        }

    }


}

