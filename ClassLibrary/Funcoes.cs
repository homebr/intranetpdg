﻿
using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Net.Mail;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public static class Funcoes
    {

        static System.Random random = new System.Random();


        public static string RemoveAcentos(String vStr)
        {
            String s1 = "";
            String s2 = "";
            s1 = "ÁÂÃÉÊÍÓÕÔÚÇáâãéêíóõôúüç";
            s2 = "AAAEEIOOOUCaaaeeiooouuc";
            if (vStr.Length != 0)
            {
                for (int i = 1; i < s1.Length; i++)
                {
                    vStr = vStr.Replace(s1.Substring(i, 1), s2.Substring(i, 1));
                }
            }
            return vStr;
        }

        public static string MaskedString(String Str)
        {
            Str = RemoveAcentos(Str);
            Str = Str.Replace("a", "[a,á,â,ã,à,Á,Â,Ã]");
            Str = Str.Replace("e", "[e,é,ê,É,Ê]");
            Str = Str.Replace("i", "[i,í,Í]");
            Str = Str.Replace("o", "[o,ó,ô,õ,Ó,Ô,Õ]");
            Str = Str.Replace("u", "[u,ú,ü,Ú,Ü]");
            Str = Str.Replace("c", "[c,ç,Ç]");
            Str = Str.Replace("A", "[a,á,â,ã,à,A,Á,Â,Ã]");
            Str = Str.Replace("E", "[e,é,ê,E,É,Ê]");
            Str = Str.Replace("I", "[i,í,I,Í]");
            Str = Str.Replace("O", "[o,ó,ô,õ,O,Ó,Ô,Õ]");
            Str = Str.Replace("U", "[u,ú,ü,U,Ú,Ü]");
            Str = Str.Replace("C", "[c,ç,C,Ç]");
            Str = Str.Replace(".", "[.,-]");
            return Str;
        }

        public static string FormataFrase(string str)
        {
            str = str.ToLower();
            string[] strArray = str.Split(new char[1]
      {
        ' '
      });
            string str1 = "";
            for (int index = 0; index < strArray.Length; ++index)
            {
                string str2 = strArray[index];
                if (str2 != "")
                    str1 = str1 + str2.Substring(0, 1).ToUpper() + str2.Substring(1).ToLower() + " ";
            }
            str = str1.Substring(0, str1.Length - 1);
            str = str.Replace("~*", " ");
            str = str.Replace(" Da ", " da ");
            str = str.Replace(" Das ", " das ");
            str = str.Replace(" De ", " de ");
            str = str.Replace(" Des ", " DES ");
            str = str.Replace(" Do ", " do ");
            str = str.Replace(" Dos ", " dos ");
            return str;
        }

        //public static object FormataData(NumData, TipoData)
        //{
        //    object functionReturnValue = null;

        //    string NumHora = null;
        //    string AUX = null;
        //    NumHora = "";
        //    AUX = "";

        //    if (object.ReferenceEquals(NumData, System.DBNull.Value)) {
        //        if (TipoData == 0) {
        //            // FormataData = ""
        //        } else if (TipoData == 11) {
        //            functionReturnValue = "00:00";
        //        }
        //        return NumData;
        //        return functionReturnValue;
        //    }
        //    //NumData = Replace(CStr(NumData), "&nbsp;", "")
        //    if ((string.IsNullOrEmpty(NumData.Trim()))) {
        //        if (TipoData == 5) {
        //            //FormataData = System.DBNull.Value
        //        }
        //        return NumData;
        //        return functionReturnValue;
        //    }


        //    if ((string.IsNullOrEmpty(Strings.Trim(TipoData)))) {
        //        return NumData;
        //        return functionReturnValue;
        //    }


        //    if (System.String.Format(NumData).Length > 15) {
        //        Array NumData1 = default(Array);
        //        NumData1 = Strings.Split(System.String.Format(NumData), " ");
        //        NumData = NumData1(0);
        //        NumHora = NumData1(1);
        //    }

        //    dynamic Dia = null;
        //    dynamic Mes = null;
        //    dynamic Ano = null;
        //    dynamic Quant = null;
        //    dynamic Data = null;

        //    Quant = Strings.Len(NumData.Trim());
        //    if (Information.IsNumeric((NumData))) {
        //        Dia = Strings.Right(NumData.Trim(), 2);
        //        Mes = Strings.Mid(NumData.Trim(), 5, 2);
        //        Ano = Strings.Left(NumData.Trim(), 4);
        //    } else {
        //        if (Quant == 10) {
        //            NumData = Strings.Replace(NumData.Trim(), ".", "/");
        //            Dia = Strings.Left(NumData.Trim(), 2);
        //            Mes = Strings.Mid(NumData.Trim(), 4, 2);
        //            Ano = Strings.Right(NumData.Trim(), 4);
        //            //ElseIf Quant < 10 And InStr(Trim(NumData), ".") > 0 Then
        //            //    AUX = NumData.Split("/")
        //            //    Dia = AUX(0)
        //            //    Mes = AUX(1)
        //            //    Ano = AUX(2)
        //        } else {
        //            //Return NumData & "TESTE"
        //            //Exit Function
        //            //If Trim(NumData) = "00.00.0000" Then
        //            //    NumData = "19000101"
        //            //End If
        //            Data = NumData.Trim();
        //            Dia = FormataNumero(Day(Data), 2);
        //            Mes = FormataNumero(Month(Data), 2);
        //            Ano = Strings.Format(Year(Data));
        //        }
        //    }
        //    Dia = AdicionaZero(Dia, 2);
        //    Mes = AdicionaZero(Mes, 2);

        //    switch (TipoData) {
        //        case 0:
        //            AUX = Dia + "/" + Mes + "/" + Ano;
        //            break;
        //        case 1:
        //            AUX = Ano + Mes + Dia;
        //            break;
        //        case 2:
        //            AUX = Ano + "/" + Mes + "/" + Dia;
        //            break;
        //        case 3:
        //            AUX = Ano + "/" + Mes;
        //            break;
        //        case 4:
        //            AUX = Mes + "/" + Ano;
        //            break;
        //        case 12:
        //            AUX = Mes + Dia;
        //            break;
        //        case 5:
        //            AUX = Mes + "/" + Dia + "/" + Ano;
        //            break;
        //        case 6:
        //            AUX = Mes + "/" + Dia + "/" + Ano + " " + NumHora;
        //            break;
        //        case 7:
        //            AUX = Dia + "/" + Mes + "/" + Ano + " " + NumHora;
        //            break;
        //        case 8:
        //            AUX = Ano + "-" + Mes + "-" + Dia;
        //            break;
        //        case 9:
        //            AUX = Ano + "-" + Mes + "-" + Dia + " " + NumHora;
        //            break;
        //        case 10:
        //            AUX = Ano + Mes + Dia;
        //            break;
        //        case 11:
        //            AUX = NumHora;
        //            break;
        //        case 14:
        //            AUX = Ano + Mes;
        //            break;
        //        default:
        //            return NumData;
        //            return functionReturnValue;
        //    }

        //    if (AUX == "01/01/1900") {
        //        return "";
        //    } else {
        //        return AUX;
        //    }
        //    return functionReturnValue;

        //}

        //public static object FormataDataValida(NumData, TipoData)
        //{
        //    object functionReturnValue = null;
        //    if (object.ReferenceEquals(NumData, System.DBNull.Value)) {
        //        if (TipoData == 0) {
        //            functionReturnValue = "";
        //        }
        //        return NumData;
        //        return functionReturnValue;
        //    }

        //    if ((string.IsNullOrEmpty(NumData.Trim()))) {
        //        if (TipoData == 5) {
        //            functionReturnValue = System.DBNull.Value;
        //        }
        //        return NumData;
        //        return functionReturnValue;
        //    } else {
        //        return NumData;
        //    }
        //    return functionReturnValue;
        //}

        //public static object FormataHora(NumData)
        //{
        //    return FormataHora(NumData, 0);
        //}

        //public static object FormataHora(NumData, Soma)
        //{
        //    object functionReturnValue = null;

        //    string NumHora = "";

        //    if (object.ReferenceEquals(NumData, System.DBNull.Value)) {
        //        return NumData;
        //        return functionReturnValue;
        //    }

        //    if ((string.IsNullOrEmpty(NumData.Trim()))) {
        //        return NumData;
        //        return functionReturnValue;
        //    }


        //    if ((string.IsNullOrEmpty(Strings.Trim(Soma)))) {
        //        return NumData;
        //        return functionReturnValue;
        //    }

        //    if (System.String.Format(NumData).Length > 15) {
        //        Array NumData1 = default(Array);
        //        NumData1 = Strings.Split(System.String.Format(NumData), " ");
        //        NumData = NumData1(0);
        //        NumHora = NumData1(1);
        //    }

        //    dynamic Dia = null;
        //    dynamic Mes = null;
        //    dynamic Ano = null;
        //    dynamic Quant = null;
        //    dynamic Data = null;

        //    Quant = Strings.Len(NumData.Trim());
        //    if (Information.IsNumeric((NumData))) {
        //        Dia = Strings.Right(NumData.Trim(), 2);
        //        Mes = Strings.Mid(NumData.Trim(), 5, 2);
        //        Ano = Strings.Left(NumData.Trim(), 4);
        //    } else {
        //        if (Quant == 10) {
        //            Dia = Strings.Left(NumData.Trim(), 2);
        //            Mes = Strings.Mid(NumData.Trim(), 4, 2);
        //            Ano = Strings.Right(NumData.Trim(), 4);
        //        } else if (Strings.Len(NumData) == 8) {
        //            //NumHora = NumData
        //        } else {
        //            Data = NumData.Trim();
        //            Dia = FormataNumero(DateAndTime.Day(Data), 2);
        //            Mes = FormataNumero(Month(Data), 2);
        //            Ano = Strings.Format(Year(Data));
        //        }
        //    }
        //    //Dia = AdicionaZero(Dia, 2)
        //    //Mes = AdicionaZero(Mes, 2)
        //    if (Strings.InStr(Ve_Nulo(NumHora), ":") > 0) {
        //        int hora = Convert.ToInt32(NumHora.Split(":")(0));
        //        hora = hora + Convert.ToInt32(Soma);
        //        string aux4 = string.Format(AdicionaZero(hora, 2));
        //        if (aux4 == "24") {
        //            aux4 = "00";
        //        }
        //        NumHora = aux4 + ":" + NumHora.Split(":")(1);
        //    } else {
        //        NumHora = "00:00";
        //    }
        //    return NumHora;
        //    return functionReturnValue;

        //}

        //public static object FormataNumero(numero, digitos)
        //{
        //    object functionReturnValue = null;

        //    if (string.IsNullOrEmpty(Strings.Trim(numero)))
        //        return functionReturnValue;
        //    if (string.IsNullOrEmpty(Strings.Trim(digitos)))
        //        return functionReturnValue;

        //    dynamic qtd_dig = null;
        //    dynamic qtd_zeros = 0;
        //    dynamic conta_zeros = null;
        //    dynamic numero_formatado = null;

        //    qtd_dig = Strings.Len(Strings.Trim(numero));
        //    //numero_formatado = Trim(numero)


        //    if (qtd_dig < digitos) {
        //        qtd_zeros = digitos - qtd_dig;

        //        while (conta_zeros < qtd_zeros) {
        //            numero_formatado = 0 + numero_formatado;
        //            conta_zeros = conta_zeros + 1;
        //        }

        //    }

        //    functionReturnValue = numero_formatado + numero;
        //    return functionReturnValue;

        //}
        //public static object FormataMoeda(decimal Valor)
        //{
        //    return FormataMoeda(Valor, 0);
        //}

        //public static object FormataMoeda(decimal ValorDec, int Tipo)
        //{
        //    object functionReturnValue = null;
        //    string Valor = Convert.ToString(ValorDec);
        //    if (Strings.Len(Valor) < 1) {
        //        return ValorDec;
        //        return functionReturnValue;
        //    }

        //    int n = 0;
        //    string[] aux = null;

        //    if (Tipo == 1) {
        //        //DECIMAL COM PONTO
        //        if (Strings.InStr(Valor, ".") < Strings.InStr(Valor, ",") & Strings.InStr(Valor, ",") > 0) {
        //            Valor = Strings.Replace(Valor, ".", "");
        //            Valor = Strings.Replace(Valor, ",", ".");
        //        } else if (Strings.InStr(Valor, ",") < Strings.InStr(Valor, ".") & Strings.InStr(Valor, ".") > 0) {
        //            Valor = Strings.Replace(Valor, ",", "");
        //        } else if (Strings.InStr(Valor, ",") > 0) {
        //            Valor = Strings.Replace(Valor, ",", ".");
        //        } else if (Strings.InStr(Valor, ".") > 0) {
        //        } else {
        //            Valor += ".";
        //        }
        //        //aux = Split(Valor, ".")
        //        //If aux(1).Length > 0 And aux(1).Length < 3 Then
        //        //    Valor = aux(0) & "."
        //        //    If Len(aux(1)) >= 2 Then
        //        //        If Mid(aux(1), 2, 1) = "0" Then
        //        //            aux(1) = Mid(aux(1), 1, 1)
        //        //        End If
        //        //    End If
        //        //    Valor += aux(1)
        //        //    For n = aux(1).Length To 2
        //        //        Valor += "0"
        //        //    Next
        //        //End If

        //    } else if (Tipo == 3) {
        //        //DECIMAL SEM SEPARADOR
        //        //If InStr(Valor, ".") < InStr(Valor, ",") And InStr(Valor, ",") > 0 Then
        //        //    Valor = Replace(Valor, ".", "")
        //        //    Valor = Replace(Valor, ",", ".")
        //        //ElseIf InStr(Valor, ",") < InStr(Valor, ".") And InStr(Valor, ".") > 0 Then
        //        //    Valor = Replace(Valor, ",", "")
        //        //ElseIf InStr(Valor, ",") > 0 Then
        //        //    Valor = Replace(Valor, ",", ".")
        //        //ElseIf InStr(Valor, ".") > 0 Then
        //        //Else
        //        //    Valor += "."
        //        //End If
        //        //aux = Split(Valor, ".")

        //        Valor = Convert.ToDecimal(Strings.Mid(Valor, 1, Valor.Length - 2) + "," + Strings.Right(Valor, 2));
        //        //Valor = FormatCurrency(ValorDec).Replace("R$ ", "")


        //    } else {
        //        Valor = string.Format("{0:###,###,###,##0.00}", ValorDec);

        //    }
        //    return Valor;
        //    return functionReturnValue;
        //}

        //Function FormataMoedaString(ByVal ValorDec As String)
        //    Dim Valor As String = CStr(ValorDec)
        //    HttpContext.Current.Response.Write("Valor=" & Valor)
        //    If Len(Valor) < 1 Then Exit Function
        //    Dim n As Integer
        //    Dim aux() As String
        //    HttpContext.Current.Response.Write("f=" & Valor)
        //    'DECIMAL COM PONTO
        //    If InStr(Valor, ".") < InStr(Valor, ",") And InStr(Valor, ",") > 0 Then
        //        Valor = Replace(Valor, ".", "")
        //        Valor = Replace(Valor, ",", ".")
        //    ElseIf InStr(Valor, ",") < InStr(Valor, ".") And InStr(Valor, ".") > 0 Then
        //        Valor = Replace(Valor, ",", "")
        //    ElseIf InStr(Valor, ",") > 0 Then
        //        Valor = Replace(Valor, ",", ".")
        //    ElseIf InStr(Valor, ".") > 0 Then
        //    Else
        //        Valor += "."
        //    End If
        //    'aux = Split(Valor, ".")
        //    'If aux.Length > 0 And aux.Length < 3 Then
        //    '    Valor = aux(0) & "."
        //    '    If Len(aux(1)) >= 2 Then
        //    '        If Mid(aux(1), 2, 1) = "0" Then
        //    '            aux(1) = Mid(aux(1), 1, 1)
        //    '        End If
        //    '    End If
        //    '    Valor += aux(1)
        //    '    For n = aux(1).Length To 2
        //    '        Valor += "0"
        //    '    Next
        //    'End If


        //    Return Valor
        //End Function

        public static object Ve_Nulo(object Texto)
        {
            object functionReturnValue = null;
            if (object.ReferenceEquals(Texto, System.DBNull.Value))
            {
                //Response.Write(Texto.GetType)
                //Response.End()
                functionReturnValue = "";
            }
            else
            {
                functionReturnValue = Texto;
            }
            return functionReturnValue;
        }

        public static object Ve_Valor(object Texto)
        {
            object functionReturnValue = null;
            if (object.ReferenceEquals(Texto, System.DBNull.Value))
            {
                functionReturnValue = 0;
            }
            else
            {
                if (Convert.ToString(Texto).Length == 0)
                    functionReturnValue = 0;
                else
                    functionReturnValue = Texto;
            }
            return functionReturnValue;
        }

        public static object Ve_Moeda(object Texto)
        {
            object functionReturnValue = null;
            if (object.ReferenceEquals(Texto, System.DBNull.Value))
            {
                functionReturnValue = 0;
            }
            else
            {
                functionReturnValue = Texto;
            }
            return functionReturnValue;
        }

        public static string Mid(string param, int startIndex, int length)
        {
        //start at the specified index in the string ang get N number of
        //characters depending on the lenght and assign it to a variable
        string result = "";
            if (param.Length>0 && startIndex < param.Length && (startIndex + length) <= param.Length && length >= 0)
                 result = param.Substring(startIndex, length);
            //return the result of the operation
            return result;
        }
        public static string Left(string param, int length)
        {
            //we start at 0 since we want to get the characters starting from the
            //left and with the specified lenght and assign it to a variable
            string result = param.Substring(0, length);
            //return the result of the operation
            return result;
        }
        public static string Right(string param, int intlength)
        {
            //start at the index based on the lenght of the sting minus
            //the specified lenght and assign it a variable
            int intnum = (param.Length);
            intnum -= intlength;
            string result = param.Substring(intnum, intlength);
            //return the result of the operation
            return result;
        }


        public static string PeriodoSingular(string Tipo)
        {
            switch (Tipo.Trim())
            {
                case "ÚNICA":
                    return "ÚNICA";
                case "MENSAIS":
                    return "MESES";
                case "BIMESTRAIS":
                    return "BIMESTRES";
                case "TRIMESTRAIS":
                    return "TRIMESTRES";
                case "SEMESTRAIS":
                    return "SEMESTRES";
                case "ANUAIS":
                    return "ANOS";
                case "INTERMEDIÁRIA":
                    return "INTERMEDIÁRIA";
                default:
                    return Tipo;
            }
        }

         public static string AdicionaZero(string data, int n)
        {
            string functionReturnValue = null;
            if (object.ReferenceEquals(data, System.DBNull.Value))
            {
                return "";
                return functionReturnValue;
            }
            if ((System.String.Format(data)).Length < n)
            {
                data = "0" + data;
            }
            functionReturnValue = data;
            return functionReturnValue;
        }

  
    public static System.DateTime ProximoDiaUtil(System.DateTime dData)
    {
        int DiaSem = 0;
        System.DateTime DiaUtil = default(System.DateTime);
        DataTable Feriados = new DataTable();

        int n = 0;
        for (n = 0; n <= 10; n++)
        {
            DiaUtil = dData;
            if (n > 0)
                DiaUtil = dData.AddDays(n);

            DiaSem = (int)DiaUtil.DayOfWeek;

            //HttpContext.Current.Response.Write("<br>DiaUtil=" + DiaUtil + " - DiaSem=" + DiaSem);

            //sábado e domingo
            if (DiaSem != 0 & DiaSem != 6)
            {
                if (FrameWork.Util.GetInt(FrameWork.DataObject.DataSource.DefaulDataSource.Execute("SELECT count(data) as qtd FROM Feriados WHERE Data = convert(varchar(8),'" + String.Format("{0:yyyyMMdd}", DiaUtil) + "',112)")) == 0)
                    break;
            }
        }


        return Convert.ToDateTime(DiaUtil);
    }

    public static int DateDiff(DateTime data1, DateTime data2)
    {
        return (data1 - data2).Days;
    }


    public static object Espacos(int qtd)
        {
            return Espacos(qtd, " ");
        }

        public static object Espacos(int qtd, string str)
        {
            string aux = "";
            int n = 0;
            for (n = 1; n <= qtd; n++)
            {
                aux += str;
            }
            return aux;
        }


        public static object AdicionaEspaco(string str, int n)
        {
            if (object.ReferenceEquals(str, System.DBNull.Value))
                return "";
            while ((System.String.Format(str).Length) < n)
            {
                str = " " + str;
            }
            return str;
        }

        public static string AdicionaEspacoFim(string str, int n)
        {
            if (object.ReferenceEquals(str, System.DBNull.Value))
            {
                str = "";
            }
            while (str.Length < n)
            {
                str = str + Convert.ToString(" ");
            }
            return str;
        }

        public static object ApenasNumeros(string str)
        {
            int n = 0;
            string aux = "";
            string aux2 = "";
            for (n = 0; n <= (System.String.Format(str).Length) - 1; n++)
            {
                if (IsNumeric(str.Substring(n, 1)))
                {
                    aux += str.Substring(n, 1);
                }
                else
                {
                    aux2 += str.Substring(n, 1);
                }
            }
            return aux;

        }

        public static bool IsNumeric(this string s)
        {
            float output;
            return float.TryParse(s, out output);
        }

        public static object ApenasLetras(string str)
        {
            int n = 0;
            string aux = "";
            string aux2 = "";
            for (n = 0; n <= (System.String.Format(str).Length) - 1; n++)
            {
                if (IsNumeric(str.Substring(n, 1)))
                {
                    aux += str.Substring(n, 1);
                }
                else
                {
                    aux2 += str.Substring(n, 1);
                }
            }
            return aux2;

        }

        public static int VeAndar(string str)
        {
            string aux = null;
            int andar = 0;
            aux = (string)ApenasNumeros(str);
            if (IsNumeric(aux))
            {
                if ((aux).Length == 1)
                {
                    andar = 0;
                }
                else if ((aux).Length == 2)
                {
                    andar = Convert.ToInt16(aux.Substring(0, 1));
                }
                else if ((aux).Length == 3)
                {
                    andar = Convert.ToInt16(aux.Substring(0, 2));
                }
                else
                {
                    andar = Convert.ToInt16(aux.Substring(0, (aux).Length - 1));
                }
            }
            else
            {
                andar = FrameWork.Util.GetInt(str);
            }

            return andar;
        }



        public static object LimpaCelula(string str)
        {
            str = str.Replace("#199;", "Ç");
            str = str.Replace("#195;", "Ã");
            str = str.Replace("#213;", "Õ");
            str = str.Replace("#211;", "Ó");
            str = str.Replace("#218;", "Ú");
            str = str.Replace("#225;", "á");
            str = str.Replace("#233;", "é");
            str = str.Replace("#193;", "Á");
            str = str.Replace("#231;", "ç");

            str = str.Replace("&#225;", "á");
            str = str.Replace("&#226;", "â");
            str = str.Replace("&#227;", "ã");
            str = str.Replace("&#233;", "é");
            str = str.Replace("&#234;", "ê");
            str = str.Replace("&#237;", "í");
            str = str.Replace("&#243;", "ó");
            str = str.Replace("&#244;", "ô");
            str = str.Replace("&#245;", "õ");
            str = str.Replace("&#250;", "ú");
            str = str.Replace("&#231;", "ç");
            str = str.Replace("&#193;", "Á");
            str = str.Replace("&#194;", "Â");
            str = str.Replace("&#195;", "Ã");
            str = str.Replace("&#201;", "É");
            str = str.Replace("&#202;", "Ê");
            str = str.Replace("&#205;", "Í");
            str = str.Replace("&#211;", "Ó");
            str = str.Replace("&#212;", "Ô");
            str = str.Replace("&#213;", "Õ");
            str = str.Replace("&#218;", "Ú");
            str = str.Replace("&#199;", "Ç");
            str = str.Replace("&#180;", "´");
            str = str.Replace("&amp;", "&");

            str = str.Trim().Replace("&nbsp;", "").Replace("nbsp;", "").Replace("&amp;", "").Replace("amp;", "");


            str = str.Replace("&", "");
            str = str.Replace("<b><font color='#FF0000'>", "");
            str = str.Replace("</font></b>", "");
            return str;
        }


        //public static object SequenciaAlfa(int n)
        //{
        //    string str = "abcdefghijklmnopqrstuvwxyz";
        //    return Strings.Mid(str, n, 1);
        //}

        public static string VeTPLOGRA(string str)
        {
            switch (str)
            {
                case "R.":
                    return "RUA";
                case "PC.":
                    return "PRAÇA";
                case "AL.":
                    return "ALAMEDA";
                default:
                    return str;
            }
        }


    //NOVO MODELO
    //'AUTENTICACAO NO SERVIDOR DE EMAIL
    //    client.Credentials = New Net.NetworkCredential("SMTP_Injection", "a82410d2e722a6fd3ce400fb8e65d59c70f6eccb")
    //    client.Host = "smtp.sparkpostmail.com"
    //    client.Port = 587
    //    client.EnableSsl = True

    //    With objEmail
    //        .From = New MailAddress("extranet@pdg.com.br", "Extranet PDG", System.Text.Encoding.UTF8)



        public static object EnvioEmail(string corpo, string titulo, string email, string emailcc, string emailbcc)
        {
            return EnvioEmail(corpo, "", titulo, email, emailcc, emailbcc);
        }
        public static object EnvioEmail(string corpo, string strde, string titulo, string email, string emailcc, string emailbcc)
        {
            string sErro = "";
            //Dim strDe As String
            string strtexto = "";
            System.Net.Mail.MailMessage objEmail = new System.Net.Mail.MailMessage();
            SmtpClient client = new SmtpClient();

             ClassLibrary.Configuracoes.SMTP objSMTP = new ClassLibrary.Configuracoes.SMTP();
            String strSql = "SELECT Valor FROM Configuracoes WHERE Nome='SMTP' ";
            DataTable dt = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSql);
            if (dt.Rows.Count == 1)
            {
                String sJson = FrameWork.Util.GetString(dt.Rows[0]["Valor"]);
                System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
                dynamic result = js.DeserializeObject(sJson);
                objSMTP.servidor = FrameWork.Util.GetString(result["servidor"]);
                objSMTP.usuario = FrameWork.Util.GetString(result["usuario"]);
                objSMTP.senha = FrameWork.Util.GetString(result["senha"]);
                objSMTP.porta = FrameWork.Util.GetString(result["porta"]);
                objSMTP.ssl = FrameWork.Util.GetString(result["ssl"]);
                objSMTP.remetente = FrameWork.Util.GetString(result["remetente"]);
            }
            else
            {
                sErro = "Erro de envio, configurações de SMTP inválidas!";
                return sErro;
            }


            //if (string.IsNullOrEmpty(strde))
            //{
            //    strde = "oliveira@homebrasil.com";
            //}
            strde = objSMTP.remetente;
            strtexto = titulo;

            //AUTENTICACAO NO SERVIDOR DE EMAIL
            client.Credentials = new System.Net.NetworkCredential(objSMTP.usuario, objSMTP.senha);
            client.Host = objSMTP.servidor;
            client.Port = FrameWork.Util.GetInt(objSMTP.porta);
            client.EnableSsl = (objSMTP.ssl=="on"?true:false);
            //client.DeliveryMethod = SmtpDeliveryMethod.Network

            var _with1 = objEmail;
            _with1.From = new MailAddress(strde);
            if (!string.IsNullOrEmpty(FrameWork.Util.GetString(email)))
            {
                email = email.Replace(";", ",");
                string[] arremail = email.Split(',');
                for (int n = 0; n <= arremail.Length - 1; n++)
                {
                    if (arremail[n].Length > 0)
                    {
                        _with1.To.Add(arremail[n]);
                    }
                }
            }
            if (!string.IsNullOrEmpty(FrameWork.Util.GetString(emailcc)))
            {
                emailcc = emailcc.Replace(";", ",");
                string[] arremail = emailcc.Split(',');
                for (int n = 0; n <= arremail.Length - 1; n++)
                {
                    if (arremail[n].Length > 0)
                    {
                        _with1.CC.Add(arremail[n]);
                    }
                }
            }
            if (!string.IsNullOrEmpty(FrameWork.Util.GetString(emailbcc)))
            {
                emailbcc = emailbcc.Replace(";", ",");
                string[] arremail = emailbcc.Split(',');
                for (int n = 0; n <= arremail.Length - 1; n++)
                {
                    if (arremail[n].Length > 0)
                    {
                        _with1.Bcc.Add(arremail[n]);
                    }
                }
            }
            _with1.Subject = titulo;
            _with1.Body = corpo;
            _with1.IsBodyHtml = true;

            try
            {
                client.Send(objEmail);
                sErro = "Email enviado com sucesso!!!!";
            }
            catch (Exception ex)
            {
                sErro = "Error no send: " + ex.ToString();
            }
            return sErro;

        }

        public static string VeTipoTelefone(string str)
        {
            switch (str)
            {
                case "1":
                    return "RES";
                case "2":
                    return "COM";
                case "3":
                    return "CEL";
                default:
                    return str;
            }
        }
        public static void msgbox(string script, Page page)
        {
            HtmlGenericControl htmlGenericControl = new HtmlGenericControl("script");
            htmlGenericControl.Attributes.Add("type", "text/javascript");
            htmlGenericControl.InnerHtml = "alert('" + script.Replace("'", "\\'") + "');";
            page.Page.Header.Controls.Add((Control)htmlGenericControl);
        }

        public static void registrarJavaScript(string script, bool colocarTag, Page page)
        {
            page.ClientScript.RegisterStartupScript(page.GetType(), "", script, colocarTag);
        }

        public static bool IsCpf(string cpf)
        {
            int[] multiplicador1 = new int[9] {
			10,
			9,
			8,
			7,
			6,
			5,
			4,
			3,
			2
		};
            int[] multiplicador2 = new int[10] {
			11,
			10,
			9,
			8,
			7,
			6,
			5,
			4,
			3,
			2
		};
            string tempCpf = null;
            string digito = null;
            int soma = 0;
            int resto = 0;
            cpf = cpf.Trim();
            cpf = cpf.Replace(".", "").Replace("-", "");
            if (cpf.Length != 11)
            {
                return false;
            }
            tempCpf = cpf.Substring(0, 9);
            soma = 0;

            for (int i = 0; i <= 8; i++)
            {
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];
            }
            resto = soma % 11;
            if (resto < 2)
            {
                resto = 0;
            }
            else
            {
                resto = 11 - resto;
            }
            digito = resto.ToString();
            tempCpf = tempCpf + digito;
            soma = 0;
            for (int i = 0; i <= 9; i++)
            {
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];
            }
            resto = soma % 11;
            if (resto < 2)
            {
                resto = 0;
            }
            else
            {
                resto = 11 - resto;
            }
            digito = digito + resto.ToString();
            return cpf.EndsWith(digito);
        }

        public static bool IsCnpj(string cnpj)
        {
            int[] multiplicador1 = new int[12] {
			5,
			4,
			3,
			2,
			9,
			8,
			7,
			6,
			5,
			4,
			3,
			2
		};
            int[] multiplicador2 = new int[13] {
			6,
			5,
			4,
			3,
			2,
			9,
			8,
			7,
			6,
			5,
			4,
			3,
			2
		};
            int soma = 0;
            int resto = 0;
            string digito = null;
            string tempCnpj = null;
            cnpj = cnpj.Trim();
            cnpj = cnpj.Replace(".", "").Replace("-", "").Replace("/", "");
            if (cnpj.Length != 14)
            {
                return false;
            }
            tempCnpj = cnpj.Substring(0, 12);
            soma = 0;
            for (int i = 0; i <= 11; i++)
            {
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador1[i];
            }
            resto = (soma % 11);
            if (resto < 2)
            {
                resto = 0;
            }
            else
            {
                resto = 11 - resto;
            }
            digito = resto.ToString();
            tempCnpj = tempCnpj + digito;
            soma = 0;
            for (int i = 0; i <= 12; i++)
            {
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador2[i];
            }
            resto = (soma % 11);
            if (resto < 2)
            {
                resto = 0;
            }
            else
            {
                resto = 11 - resto;
            }
            digito = digito + resto.ToString();
            return cnpj.EndsWith(digito);
        }


 //       public static string FormataData(String NumData, Int16 TipoData)
	//{
	//	string functionReturnValue = null;

	//	string NumHora = null;
	//	string AUX = null;
	//	NumHora = "";
	//	AUX = "";

	//	if (object.ReferenceEquals(NumData, System.DBNull.Value)) {
	//		if (TipoData == 0) {
	//			// FormataData = ""
	//		} else if (TipoData == 11) {
	//			functionReturnValue = "00:00";
	//		}
	//		return NumData;
	//		return functionReturnValue;
	//	}
	//	//NumData = Replace(CStr(NumData), "&nbsp;", "")
	//	if ((string.IsNullOrEmpty(NumData.Trim()))) {
	//		if (TipoData == 5) {
	//			//FormataData = System.DBNull.Value
	//		}
	//		return NumData;
	//		return functionReturnValue;
	//	}


	//	if (TipoData == 0) {
	//		return NumData;
	//		return functionReturnValue;
	//	}


	//	if (System.String.Format(NumData).Length > 15) {
	//		String[] NumData1;
	//		NumData1 = System.String.Format(NumData).Split(' ');
 //           NumData = NumData1[0];
	//		NumHora = NumData1[1];
	//	}

	//	dynamic Dia = null;
	//	dynamic Mes = null;
	//	dynamic Ano = null;
	//	dynamic Quant = null;
	//	dynamic Data = null;

	//	Quant = Strings.Len(NumData.Trim());
	//	if (Information.IsNumeric((NumData))) {
	//		Dia = Strings.Right(NumData.Trim(), 2);
	//		Mes = Strings.Mid(NumData.Trim(), 5, 2);
	//		Ano = Strings.Left(NumData.Trim(), 4);
	//	} else {
	//		if (Quant == 10) {
	//			NumData = Strings.Replace(NumData.Trim(), ".", "/");
	//			Dia = Strings.Left(NumData.Trim(), 2);
	//			Mes = Strings.Mid(NumData.Trim(), 4, 2);
	//			Ano = Strings.Right(NumData.Trim(), 4);
	//			//ElseIf Quant < 10 And InStr(Trim(NumData), ".") > 0 Then
	//			//    AUX = NumData.Split("/")
	//			//    Dia = AUX(0)
	//			//    Mes = AUX(1)
	//			//    Ano = AUX(2)
	//		} else {
	//			//Return NumData & "TESTE"
	//			//Exit Function
	//			//If Trim(NumData) = "00.00.0000" Then
	//			//    NumData = "19000101"
	//			//End If
	//			Data = NumData.Trim();
 //               Dia = FormataNumero(DateAndTime.Day(Data), 2);
 //               Mes = FormataNumero(DateAndTime.Month(Data), 2);
 //               Ano = Strings.Format(DateAndTime.Year(Data));
	//		}
	//	}
	//	Dia = AdicionaZero(Dia, 2);
	//	Mes = AdicionaZero(Mes, 2);

	//	switch (TipoData) {
	//		case 0:
	//			AUX = Dia + "/" + Mes + "/" + Ano;
	//			break;
	//		case 1:
	//			AUX = Ano + Mes + Dia;
	//			break;
	//		case 2:
	//			AUX = Ano + "/" + Mes + "/" + Dia;
	//			break;
	//		case 3:
	//			AUX = Ano + "/" + Mes;
	//			break;
	//		case 4:
	//			AUX = Mes + "/" + Ano;
	//			break;
	//		case 12:
	//			AUX = Mes + Dia;
	//			break;
	//		case 5:
	//			AUX = Mes + "/" + Dia + "/" + Ano;
	//			break;
	//		case 6:
	//			AUX = Mes + "/" + Dia + "/" + Ano + " " + NumHora;
	//			break;
	//		case 7:
	//			AUX = Dia + "/" + Mes + "/" + Ano + " " + NumHora;
	//			break;
	//		case 8:
	//			AUX = Ano + "-" + Mes + "-" + Dia;
	//			break;
	//		case 9:
	//			AUX = Ano + "-" + Mes + "-" + Dia + " " + NumHora;
	//			break;
	//		case 10:
	//			AUX = Ano + Mes + Dia;
	//			break;
	//		case 11:
	//			AUX = NumHora;
	//			break;
	//		case 14:
	//			AUX = Ano + Mes;
	//			break;
	//		default:
	//			return NumData;
	//			return functionReturnValue;
	//	}

	//	if (AUX == "01/01/1900") {
	//		return "";
	//	} else {
	//		return AUX;
	//	}
	//	return functionReturnValue;

	//}

 //   public static string FormataDataValida(String NumData, Int16 TipoData)
	//{
	//	string functionReturnValue = null;
	//	if (object.ReferenceEquals(NumData, System.DBNull.Value)) {
	//		if (TipoData == 0) {
	//			functionReturnValue = "";
	//		}
	//		return NumData;
	//		return functionReturnValue;
	//	}

	//	if ((string.IsNullOrEmpty(NumData.Trim()))) {
	//		if (TipoData == 5) {
	//			functionReturnValue = "";
	//		}
	//		return NumData;
	//		return functionReturnValue;
	//	} else {
	//		return NumData;
	//	}
	//	return functionReturnValue;
	//}

	//public static string FormataHora(String NumData)
	//{
	//	return FormataHora(NumData, 0);
	//}

 //   public static string FormataHora(String NumData, Int32 Soma)
	//{
	//	string functionReturnValue = null;

	//	string NumHora = "";

	//	if (object.ReferenceEquals(NumData, System.DBNull.Value)) {
	//		return NumData;
	//		return functionReturnValue;
	//	}

	//	if ((string.IsNullOrEmpty(NumData.Trim()))) {
	//		return NumData;
	//		return functionReturnValue;
	//	}


	//	if (Soma==0) {
	//		return NumData;
	//		return functionReturnValue;
	//	}

	//	if (System.String.Format(NumData).Length > 15) {
 //           String[] NumData1;
 //           NumData1 = System.String.Format(NumData).Split(' ');
 //           NumData = NumData1[0];
 //           NumHora = NumData1[1];
	//	}

	//	dynamic Dia = null;
	//	dynamic Mes = null;
	//	dynamic Ano = null;
	//	dynamic Quant = null;
	//	dynamic Data = null;

	//	Quant = Strings.Len(NumData.Trim());
	//	if (Information.IsNumeric((NumData))) {
	//		Dia = Strings.Right(NumData.Trim(), 2);
	//		Mes = Strings.Mid(NumData.Trim(), 5, 2);
	//		Ano = Strings.Left(NumData.Trim(), 4);
	//	} else {
	//		if (Quant == 10) {
	//			Dia = Strings.Left(NumData.Trim(), 2);
	//			Mes = Strings.Mid(NumData.Trim(), 4, 2);
	//			Ano = Strings.Right(NumData.Trim(), 4);
	//		} else if (Strings.Len(NumData) == 8) {
	//			//NumHora = NumData
	//		} else {
	//			Data = NumData.Trim();
	//			Dia = FormataNumero(DateAndTime.Day(Data), 2);
 //               Mes = FormataNumero(DateAndTime.Month(Data), 2);
	//			Ano = Strings.Format(DateAndTime.Year(Data));
	//		}
	//	}
	//	//Dia = AdicionaZero(Dia, 2)
	//	//Mes = AdicionaZero(Mes, 2)
	//	if (Strings.InStr(FrameWork.Util.GetString(NumHora), ":") > 0) {
 //           int hora = Convert.ToInt32(FrameWork.Util.GetString(NumHora).Split(':')[0]);
	//		hora = hora + Convert.ToInt32(Soma);
 //           string aux4 = String.Format(AdicionaZero(FrameWork.Util.GetString(hora), 2));
	//		if (aux4 == "24") {
	//			aux4 = "00";
	//		}
	//		NumHora = aux4 + ":" + NumHora.Split(':')[1];
	//	} else {
	//		NumHora = "00:00";
	//	}
	//	return NumHora;
	//	return functionReturnValue;

	//}

 //   public static string FormataNumero(String numero, Int16 digitos)
	//{
	//	string functionReturnValue = null;

	//	if (string.IsNullOrEmpty(Strings.Trim(numero)))
	//		return functionReturnValue;
	//	if (digitos == 0)
	//		return functionReturnValue;

	//	dynamic qtd_dig = null;
	//	dynamic qtd_zeros = 0;
	//	dynamic conta_zeros = null;
	//	dynamic numero_formatado = null;

	//	qtd_dig = Strings.Len(Strings.Trim(numero));
	//	//numero_formatado = Trim(numero)


	//	if (qtd_dig < digitos) {
	//		qtd_zeros = digitos - qtd_dig;

	//		while (conta_zeros < qtd_zeros) {
	//			numero_formatado = 0 + numero_formatado;
	//			conta_zeros = conta_zeros + 1;
	//		}

	//	}

	//	functionReturnValue = numero_formatado + numero;
	//	return functionReturnValue;

	//}


    public static Boolean IsDouble(string strVal)
    {
        try
        {
            double doub = double.Parse(strVal);
            return true;
        }
        catch
        {
            return false;
        }
    }

    public static Boolean IsDate(string sdate)
    {

        DateTime dt;
        bool isDate = true;
        try
        {
            dt = DateTime.Parse(sdate);
        }
        catch
        {
            return false;
        }
        return isDate;
    }


    //public System.DateTime ProximoDiaUtil(System.DateTime Data)
    //{
    //    System.DateTime functionReturnValue = default(System.DateTime);
    //    int DiaSem = 0;
    //    System.DateTime DiaUtil = default(System.DateTime);
    //    DataTable Feriados = new DataTable();
    //    string sErro = null;
    //    int n = 0;
    //    int nTot = 0;
    //    nTot = DateDiff(DateInterval.Day, Data, Now);
    //    for (n = 1; n <= nTot; n++)
    //    {
    //        DiaUtil = DateAdd("d", 1, Data);
    //        HttpContext.Current.Response.Write("<br>DiaUtil=" + DiaUtil);
    //        DiaSem = DiaUtil.Date.DayOfWeek();
    //        //sábado e domingo
    //        if (DiaSem != 0 & DiaSem != 1)
    //        {
    //            if (!cData.AbreConsulta(conn, Feriados, "SELECT * FROM Feriados WHERE Dia = '" + DiaUtil + "'", sErro))
    //                return functionReturnValue;
    //            if (Feriados.Rows.Count == 0)
    //                break; // TODO: might not be correct. Was : Exit For
    //        }
    //    }
    //    functionReturnValue = Convert.ToDateTime(DiaUtil);

    //    conn.Close();
    //    return functionReturnValue;
    //}


    public static string MesExtenso(System.DateTime Dt)
    {
        //return DolarPiece("Janeiro,Fevereiro,Março,Abril,Maio,Junho,Julho,Agosto,Setembro,Outubro,Novembro,Dezembro", ",", Dt.Month);
        String[] meses = { "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" };

        return meses[Dt.Month - 1];
    }
    public static string DataExtenso1(System.DateTime Dt)
    {
        string aux = String.Format("{0:dd}",Dt) + " de " + MesExtenso(Dt) + " de " + String.Format("{0:yyyy}", Dt);
        return aux;
    }
    public static string DolarPiece(string VarV, string Sep, int numpie)
    {
        return DolarPiece(VarV, Sep, numpie, 0);
    }

    public static string DolarPiece(string VarV, string Sep, int numpie, int AteoFim)
    {
        string functionReturnValue = null;
        int PosIni = 0;
        int posfim = 0;
        int i = 0;

        PosIni = 0;
        for (i = 1; i <= numpie - 1; i++)
        {
            //PosIni = String.InStr(PosIni + 1, VarV, Sep);
            PosIni = VarV.IndexOf(Sep, PosIni + 1);
            if (PosIni == 0)
            {
                functionReturnValue = "";
                return functionReturnValue;
            }
        }
        //posfim = Strings.InStr(PosIni + 1, VarV, Sep);
        posfim = VarV.IndexOf(Sep, PosIni + 1);
        if (posfim == 0)
        {
            functionReturnValue = Mid(VarV, PosIni + 1, VarV.Length);
        }
        else {
            if (AteoFim == 1)
            {
                functionReturnValue = Mid(VarV, PosIni + 1, VarV.Length);
            }
            else {
                functionReturnValue = Mid(VarV, PosIni + 1, posfim - PosIni - 1);
            }
        }
        return functionReturnValue;
    }


    public static string GetIPAddress()
    {
        System.Web.HttpContext context = System.Web.HttpContext.Current;

        if (context.Request.IsLocal)
            return "187.11.115.116";

        string ipAddress = FrameWork.Util.GetString(context.Request.ServerVariables["HTTP_CF_CONNECTING_IP"]);
        if (!string.IsNullOrEmpty(ipAddress))
        {
            string[] addresses = ipAddress.Split(',');
            if (addresses.Length != 0 && (!addresses.ToString().Contains(":")))
            {
                return addresses[0];
            }
        }

        ipAddress = FrameWork.Util.GetString(context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"]);
        if (!string.IsNullOrEmpty(ipAddress))
        {
            string[] addresses = ipAddress.Split(',');
            if (addresses.Length != 0 && (!addresses.ToString().Contains(":")))
            {
                return addresses[0];
            }
        }

        return FrameWork.Util.GetString(context.Request.ServerVariables["REMOTE_ADDR"]);
    }

    public static string ReadCookie(string name)
    {
        foreach (string strCookie in HttpContext.Current.Request.Cookies.AllKeys)
        {
            if (strCookie == name)
            {
                return HttpContext.Current.Request.Cookies[name].Value;
            }
        }

        return null;
    }

    //trata o cpf para remover os parametros op enviados pelo AZ no acesso collection
    public static string TrataCPF(String strUsuario)
    {
        String retorno = strUsuario;
        if (strUsuario.Contains("op"))
        {
            retorno = strUsuario.Split('o')[0];
        }
        return retorno;
    }
}
