using System;
using System.Collections.Generic;
using System.Text;
using FrameWork.DataObject;
using FrameWork.WebControls;
using System.Web.UI.WebControls;


namespace ClassLibrary
{

    [ClassData("CodEmpreendimento", false, LogXML = "xmlLOG")//,
     //ClassControl(Title = "Produtos", GridWidth = -99),
     //ClassMenu(MenuNivel.Menu, "Produtos", "Empreendimentos", URL = "", Sequencia = 1, ClientID = "A"),
     //ClassRight(101, "C,E,I,X", "Cadastro de Im�veis", Group = "Produtos")
    ]
    public class Imoveis : ObjectData
    {
        /// <summary>
        /// C�digo interno de refernecia �nica para relacionamento no banco de dados
        /// </summary>
        [FieldData(Length = 10)]
        public int CodEmpresa;

        [FieldData(Length = 10)]
        public string Empresa;

        [FieldData(Length = 20)]
        public string CompanyCode;

        [FieldData(Length = 20)]
        public string NewCode;

        [FieldData(Length = 20)]
        public string Regional;

        [FieldData()]
        public int CodEmpreendimento;

        [FieldData()]
        public int IDSiteAntigo;

        [FieldData()]
        public Boolean Urbanismo;


        /// <summary>
        /// Nome do Pr�dio ou Condominio
        /// </summary>
        [FieldData(Length = 80)]
        public string Nome;

        [FieldData(Length = 80)]
        public string Apelido;

        //[
        // FieldData(Length = 2),
        //FieldControl(),
        //    //FieldControl(Type = FieldControlTypes.DropDownList, AutoPostBack = true, Load = "SELECT Estado,Nome FROM Site_Estados Order by Nome add:(selecione)|0", Validate = "!", ValidateError = "Selecione o Estado"),
        //    //FieldFilter(FieldFilterTypes.DropDownList, Text = "Selecione o Estado:", FieldName = "Site_Empreendimentos.Estado", Load = "SELECT Estado,Nome FROM Site_Estados Order by Nome add:(todos)|0", Empty = "0"),
        // FieldTitle("Regi�o: ")
        //]
        //public string Regiao;

        [
         FieldData(Length = 2),
        FieldControl(),
         //FieldControl(Type = FieldControlTypes.DropDownList, AutoPostBack = true, Load = "SELECT Estado,Nome FROM Site_Estados Order by Nome add:(selecione)|0", Validate = "!", ValidateError = "Selecione o Estado"),
         //FieldFilter(FieldFilterTypes.DropDownList, Text = "Selecione o Estado:", FieldName = "Site_Empreendimentos.Estado", Load = "SELECT Estado,Nome FROM Site_Estados Order by Nome add:(todos)|0", Empty = "0"),
         FieldTitle("Estado: ")
        ]
        public string Estado;

        [
         FieldData(),
         FieldControl(MaxLength=40),
         FieldTitle("Cidade: ")
        ]
        public string Cidade;

        //[FieldColumn(Header = "Cidade", Width = 100, Field = "Site_Cidades.Cidade as NomeCidade", Union = "left join Site_Cidades on Site_Cidades.IDCidade = Site_Empreendimentos.IDCidade")]
        //public string NomeCidade;

        [FieldData(),
         FieldTitle("Bairro: ")]
        public string Bairro;

        [FieldData(Length = 100),
         FieldControl(MaxLength = 100, Width = 300),
         FieldColumn(Header = "Endere�o"),
         FieldTitle("Endere�o: ")]
        public string Endereco;

        [FieldData(),
         FieldControl(MaxLength = 20, Width = 100),
         FieldColumn(Header = "N�mero"),
         FieldTitle("N�mero: ")]
        public string Numero;

        [FieldData(Length = 100),
        FieldControl(MaxLength = 100, Width = 200),
        FieldColumn(Header = "Complemento"),
        FieldTitle("Complemento: ")]
        public string Complemento;


        [FieldData(Length = 9),
         FieldControl(MaxLength = 9, Width = 70),
         FieldTitle("CEP: ")]
        public string CEP;

        //[FieldData(),
        // FieldTitle("Descri��o Oferta: ")]
        //public string DescricaoOferta;


        //[FieldData(Length = 200)]
        //public string Dormitorios_texto;

        //[FieldData(Length = 200)]
        //public string Suites_texto;

        //[FieldData(Length = 200)]
        //public string Areas_texto;

        //[FieldData(Length = 200)]
        //public string Valor_texto;

       


       // [FieldData(Length = 50)]
       //public string Geocoordenada;


       // [FieldData(),
       // FieldControl(Type = FieldControlTypes.TextMultiline)]
       // public String AreasComuns;


        //[FieldData(Length = 250),
        // FieldControl(MaxLength = 250, Width = 300),
        // FieldTitle("UrlExterna: ")
        //]
        //public string UrlExterna;

        //[FieldData(Length = 255),
        // FieldControl(MaxLength = 255, Width = 300),
        // FieldTitle("E-mail Vendas: ")
        //]
        //public string EmailVendas;


       

        /// <summary>
        /// Situa��o do empreendimento
        /// </summary>
        [FieldData(Length = 1)]
        public string Situacao;

        [FieldData(Length = 50)]
        public string Fase;

        //[FieldData(Length = 100)]
        //public String Chamada;

        ////[FieldData(Length = 250)] vide UrlExterna
        ////public String HotSite;

        //[FieldData(Length = 250)]
        //public String TourVirtual;

        //[FieldData(Length = 250)]
        //public String Video;


        //[FieldData(ColumnType = ColumnTypes.Text)]
        // public String OutrosDetalhes;


        //[FieldData(ColumnType = ColumnTypes.Text)]
        //public String TextoLocalizacao;



        //[FieldData()]
        //public int CondicaoID;


        //Logo do Im�vel - 175x75
        [FieldData()]
        public String Logo;

        //Foto Destaque - 1980x600
        [FieldData()]
        public String FotoDestaque;

        //Foto Principal - 537 x 287
        [FieldData()]
        public String FotoPrincipal;

        [FieldData()]
        public int ServicosPreliminares;

        [FieldData()]
        public int Fundacoes;

        [FieldData()]
        public int Estrutura;

        [FieldData()]
        public int Alvenaria;

        [FieldData()]
        public int Instalacoes;

        [FieldData()]
        public int RevestimentosInternos;

        [FieldData()]
        public int Acabamentos;

        [FieldData()]
        public int Fachada;

        [FieldData()]
        public int Entrega;

        [FieldData()]
        public int Calcada;

        //[FieldData(Length = 50)]
        //public String sku;

        //[FieldData(Length = 140)]
        //public String SEO_Titulo;

        //[FieldData(ColumnType = ColumnTypes.Text)]
        //public String SEO_Descricao;

        /// <summary>
        /// Data Inclusao do registro
        /// </summary>
        [FieldData(), FieldControl(Type=FieldControlTypes.Label), 
         //FieldColumn(Header="Data de Inclus�o", Format="{0:dd/MM/yyyy}", Width=100), 
         FieldTitle("Data de Inclus�o: ")]
        public DateTime DataInclusao = DateTime.Now;

        /// <summary>
        /// Data da ultima altera��o do registro
        /// </summary>
        [FieldData(), FieldControl(Type=FieldControlTypes.Label), 
         //FieldColumn(Header="Data de Inclus�o", Format="{0:dd/MM/yyyy}", Width=100), 
         FieldTitle("Data de Altera��o: ")]
        public DateTime DataAlteracao;

        ///// <summary>
        /////  Infra Estrutura ID, para checkbox list
        ///// </summary>
        ///// 
        //[FieldData(), FieldControl(Type = FieldControlTypes.Label),
        //FieldTitle("Infra-Estrutura ID: ")]
        //public string InfraEstruturaID;

        /// <summary>
        /// Usuario da ultima altera��o do registro
        /// </summary>
        [FieldData(), FieldControl(Type=FieldControlTypes.Label),
         //FieldColumn(Header = "Usu�rio", Width=100),
         FieldTitle("Usu�rio: ")] 
        public string Usuario;

        [FieldData(ColumnType = ColumnTypes.Text),
         FieldControl(Type = FieldControlTypes.LogDisplay)]
        public String xmlLOG;

       


    }


}
