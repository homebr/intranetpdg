﻿using FrameWork.DataObject;
using FrameWork.Metricas;
using System;
using System.Data;
using System.Web;

namespace ClassLibrary
{
    [ClassData("PageViewID", true)]
    public class VisitantesPaginas : ObjectData
    {

        public static Type TypeVisitantePaginas;

        #region Campos

        [FieldData()]
        public int PageViewID;

        [FieldData()]
        public DateTime Data = DateTime.Now;

        [FieldData(Index = true)]
        public int VisitanteID;

        [FieldData(Index = true, Truncate = true)]
        public string Pagina;

        [FieldData(Index = true, Truncate = true)]
        public string Modulo;

        [FieldData(Index = true)]
        public int ItemID;

        [FieldData()]
        public bool Post;

        [FieldData(Length = 255, Truncate = true)]
        public string Variaveis;

        #endregion

        /// <summary>
        /// Retorna o SQL SELECT a ser usada para se obter o resutaldo dos produtos mais acessados.
        /// Será passado os paramentros Mes, Ano para String.Format
        /// </summary>
        public virtual string GetSqlVisitaProdutos(DateTime dt1, DateTime dt2)
        {
            string cFieldID = this.GetFieldData("ItemID").ColumnName;
            return "SELECT " + cFieldID + " as ID, COUNT(*) AS Visitas " +
               "FROM " + this.GetClassData().TableName + " " +
               //"WHERE MONTH(Data)=" + nMes.ToString() + " AND YEAR(Data)=" + nAno.ToString() + 
               "WHERE Data>=@DT1 AND Data<=@DT2 " +
               " AND " + cFieldID + ">0 " +
               "GROUP BY " + cFieldID + " " +
               "ORDER BY Visitas DESC";
        }

        /// <summary>
        /// Traduz os valores da variável
        /// </summary>
        /// <param name="tb">Tabela com os valores</param>
        /// <param name="cVar">Nome da variável de referencia</param>
        public virtual void GetTableVar(ref DataTable tb, string cVar)
        {
        }

        public VisitantesPaginas()
        {
            if (TypeVisitantePaginas == null)
                TypeVisitantePaginas = this.GetType();
        }

        public virtual bool Create(HttpContext http)
        {
            return true;
        }

    }

}

