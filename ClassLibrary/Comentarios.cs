﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using FrameWork.DataObject;
using FrameWork.WebControls;
using System.Data;

namespace ClassLibrary
{
    [ClassData("ID", true) ]
    public class Comentarios : ObjectData
    {

        /// <summary>
        /// Código interno de referencia única para relacionamento no banco de dados
        /// </summary>
        [FieldData()]
        public int ID;

        [FieldData()]
        public int idFeed;

        [FieldData()]
        public int idColaborador;

        [FieldData()]
        public int IdTipoFeed;

        /// <summary>
        /// Título
        /// </summary>
        [FieldData(Length = 250)]
        public string Comentario;




        [FieldData()]
        public Boolean Liberado; 

     
        [FieldData()]
        public DateTime DataInclusao; // = DateTime.Now;

      

        // c.IdTipoFeed, c.Comentario, c.DataInclusao, c.Liberado  FROM            Comentarios AS c INNER JOIN                         Colaboradores AS co ON co.ID = c.idColaborador";


    }
}
