﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using FrameWork.WebControls;
using FrameWork.DataObject;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Runtime.InteropServices;
using ClassLibrary;

namespace ClassLibrary
{
    [ClassData("ImagemID", true)]
    public class NoticiasImagens : FrameWork.DataObject.ObjectData
    {

        [FieldData(), TableLayout()]
        public int ImagemID;

        [FieldData]
        public int IDEvento;

        [FieldData]
        public int Posicao;

        [FieldData()]
        public string Descricao;

        [FieldData()]
        public string FileName;

        [FieldData(), FieldTitle("Arquivo (*.jpg):"), FieldControl(Type = FieldControlTypes.FileUpload, Execute = "SaveImage", Options = ".jpg .jpeg")]
        public string FileName_600x600;

        [FieldData()]
        public string FileNameOriginal;

        [FieldData]
        public DateTime DataInclusao;

        [FieldData()]
        public DateTime DataAlteracao;

        [FieldData()]
        public string Usuario;


    }
}