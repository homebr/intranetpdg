﻿using System;
using FrameWork.DataObject;

namespace ClassLibrary
{
    [ClassData("ID", true, LogXML = "xmlLOG")]
    public class DicionarioImobiliario : ObjectData
    {

        [FieldData()]
        public int ID;

        [FieldData(Length = 250)]
        public string Palavra;

        [FieldData(ColumnType = ColumnTypes.Text)]
        public string Significado;

        [FieldData()]
        public DateTime DataInclusao;

        [FieldData()]
        public DateTime DataAlteracao;

        [FieldData()]
        public string Usuario;

        [FieldData(ColumnType = ColumnTypes.Text)]
        public string xmlLOG;
    }
}