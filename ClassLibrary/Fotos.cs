﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using FrameWork.WebControls;
using FrameWork.DataObject;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Runtime.InteropServices;

namespace ClassLibrary
{
    [ClassData("ImagemID", true),ClassControl(Title = "Fotos")]
    public class Fotos : ObjectData
    {

        [FieldData(), TableLayout()]
        public int ImagemID;

        [FieldData]
        public int NoticiaID;

        [FieldData]
        public int Posicao;

        //[FieldData(), FieldControl(Type = FieldControlTypes.Label, Width = 350, Rights = "99", Format = "<strong>{0}</strong>"), FieldTitle("Descrição:", Width = 100)]
        //public string Descricao;

        [FieldData(), FieldTitle("Arquivo (*.jpg):"), FieldControl(Type = FieldControlTypes.FileUpload, Execute = "SaveImage", Options = ".jpg .jpeg")]
        public string FileName;

    }
}
