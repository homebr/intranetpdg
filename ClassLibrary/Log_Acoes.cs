﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using FrameWork.DataObject;
using FrameWork.WebControls;
using System.Data;

namespace ClassLibrary
{

    [ClassData("LogID", true)]
    public class Log_Acoes : ObjectData
    {
        [FieldData()]
        public int LogID;

        [FieldData(Length = 20)]
        public string CPF;

        [FieldData()]
        public int IDImovel;

        [FieldData(Length = 50)]
        public string Bloco;

        [FieldData(Length = 20)]
        public string Unidade;

        [FieldData()]
        public DateTime Data;

        [FieldData(Length = 200)]
        public string Acao;

        [FieldData(Length = 500)]
        public string Obs;


        public void AddLog(string sCPF, int sIDImovel, string sBloco, string sUnidade, string sAcao, string sObs)
        {
            try
            {
                Log_Acoes log = new Log_Acoes();
                log.CPF = sCPF;
                log.IDImovel = sIDImovel;
                log.Bloco = sBloco;
                log.Unidade = sUnidade;
                log.Acao = sAcao;
                log.Obs = sObs;
                log.Save();
            }
            catch
            {

            }
        }

    }


}
