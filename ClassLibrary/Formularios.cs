﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using FrameWork.DataObject;
using FrameWork.WebControls;
using FrameWork.WebUtils;

namespace ClassLibrary
{

    [ClassData("ID", true, LogXML = "xmlLOG")]
    public class Formularios : ObjectData
    {

        [FieldData()]
        public int ID;

        [FieldData()]
        public string Formulario;

        [FieldData()]
        public string SubFormulario;

        [FieldData(), FieldTitle("Tipo Formúlario"), FieldControl(Type = FieldControlTypes.DropDownList, Load = ".TipoFormulario STRING")]
        public string TipoFormulario;

        [FieldData()]
        public string Link;

        //[FieldData(), FieldTitle("Tipo Link"), FieldControl(Type = FieldControlTypes.DropDownList, Load = ".TipoLinkFormulario STRING")]
        //public string TipoLink;

        [FieldData()]
        public string Icone;


        [FieldData()]
        public string Status;

        [FieldData()]
        public string Funcoes;


        [FieldData()]
        public DateTime DataInclusao;

        [FieldData()]
        public DateTime DataAlteracao;

        [FieldData(), FieldControl(Type = FieldControlTypes.Label), FieldColumn(Header = "Usuário", Width = 100), FieldTitle("Usuário")]
        public string Usuario;


        [FieldData()]
        public int Sequencia;


        [FieldData(ColumnType = ColumnTypes.Text), FieldControl(Type = FieldControlTypes.LogDisplay), FieldTitle(null)]
        public string xmlLOG;


        public static void Insert(string cForm, string cSubform, string cTipo, string cLink, string cTipoLink, string cIcone)
        {
            Formularios f = new Formularios();
            f.Formulario = cForm;
            f.SubFormulario = cSubform;
            f.TipoFormulario = cTipo;
            f.Link = cLink;
            //f.TipoLink = cTipoLink;
            f.Icone = cIcone;
            f.Save();
        }


    }
}