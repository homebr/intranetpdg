﻿
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using FrameWork.WebControls;


public enum Idiomas
{
    [FieldTitle("Portugues")]
    pt,
    [FieldTitle("English")]
    en,
    [FieldTitle("Deutsch")]
    de
}


public enum enumerador
{
    [FieldTitle("SIM")]
    S,
    [FieldTitle("NÃO")]
    N

}

public enum enumnio
{

    [FieldTitle("Não Contratado")]
    N,
    [FieldTitle("Iniciado")]
    I,
    [FieldTitle("Construção")]
    C,
    [FieldTitle("OK")]
    O


}

public enum enuartes
{

    [FieldTitle("Não Contratado")]
    N,
    [FieldTitle("Iniciado")]
    I,
    [FieldTitle("Desenvolvimento")]
    D,
    [FieldTitle("OK")]
    O


}

public enum enupaisa
{


    [FieldTitle("Desenvolvimento")]
    D,
    [FieldTitle("OK")]
    O


}
public enum EnuSistemaVendas
{

    [FieldTitle("Preço Fechado")]
    PF,
    [FieldTitle("Preço de Venda")]
    PV

}

public enum enupre
{


    [FieldTitle("Análise")]
    A,
    [FieldTitle("Não Entrou")]
    N,
    [FieldTitle("OK")]
    O


}

public enum EnumStatus
{


    [FieldTitle("Cadastrando")]
    CA,
    [FieldTitle("Breve Lançamento")]
    BL,
    [FieldTitle("Livre")]
    LV,
    [FieldTitle("Suspenso Definitivo")]
    SD,
    [FieldTitle("Suspenso Temporário")]
    ST,
    [FieldTitle("Totalmente Vendido")]
    TV

}
public enum EnumCargos
{
    [FieldTitle("Corretor")]
    Corretor,
    [FieldTitle("Corretor 2")]
    Corretor2,
    [FieldTitle("Gerente")]
    Gerente1,
    [FieldTitle("Gerente 2")]
    Gerente2,
    [FieldTitle("Imobiliária")]
    Imobiliária
}
public enum enumstatusorcamento
{


    [FieldTitle("")]
    C,
    [FieldTitle("Liberado")]
    L

}

public enum EnumTipoPessoa
{
    [FieldTitle("Juridica")]
    Juridica,
    [FieldTitle("Física")]
    Física
}

public enum EnumAtivo
{
    [FieldTitle("Ativo")]
    Ativo,
    [FieldTitle("Inativo")]
    Inativo
}

public enum EnumSituacao
{
    [FieldTitle("Ativo")]
    A,
    [FieldTitle("Suspenso")]
    S,
    [FieldTitle("Pré-Ativo")]
    PA
}

public enum EnumSexo
{
    [FieldTitle("Masculino")]
    M,
    [FieldTitle("Feminino")]
    F
}

public enum EnumTipoImagem
{
    [FieldTitle("Logo")]
    L,
    [FieldTitle("Ilustração")]
    I,
    [FieldTitle("Planta")]
    P,
    [FieldTitle("Implantação")]
    T,
    [FieldTitle("Mapa")]
    M,
    [FieldTitle("Oportunidades")]
    O
}


public enum SequenciaReplicacao
{
    [FieldTitle("Dezena")]
    D,
    [FieldTitle("Centena")]
    C,
    [FieldTitle("Milhar")]
    M
}

public enum DebCred
{
    [FieldTitle("Débito")]
    D,
    [FieldTitle("Crédito")]
    C
}

public enum EnumSegmentoTabela
{

    //<FieldTitle("Econômico")> EC
    //<FieldTitle("Médio Padrão")> MP
    //<FieldTitle("Alto Padrão")> AP
    [FieldTitle("CARMEN'")]
    CL,
    [FieldTitle("CARMEN")]
    CA,
    [FieldTitle("EUGÊNIA")]
    EU,
    [FieldTitle("BENEDITA")]
    BE,
    [FieldTitle("APARECIDA")]
    AP,
    [FieldTitle("CURTA")]
    CU

}

public enum EnumTabelaTipo
{

    [FieldTitle("Financiada")]
    FI,
    [FieldTitle("Financiada e Direta - 120")]
    FD

}


public enum EnumPeridiocidade
{

    [FieldTitle("Diário")]
    A,
    [FieldTitle("Semanal")]
    B,
    [FieldTitle("Mensal")]
    C,
    [FieldTitle("Bimestral")]
    D,
    [FieldTitle("Trimestral")]
    E,
    [FieldTitle("Semestral")]
    F,
    [FieldTitle("Anual")]
    G

}

public enum EnumBraco
{
    NENHUMA = 0,
    VENDAS = 1,
    WEB = 2,
    IMOB = 3,
    TERCEIROS = 5
}

public enum EnumDireitos
{
    [FieldTitle("Consulta")]
    O,
    [FieldTitle("Cadastro")]
    C,
    [FieldTitle("Editar")]
    E,
    [FieldTitle("Excluir")]
    X,
    [FieldTitle("Relatorio")]
    R,
    [FieldTitle("Imprimir")]
    I
    //<FieldTitle("Tipo de Imagem")> TipoImagem
    //Status
}


public enum EnumRegioes
{
    [FieldTitle("Não Cadastrado")]
    Nao = 0,
    [FieldTitle("São Paulo")]
    SP = 1,
    [FieldTitle("ABC")]
    ABC = 2,
    [FieldTitle("Santos")]
    SANTOS = 3,
    [FieldTitle("Interior")]
    INTERIOR = 4,
    [FieldTitle("Norte")]
    NORTE = 5,
    [FieldTitle("Nordeste")]
    NORDESTE = 6,
    [FieldTitle("Rio de Janeiro")]
    RIO = 7,
    [FieldTitle("Agra Loteadora")]
    LOTEADORA = 8
}

public enum EnumUnidadeNegocio
{
    [FieldTitle("Não Cadastrado")]
    Nao = 0,
    [FieldTitle("São Paulo")]
    SP = 1,
    [FieldTitle("Rio de Janeiro")]
    RJ = 2,
    [FieldTitle("Nordeste")]
    NORDESTE = 3,
    [FieldTitle("Norte")]
    Norte = 4,
    [FieldTitle("Sul")]
    SUL = 5,
    [FieldTitle("Outros")]
    OUTROS = 6
}

public enum EnumEmpresa
{
    NENHUMA = 0,
    AGRA = 1,
    KLABIN = 2,
    ABYARA = 3
}

public enum EnumSubTipoDocumento
{
    [FieldTitle("Diversos")]
    Diversos,
    [FieldTitle("Identidade")]
    Identidade,
    [FieldTitle("Comprovante Endereço")]
    Endereço,
    [FieldTitle("Comprovante Estado Civil")]
    EstadoCivil,
    [FieldTitle("Comprovante Renda")]
    Renda,
    [FieldTitle("Ficha Cadastral")]
    FichaCadastral
}

public enum EnumTipoDocumento
{
    [FieldTitle("Empreendimento")]
    Empreendimento,
    [FieldTitle("Contrato")]
    Contrato,
    [FieldTitle("Cliente")]
    Cliente,
    [FieldTitle("Processo")]
    Processo
}

public enum TipoLinkFormulario
{
    [FieldTitle("Imagem")]
    I,

    [FieldTitle("Texto")]
    T
}


public enum TipoFormulario
{
    [FieldTitle("Titulo (Item)")]
    T,

    [FieldTitle("Sub Itens")]
    S
}

public enum UsuariosStatus
{
    [FieldTitle("Ativo")]
    A,
    [FieldTitle("Suspenso")]
    S
}


public enum Rights
{
    AD = 1,
    DI = 2,
    OP = 3,
    [FieldTitle("Terrenos: consulta")]
    TerrenosCadastro = 5,
    TerrenosConsulta = 6,
    EmpreendimentosCadastro = 40
}

public enum Status
{
    [FieldTitle("Ativo")]
    A,
    [FieldTitle("Suspenso")]
    S
}

public enum EnumTipoLink
{
    [FieldTitle("N = Nenhum")]
    N,
    [FieldTitle("H = Hot-Site / Externo")]
    H,
    [FieldTitle("I = Interno")]
    I,
    [FieldTitle("S = Script")]
    S,
    [FieldTitle("A = AddClick")]
    A
}


