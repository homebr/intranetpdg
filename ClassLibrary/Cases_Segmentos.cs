﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using FrameWork.DataObject;
using FrameWork.WebControls;
using System.Data;

namespace ClassLibrary
{
    [ClassData("ID", true) ]
    public class Cases_Segmentos : ObjectData
    {

        /// <summary>
        /// Código interno de referencia única para relacionamento no banco de dados
        /// </summary>
        [FieldData()]
        public int ID;

        [FieldData()]
        public int IDCase;

        [FieldData()]
        public int IDSegmento;

        [FieldData()]
        public DateTime DataInclusao = DateTime.Now;


    }
}
