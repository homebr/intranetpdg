﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using FrameWork.DataObject;
using FrameWork.WebControls;
using System.Web.UI.WebControls;

namespace ClassLibrary
{

    [ClassData("ID", true, LogXML = "xmlLOG", NoLogFields = "UltimoAcesso,UltimoIP,AlteracaoSenha,Senha")]
    public class ClientesLogins : ObjectData
    {

        /// <summary>
        /// ID CHAVE PRIMARIA
        /// </summary>
        [FieldData()]
        public int ID;

        /// <summary>
        /// ID Nome
        /// </summary>
        [FieldData(Length = 100)]
        public string Nome;


        [FieldData()]
        public string Telefone;

        [FieldData()]
        public string Celular;

        [FieldData(Length = 100)]
        public string Email;

        [FieldData()]
        public string Senha;

        [FieldData(Length = 1),
         FieldTitle("Situação: ")]
        public string Situacao;

        [FieldData(Length = 20)]
        public string Aniversario;

        [FieldData(Length = 100)]
        public string IDFacebook;

        [FieldData(Length = 100)]
        public string IDGoogle;

        [FieldData()]
        public DateTime UltimoAcesso;

        [FieldData()]
        public string UltimoIP;

        [FieldData()]
        public DateTime AlteracaoSenha;

        /// <summary>
        /// ID DataInclusao
        /// </summary>
        [FieldData()]
        public DateTime DataInclusao;

        /// <summary>
        /// ID DataAlteracao
        /// </summary>
        [FieldData()]
        public DateTime DataAlteracao;

        [FieldData()]
        public string Usuario;

        [FieldData()]
        public Boolean Notificacao_Email;

        [FieldData()]
        public Boolean Notificacao_SMS;


        [FieldData(ColumnType = ColumnTypes.Text)]
        [FieldControl(Type = FieldControlTypes.LogDisplay)]

        public string xmlLOG;
    }


}