﻿using System;
using FrameWork.DataObject;
using FrameWork.WebControls;

namespace ClassLibrary
{

    [ClassData("ID", false, LogXML = "xmlLOG",NoLogFields = "UltimoAcesso,UltimoIP,AlteracaoSenha,Senha,flagAlteracao")]
    public class Clientes : ObjectData
    {

        /// <summary>
        /// ID CHAVE PRIMARIA
        /// </summary>
        [FieldData()]
        public int ID;

        /// <summary>
        /// ID Nome
        /// </summary>
        [FieldData(Length = 150)]
        public string Nome;

        [FieldData()]
        public string CpfCnpj;

        [FieldData(Length = 1)]
        public string Pessoa;

        [FieldData(Length = 150)]
        public string Endereco;

        [FieldData()]
        public string Numero;

        [FieldData()]
        public string Complemento;

        [FieldData(Length = 150)]
        public string Bairro;

        [FieldData(Length = 150)]
        public string Cidade;

        [FieldData()]
        public string Estado;

        [FieldData()]
        public string Cep;

        [FieldData()]
        public string Telefone;

        [FieldData()]
        public string Celular;

        [FieldData(Length=200)]
        public string Email;

        [FieldData(Length = 30)]
        public string Login;

        [FieldData(Length = 50)]
        public string Senha;

        /// <summary>
        /// Situação Ativo
        /// </summary>
        [FieldData()]
        public Boolean Ativo;

        [FieldData(Length = 50)]
        public string Foto;

        [FieldData()]
        public DateTime UltimoAcesso;

        [ FieldData()]
        public string UltimoIP;

        [FieldData()]
        public DateTime AlteracaoSenha;

        /// <summary>
        /// ID DataInclusao
        /// </summary>
        [FieldData()]
        public DateTime DataInclusao;

        /// <summary>
        /// ID DataAlteracao
        /// </summary>
        [FieldData()]
        public DateTime DataAlteracao;

        [FieldData()]
        public string Usuario;

        //[FieldData()]
        //public Boolean Notificacao_Email;

        //[FieldData()]
        //public Boolean Notificacao_SMS;

        ////flag verificacao de alteração pendente
        //[FieldData()]
        //public Boolean flagAlteracao;


        [FieldData()]
        public int Correio;

        [FieldData()]
        public int ID_Antigo;

        [FieldData()]
        public DateTime DataImportacao;

        [FieldData(ColumnType = ColumnTypes.Text)]
        public string Observacao;

        [FieldData(ColumnType = ColumnTypes.Text)]
        public string xmlLOG;
    }


}