﻿using System;
using System.Collections.Generic;
using FrameWork.DataObject;
using FrameWork.WebControls;
using System.Web;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace ClassLibrary
{
    [ClassData("VisitanteID", true)]
    public class Visitantes : ObjectData
    {
        public static Type TypeVisitantes;
        public static string SessionCookieName = "_VisitanteID";
        public static string RequestOrigemName = "o";
        public static string RequestItemIDName = "ID";
        public static string RequestModuloName = "m";

        public static string sqlUpdateQTD =
            "UPDATE Visitantes SET QTD = VP.QTD " +
            "FROM Visitantes AS V INNER JOIN " +
            "( " +
            "SELECT VisitanteID, COUNT(*) AS QTD " +
            "FROM VisitantePaginas  " +
            "WHERE VisitanteID IN (SELECT VisitanteID FROM Visitantes WHERE QTD=0) " +
            "GROUP BY VisitanteID  " +
            ") AS VP ON V.VisitanteID = VP.VisitanteID";

        public static string sqlDeleteNoReferer =
            "DELETE FROM Visitantes WHERE QTD=1 AND AnteriorID=0 AND Referer=''";

        public static string sqlDeleteNoVisitas =
            "DELETE FROM VisitantePaginas WHERE NOT (VisitanteID IN (SELECT VisitanteID FROM Visitantes))";

        public Visitantes()
        {
            if (TypeVisitantes == null)
                TypeVisitantes = this.GetType();
        }

        #region Campos

        [FieldData(Index = true)]
        public DateTime Data = DateTime.Now;

        [FieldData(AutoIncrement = true)]
        public int VisitanteID;

        [FieldData(Index = true)]
        public int AnteriorID;

        [FieldData(Length = 15, Index = true),
         FieldColumn(Width = 100),
         FieldControl(Type = FieldControlTypes.Label)]
        public string IP;

        [FieldData(Index = true)]
        public int UsuarioID;

        [FieldData(Index = true, Truncate = true),
         FieldColumn(),
         FieldControl(Type = FieldControlTypes.Label)]
        public string Origem;

        [FieldData(Length = 100, Truncate = true)]
        public string StartPage;
        public string StartModulo;

        [FieldData(Index = true),
         FieldColumn(Width = 60),
         FieldControl(Type = FieldControlTypes.Label)]
        public int QTD;

        [FieldData(Length = 255, Truncate = true, Index = true),
         FieldControl(Type = FieldControlTypes.Label, Load = "LoadRefer")]
        public string Referer;

        [FieldData(Length = 255, Truncate = true),
         FieldControl(Type = FieldControlTypes.Label)]
        public string UserAgent;

        [FieldFilter(FieldFilterTypes.DropDownList, Load = "Impactro.WebUtils,Impactro.Metricas.VisitantesTipos add:(todos)|0", Execute = "FiltroEspecial", Empty = "0", Text = "Filtrar por: ")]
        public string Filtro;

        public string VisitantesAtivos;

        [FieldData(Length = 4)]
        public string Regional;

        [FieldData(ColumnType = ColumnTypes.Text)]
        public string LinkCompleto;

        #endregion

        public virtual void LoadRefer(Label lbl)
        {
            if (Referer.StartsWith("http://"))
                lbl.Text = String.Format("<a href=\"{0}\" target='_blank'>{0}</a>", Referer);
        }

        public virtual void LoadUsuario(Label lbl)
        {
        }

        /// <summary>
        /// Registra a visita informando se foi um Robo/Spider ou Usuário
        /// </summary>
        /// <param name="http">Contexto HTTP</param>
        /// <param name="cPage">Nome da página ou nome de controle</param>
        /// <param name="bPost">Informação sobre o tipo de acesso, GET ou POST (true para PostBack)</param>
        /// <param name="cVars">Váriáveis adicionais a serem adicionadas</param>
        /// <param name="cModulo">Força a definição do modulo, sem ler a variável via request["m"] (definida em: RequestModuloName)<remarks>informe null para usar o request</remarks></param>
        /// <returns>Retorna 'True' quando for um Usuárioou 'False' Robo/Spider </returns>
        public static bool RegistraVisita(HttpContext http, string cPage, bool bPost, string cVars, string cModulo)
        {
            // Remove valores em brancos
            int n;
            if (cVars != "")
            {
                cVars += "&";
                do
                {
                    n = cVars.Length;
                    cVars = Regex.Replace(cVars, @"&\w+=&", "&");
                } while (n != cVars.Length);
                if (cVars.EndsWith("&"))
                    cVars = cVars.Substring(0, cVars.Length - 1);
            }


            if (VisitantesRobos.IsRobo(http))
            {
                //VisitantesRobos vr = (VisitantesRobos)VisitantesRobos.TypeVisitantesRobos.Assembly.CreateInstance(VisitantesRobos.TypeVisitantesRobos.FullName);
                VisitantesRobos vr = new VisitantesRobos();
                vr.Pagina = cPage;
                vr.UserAgent = http.Request.UserAgent;

                string u = vr.UserAgent;
                if (u == "" || u == null)
                    vr.Robo = "?";
                else
                    vr.Robo = VisitantesRobos.GetRoboName(u);

                if (vr.Create(http))
                    vr.Save();

                return false;
            }
            else
            {
                //int nVisitanteID = WebUtil.GetInt(http.Session[SessionCookieName]);
                int nVisitanteID = FrameWork.Util.GetInt(http.Session[SessionCookieName]);
                if (nVisitanteID == 0)
                {
                    //Visitantes v = (Visitantes)Visitantes.TypeVisitantes.Assembly.CreateInstance(Visitantes.TypeVisitantes.FullName);
                    Visitantes v = new Visitantes();


                    // Obtem o ultimo cookie se houver
                    HttpCookie c = http.Request.Cookies[SessionCookieName];
                    if (c != null)
                        v.AnteriorID = FrameWork.Util.GetInt(c.Value);

                    v.IP = Funcoes.GetIPAddress();
                    v.StartPage = cPage;
                    v.StartModulo = cModulo;
                    v.UserAgent = http.Request.UserAgent;
                    v.LinkCompleto = FrameWork.Util.GetString(http.Request.Url);

                    if (FrameWork.Util.GetString(http.Request.Url).Contains("Patrocinados"))
                    {
                        v.Origem = "Adwords";
                        if (FrameWork.Util.GetString(http.Session["utm_medium"]) == "")
                            http.Session["utm_medium"] = "Links-Patrocinados";
                    }
                    else if (FrameWork.Util.GetString(http.Request.Url).Contains("EmailMarketing"))
                    {
                        v.Origem = "EmailMarketing";
                        if (FrameWork.Util.GetString(http.Session["utm_medium"]) == "")
                            http.Session["utm_medium"] = "EmailMarketing";
                    }
                    else if (!String.IsNullOrEmpty(FrameWork.Util.GetString(http.Request[RequestOrigemName])))
                        v.Origem = FrameWork.Util.GetString(http.Request[RequestOrigemName]);
                    else if (!String.IsNullOrEmpty(FrameWork.Util.GetString(http.Session["utm_source"])))
                        v.Origem = FrameWork.Util.GetString(http.Session["utm_source"]);
                    else
                        v.Origem = http.Request[RequestOrigemName] ?? "";


                    if (http.Request.UrlReferrer != null)
                    {
                        v.Referer = http.Request.UrlReferrer.ToString();
                        if (v.Origem == "")
                            v.Origem = GetOrigem(http.Request.Url.DnsSafeHost, v.Referer);
                        if (v.Origem.Contains("google") && FrameWork.Util.GetString(http.Session["utm_medium"]) == "")
                        {
                            Random random = new Random();
                            int randomNumber = random.Next(1, 3);
                            if (randomNumber == 2)
                                http.Session["utm_medium"] = "Links-Patrocinados";
                            else
                                http.Session["utm_medium"] = "Organico";
                        }
                    }
                    else
                        v.Referer = "";

                    if (FrameWork.Util.GetString(http.Session["_Regiao"]) != "")
                        v.Regional = FrameWork.Util.GetString(http.Session["_Regiao"]);


                    if (v.Create(http))
                    {
                        if (v.TotalLoad == 0)
                            v.Save();
                    }
                    else
                    {
                        http.Session[SessionCookieName] = -1;
                        return false;
                    }

                    // Obtem o numero do novo visitante
                    nVisitanteID = v.VisitanteID;

                    
                    // Grava o numero atual da visitante na session
                    http.Session[SessionCookieName] = nVisitanteID;

                    // Grava o numero atual da visitante em coockie para registar o AnteriorID da proxima vez
                    c = new HttpCookie(SessionCookieName, nVisitanteID.ToString());
                    c.Expires = DateTime.Now.AddMonths(6);
                    http.Response.Cookies.Add(c);

                }
                else if (nVisitanteID == -1)
                    return false;

                //VisitantesPaginas vp = (VisitantesPaginas)VisitantesPaginas.TypeVisitantePaginas.Assembly.CreateInstance(VisitantesPaginas.TypeVisitantePaginas.FullName);
                VisitantesPaginas vp = new VisitantesPaginas();

                vp.VisitanteID = nVisitanteID;
                vp.Pagina = cPage;
                if (cModulo == null)
                    vp.Modulo = http.Request[RequestModuloName] ?? "";
                else
                    vp.Modulo = cModulo;

                vp.ItemID = FrameWork.Util.GetInt(http.Request[RequestItemIDName]);
                if (vp.ItemID == 0 && cVars.Contains(RequestItemIDName + "="))
                {
                    Match m = Regex.Match(cVars, RequestItemIDName + @"=(\d+)");
                    if (m.Success)
                        //vp.ItemID = WebUtil.GetInt(m.Groups[1].Value);
                        vp.ItemID = FrameWork.Util.GetInt(m.Groups[1].Value);
                }
                vp.Post = bPost;
                vp.Variaveis = cVars;

                if (vp.Create(http))
                    vp.Save();
                else
                    return false;

                return true;
            }
        }

        public static string GetOrigem(string cLocal, string cReferer)
        {
            if (cReferer == null)
                return "";

            string cURL = cReferer.Replace("http://", "").Replace("https://", "");
            int n = cURL.IndexOf("?");
            if (n > 0) // Buscador ou url parametrizada
            {
                string cParametros = cURL.Substring(n + 1);
                SortedList<String, String> vars = new SortedList<string, string>();
                vars = WebUtil.GetKeyVar(cParametros.Replace("&", ","));
                cURL = cURL.Substring(0, n);
                n = cURL.IndexOf("/");
                if (n > 0)
                    cURL = cURL.Substring(0, n);

                if (cURL.Contains("google") && (vars.ContainsKey("adurl") || vars.ContainsKey("adservices") || vars.ContainsKey("patrocinado")))
                    cURL = "adwords";
                else if (cURL.Contains("google"))
                    cURL = "google";
                //else if (cURL.Contains("bing"))
                //    cURL += "/bing";


                if (vars == null)
                    cReferer = cURL; // Request sem parametros!
                else if (vars.ContainsKey("q")) // Google, Msn
                    cReferer = cURL + ": " + vars["q"].Replace("+", " ");
                else if (vars.ContainsKey("p")) // Yahoo
                    cReferer = cURL + ": " + vars["p"].Replace("+", " ");
                else
                    cReferer = cURL;

            }
            else
            {
                n = cURL.IndexOf("/");
                if (n > 0)
                    cURL = cURL.Substring(0, n);

                if (cURL.StartsWith(cLocal)) // Proprio site!
                    cReferer = "";
                else
                    cReferer = cURL;
            }
            return cReferer;
        }

        /// <summary>
        /// Metodo para validar a inclusão
        /// </summary>
        /// <param name="http">contexto da chamada</param>
        /// <returns>Verdadeiro se puder registrar a visita</returns>
        public virtual bool Create(HttpContext http)
        {
            return true;
        }

        internal string GetSqlVisitaUsuarios(DateTime dtMesAno1, DateTime dtMesAno2)
        {
            string cFieldID = this.GetFieldData("UsuarioID").ColumnName;
            return "SELECT " + cFieldID + " as ID, COUNT(*) AS Visitas " +
               "FROM " + this.GetClassData().TableName + " " +
               "WHERE Data>=@DT1 AND Data<=@DT2 " +
               " AND " + cFieldID + ">0 " +
               "GROUP BY " + cFieldID + " " +
               "ORDER BY Visitas DESC";
        }

        public virtual string FiltroEspecial(string cFiled, string cValue)
        {
            VisitantesTipos vt = (VisitantesTipos)Int32.Parse(cValue);
            switch (vt)
            {
                case VisitantesTipos.Conectados:
                    if (this.VisitantesAtivos != "")
                    {
                        string cIDs = this.VisitantesAtivos;
                        cIDs = cIDs.Replace("}{", ",");
                        cIDs = cIDs.Substring(1, cIDs.Length - 2);
                        return "Visitantes.VisitanteID IN (" + cIDs + ")";
                    }
                    else
                        return "Visitantes.VisitanteID=0";
                case VisitantesTipos.Google:
                    return "Origem LIKE '%google%'";
                case VisitantesTipos.Yahoo:
                    return "Origem LIKE '%yahoo%'";
                case VisitantesTipos.Bing:
                    return "Origem LIKE '%bing%'";
                case VisitantesTipos.Orkut:
                    return "Origem LIKE '%orkut%'";
                case VisitantesTipos.Twitter:
                    return "Origem LIKE '%twitter%'";
                case VisitantesTipos.SomenteBuscadores:
                    return "(Origem LIKE '%yahoo%' OR Origem LIKE '%google%' OR Origem LIKE '%bing%')";
                case VisitantesTipos.ExcetoBuscadores:
                    return "NOT (Origem LIKE '%yahoo%' OR Origem LIKE '%google%' OR Origem LIKE '%bing%')";
                case VisitantesTipos.Direto:
                    return "Origem=''";
                case VisitantesTipos.Qualquer:
                    return "NOT Origem=''";
                case VisitantesTipos.WebMail:
                    return "Origem LIKE '%mail%'";
                case VisitantesTipos.GoogleADS:
                    //return "Origem='googleads.g.doubleclick.net'";
                    return "Origem LIKE '%/adwords%'";
                case VisitantesTipos.Buscape:
                    return "Origem='busca.buscape.com.br'";
                case VisitantesTipos.Erro404:
                    // BUG: Precisa implementar o reconhecimento do nome da tabela na classe derivada
                    return "Visitantes.VisitanteID IN (SELECT DISTINCT VisitanteID FROM VisitantePaginas WHERE Modulo='Error-404')";
                default:
                    throw new Exception("Filtro invalido");
            }
        }

        //public virtual void ControlInit(ObjectEdit oe)
        //{
        //    LoadVisitantesAtivos();
        //    //if( oe.PageLayout.Request["id"]==null )
        //    //    this.SelectGroupBy = "VisitantePaginas.VisitanteID, Visitantes.AnteriorID, Visitantes.Origem, Visitantes.IP";
        //}



        public string CacheVisitaOrigens = "_VisitaOrigens";
    }

    public enum VisitantesTipos : int
    {
        Conectados = 1,
        Google = 2,
        Yahoo = 3,
        Bing = 4,
        Orkut = 5,
        Twitter = 6,
        [FieldTitle("Somente Buscadores")]
        SomenteBuscadores = 10,
        [FieldTitle("Exceto Buscadores ")]
        ExcetoBuscadores = 11,
        Direto = 18,
        [FieldTitle("Qualquer Origem")]
        Qualquer = 19,
        WebMail = 20,
        [FieldTitle("AdWords Google")]
        GoogleADS = 30,
        Buscape = 40,
        Erro404 = 404
    }
}