﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FrameWork.DataObject;

namespace ClassLibrary
{
    [ClassData("",false)]
    public class RegistraViews: ObjectData 
    {
        [FieldData()]
        public int IDVisitante;

        [FieldData()]
        public DateTime Data;

        [FieldData()]
        public String Pagina;

        [FieldData()]
        public String Objeto;

        [FieldData()]
        public int IDRegistro;

        [FieldData()]
        public String IP;

    }
}
