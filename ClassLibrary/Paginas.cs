﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using FrameWork.DataObject;
using FrameWork.WebControls;
using System.Data;

namespace ClassLibrary
{
    [ClassData("ID", true),
     ClassControl(Title = "Páginas")    ]
    public class Paginas : ObjectData
    {
        #region campos
        /// <summary>
        /// Código interno de referencia única para relacionamento no banco de dados
        /// </summary>
        [FieldData(), TableLayout(), FieldColumn("PaginaID"),
         FieldControl(Type = FieldControlTypes.Label)]
        public int ID;

        //[FieldData(),
        //FieldControl(Type = FieldControlTypes.DropDownList, Load = "SELECT CanalID,Canal as Nome FROM Canais Order by Canal add:(selecione)|0"),
        //FieldTitle("Canal")]
        //public int CanalID;

        /// <summary>
        /// Título
        /// </summary>
        [FieldData(Length = 100),
         FieldTitle("Título: ")]
        public string Titulo;


        [FieldData(Length = 100),
        FieldTitle("Sub-Título: ")]
        public string SubTitulo;

        [FieldData(ColumnType = ColumnTypes.Text),
        FieldControl(Type = FieldControlTypes.TextHTML, Category = 1, Width = 600, Height = 500)]
        public string Conteudo;


 
        //Video,TextoCurto

        //[FieldData(),
        // FieldTitle("Modelo de Layout:"),
        // FieldControl(Type = FieldControlTypes.DropDownList, Category = 1,
        //  Load = "SELECT ModeloID, Nome FROM Modelos WHERE TipoID=2 ORDER BY Nome add:(Padrão)|0",
        //   ParentLink = "DataBase.Modelos&ID={0}")]
        //public int LayoutModeloID;


        [FieldData(Length = 1),
         FieldTitle("Situação: ")]
        public string Situacao;


        [FieldData()]
        public Boolean Destaques;

        [FieldData()]
        public Boolean Video;

        [FieldData(Length = 150)]
        public string pagina;


        [FieldData(),FieldTitle("Data de Inclusão: ")]
        public DateTime DataInclusao; // = DateTime.Now;

        [FieldData(),FieldTitle("Data de Alteração: ") ]
        public DateTime DataAlteracao; // = DateTime.Now;

        /// <summary>
        /// Usuario da ultima alteração do registro
        /// </summary>
        [
         FieldData(),
         FieldControl(Type = FieldControlTypes.Label),
         FieldTitle("Usuário: ")
        ]
        public string Usuario;

        #endregion

        public Fotos Fotos
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
            }
        }


    }
}