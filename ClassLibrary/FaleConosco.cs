﻿using System;
using FrameWork.DataObject;
using FrameWork.WebControls;

namespace ClassLibrary
{

    [ClassData("ID", false, LogXML = "xmlLOG",NoLogFields = "UltimoAcesso,UltimoIP,AlteracaoSenha,Senha,flagAlteracao")]
    public class FaleConosco : ObjectData
    {

        /// <summary>
        /// ID CHAVE PRIMARIA
        /// </summary>
        [FieldData()]
        public int ID;

        /// <summary>
        /// ID Nome
        /// </summary>
        [FieldData(Length = 150)]
        public string Nome;

        [FieldData(Length = 200)]
        public string Email;

        [FieldData(Length = 150)]
        public string Cargo;

        [FieldData(ColumnType = ColumnTypes.Text)]
        public string Mensagem;

        [FieldData(Length = 150)]
        public string Assunto;

         
        /// </summary>
        [FieldData()]
        public DateTime DataInclusao;

        [FieldData(ColumnType = ColumnTypes.Text)]
        public string xmlLOG;

    }


}