﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using FrameWork.DataObject;
using FrameWork.WebControls;
using System.Data;

namespace ClassLibrary
{
    [ClassData("ID", true) ]
    public class Area : ObjectData
    {

        /// <summary>
        /// Código interno de referencia única para relacionamento no banco de dados
        /// </summary>
        [FieldData()]
        public int ID;

        /// <summary>
        /// Título
        /// </summary>
        [FieldData(Length = 140)]
        public string Setor;




        [FieldData()]
        public Boolean Ativo; 

     
        [FieldData()]
        public DateTime DataInclusao; // = DateTime.Now;

        [FieldData()]
        public DateTime DataAlteracao; // = DateTime.Now;

        /// <summary>
        /// Usuario da ultima alteração do registro
        /// </summary>
        [FieldData()]
        public string Usuario;



    }
}
