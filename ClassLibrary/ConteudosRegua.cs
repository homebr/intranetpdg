﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using FrameWork.DataObject;
using FrameWork.WebControls;
using System.Data;

namespace ClassLibrary
{
    [ClassData("ID", true, LogXML = "xmlLOG")   ]
    public class ConteudosRegua : ObjectData
    {
        #region campos
        /// <summary>
        /// Código interno de referencia única para relacionamento no banco de dados
        /// </summary>
        [FieldData(), TableLayout(), FieldColumn("PaginaID"),
         FieldControl(Type = FieldControlTypes.Label)]
        public int ID;

        //[FieldData(),
        //FieldControl(Type = FieldControlTypes.DropDownList, Load = "SELECT CanalID,Canal as Nome FROM Canais Order by Canal add:(selecione)|0"),
        //FieldTitle("Canal")]
        //public int CanalID;

        /// <summary>
        /// Título
        /// </summary>
        [FieldData(Length = 100),
         FieldTitle("Título: ")]
        public string Titulo;


        [FieldData(ColumnType = ColumnTypes.Text)]
        public string Conteudo;


        [FieldData(ColumnType = ColumnTypes.Text)]
        public string ConteudoLateral;

        [FieldData(Length = 80)]
        public string Texto1;

        [FieldData(Length = 80)]
        public string Link1;

        [FieldData(Length = 80)]
        public string Texto2;

        [FieldData(Length = 80)]
        public string Link2;

        [FieldData(Length = 80)]
        public string Texto3;

        [FieldData(Length = 80)]
        public string Link3;

        [FieldData(Length = 80)]
        public string Texto4;

        [FieldData(Length = 80)]
        public string Link4;

        //[FieldData(Length = 1),
        // FieldTitle("Situação: ")]
        //public string Situacao;


        //[FieldData()]
        //public Boolean Destaques;

        //[FieldData()]
        //public Boolean Video;

        //[FieldData(Length = 150)]
        //public string pagina;


        [FieldData(),FieldTitle("Data de Inclusão: ")]
        public DateTime DataInclusao; // = DateTime.Now;

        [FieldData(),FieldTitle("Data de Alteração: ") ]
        public DateTime DataAlteracao; // = DateTime.Now;

        /// <summary>
        /// Usuario da ultima alteração do registro
        /// </summary>
        [
         FieldData(),
         FieldControl(Type = FieldControlTypes.Label),
         FieldTitle("Usuário: ")
        ]
        public string Usuario;

        [FieldData(ColumnType = ColumnTypes.Text)]
        public string xmlLOG;

        #endregion




    }
}