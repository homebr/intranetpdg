﻿using System;
using System.Web;
using FrameWork.DataObject;
using FrameWork.Metricas;
using FrameWork.WebControls;


namespace ClassLibrary
{
    [ClassData("VisitaRoboID", true)]
    public class VisitantesRobos : ObjectData
    {

        public static Type TypeVisitantesRobos;

        #region Campos

        [FieldData()]
        public int VisitaRoboID;

        [FieldData(Index = true)]
        public DateTime Data = DateTime.Now;

        [FieldData(Index = true, Truncate = true)]
        public string Pagina;

        [FieldData(Index = true, Truncate = true)]
        public string Robo;

        [FieldData(Length = 255, Truncate = true)]
        public string UserAgent;

        #endregion

        public VisitantesRobos()
        {
            if (TypeVisitantesRobos == null)
                TypeVisitantesRobos = this.GetType();
        }

        /// <summary>
        /// Retorna se é um Robo
        /// </summary>
        public static bool IsRobo(HttpContext http)
        {
            //crawler
            string ua = http.Request.UserAgent;

            if (http.Request.Browser.Crawler ||
                ua == null ||
                ua.Contains("spider") ||
                ua.Contains("bingbot") ||
                ua.Contains("robot") ||
                ua.Contains("Yahoo") ||
                ua.Contains("Yandex") ||
                ua.Contains("Ezooms") ||
                ua.Contains("Preview") ||
                ua.StartsWith("InfoWeb") ||
                ua.IndexOf(" ") == -1)
            {
                // provavel Robo, exceto se for:
                if (ua != null &&
                    ua.Contains("Googlebot-Mobile"))
                    return false;
                else
                    return true;
            }
            else
            {
                string rf;
                if (http.Request.UrlReferrer != null)
                {
                    rf = http.Request.UrlReferrer.OriginalString;
                    if (rf.Contains("radarbit.com"))
                        return true;
                    else if (rf.Contains("preco2.buscape.com.br"))
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }
        }

        public static string GetRoboName(string cUserAgent)
        {
            cUserAgent = cUserAgent.Replace("(", ";");
            if (cUserAgent.EndsWith(")"))
                cUserAgent = cUserAgent.Substring(0, cUserAgent.Length - 1);

            string cRobo = "?";
            foreach (string r in cUserAgent.Split(';'))
                if (!r.Contains("http:"))
                    cRobo = r.Trim(); // obtem o utimo nome da sequencia!

            return cRobo;
        }

        public static string LogROBO(string cRoboLog, string cRoboInfo, ref Boolean lGoogleHead, ref Boolean lMsnHead, ref Boolean lYahooHead)
        {

            string cLog = String.Format("{0:dd/MM/yyyy HH:mm:ss} {1}\r\n", DateTime.Now, cRoboInfo);

            if (cRoboLog.Length > 2000)
            {
                int n = cRoboLog.IndexOf("\n", 1500);
                if (n > 0)
                    cLog += cRoboLog.Substring(0, n) + "...";
                else
                    cLog += cRoboLog;
            }
            else
                cLog += cRoboLog;

            if (cRoboInfo.Contains("Google"))
                lGoogleHead = true;
            else if (cRoboInfo.Contains("msn") || cRoboInfo.Contains("bing"))
                lMsnHead = true;
            else if (cRoboInfo.Contains("Yahoo"))
                lYahooHead = true;

            return cLog;
        }

        public virtual bool Create(HttpContext http)
        {
            return true;
        }
    }
}
