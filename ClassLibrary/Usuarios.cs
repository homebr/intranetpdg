﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using FrameWork;
using FrameWork.DataObject;
using FrameWork.WebControls;

namespace ClassLibrary
{

    [ClassData("LoginAcesso", false, LogXML = "xmlLOG")]
    public class Usuarios : ObjectData
    {

        [FieldData()]
        public string LoginAcesso;

        [FieldData()]
        public string Senha;

        [FieldData(Length = 250)]
        public string Nome;

        [FieldData(Length = 250)]
        public string Email;

        [FieldData(Length = 2)]
        public string Grupo;

        [FieldData()]
        public string Foto;

        [FieldData()]
        public bool Admin;

        [FieldData()]
        public DateTime AlteracaoSenha;

        [FieldData(Length = 2)]
        public string SituacaoLogin;

        [FieldData()]
        public DateTime UltimoAcesso;

        [FieldData()]
        public string UltimoIP;

        [FieldData()]
        public string SessionID;

        [FieldData()]
        public DateTime DataInclusao;

        [FieldData()]
        public DateTime DataAlteracao;

        [FieldData()]
        public string Usuario;

        [FieldData(ColumnType = ColumnTypes.Text), FieldControl(Type = FieldControlTypes.LogDisplay), FieldTitle(null)]
        public string xmlLOG;

        public override void Save()
        {
            if (this.TotalLoad == 0)
            {
                this.DataInclusao = DateTime.Now;
            }
            base.Save();
        }



        //public static UserInfo ValidateLogin(object sender, string cUser, string cPassword, string cToken)
        // {
        //     UserInfo ui = new UserInfo();
        //     Usuarios user = new Usuarios();
        //     if (user.LoadFirst("LoginAcesso=@User", new ParameterItem("@User", cUser)))
        //     {
        //         String uPass = Geral.Cript.Descript(user.Senha);
        //         if (uPass == cPassword)
        //         {
        //             ui.UserID = user.IDLogin;
        //             ui.Admin = user.Admin;
        //             ui.User = user.LoginAcesso;
        //             ui.Name = user.Nome;
        //             if (user.IDGrupo > 0 && !ui.Admin)
        //             {

        //                 //se pertencer ao grupo de digitadores, verifica se já está sem uso a mais de 12 dias
        //                 if (user.IDGrupo == 1 || user.IDGrupo == 3)
        //                 {
        //                     //se o campo ultimo acesso for null então a partir da data informada não permitirá mais o acesso
        //                     if (((user.UltimoAcesso == null || FrameWork.Util.GetString(user.UltimoAcesso).Contains("0001")) && DateTime.Now > user.DataAlteracao.AddDays(13)) || (!(user.UltimoAcesso == null || FrameWork.Util.GetString(user.UltimoAcesso).Contains("0001")) && user.UltimoAcesso.AddDays(13) < DateTime.Now && user.DataAlteracao.AddDays(13) < DateTime.Now))
        //                     {
        //                         throw new Exception("Favor verificar o seu acesso com o Administrador!");
        //                     }

        //                 }

        //                 GrupoUsuarios group = new GrupoUsuarios();
        //                 if (group.Load(user.IDGrupo) == 1)
        //                 {
        //                     ui.Rights = group.Acessos;
        //                     ui.Name += " (" + group.Grupo + ")";
        //                 }
        //                 else
        //                     ui.Rights = "";
        //             }
        //             else
        //                 ui.Rights = "";

        //             System.Web.HttpContext.Current.Session["_uiGrupo"] = user.IDGrupo.ToString();

        //             user.UltimoAcesso = DateTime.Now;
        //             user.UltimoIP = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
        //             //user.Save();
        //         }
        //         else
        //             throw new Exception("Senha invalida");
        //     }
        //     else
        //         throw new Exception("Usuário invalido");
        //     return ui;
        // }

    }
}




