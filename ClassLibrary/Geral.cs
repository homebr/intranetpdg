﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Configuration;
using System.Reflection;
using System.Net.Mail;
using System.Net;
using System.IO;
using FrameWork;
using FrameWork.DataObject;
using FrameWork.WebControls;
using FrameWork.Metricas;

public class Geral : System.Web.UI.Page
{

    #region "Variaveis"

    //public static string strPathImagens = System.Configuration.ConfigurationManager.AppSettings["pathImagens"];
    //public static string strPathImagensFS = System.Configuration.ConfigurationManager.AppSettings["pathImagensFS"];
    //public string strPathSite = System.Configuration.ConfigurationManager.AppSettings["pathSite"];
    //public static string strPathFotos, strPathFotosFS, strPathSite, strParametros;
    public string strEmailCopia = System.Configuration.ConfigurationManager.AppSettings["EmailCopia"];
    public string strEmailHost = System.Configuration.ConfigurationManager.AppSettings["EmailHost"];
    public static string strConn;
    public bool ReloadMenu = false;
    public bool flagBrowser = false;
    public static string[,] arrIDEmps = new string[20, 2];
    public string strOrigem, strEmailRC, strEmpC, strNomeExt, strEmailExt; //variaveis de origem externa


    public static string strConnSql;
    public static SortedDictionary<string, string> UserSession = new SortedDictionary<string, string>();
    public static string LoadPC2log = "Inicializando \r\n ";
    public static long TotalRequest = 0;
    public static DateTime StartDate;
    public static int TotalRobos = 0;
    public static double TotalTime = 0;
    public static double MaxTime = 0;
    public static string MaxURL = "";
    public static string MaxDelayUserURL = "";
    public static string MaxDelayRoboURL = "";
    public static int TotalNoDelay = 0;

    public static List<string> ipLock = new List<string>();
    public const string FormatDateFull = "{0:dd/MM/yy HH:mm:ss}";

    public static Queue<string> TaskSQL = new Queue<string>();


    #endregion

    public static CriptHEX Cript;

    /// <summary>
    /// Inicialização do sistema
    /// </summary>
    protected override void OnInit(EventArgs e)
    {
        //base.OnInit(e);
        if (strConn == null)
            strConn = (System.Configuration.ConfigurationManager.AppSettings["strConexao"].ToString());
        //strConn = FrameWork.WebControls.WebSecurity.FromBase64(System.Configuration.ConfigurationManager.AppSettings["strConexao"].ToString());
        //"server=187.45.208.187; user id='portalagra'; password='user2009agra'; database='PortalAgra'"; //
        //DataSource.DefaulDataSource = new DataSource(strConn);

        //DataSource.CheckAllTable("ClassLibrary");


        //if (HttpContext.Current.Request.IsLocal)
        //{
        //    strPathFotos = System.Configuration.ConfigurationManager.AppSettings["Local_pathImagens"];
        //    strPathFotosFS = System.Configuration.ConfigurationManager.AppSettings["Local_pathImagensFS"];
        //    strPathSite = System.Configuration.ConfigurationManager.AppSettings["Local_pathSite"];
        //}

        //para gravar cookie quando há frame no site
        //this.Response.AddHeader("p3p","CP=\"CAO PSA OUR\"");

        //if ((!IsPostBack) && (HttpContext.Current.Session["flagBrowser"] == null))
        //{

        //    // verifica a compatibilidade do browser!
        //    flagBrowser = true;

        //    switch (Request.Browser.Browser.ToLower())
        //    {
        //        case "google":
        //        case "msn":
        //        case "yahoo":
        //            flagBrowser = true; // autoriza qualquer robo de busca indexar (vizualizar o site)
        //            break;

        //        case "ie":
        //            flagBrowser = Request.Browser.MajorVersion >= 6;
        //            break;

        //        case "firefox": // NetScape funciona como FireFox
        //            flagBrowser = Request.Browser.MajorVersion >= 3;
        //            break;

        //        case "opera":
        //            flagBrowser = Request.Browser.MajorVersion >= 9;
        //            break;

        //        case "applemac-safari":
        //            //strSyleCss = "_SiteSafari.css";
        //            flagBrowser = Request.Browser.MajorVersion >= 5;
        //            break;

        //        default:
        //            flagBrowser = false;
        //            break;

        //    }

        //    if (!flagBrowser)
        //    {
        //        // enviar e-mail para a agra notificando a incompatibilidade
        //        // redirecionar para a página 'BrowserIncopativel.htm' links para donwloads 
        //        // das umas versões dos browsers compatíveis
        //        string cInfo = Request.Browser.Browser + " - Versão: " + Request.Browser.MajorVersion + "." + Request.Browser.MajorVersion + "<br>Browser não suportado!";
        //        //Response.Write(cInfo);
        //        //WebUtil.SendInfo(WebUtil.SendErrorTitle, cInfo, false);
        //        //Response.End();
        //        //Response.Redirect("BrowserIncopativel.htm");
        //        HttpContext.Current.Session["flagBrowser"] = false;

        //    }

        //}
        //else
        //    flagBrowser = true;


        //Obtem o nome da página
        string strPagina = Request.Path;
        string strModulo = null;
        //checa se a default não é da campanha
        if (strPagina.IndexOf("campanha/Default") > -1)
            strPagina = "campanha";
        else
            strPagina = strPagina.Substring(strPagina.LastIndexOf("/") + 1);

        //int i = strPagina.IndexOf(".");
        //if (i == -1 && strPagina != "campanha")
        //{
        //    DataTable tb = DataSource.DefaulDataSource.Select("SELECT IDEmpreendimento FROM Site_Empreendimentos WHERE Replace(Nome,' ','') LIKE @Nome", new ParameterItem("@Nome", "%" + strPagina + "%"));
        //    if (tb.Rows.Count == 1)
        //        Response.Redirect("~/ImoveisApresentacao.aspx?m=Info&ID=" + tb.Rows[0][0].ToString(), true);
        //    else
        //        Response.Redirect("~/imoveis.aspx");
        //}
        //else if (strPagina != "campanha")
        //{
        //    strPagina = strPagina.Substring(0, i);
        //    strPagina = strPagina.ToLower();
        //}

        string cVar = "";
        Boolean FlagRegistraVisita = true;
        //if (this.IsPostBack)
        //{
        //    cVar = "";
        //    string cValor, cAux;
        //    string cAuxchk = "";
        //    DataTable tbInfra = new DataTable();
        //    foreach (string key in Request.Form.AllKeys)
        //    {
        //        if (key != null)
        //        {
        //            cValor = (Request.Form[key] ?? "").Trim();

        //            if ((key == "m" && cValor == "BuscaTopo") || (key == "Master$mnup$campoBuscaTopo"))
        //                strModulo = "BuscaTopo";

        //            if (cValor != "0" && cValor != "X" && cValor != "")
        //            {
        //                if ((i = key.IndexOf("$Lateral$ctl00$")) > 0)
        //                {
        //                    cVar += key.Substring(i + "$Lateral$ctl00$".Length) + "=" + cValor + "&";
        //                    strModulo = "Busca";
        //                }
        //                else if ((i = key.IndexOf("mnup")) > 0)
        //                {

        //                    if (cValor != "Buscar por nome ou endereço")
        //                    {
        //                        cVar += key.Substring(i + 5) + "=" + cValor + "&";
        //                        if (strModulo != "BuscaTopo")
        //                            FlagRegistraVisita = false;
        //                    }
        //                }
        //                else if (strPagina == "buscaavancada")
        //                {
        //                    strModulo = "BuscaAvancada";
        //                    i = key.LastIndexOf("$");
        //                    if (i > 0)
        //                    {
        //                        cAux = key.Substring(i + 1);
        //                        if (cAux.StartsWith("ddl"))
        //                            cVar += key.Substring(i + 4) + "=" + cValor + "&";
        //                        else
        //                        {
        //                            if ((key.IndexOf("chk") > -1) && (cValor == "on"))
        //                            {

        //                                i = key.LastIndexOf("$");
        //                                int intInicialNome = key.IndexOf("chk") + 3;
        //                                int intQtdNome = i - intInicialNome;
        //                                cAux = key.Substring(intInicialNome, intQtdNome);
        //                                if (cAuxchk == "InfraEstrutura" && cAux == "Diferenciais")
        //                                    tbInfra = new DataTable();
        //                                if (tbInfra.Rows.Count == 0)
        //                                {
        //                                    //if (cAux == "InfraEstrutura")
        //                                    //    strSql = "SELECT IDInfraEstrutura, Descricao FROM Site_InfraEstrutura WHERE Situacao='A' ORDER BY Descricao";
        //                                    //else
        //                                    //    strSql = "SELECT IDDiferenciais, Descricao FROM Site_Diferenciais WHERE Situacao='A' ORDER BY Descricao";
        //                                    //tbInfra = DataSource.DefaulDataSource.Select(strSql);
        //                                }
        //                                cAuxchk = cAux;
        //                                cValor = tbInfra.Rows[Util.GetInt(key.Substring(i + 1))][0].ToString();
        //                                if (cVar.IndexOf(cAux) > -1)
        //                                    cVar = cVar.Substring(0, cVar.Length - 1) + "|" + cValor + "&";
        //                                else
        //                                    cVar += cAux + "=" + cValor + "&";
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //}
        //else
        //    cVar = Request.QueryString.ToString();


        //Váriaveis externas //GERA SESSAO CASO PARAMETRO ORIGEM NAO SEJA VAZIO
        if (FrameWork.Util.GetString(Request["origem"]) != "")
        {
            strOrigem = Request["origem"];
            Session["origem"] = strOrigem;
            Visitantes.RequestOrigemName = "origem";
        }
        else if (FrameWork.Util.GetString(Request["o"]) != "")
        {
            strOrigem = Request["o"];
            Session["origem"] = strOrigem;
        }
        else if (FrameWork.Util.GetString(Session["origem"]) != "")
        {
            strOrigem = (string)Session["origem"];
        }

        ////Grava Visitas
        if (FlagRegistraVisita) // && !this.Request.IsLocal)
            Visitantes.RegistraVisita(this.Context, strPagina, this.IsPostBack, cVar, strModulo);


        //if (FrameWork.Util.GetString(Request["emailrc"]) != "")
        //{
        //    strEmailRC = Request["emailrc"];
        //    Session["emailrc"] = strEmailRC;
        //}
        //else if (FrameWork.Util.GetString(Session["emailrc"]) != "")
        //{
        //    strEmailRC = (string)Session["emailrc"];
        //}

    }



    private void Page_Init(object sender, System.EventArgs e)
    {
        if (FrameWork.DataObject.DataSource.DefaulDataSource.ConnectionString == null)
        {
            strConnSql = (System.Configuration.ConfigurationManager.AppSettings["Conexao"].ToString());
            DataSource.DefaulDataSource = new DataSource(strConnSql);
        }
    }

    public static object CreateInstance(string cInstanceName)
    {
        Assembly asm = Assembly.GetExecutingAssembly();
        return asm.CreateInstance(cInstanceName);
    }

    private void StartCookie(String nomeCookie, String valorCookie)
    {
        HttpCookie objCookie = new HttpCookie(nomeCookie, valorCookie);
        objCookie.Expires = DateTime.Now.AddYears(1);
        Response.Cookies.Add(objCookie);
    }

    private string ViewCookie(String nomeCookie)
    {
        string strValorCookie;

        HttpCookie objCookie = Request.Cookies[nomeCookie];
        if (objCookie == null)
            strValorCookie = "";
        else
            strValorCookie = (objCookie.Value);
        return strValorCookie;
    }


    /// <summary>
    /// Inicialização do site ou do Admin de acordo com o paramentro informado
    /// </summary>
    /// <param name="app">Instancia 'this' do application</param>
    /// <param name="lAdmin">Informa se é o ADMIN ou SITE (true, para admin, false para Site)</param>
    public static void Start(HttpApplication app, bool lAdmin)
    {


        // *************************************
        // Configurações Comum
        if (System.Configuration.ConfigurationManager.AppSettings["PerformanceMonitor"] == "True")
        {
            DataSource.PerformanceMonitor = true;
            DataSource.PerformanceTime = 3000;
            if (app.Context.Trace.IsEnabled)
            {
                //DataSource.PerformaceEnd += PerformanceTraceSQL();
            }
        }



        // Configura a classe de seleção de dados
        FrameWork.WebControls.List.onSelect += FrameWork.DataObject.DataSourceObject.SelectData;

        // Cofigura as classes de metricas
        Visitantes.TypeVisitantes = typeof(Visitantes);
        VisitantesPaginas.TypeVisitantePaginas = typeof(VisitantesPaginas);
        VisitantesRobos.TypeVisitantesRobos = typeof(VisitantesRobos);


        // Configuração e Autenticação do Servidor de e-mails
        Util.smtpDefault = new SmtpClient();
        Util.smtpDefault.Host = FrameWork.Util.GetString(ConfigurationManager.AppSettings["email_EmailHost"]);
        Util.smtpDefault.UseDefaultCredentials = false;
        Util.smtpDefault.EnableSsl = true;
        Util.smtpDefault.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["email_UsuarioAutenticacao"],
                                                             ConfigurationManager.AppSettings["email_SenhaAutenticacao"]);
        Util.smtpDefault.Port = 587;
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;
        Util.smtpDefault.Timeout = 5000; // se não enviar em 5 segundos! Erro!
        Util.SendInfoFrom = System.Configuration.ConfigurationManager.AppSettings["email_SendInfoFrom"]; //Usado para envio de informativo
        Util.SendInfoTo = System.Configuration.ConfigurationManager.AppSettings["email_SendInfoTo"];

        //Erro.GetException += FrameWork.WebControls.WebUtil.GetAppException;
        Erro.RenderException += FrameWork.WebControls.WebUtil.RenderAppException;
        Erro.SendFrom = System.Configuration.ConfigurationManager.AppSettings["email_SendInfoFrom"]; //Usado para envio de erro
        Erro.SendTo = System.Configuration.ConfigurationManager.AppSettings["email_SendInfoTo"];
        Erro.LogPath = app.Server.MapPath("~/LOG");

        //definindo o html modelo da página de erro
        string FilePath = System.Web.HttpContext.Current.Server.MapPath("~/Erro.htm");
        StreamReader objStreamReader;
        objStreamReader = System.IO.File.OpenText(FilePath);
        string strArq = objStreamReader.ReadToEnd();
        WebUtil.ErrorHTML = strArq;

        // Por ultimo conecta-se ao banco
        try
        {
            strConn = FrameWork.Util.GetString(System.Configuration.ConfigurationManager.AppSettings["Conexao"]);
            DataSource.DefaulDataSource = new DataSource(strConn);
            LoadPC2log += "(sql)";

        }
        catch (Exception ex)
        {
            ex.Data.Add("ErroConexao", "Erro ao se conectar com o Banco de dados");
            LoadPC2log += "ERRO ao obter as configurações do SQL" + System.Environment.NewLine + ex.Message + System.Environment.NewLine + ex.StackTrace + System.Environment.NewLine;
            Erro.SendApplicationError(ex);
        }

    }

    public static bool PerformanceTraceSQL(SqlMonitor sm)
    {
        if ((HttpContext.Current != null))
        {
            string cSql = sm.SQL.ToLower();
            int n = cSql.IndexOf(" from ");
            if (n > 0)
            {
                cSql = cSql.Substring(n);
            }
            if (cSql.Length > 50)
            {
                cSql = cSql.Substring(0, 50);
            }
            HttpContext.Current.Trace.Write("+SQL", " > (" + sm.Total.TotalMilliseconds.ToString("###,##0") + "/" + sm.QTD + ") " + cSql);
        }
        return true;
    }
    public Geral()
    {
        Init += Page_Init;
    }

    #region "Erro"

    //Public Shared Sub SendLojaErro(ByVal ex As Exception, ByVal cLog As String, ByVal ilpg As Page, Optional ByVal lNotify As Boolean = False)
    //    Dim cURL As String = ilpg.Request.Url.AbsoluteUri
    //    ex.Data.Add("[PC-pg] URL", cURL)
    //    ex.Data.Add("[PC-pg] LOG", cLog)
    //    'ex = New ContextException(ilpg, ex)
    //    If lNotify Then
    //        Dim cc As String = Erro.SendTo
    //        FrameWork.Erro.SendApplicationErrorCC(ex, cc)
    //    Else
    //        FrameWork.Erro.SendApplicationError(ex)
    //    End If
    //End Sub

    //Public Shared Sub SendLojaErro(ByVal ex As Exception, ByVal cLog As String, ByVal cLoja As String, Optional ByVal lNotify As Boolean = False)
    //    ex.Data.Add("[PC-lj] LOG", cLog)
    //    If lNotify Then
    //        Dim cc As String = Erro.SendTo
    //        FrameWork.Erro.SendApplicationErrorCC(ex, cc)
    //    Else
    //        FrameWork.Erro.SendApplicationError(ex)
    //    End If
    //End Sub

    //Public Shared Function GetLojaException(ByVal sender As Object, ByRef ex As Exception) As Boolean

    //    If sender.GetType() Is GetType(HttpApplication) OrElse _
    //        sender.GetType() Is GetType(ContextException) OrElse _
    //        sender.GetType().IsSubclassOf(GetType(HttpApplication)) Then

    //        'Obtem o contexto do erro
    //        Dim c As HttpContext
    //        If sender.GetType() Is GetType(HttpApplication) OrElse _
    //            sender.GetType().IsSubclassOf(GetType(HttpApplication)) Then
    //            c = CType(sender, HttpApplication).Context
    //        Else
    //            c = CType(sender, ContextException).Context
    //        End If

    //        'Aqui já ira realizar os testes padrão e inserir informações dos request
    //        If Not WebUtil.GetAppException(sender, ex) Then
    //            ex.Data.Add("[PC] error", "GetAppException() false")
    //            Return False
    //        End If

    //        'Se o erro tiver sido falso ou abotado!
    //        If ex Is Nothing Then
    //            Return True
    //        ElseIf ex.Message.EndsWith("does not exist.") OrElse _
    //            ex.Message.EndsWith("was not found") OrElse _
    //            ex.Message.Contains("Validation of viewstate") Then
    //            'Ignora ViewState corrompida
    //            'por padrão a rotina de obtenção de erro já limpa o erro
    //            'não é mais necessário do app.Server.ClearError()
    //            ex = Nothing
    //            c.Response.Redirect("~/default.aspx", True)
    //            Return True
    //        ElseIf ex.Message.Contains("invalid script resource request") OrElse _
    //            ex.Message.Contains("invalid webresource request") Then
    //            'Provavel erro no "ScriptResource.axd"
    //            ex = Nothing
    //            Return True
    //        End If

    //        ex.Data.Add("[PC]", "GetLojaException()")

    //        'Request
    //        Try
    //            If c.Request Is Nothing Then
    //                'Não faz nada pois não há request
    //                ex.Data.Add("[PC] REQUEST", "(indefinido)")
    //            ElseIf c.Request.RawUrl.Contains(".asmx") Then
    //                c.Response.StatusCode = 404
    //                If Not c.Session Is Nothing Then
    //                    c.Session.Abandon()
    //                End If
    //                ex = Nothing
    //                Return True
    //            ElseIf c.Request.RawUrl.Contains(".axd") Then
    //                ex = Nothing
    //                Return True
    //                c.Response.Redirect("~/default.aspx", True)
    //            Else

    //                ex.Source = "[" & c.Request.Url.Host & "]"

    //                Dim tmp As String = c.Request.QueryString.ToString().ToLower().Replace("+", " ")

    //                Dim lSQLinjectRequest As Boolean = _
    //                    tmp.Contains(" and ") OrElse _
    //                    tmp.Contains(" or ") OrElse _
    //                    tmp.Contains("'") OrElse _
    //                    tmp.Contains("char(") OrElse _
    //                    tmp.Contains("http://")

    //                tmp = ex.Message.ToLower()
    //                Dim lDetectEspecial As Boolean = _
    //                    tmp.Contains("select") OrElse _
    //                    tmp.Contains("char(")

    //                If lSQLinjectRequest OrElse lDetectEspecial Then
    //                    Dim lADM As Boolean = c.Request.Path.ToLower().EndsWith("/adm/default.aspx")
    //                    'O IP já está em outra variável da coleção
    //                    ex.Data.Add("[PC] SQL Inject", lSQLinjectRequest)
    //                    ex.Data.Add("[PC] Erro Estranho", lDetectEspecial)
    //                    ex.Data.Add("[PC] Request.Path", c.Request.Path.ToLower())
    //                    If Not lADM Then
    //                        ex.Data.Add("[PC] TENTATIVA DE INVASÃO", "IP " & c.Request.UserHostAddress & " Bloqueado")
    //                        ex.Data.Add("[PC] IP-Global-Local", "http://whatismyipaddress.com/ip/" & c.Request.UserHostAddress)
    //                        Geral.ipLock.Add(c.Request.UserHostAddress)
    //                        Util.SendInfo("SQL Inject IP Bloqueado", Erro.htmlException(ex), True)
    //                    End If
    //                    If Not c.Session Is Nothing Then
    //                        c.Session.Abandon()
    //                    End If
    //                End If
    //            End If
    //        Catch exr As ThreadAbortException
    //            ex = Nothing
    //            Return True
    //        Catch exr As Exception
    //            ex.Data.Add("[PC] REQUEST", exr.Message)
    //        End Try

    //        If ex.Message.Contains("DATASOURCE") Then
    //            ex.Data.Add("[PC] Performance LOG", DataSource.PerfomanceLog)
    //        End If

    //        Return True
    //    Else
    //        Return False
    //    End If

    //End Function

    #endregion



}

