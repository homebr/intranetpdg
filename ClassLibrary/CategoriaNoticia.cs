﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using FrameWork.DataObject;
using FrameWork.WebControls;
using System.Data;

namespace ClassLibrary
{
    [ClassData("ID", true) ]
    public class CategoriaNoticia : ObjectData
    {

        /// <summary>
        /// Código interno de referencia única para relacionamento no banco de dados
        /// </summary>
        [FieldData()]
        public int ID;

        /// <summary>
        /// Título
        /// </summary>
        [FieldData(Length = 140)]
        public string Categoria;

        //[FieldData()]
        //public DateTime Data;

        //[FieldData()]
        //public string Autor;

        //[FieldData(ColumnType = ColumnTypes.Text)]
        //public String Chamada;

        //[FieldData(ColumnType=ColumnTypes.Text)] 
        //public String Conteudo;

        //[FieldData(Length = 100)]
        //public string Foto;

        //[FieldData(Length = 100)]
        //public string FotoPequena;

        //[FieldData(DefaultValue = "1")]
        //public int Tipo;

        //[FieldData(DefaultValue = "0")]
        //public Boolean Destaque;

        [FieldData()]
        public Boolean Ativo;

        //[FieldData(Length = 150)]
        //public string sku;

        [FieldData()]
        public DateTime DataInclusao; // = DateTime.Now;

        [FieldData()]
        public DateTime DataAlteracao; // = DateTime.Now;

        /// <summary>
        /// Usuario da ultima alteração do registro
        /// </summary>
        [FieldData()]
        public string Usuario;



    }
}
