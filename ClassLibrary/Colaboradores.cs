﻿using System;
using FrameWork.DataObject;
using FrameWork.WebControls;

namespace ClassLibrary
{

    [ClassData("ID", false, LogXML = "xmlLOG",NoLogFields = "UltimoAcesso,UltimoIP,AlteracaoSenha,Senha,flagAlteracao")]
    public class Colaboradores : ObjectData
    {

        /// <summary>
        /// ID CHAVE PRIMARIA
        /// </summary>
        [FieldData()]
        public int ID;

        [FieldData()]
        public int Setor;


        [FieldData()]
        public int Regional;

        /// <summary>
        /// ID Nome
        /// </summary>
        [FieldData(Length = 150)]
        public string Nome;

        [FieldData(Length = 50)]
        public string Cpf;

        [FieldData(Length = 50)]
        public string Senha;

        [FieldData(Length = 150)]
        public string Cargo;

        [FieldData(ColumnType = ColumnTypes.Text)]
        public string Descricao;

        [FieldData(ColumnType = ColumnTypes.Text)]
        public string Formacao;

        [FieldData(Length=200)]
        public string Email;

        [FieldData(Length = 50)]
        public string Foto;

      

        [FieldData()]
        public Boolean Ativo;

        [FieldData(Length = 500)]
        public string Linkedin;

        [FieldData(Length = 200)]
        public string LoginRede;

        [FieldData(Length = 50)]
        public string Ramal;

        [FieldData()]
        public int Sequencia;

        /// <summary>
        /// ID DataInclusao
        /// </summary>
        [FieldData()]
        public DateTime DataInclusao;

        [FieldData()]
        public DateTime DataAniversario;


        [FieldData()]
        public DateTime DataAdmissao;

        /// <summary>
        /// ID DataAlteracao
        /// </summary>
        [FieldData()]
        public DateTime DataAlteracao;

        [FieldData()]
        public string Usuario;

          
        [FieldData(ColumnType = ColumnTypes.Text)]
        public string xmlLOG;
    }


}