﻿using System;
using System.Web.UI;
using System.Data;
using FrameWork.DataObject;
using FrameWork.WebControls;
using System.Web.UI.WebControls;


namespace ClassLibrary
{
    

    [ClassData("IDGrupo", true),
     ClassControl(Title = "Grupos Usuários", GridWidth=-99),
     ClassMenu(MenuNivel.Menu, "Ferramentas", "Grupo Usuários", Sequencia = 4),
     ClassRight(170, "C,E,I,X", "Cadastro de Grupos Usuario", Group = "Ferramentas")
    ]
    public class GrupoUsuarios:ObjectData
    {
        [
         FieldData(), 
         TableLayout(Width=-99),
         FieldControl(Type = FieldControlTypes.Label)]
        public int IDGrupo;

        [
         FieldData(),
         FieldControl(MaxLength = 80, Width = 200, Validate="!", ValidateError="Informe o nome do Grupo"),
         FieldColumn("IDGrupo"),
         FieldTitle("Grupo: ")
        ]
        public string Grupo;

        [
         FieldData(ColumnType = ColumnTypes.Text),
         FieldControl(Type=FieldControlTypes.Custom, Load="LoadAcessos"),
         FieldTitle("Acessos: ")
        ]
        public string Acessos="";


        [FieldData(Length = 1)]
        public string Situacao;


        [
         FieldData(),
         FieldControl(Type = FieldControlTypes.Label), 
         FieldTitle("Data de Inclusão: ")
        ]
        public DateTime DataInclusao; // = DateTime.Now;

        [
         FieldData(),
         FieldControl(Type = FieldControlTypes.Label), 
         FieldTitle("Data de Alteração: ")        
        ]
        public DateTime DataAlteracao; // = DateTime.Now;

        /// <summary>
        /// Usuario da ultima alteração do registro
        /// </summary>
        [
         FieldData(), 
         FieldControl(Type = FieldControlTypes.Label),
         FieldTitle("Usuário: ")
        ]
        public string Usuario;

        public Usuarios Site_Usuarios
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
            }
        }

        public void LoadAcessos(Control ctrl)
        {
            //System.Data.DataTable dtRight = new System.Data.DataTable();
            this.Acessos = ClassRight.MakeGridRights(ctrl, this.Acessos); //,dtRight
        }

        public void LoadDireitos(Control ctrl)
        {
            if (System.Web.HttpContext.Current.Request["ID"] != "0")
            {
                String Sql = "SELECT ID,isnull(FUNCOES,'') as FUNC,SubFormulario AS 'DESC',(Formulario) AS 'GROUP',SEQUENCIA AS ORD FROM Formularios WHERE Status='A' AND isnull(Link,'')<>'' ORDER BY 'GROUP',ORD ";
                DataTable dtRight = this.DataSource.Select(Sql);
                dtRight.Constraints.Add("PK", dtRight.Columns[0], true);

                FrameWork.WebControls.ClassRight cr;
                
                this.Acessos = MakeGridRights(ctrl, this.Acessos, dtRight); //,dtRight
            }
           
        }


        public static string MakeGridRights(Control ctrl, string cRights, DataTable TableRight)
        {
            DataView dv = new DataView(TableRight);
            dv.Sort = "GROUP,ORD";
            string cGroupTitle = null;
            int nID;
            string cDesc, cFunc, cGroup, F;
            CheckBox chk;
            int n, i;
            Table tb = null;
            TableRow tr;
            TableCell td;
           // FrameWork.IFiledList ifl = (FrameWork.IFiledList)obj;

            //System.Reflection.FieldInfo[] f  = ifl.GetFields(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static);
            String[] f;
            f = System.Enum.GetNames(typeof(EnumDireitos));

            tb = new Table();
            tb.CssClass = "ClassRightTableControlHeader";
            tb.CellPadding = 0;
            tb.CellSpacing = 0;
            tr = new TableRow();
            tb.Rows.Add(tr);
            td = new TableHeaderCell();
            tr.Cells.Add(td);
            for (i = 0; i < f.Length; i++)
            {
                td = new TableCell();
                tr.Cells.Add(td);
                //td.Text = f[i]; // FieldTitle.GetTitle((Enum)Functions.Assembly.CreateInstance(Functions.FullName), f[i].Name);
                // td.Text = FieldTitle.GetTitle(typeof(EnumDireitos).Assembly.CreateInstance(typeof(EnumDireitos)), f[i]);
                String ci = typeof(EnumDireitos).FullName;
                System.Enum en = (System.Enum)typeof(EnumDireitos).Assembly.CreateInstance(ci);
                td.Text = FrameWork.Util.GetString(FieldTitle.GetTitle(en, f[i]));
            }
            ctrl.Controls.Add(tb);
            tb = null;

            if (ctrl.Page.IsPostBack)
                cRights = "";
            bool lAlter = false;
            for (n = 0; n < dv.Count; n++)
            {
                nID = (int)dv[n]["ID"];
               cDesc = (string)dv[n]["DESC"];
                cFunc = (string)dv[n]["FUNC"];
                cGroup = (string)dv[n]["GROUP"];

                if (cGroupTitle != cGroup)
                {
                    if (tb != null)
                        ctrl.Controls.Add(tb);

                    tb = new Table();
                    tb.CssClass = "ClassRightTableControl";
                    tb.CellPadding = 0;
                    tb.CellSpacing = 0;

                    cGroupTitle = cGroup;
                    tb.Caption = cGroupTitle;
                    lAlter = false;
                }
                tr = new TableRow();
                tb.Rows.Add(tr);

                if (lAlter)
                    tr.CssClass = "ClassRightTableRowAlter";
                lAlter = !lAlter;

                td = new TableHeaderCell();
                tr.Cells.Add(td);

                if (cGroup == "")
                    td.Text = cDesc;
                else
                    td.Text = "<span>" + cDesc + "</span>";

                string cNewRight = "";
                for (i = 0; i < f.Length; i++)
                {
                    td = new TableCell();
                    tr.Cells.Add(td);
                    F = f[i];
                    if (cFunc.Contains(F))
                    {
                        chk = new CheckBox();
                        chk.ID = string.Format("ClassRight_{0}_{1}", nID, F);
                        td.Controls.Add(chk);
                        if (ctrl.Page.IsPostBack)
                        {
                            if (ctrl.Page.Request[chk.UniqueID] != null)
                                cNewRight += F + ",";
                        }
                        else
                            chk.Checked = CheckRights2(cRights, nID, F);
                    }
                }
                if (cNewRight != "")
                    cRights += "{" + nID.ToString() + ":" + cNewRight.Substring(0, cNewRight.Length - 1) + "}";
            }
            ctrl.Controls.Add(tb);
            return cRights;
        }

        public static bool CheckRights2(string cRights, int nID, string cFunc)
        {
            string cRight = "{" + nID.ToString() + ":";
            int n = cRights.IndexOf(cRight);
            if (n == -1)
                return false;
            n += cRight.Length;
            cRight = cRights.Substring(n, cRights.IndexOf("}", n) - n);
            cRight += ",";
            return cRight.Contains(cFunc + ",");
        }



    }




}
