﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using FrameWork.DataObject;
using FrameWork.WebControls;
using System.Web.UI.WebControls;


namespace ClassLibrary
{
    [ClassData("ID", true)]
    public class ClientesImoveis : ObjectData
    {

        /// <summary>
        /// ID CHAVE PRIMARIA
        /// </summary>
        [FieldData()]
        public int ID;

        [FieldData()]
        public int IDCliente;

        [FieldData()]
        public int IDContrato;

        [FieldData()]
        public int CodEmpreendimento;

        [FieldData(Length = 200)]
        public string NomeEmpreendimento;

        [FieldData(Length =200)]
        public string Bloco;

        [FieldData()]
        public string Unidade;

        [FieldData()]
        public DateTime DataInclusao = DateTime.Now;
    }
}
