﻿using System;
using FrameWork.DataObject;

namespace ClassLibrary
{
    [ClassData("ID", true, LogXML = "xmlLOG")]
    public class Faq : ObjectData
    {

        [FieldData()]
        public int ID;

        [FieldData(Length = 250)]
        public string Titulo;

        [FieldData()]
        public string Categoria;

        [FieldData(ColumnType = ColumnTypes.Text)]
        public string Texto;

        [FieldData]
        public int Posicao;

        [FieldData(Length = 1)]
        public string Situacao;

        [FieldData()]
        public DateTime DataInclusao;

        [FieldData()]
        public DateTime DataAlteracao;

        [FieldData()]
        public string Usuario;

        [FieldData(ColumnType = ColumnTypes.Text)]
        public string xmlLOG;
    }
}
