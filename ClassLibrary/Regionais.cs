﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using FrameWork.DataObject;
using FrameWork.WebControls;
using System.Web.UI.WebControls;

namespace ClassLibrary
{
    [ClassData("ID", true)]
    public class Regionais : ObjectData
    {

        /// <summary>
        /// ID CHAVE PRIMARIA
        /// </summary>
        [FieldData()]
        public int ID;

        [FieldData(Length =80)]
        public string Nome;

        [FieldData(ColumnType = ColumnTypes.Text)]
        public string Descricao;

        [FieldData(Length = 100)]
        public string Endereco;
        /// <summary>
        /// ID Numero
        /// </summary>
        [FieldData()]
        public string Numero;
        /// <summary>
        /// ID Complemento
        /// </summary>
        [FieldData()]
        public string Complemento;
        /// <summary>
        /// ID Bairro
        /// </summary>
        [FieldData()]
        public string Bairro;
        /// <summary>
        /// ID Cidade
        /// </summary>
        [FieldData()]
        public string Cidade;
        /// <summary>
        /// ID Uf
        /// </summary>
        [FieldData()]
        public string Uf;


        /// <summary>
        /// ID Cep
        /// </summary>
        [FieldData()]
        public string Cep;

        [FieldData(Length = 300)]
        public string Referencia;

        [FieldData()]
        public string Telefone;



        [FieldData(Length = 100)]
        public string Email; //Email de Vendas

        [FieldData()]
        public string Geolocalizacao;

        [FieldData(Length = 1000)]
        public string Regioes;

      

        [FieldData(Length = 1),
         FieldTitle("Situação: ")]
        public string Situacao;


        /// <summary>
        /// ID DataInclusao
        /// </summary>
        [FieldData()]
        public DateTime DataInclusao;

        [FieldData()]
        public DateTime DataAlteracao;

        [FieldData()]
        public string Usuario;


    }




}
