﻿using System;
using System.Collections.Generic;
using System.Text;
using FrameWork.DataObject;
using FrameWork.WebControls;
using System.Web.UI.WebControls;

namespace ClassLibrary
{
    [ClassData("ID", true, LogXML = "xmlLOG") ]
    public class Configuracoes : ObjectData
    {
        /// <summary>
        /// ID CHAVE PRIMARIA
        /// </summary>
        [FieldData()]
        public int ID;

        [FieldData()]
        public string Nome;

        [FieldData(Length = 500)]
        public string Valor;

        /// <summary>
        /// ID DataInclusao
        /// </summary>
        [FieldData()]
        public DateTime DataInclusao;

        /// <summary>
        /// ID DataAlteracao
        /// </summary>
        [FieldData()]
        public DateTime DataAlteracao;

        /// <summary>
        /// ID Usuario
        /// </summary>
        [FieldData()]
        public string Usuario;

        [FieldData(ColumnType = ColumnTypes.Text)]
        public string xmlLOG;



        public class EMPRESA
        {
            public string nome { get; set; }
            public string telefone { get; set; }
            public string telefone2 { get; set; }
            public string endereco { get; set; }

        }

        public class REDES
        {
            public string facebook { get; set; }
            public string youtube { get; set; }
            public string instagram { get; set; }

        }


        public class SMTP
        {
            public string servidor { get; set; }
            public string usuario { get; set; }
            public string senha { get; set; }
            public string porta { get; set; }
            public string ssl { get; set; }
            public string remetente { get; set; }
        }


        public class DESTINATARIOS
        {
            public class faleconosco { 
                public string para { get; set; }
                public string cc { get; set; }
            }
            public class ligamos
            {
                public string para { get; set; }
                public string cc { get; set; }
            }
            public class souvizinhoobra
            {
                public string para { get; set; }
                public string cc { get; set; }
            }
            public class oferecerterreno
            {
                public string para { get; set; }
                public string cc { get; set; }
            }
            public class trabalheconosco
            {
                public string para { get; set; }
                public string cc { get; set; }
            }
            public class corretor
            {
                public string para { get; set; }
                public string cc { get; set; }
            }
            //public class newsletter
            //{
            //    public string para { get; set; }
            //    public string cc { get; set; }
            //}
            public class whatsapp
            {
                public string para { get; set; }
                public string cc { get; set; }
            }
            public class informacoesporemail
            {
                public string para { get; set; }
                public string cc { get; set; }
            }
  
        }

        public class CHAT
        {
            public string link { get; set; }

        }

        public class GOOGLE
        {
            public string script { get; set; }

        }
    }


}
