﻿<%@ WebHandler Language="C#" Class="wsLogAcao" %>

using System;
using System.Web;

public class wsLogAcao : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";

        string modulo = FrameWork.Util.GetString(context.Request["modulo"]);
        int id = FrameWork.Util.GetInt(context.Request["id"]);
        string cpf = FrameWork.Util.GetString(context.Request["cpf"]);
        int idimovel = FrameWork.Util.GetInt(context.Request["idimovel"]);
        string bloco = FrameWork.Util.GetString(context.Request["bloco"]);
        string unidade = FrameWork.Util.GetString(context.Request["unidade"]);
        string acao = FrameWork.Util.GetString(context.Request["acao"]);
        string obs = FrameWork.Util.GetString(context.Request["obs"]);

        string strSql = "";

        if (modulo == "comunicado")
        {
            strSql = "insert ComunicadosVisualizacao (IDComunicado,CPF) values (" + id + ",'" + cpf + "') ";
            FrameWork.DataObject.DataSource.DefaulDataSource.Execute(strSql);

            ClassLibrary.Log_Acoes c = new ClassLibrary.Log_Acoes();
            c.AddLog(cpf,idimovel,bloco,unidade,acao,obs);
                
        }
        //else if (t=="eventos")
        //    strSql = "delete EventosImagens where ImagemID=" + id + " ";
        //else
        //    strSql = "delete ImovelImagens where ImagemID=" + id + " ";


        

        //        cpf: '<%=FrameWork.Util.GetString(Session["_uiCPF"])%>',
        //idimovel: '<%=FrameWork.Util.GetString(Session["_IDImovel"])%>',
        //bloco: '<%=FrameWork.Util.GetString(Session["_Bloco"])%>',
        //unidade: '<%=FrameWork.Util.GetString(Session["_Unidade"])%>',
        //acao: 'Visualizacao do Comunicado: ' + id,
        //obs: '',
        //modulo: 'comunicado'
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}