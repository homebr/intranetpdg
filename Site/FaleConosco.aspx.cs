﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _FaleConosco : System.Web.UI.Page
{
    public ClassLibrary.Colaboradores objCliente = new ClassLibrary.Colaboradores();

    public ClassLibrary.FaleConosco objFaleconosco = new ClassLibrary.FaleConosco();
    //caminho fisico no server
    string pathFisico = System.Configuration.ConfigurationManager.AppSettings["pathFisico"] + "uploads\\perfil\\";
    string pathVirtual = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "uploads/perfil/";

    public string[] sAux = new string[8];
    public string[] sAuxOrigens = new string[8];
    public String sLabels = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            
         
            if (FrameWork.Util.GetString(Session["_uiUserID"]) == "")
            {

                Response.Redirect("/login", true);
            }


            try
            {
                String strUsuario = FrameWork.Util.GetString(Session["_uiCPF"]);
                //String strSenha = FrameWork.Util.GetString(Session["_uiSenha"]);

                //hID.Value = (FrameWork.Util.GetString(FrameWork.Util.GetString(Session["_uiUserID"])));
                hCPF_Cliente.Value = (strUsuario);


                //objCliente.Load(FrameWork.Util.GetString(Session["_uiCodCliente"]));

                objCliente.Load(FrameWork.Util.GetString(Session["_uiUserID"]));

                if (objCliente.TotalLoad == 1)
                {

                    txtNome.Text = objCliente.Nome;
                    txtEmail.Text = objCliente.Email;
                    txtcargo.Text = objCliente.Cargo;

                    //txtCPF.Text = objCliente.Cpf;
                   
                    //txtRamal.Text = objCliente.Ramal;
                    //txtaniversario.Text = objCliente.DataAniversario;

                   
                }

            }
            catch (Exception ex)
            {

                Msg(ex.Message, false);
                return;
            }


        }
    }



    protected void btnAlterarDados_Click(object sender, EventArgs e)
    {

        try
        {
            String strUsuario = FrameWork.Util.GetString(Session["_uiCPF"]);
            String sEmail = FrameWork.Util.GetString(txtEmail.Text);

            String sCargo = FrameWork.Util.GetString(txtcargo.Text);
            //  String sRamal = FrameWork.Util.GetString(txtRamal.Text);
            //String sCelular = FrameWork.Util.GetString(txtCelular.Text);

            //objCliente.Load(FrameWork.Util.GetString(Session["_uiCodCliente"]));

            objFaleconosco.Load(FrameWork.Util.GetString(Session["_uiUserID"]));
            ///
            // objCliente.Ramal = FrameWork.Util.GetString(txtRamal.Text);
            objFaleconosco.Cargo = FrameWork.Util.GetString(txtcargo.Text);
            objFaleconosco.Nome = FrameWork.Util.GetString(txtNome.Text);
            objFaleconosco.Mensagem = FrameWork.Util.GetString(txtMensagem.Text);
            objFaleconosco.Assunto = FrameWork.Util.GetString(ddlassunto.SelectedValue);

            //objCliente.Nome = FrameWork.Util.GetString(txtNome.Text);
            objFaleconosco.DataInclusao = DateTime.Now;
            objFaleconosco.Email = sEmail;

            objFaleconosco.Save();

            if (!String.IsNullOrEmpty(txtEmail.Text))
            {
                //ENVIO AO CLIENTE
                System.IO.StreamReader objStreamReader;
                String strResultadoEnvio = "";
                string strAssunto = "Intranet - Fale Conosco";
                string FilePath = Server.MapPath("template_email/ModeloAtualizacaoCadastral.htm");
                objStreamReader = System.IO.File.OpenText(FilePath);
                string strArq = objStreamReader.ReadToEnd();
                strArq = strArq.Replace("#NOME#", txtNome.Text);
                strArq = strArq.Replace("#IP#", Request.ServerVariables["REMOTE_ADDR"]);
                strArq = strArq.Replace("#DATA#", String.Format("{0:dd/MM/yyyy hh:mm}", DateTime.Now));
                string pathVirtual = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"];
                strArq = strArq.Replace("src=\"img/", "src=\"" + (pathVirtual + "template_email/") + "img/");
                strArq = strArq.Replace("#PathVirtual#", pathVirtual);

                string strTo = "naoresponda@homebrasil.com";
                string strPara = "fernando@homebrasil.com"; // strEmailRecupera;
                string strCc = "helena.lima@pdg.com.br";

                strResultadoEnvio = FrameWork.Util.GetString(Funcoes.EnvioEmail(strArq, strTo, strAssunto, strPara, strCc, ""));
                objStreamReader.Dispose();
                objStreamReader.Close();


                Msg("Suas informações foram enviada com sucesso!", true);
            }
        }
        catch (Exception ex)
        {

            Msg(ex.Message, false);
            return;
        }
    }

    protected void Msg(String msg, Boolean tipo)
    {
        lblMensagem.CssClass = (tipo ? "box-danger alert alert-success" : "box-danger alert alert-danger");
        lblMensagem.Text = msg;
        lblMensagem.Visible = true;
    }

  

}