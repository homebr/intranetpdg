﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{

    public string[] sAux = new string[8];
    public string[] sAuxOrigens = new string[8];
    public String sLabels = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
          

      
                    
            CarregaAniversariantes();
            CarregaDocumentos();
            CarregaManuais();
            CarregaLinks();
            CarregaParceiros();
            CarregaBanneres();
            //CarregaFeed();
        }
        //Response.Write(FrameWork.Util.GetString(Session["_uiUserID"]));
        //Response.End();
    }


    //public void CarregaFeed()
    //{

    //    string pathVirtual = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "uploads/documentos/";
    //    String strUsuario = FrameWork.Util.GetString(Session["_uiUser"]);

    //    string strSql = "select '' as class,'Evento' as tipo,1 as idtipo,id,titulo,data,chamada,conteudo,imagem,datainclusao,CASE    WHEN len(imagem) > 0 THEN ('http://intranet.pdg.com.br/uploads/imgeventos/' +imagem)   ELSE('') END AS ImgLink,video,link,('http://intranet.pdg.com.br/eventosView?id=' +convert(char,(id))) as Link1  from Eventos where ativo =1 union all ";
    //    strSql += " select '' as class,'PDG Comunica' as tipo,2 as idtipo,id, titulo,data,chamada,conteudo,imagem,datainclusao,CASE    WHEN len(imagem) > 0 THEN ('http://intranet.pdg.com.br/uploads/imgpdgcomunica/' +imagem)   ELSE('') END AS ImgLink,video,link,('http://intranet.pdg.com.br/comunicadosView?id=' +convert(char,(id))) as Link1  from PDGComunica where ativo = 1 union all ";
    //    strSql += " select '' as class,'Pdg em casa' as tipo,3 as idtipo, id,titulo,data,chamada,conteudo,imagem,datainclusao,CASE    WHEN len(imagem) > 0 THEN ('http://intranet.pdg.com.br/uploads/imgpdgemcasa/' +imagem)   ELSE('') END AS ImgLink,video,link,('http://intranet.pdg.com.br/PDGemcasaView?id=' +convert(char,(id))) as Link1 from Pdgemcasa where ativo = 1 union all ";
    //    strSql += " select '' as class,'Treinamento' as tipo,4 as idtipo, id,titulo,data,chamada,conteudo,imagem,datainclusao,CASE    WHEN len(imagem) > 0 THEN ('http://intranet.pdg.com.br/uploads/imgtreinamento/' +imagem)   ELSE('') END AS ImgLink,video,link,('http://intranet.pdg.com.br/treinamentoView?id=' +convert(char,(id))) as Link1  from treinamentos where ativo = 1 ";
    //    strSql += " order by data desc,datainclusao desc ";

    //    //strSql += " ORDER BY data desc";
    //    // Response.Write(strSql);
    //    // Response.End();
    //    DataTable dt = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSql);
    //    rptFeed.DataSource = dt;
    //    rptFeed.DataBind();

    //    if (dt.Rows.Count > 0)
    //    {
    //        string strSql1 = "SELECT        TOP (200) id, idFeed, idColaborador, IdTipoFeed, Comentario, DataInclusao, Liberado FROM Comentarios";
    //        //strSql += " ORDER BY data desc";
    //        // Response.Write(strSql);
    //        // Response.End();
    //        DataTable dt1 = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSql1);
    //        if (dt1.Rows.Count > 0)
    //        {


    //            //pnlComprovantes.Visible = false;
    //            //ltlComentarios.Text = "Aguardando validação manualmente na PDG!";

    //        }

    //        //pnlComprovantes.Visible = false;
    //        // ltlTextoEndereco.Text = "Aguardando validação manualmente na PDG!";

    //    }





    //}
    public void CarregaDocumentos()
    {

        string pathVirtual = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "uploads/documentos/";
        String strUsuario = FrameWork.Util.GetString(Session["_uiUser"]);

        string strSql = "SELECT *,('" + pathVirtual + "' + Arquivo) as Link FROM Documentos    where 1=1 and  ativo =1 and tipo = 'Documento' ";
      
        strSql += " ORDER BY descricao desc";
        // Response.Write(strSql);
        // Response.End();
        DataTable dt = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSql);
        rptDocs.DataSource = dt;
        rptDocs.DataBind();

        if (dt.Rows.Count > 0)
        {


            //pnlComprovantes.Visible = false;
            // ltlTextoEndereco.Text = "Aguardando validação manualmente na PDG!";

        }





    }



    public void CarregaManuais()
    {

        string pathVirtual = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "uploads/documentos/";
        String strUsuario = FrameWork.Util.GetString(Session["_uiUser"]);

        string strSql = "SELECT *,('" + pathVirtual + "' + Arquivo) as Link FROM Documentos    where 1=1 and  ativo =1 and tipo = 'Manual' ";

        strSql += " ORDER BY descricao desc";
        // Response.Write(strSql);
        // Response.End();
        DataTable dt = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSql);
        rptManual.DataSource = dt;
        rptManual.DataBind();

        if (dt.Rows.Count > 0)
        {


            //pnlComprovantes.Visible = false;
            // ltlTextoEndereco.Text = "Aguardando validação manualmente na PDG!";

        }





    }

    public void CarregaAniversariantes()
    {

        string pathVirtual = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "uploads/colaboradores/";
        String strUsuario = FrameWork.Util.GetString(Session["_uiUser"]);

        string strSql = "SELECT *,('" + pathVirtual + "' + foto) as Link1,CASE    WHEN len(foto) > 0 THEN ('" + pathVirtual + "' + foto)        ELSE('" + pathVirtual + "foto-perfil.jpg') END AS Link from colaboradores where ativo =1  ";
        
        strSql += "and Convert(char(2),dataAniversario,101) = '" + String.Format("{0:MM}", DateTime.Now) + "' ";
        //if (grupo == "4")

        //{
        //    strSql += " and acesso in (0,1)";
        //}
        //if (grupo == "6")

        //{
        //    strSql += " and acesso in (0,2)";
        //}

        strSql += " order by dataAniversario ";
        //Response.Write(strSql);
        //Response.End();
        DataTable dt = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSql);
        rptAniv.DataSource = dt;
        rptAniv.DataBind();

        if (dt.Rows.Count > 0)
        {


            //pnlComprovantes.Visible = false;
            // ltlTextoEndereco.Text = "Aguardando validação manualmente na PDG!";

        }





    }


    public void CarregaLinks()
    {

        string pathVirtual = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "uploads/logoprogramas/";
        String strUsuario = FrameWork.Util.GetString(Session["_uiUser"]);

        string strSql = "SELECT *,('" + pathVirtual + "' + logo) as linklogo from linkProgramas where ativo =1  ";
        //if (grupo == "4")

        //{
        //    strSql += " and acesso in (0,1)";
        //}
        //if (grupo == "6")

        //{
        //    strSql += " and acesso in (0,2)";
        //}

        strSql += " order by Sequencia ";
        DataTable dt = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSql);
        rptLinks.DataSource = dt;
        rptLinks.DataBind();

        if (dt.Rows.Count > 0)
        {


            //pnlComprovantes.Visible = false;
            // ltlTextoEndereco.Text = "Aguardando validação manualmente na PDG!";

        }





    }


    public void CarregaParceiros()
    {

        string pathVirtual = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "uploads/logoparceria/";
        String strUsuario = FrameWork.Util.GetString(Session["_uiUser"]);

        string strSql = "SELECT *,('" + pathVirtual + "' + logo) as linklogo from Parceria where ativo =1  ";
        //if (grupo == "4")

        //{
        //    strSql += " and acesso in (0,1)";
        //}
        //if (grupo == "6")

        //{
        //    strSql += " and acesso in (0,2)";
        //}

        strSql += " order by empresa desc ";
        DataTable dt = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSql);
        rptParceria.DataSource = dt;
        rptParceria.DataBind();

        if (dt.Rows.Count > 0)
        {


            //pnlComprovantes.Visible = false;
            // ltlTextoEndereco.Text = "Aguardando validação manualmente na PDG!";

        }





    }




    public void CarregaBanneres()
    {

        string pathVirtual = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "uploads/banners/";
        String strUsuario = FrameWork.Util.GetString(Session["_uiUser"]);

        string strSql = "SELECT *,('" + pathVirtual + "' + imagem) as LinkImagem,CASE    WHEN posicao = 0 THEN ('active')   ELSE('') END AS  class from banners where ativo =1 ";
        //strSql += "and Convert(char(2),dataAniversario,101) = '" + String.Format("{0:MM}", DateTime.Now) + "' ";
        //if (grupo == "4")

        //{
        //    strSql += " and acesso in (0,1)";
        //}
        //if (grupo == "6")

        //{
        //    strSql += " and acesso in (0,2)";
        //}

        strSql += " order by posicao ";
        //Response.Write(strSql);
        DataTable dt = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSql);
        rptBanners.DataSource = dt;
        rptBanners.DataBind();
        rptBanners1.DataSource = dt;
        rptBanners1.DataBind();

        if (dt.Rows.Count > 0)
        {


            //pnlComprovantes.Visible = false;
            // ltlTextoEndereco.Text = "Aguardando validação manualmente na PDG!";

        }





    }


}