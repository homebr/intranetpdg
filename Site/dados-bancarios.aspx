﻿<%@ Page Title="Dados bancários | Portal do Investidor Hoteleiro | JLL" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="dados-bancarios.aspx.cs" Inherits="dados_bancarios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="Server">
    <div class="col-12 dashboard">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-5 titulo">
                <h2>Dados bancários </h2>
            </div>
            <div class="col-12  col-sm-12  col-md-12 filtro mobilenone">
                <a class="btn-geral4 ativo btncontasalva">Minhas contas cadastradas</a>
                <a class="btn-geral4 btnnovaconta">Cadastrar uma nova conta</a>
            </div>
        </div>



        <div class="row dados-bancarios">
            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-5 grid-dadosbancarios">
            </div>

            <div class="col-12 filtro mobileshow">
                <a class="btn-geral4 ativo btncontasalva">Minhas contas cadastradas</a>
                <a class="btn-geral4 btnnovaconta">Cadastrar uma nova conta</a>
            </div>

            <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-7 conta">
                <div class="box-dashboard collapse" id="novaconta">
                    <div class="titulo-form"><span>Criar uma nova conta </span></div>
                    <form>
                        <div class="form-row">
                            <div class="col-12 col-md-4">
                                <label>Banco</label>
                                <select class="form-control" id="ddlBancos" style="width: 100%">
                                    <option>237 - Banco Bradesco  S.A</option>
                                </select>
                            </div>
                            <div class="col-12 col-md-3">
                                <label>Tipo</label>
                                <select class="form-control" id="ddlTipo">
                                    <option value="1">Corrente</option>
                                    <option value="2">Poupança</option>
                                </select>
                            </div>
                            <div class="col-12 col-md-2">
                                <label>Agência</label>
                                <input type="text" class="form-control" id="txtCB_Agencia" placeholder="0000" maxlength="10" />
                            </div>
                            <div class="col-12 col-md-3">
                                <label>Conta</label>
                                <input type="text" class="form-control" id="txtCB_Conta" placeholder="00000-0" maxlength="30" />
                            </div>
                            <div class="col-12">
                                <label>Apelido da Conta (opcional)</label>
                                <input type="text" class="form-control" id="txtCB_Apelido" placeholder="Apelido da minha conta" maxlength="50" />
                            </div>




                            <div class="col-6">
                                <div class="alert alert-criarconta"></div>
                            </div>

                            <div class="col-12 col-md-6 text-right">
                                <button type="submit" class="btn-geral2 pequeno btn-send-criarconta">Salvar esta conta</button>
                            </div>
                        </div>
                    </form>
                </div>



                <div class="box-dashboard collapse show" id="contasalva">
                    <div class="titulo-form"><span>Minhas contas cadastradas</span></div>
                    <div class="form-row listconta">
                        <div class="grid-contas-titulo">
                            <div class="row">
                                <div class="col-1"></div>
                                <div class="col-4 col-md-3">
                                    <label>Banco</label>
                                </div>
                                <div class="col-2">
                                    <label>Tipo</label>

                                </div>
                                <div class="col-2">
                                    <label>Agência</label>

                                </div>
                                <div class="col-2 col-de-3">
                                    <label>Conta</label>
                                </div>
                                <div class="col-1"></div>
                            </div>
                        </div>

                        <div class="grid-contas" style="width: 100%">
                        </div>

                        <div class="col-6">
                            <div class="alert alert-gridcontas"></div>
                        </div>
                    </div>


                    <div class="box-vincular-contas" style="width: 100%">
                        <div class="titulo-form"><span>Vincular hotel à conta selecionada</span></div>
                        <div class="form-row">


                            <div class="grid-hoteis" style="width: 100%">
                            </div>


                            <div class="col-6">
                                <div class="alert alert-vincularcontas"></div>
                            </div>
                            <div class="col-12 col-md-6 text-right">
                                <button type="submit" class="btn-geral2 pequeno btn-send-vincular">aplicar alterações</button>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>

    </div>


    <!-- Modal Mensagem-->
    <!--===================================================-->
    <div class="modal" id="modal-mensagem" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">

                <!--Modal header-->
                <div class="modal-header" style="display: block;">
                    <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
                    <h4 class="modal-title"><i class="fa fa-exclamation-triangle" style="color: red;"></i>Atenção</h4>
                </div>


                <!--Modal body-->
                <div class="modal-body">
                    <p>
                        Ao cadastrar uma nova conta, você afirma que a mesma é de sua titularidade, sob o documento de número <span class="doc-cpf"></span>, e que possíveis erros no cadastro são de sua responsabilidade.
                    </p>
                    <br>
                    <p>
                        Você também informa estar ciente de que, caso a conta cadastrada não seja de sua titularidade, as distribuições direcionadas a ela não serão efetivadas, sendo necessário que você informe uma nova conta para recebimento dos respectivos valores.

                    </p>
                    <br>
                    <p class="text-semibold text-main">
                        Você deseja cadastrar a seguinte conta?<br />
                        <span class="doc-conta"></span>
                    </p>

                </div>


                <!--Modal footer-->
                <div class="modal-footer">
                    <button class="btn btn-primary btn-aceito-criarconta">ACEITO</button>
                    <button data-dismiss="modal" class="btn btn-default" type="button">CANCELAR</button>

                </div>
            </div>
        </div>
    </div>
    <!--===================================================-->
    <!--End Modal Mensagem-->

    <!-- Modal Mensagem Vincular-->
    <!--===================================================-->
    <div class="modal" id="modal-mensagem-vincular" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">

                <!--Modal header-->
                <div class="modal-header" style="display: block;">
                    <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
                    <h4 class="modal-title"><i class="fa fa-exclamation-triangle" style="color: red;"></i>Atenção</h4>
                </div>

                <!--Modal body-->
                <div class="modal-body">
                    <p>
                        Você está alterando a conta de recebimento das distribuições do(s) seguinte(s) hotel(is):
                        <br />
                        <span class="doc-hoteis"></span>
                    </p>
                    <br>
                    
                    <p class="text-semibold text-main">
                        A nova conta é:<br />
                        <span class="doc-conta"></span>
                    </p>
                    <p>
                        Ao confirmar, você concorda em receber suas respectivas distribuições na nova conta <strong>a partir de <span class="doc-data"></span></strong>.
                    </p>
                    <br>
                </div>


                <!--Modal footer-->
                <div class="modal-footer">
                    <button class="btn btn-primary btn-aceito-vincular">ACEITO</button>
                    <button data-dismiss="modal" class="btn btn-default" type="button">CANCELAR</button>

                </div>
            </div>
        </div>
    </div>
    <!--===================================================-->
    <!--End Modal Mensagem Vincular-->

    <!-- Modal Mensagem Vincular Alteracao-->
    <!--===================================================-->
    <div class="modal" id="modal-mensagem-vincular-a" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">

                <!--Modal header-->
                <div class="modal-header" style="display: block;">
                    <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
                    <h4 class="modal-title"><i class="fa fa-exclamation-triangle" style="color: red;"></i>Atenção</h4>
                </div>

                <!--Modal body-->
                <div class="modal-body">
                    <p>
                        Você está alterando a conta de recebimento das distribuições do hotel:
                        <br />
                        <span class="doc-hoteis"></span>
                    </p>
                    <br>
                    
                    <p class="text-semibold text-main">
                        A nova conta é:<br />
                        <span class="doc-conta"></span>
                    </p>
                    <p>
                        Ao confirmar, você concorda em receber suas respectivas distribuições na nova conta <strong>a partir de <span class="doc-data"></span></strong>.
                    </p>
                    <br>
                </div>


                <!--Modal footer-->
                <div class="modal-footer">
                    <button class="btn btn-primary btn-aceito-vincular-a">ACEITO</button>
                    <button data-dismiss="modal" class="btn btn-default" type="button">CANCELAR</button>
                    <input type="hidden" id="hIDContaVinculada" value="" />
                </div>
            </div>
        </div>
    </div>
    <!--===================================================-->
    <!--End Modal Mensagem Vincular Alteracao-->

    <input type="hidden" id="hcpf" value="<%=FrameWork.Util.GetString(Session["_uiCPF"]) %>" />


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="Server">
    <link href="content/css/select2.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="content/js/dadosbancarios.js"></script>

    <!--Magic Checkbox [ OPTIONAL ]-->
    <link href="content/plugins/magic-check/css/magic-check.css" rel="stylesheet">
</asp:Content>

