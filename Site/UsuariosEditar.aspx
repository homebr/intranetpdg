﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="UsuariosEditar.aspx.cs" Inherits="Ferramentas_UsuariosEditar" %>

<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Bem-vindo ao Portal do Conselho</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />


    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/bootstrap-theme.min.css">
    <link href="/skins/all.css?v=1.0.2" rel="stylesheet">
    <link href="/skins/flat/yellow.css" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet">

    <script src="/js/jquery-1.12.4.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/skins/icheck.js?v=1.0.2"></script>
<%--    <script>
        $(document).ready(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_flat-yellow',
                radioClass: 'iradio_flat-yellow'
            });
        });
    </script>--%>
    <style>
        .myBtn {
  display: Block;
  position: fixed;
  bottom: 50px;
  right: 20px;
  z-index: 999999;
  border: none;
  outline: none;
  background-color: #74a838;
  color: black;
  cursor: pointer;
  padding: 15px;
  border-radius: 10px;
}
    </style>
    <link href="/css/style.css" rel="stylesheet"/>
</head>
<body>

    <script type="text/javascript">
        function consistir(sender, arg) {
            with (document.forms[0]) {
                if (txtSenha.value == "") {
                    alert("Senha Atual é campo obrigatório!");
                    (txtSenha.focus());
                    arg.IsValid = false;
                }

                aux = 0;
                xnova = txtNova.value;
                xredigite = txtRedigite.value;
                if (aux == 0) {
                    if (xnova == "") {
                        aux = 1;
                        alert("Preencha o campo Nova Senha");
                        txtNova.focus();
                        arg.IsValid = false;
                    }
                }
                if (aux == 0) {
                    if (xnova != xredigite) {
                        aux = 1;
                        alert("Campo Nova Senha e Redigite precisam ser iguais!");
                        txtNova.focus();
                        arg.IsValid = false;
                    }
                }
            }
        }
        function ImprimeUsuario(usuario) { //v2.0
            //theURL = 'http://200.204.154.59/Relatorios.aspx?rel='+Relatorio+'&Titulo='+Titulo+'&Parametros='+Parametros;
            theURL = '../GeraImpressao.aspx?hUsu=' + usuario;
            winName = "Imob";
            Tipo = '';
            if (Tipo == 'Vertical') {
                features = "scrollbars=yes,width=1000,height=650,top=5,left=10";
            } else {
                features = "scrollbars=yes,width=790,height=650,top=5,left=10";
            }
            window.open(theURL, winName, features);
        }

        function VerificarGrauDaSenha(senha, user) {
            var points = 0;
            var descr = new Array();
            descr[0] = "Muito fraco";
            descr[1] = "Fraco";
            descr[2] = "Razoável";
            descr[3] = "Médio";
            descr[4] = "Forte";
            descr[5] = "Muito forte";

            // grau se o temanho é maior que 4 caracteres, aumenta 1 ponto        
            if (senha.length > 4) points++;

            // grau se a senha contém caracteres maiúsculos e minúsculos, aumenta 1 pto        
            if ((senha.match(/[a-z]/)) && (senha.match(/[A-Z]/))) points++;

            // grau se a senha tem algum número, aumenta 1 ponto        
            if (senha.match(/\d+/)) points++;

            // grau se a senha tem algum caractere especial, aumenta 1 ponto        
            if (senha.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)) points++;

            // grau se o tamanho da senha for maior que 12, aumenta 1 ponto        
            if (senha.length > 12) points++;

            if (senha == user || senha == "102030" || senha == "12345" || senha == "123456") points = 0;

            if (points > 0 && (senha.indexOf(user) > -1 || senha.indexOf("102030") > -1 || senha.indexOf("12345") > -1)) points--;


            // muda descrição e a classe do div conforme o grau da senha        
            document.getElementById("descricaoGrau").innerHTML = descr[points];
            document.getElementById("grauSenha").className = "grau" + points;


            //    if (points<=0)
            //        document.getElementById("DataControl_ButtonSave").style.display = "none";   
            //    else
            //        document.getElementById("DataControl_ButtonSave").style.display = "block";
            //    
            //    

        }

        //-->
    </script>
    <style type="text/css">
        #dificuldade {
            float: left;
        }

        #descricaoGrau {
            font-size: 12px;
        }

        #grauSenha {
            height: 10px;
        }

        #bordaSenha {
            width: 144px;
            height: 10px;
            border: 1px solid black;
        }

        .grau0 {
            width: 144px;
            background: #ff0000;
        }

        .grau1 {
            width: 40px;
            background: #ff5f5f;
        }

        .grau2 {
            width: 60px;
            background: #cccccc;
        }

        .grau3 {
            width: 80px;
            background: #56e500;
        }

        .grau4 {
            background: #4dcd00;
            width: 100px;
        }

        .grau5 {
            background: #399800;
            width: 144px;
        }
    </style>


    <!--Page Title-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <div id="page-title">
        <h1 class="page-header text-overflow">&nbsp;Usuários</h1>

    </div>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End page title-->

    <!--Breadcrumb-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
  <%--  <ol class="breadcrumb">
        <li><a href="/">Home</a></li>
        <li class="active">Usuários</li>
        <li>Editar</li>
    </ol>--%>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End breadcrumb-->
       <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">
                <img src="/img/logo.JPG" width="180" alt="PDG Portal Conselho" /></a>
        </div>






        <div class="navbar-collapse navbar-right collapse">
            <ul class="nav navbar-left top-nav">
             <%--   <p class="obs right">Observações do upload</p>

                <p class="obs right">
                    O tamanho máximo de arquivo para uploads é de 5000 KB.<br>
                    Somente arquivos (PDF, JPG) são permitidos.
                           
                </p>--%>
            </ul>
            <ul class="nav navbar-right top-nav">
                <% 
                    if (FrameWork.Util.GetInt(Session["qtdContratos"]) > 0)
                    { %>
                <li class="dropdown">
                    <a title="Meus Contratos" data-toggle="tooltip" data-placement="bottom" href="/?c=1" class="dropdown-toggle"><i class="fa fa-building-o" aria-hidden="true"></i></a>
                </li>
                <% } %>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-bell"></i><b class="caret"></b></a>
                    <ul class="dropdown-menu message-dropdown">
                        <% if (FrameWork.Util.GetString(Session["_uiAlertas"]).Length > 1)
                            {
                                Response.Write(FrameWork.Util.GetString(Session["_uiAlertas"]));
                            }
                            else
                            {    %>

                        <li class="message-footer"><a href="#">Nenhum alerta!</a></li>
                        <% } %>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-user" aria-hidden="true"></i>
                        <b class="caret"></b></a>

                    <ul class="dropdown-menu perfil">
                        <h6>Bem-vindo</h6>
                        <h3>
                            <asp:Literal ID="lblNomeUsuario" runat="server" Text=""></asp:Literal></h3>
                    </ul>
                </li>
                <li class="dropdown">
                    <a title="Sair" data-toggle="tooltip" data-placement="bottom" href="/login" class="dropdown-toggle" id="exit"><i class="fa fa-power-off" aria-hidden="true"></i></a>
                </li>
            </ul>
        </div>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
        </div>
        <!-- /.navbar-collapse -->
    </nav>

    <form id="form2" runat="server">


        <asp:Label ID="lblMensagem" runat="server" CssClass="box-danger alert alert-danger" Style="display: block" Visible="false"></asp:Label>

        <div id="page-content">

            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <asp:Label ID="lblNomeImovel" runat="server" Text=""></asp:Label></h3>
                </div>
                <div class="panel-body">


                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>LoginAcesso: *</label>
                            <asp:TextBox ID="txtLoginAcesso" CssClass="form-control" runat="server" Width="100%" MaxLength="20"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>Senha: *</label>
                            <asp:TextBox ID="txtSenha" CssClass="form-control" TextMode="Password" onkeyup="VerificarGrauDaSenha(this.value,document.getElementById('_ctl0_CenterContent_txtSenha').value);" runat="server" Width="100%" MaxLength="20"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>Nome: *</label>
                            <asp:TextBox ID="txtNome" CssClass="form-control" runat="server" Width="100%" MaxLength="50"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>E-mail:</label>

                                <asp:TextBox ID="txtEmail" CssClass="form-control" runat="server" Width="100%" MaxLength="250"></asp:TextBox></td>

                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2">
                                <label for="ContentPlaceHolder1_chkAtivo">Ativo:</label>
                                <div>
                                    <div class="make-switch switch-small" data-on="success" data-off="danger" data-on-label="<i class='fa fa-check icon-white'></i>" data-off-label="<i class='fa fa-times'></i>">
                                        <asp:CheckBox ID="chkAtivo" runat="server" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-8 no-admin">
                                <label>Grupo:</label>
                                <asp:DropDownList ID="ddlGrupo" CssClass="form-control" runat="server" Width="100%">
                                    <asp:ListItem Value="PA">Conselho ADM</asp:ListItem>
                                    <asp:ListItem Value="PF">Conselho Fiscal</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>

                     
                        <div class="col-sm-12">
                            <div class="separator"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4">
                                <label for="ContentPlaceHolder1_lblDataInclusao">Data Inclusão:</label>
                                <asp:Label ID="lblDataInclusao" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="col-sm-4">
                                <label for="ContentPlaceHolder1_lblDataAlteracao">Data Alteração:</label>
                                <asp:Label ID="lblDataAlteracao" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="col-sm-4">
                                <label for="ContentPlaceHolder1_lblUsuario">Usuário:</label>
                                <asp:Label ID="lblUsuario" runat="server" Text=""></asp:Label>
                            </div>
                        </div>


                    </div>


                </div>
                <div class="panel-footer text-right">
                    <asp:Button ID="btnSalvar" runat="server" Text="SALVAR" CssClass="btn btn-success fa fa-check-circle" OnClick="btnSalvar_Click" />
                </div>
            </div>





            <asp:HiddenField ID="hOperacao" runat="server" />
            <asp:HiddenField ID="hID" runat="server" />
            <asp:HiddenField ID="hUrlReferrer" runat="server" />
            <asp:HiddenField ID="hLogin" runat="server" />
            <asp:HiddenField ID="hSenha" runat="server" />

            <asp:Literal ID="Literal1" runat="server"></asp:Literal>

  




      </div>
    </form>

    <div class="footer-tools">
        <span class="go-top">
            <i class="fa fa-chevron-up"></i>Top
        </span>
    </div>



   <footer class=" no-print">
        By Homebrasil 
   
    </footer>
    </div>


    
   



    <script src="/js/geral.js"></script>




    <link rel="stylesheet" href="/css/jquery.fileupload.css">
    <script type="text/jscript" src="/js/jquery.maskedinput-1.3.1.js"></script>



        $(document).ready(function () {
            var campo = '<%=hID.ClientID.ToString().Replace("hID","")%>';

            if ($("#" + campo + "chkAdministrador").attr("checked") == "checked")
                $(".no-admin").hide();



            $(".class-administrador").click(function () {
                if ($(this).children().hasClass("switch-on"))
                    $(".no-admin").hide();
                else
                    $(".no-admin").show();
            });

        });

    </script>



