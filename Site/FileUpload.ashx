﻿<%@ WebHandler Language="C#" Class="FileUpload" %>

using System;
using System.Web;
using System.IO;
using System.Web.SessionState;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public class FileUpload : IHttpHandler, IRequiresSessionState  {

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/json";
        System.Globalization.CultureInfo culture =  new System.Globalization.CultureInfo("pt-BR");

        string json = "";
        string dirFullPath = System.Configuration.ConfigurationManager.AppSettings["pathFisico"] + "uploads\\comprovante-endereco\\";
        string[] files;
        int numFiles;

        String id = FrameWork.Util.GetString(context.Request["id"]);

        String nome = FrameWork.Util.GetString(context.Request["ctl00$content$txtNome"]);
        String endereco = FrameWork.Util.GetString(context.Request["ctl00$content$txtEndereco"]);
        String numero = FrameWork.Util.GetString(context.Request["ctl00$content$txtNumero"]);
        String complemento = FrameWork.Util.GetString(context.Request["ctl00$content$txtComplemento"]);
        String estado = FrameWork.Util.GetString(context.Request["ctl00$content$txtEstado"]);
        String cidade = FrameWork.Util.GetString(context.Request["ctl00$content$txtCidade"]);
        String bairro = FrameWork.Util.GetString(context.Request["ctl00$content$txtBairro"]);
        String cep = FrameWork.Util.GetString(context.Request["ctl00$content$txtCep"]);

        //apenas para debug
        //json = "{ \"response\": \"error\",\"message\": \"" + endereco + "\",\"arquivo\": \"" + endereco + "\" }";
        //context.Response.Write(json);
        //return;

        files = System.IO.Directory.GetFiles(dirFullPath); // + id + "*"
        numFiles = files.Length;
        numFiles = numFiles + 1;


        string str_image = "";



        foreach (string s in context.Request.Files)
        {
            HttpPostedFile file = context.Request.Files[s];
            //  int fileSizeInBytes = file.ContentLength;
            string fileName = file.FileName;
            string fileExtension = Path.GetExtension(fileName); // file.ContentType.ToLower();
            string extensao = fileExtension.ToLower();

            if (!(extensao == ".pdf" || extensao == ".jpg" || extensao == ".gif" || extensao == ".png"))
            {
                json = "{ \"response\": \"error\",\"message\": \"Extensão (" + extensao + ") do arquivo inválida!\" }";
                context.Response.Write(json);
                return;
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                //fileExtension = Path.GetExtension(fileName);
                str_image = "comprovante_" + id + "_" + numFiles.ToString() + extensao;
                string pathToSave_100 = dirFullPath + str_image;


                file.SaveAs(pathToSave_100);

                ClassLibrary.Comprovantes comp = new ClassLibrary.Comprovantes();
                comp.CPF = id;
                comp.Posicao = numFiles;
                comp.Arquivo = str_image;
                comp.Status = "P";
                comp.Endereco = endereco;
                comp.Numero = numero;
                comp.Complemento = complemento;
                comp.Bairro = bairro;
                comp.Cidade = cidade;
                comp.Estado = estado;
                comp.Cep = cep;
                comp.Save();

                //string strSql = "insert into Comprovantes (CPF,Posicao,Arquivo,Status ";
                //strSql += ") values ('" + id + "'," + numFiles.ToString() + ",'" + str_image.ToString() + "','P')";
                //FrameWork.DataObject.DataSource.DefaulDataSource.Execute(strSql);

                    //atualizacaocadastral@pdg.com.br 
                    //ModeloEnderecoAtualizado.html

                    try
                    {

                        System.IO.StreamReader objStreamReader;
                        String strResultadoEnvio = "";

                        string strAssunto = "Portal do Cliente - Alteração de Endereço";

                        string FilePath = context.Server.MapPath("template_email/ModeloEnderecoAtualizado.htm");
                        objStreamReader = System.IO.File.OpenText(FilePath);
                        string strArq = objStreamReader.ReadToEnd();


                        strArq = strArq.Replace("#CPF#", id);
                        strArq = strArq.Replace("#Nome#", nome);
                        strArq = strArq.Replace("#Endereco#", endereco);
                        strArq = strArq.Replace("#Numero#", numero);
                        strArq = strArq.Replace("#Complemento#", complemento);
                        strArq = strArq.Replace("#Bairro#", bairro);
                        strArq = strArq.Replace("#Cidade#", cidade);
                        strArq = strArq.Replace("#Estado#", estado);
                        strArq = strArq.Replace("#Cep#", cep);

                        string pathVirtual = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"];

                        strArq = strArq.Replace("#Comprovante#", "<a href='" + pathVirtual + "uploads/comprovante-endereco/" + str_image + "' target='_blank'>Visualizar</a> ");

                        strArq = strArq.Replace("#IP#", context.Request.ServerVariables["REMOTE_ADDR"]);
                        strArq = strArq.Replace("#DATACADASTRO#", String.Format("{0:dd/MM/yyyy hh:mm}", DateTime.Now));

                        strArq = strArq.Replace("src=\"img/", "src=\"" + pathVirtual + "template_email/img/");

                        strResultadoEnvio = FrameWork.Util.GetString(Funcoes.EnvioEmail(strArq, "interacoes@pdg.com.br", strAssunto, "atualizacaocadastral@pdg.com.br", "oliveira@homebrasil.com", ""));

                        objStreamReader.Dispose();
                        objStreamReader.Close();

                        return;


                    }
                    catch
                    {

                    }


                json = "{ \"response\": \"ok\",\"message\": \"Comprovante salvo com sucesso!\" }";

            }
            else
            {

                json = "{ \"response\": \"error\",\"message\": \"Arquivo inválido!\",\"arquivo\": \"" + str_image + "\" }";
            }


        }
        context.Response.Write(json);
    }


    private static RotateFlipType OrientationToFlipType(int orientation)
    {
        switch (orientation)
        {
            case 1:
                return RotateFlipType.RotateNoneFlipNone;
                break;
            case 2:
                return RotateFlipType.RotateNoneFlipX;
                break;
            case 3:
                return RotateFlipType.Rotate180FlipNone;
                break;
            case 4:
                return RotateFlipType.Rotate180FlipX;
                break;
            case 5:
                return RotateFlipType.Rotate90FlipX;
                break;
            case 6:
                return RotateFlipType.Rotate90FlipNone;
                break;
            case 7:
                return RotateFlipType.Rotate270FlipX;
                break;
            case 8:
                return RotateFlipType.Rotate270FlipNone;
                break;
            default:
                return RotateFlipType.RotateNoneFlipNone;
        }
    }


    //private System.Drawing.Bitmap rotateImage90(System.Drawing.Bitmap b)
    //{
    //    System.Drawing.Bitmap returnBitmap = new System.Drawing.Bitmap(b.Height, b.Width);
    //    System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(returnBitmap);
    //    g.TranslateTransform((float)b.Width / 2, (float)b.Height / 2);
    //    g.RotateTransform(90);
    //    g.TranslateTransform(-(float)b.Width / 2, -(float)b.Height / 2);
    //    g.DrawImage(b, new System.Drawing.Point(0, 0));
    //    return returnBitmap;
    //}


    public bool IsReusable {
        get {
            return false;
        }
    }



}