﻿<%@ Page Title="" Language="C#" MasterPageFile="/MasterPage.master" AutoEventWireup="true" CodeFile="documentos.aspx.cs" Inherits="_Documentos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">



        <!--Specify page [ SAMPLE ]-->
        <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="Server">

    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">



            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>Lista de Documentos
            <small></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="default"><i class="fa fa-dashboard"></i>Home</a></li>
                        <li><a href="documentos">Lista de Documentos</a></li>
                        <%--<li class="active">Data tables</li>--%>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">


                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Lista de Documentos</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th></th>
                                              
                                                <th>Setor</th>
                                                  <th>Descrição</th>
                                               <%-- <th>Ramal(s)</th>
                                                <th>Email</th>--%>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           

                                                        <asp:Repeater ID="rptDocs" runat="server">
                                                            <ItemTemplate>
                                                                 <tr>
                                                                <td>
                                                                    <div class="product-img">
                                                                        <i class="fa fa-file-<%# DataBinder.Eval(Container, "DataItem.setor")%>-o arquivo"></i>
                                                                    </div>
                                                                    </td>
                                                                     <td>
                                                                    <div class="product-info">
                                                                   <a href="<%# DataBinder.Eval(Container, "DataItem.Link")%>" class="product-title"><%# DataBinder.Eval(Container, "DataItem.setorempresa")%><span class="label  pull-right">
                                                                       
                                                                    </div>
                                                                    </td>
                                                                <td>
                                                                    <div class="product-info">
                                                                        <a href="<%# DataBinder.Eval(Container, "DataItem.Link")%>" class="product-title"><%# DataBinder.Eval(Container, "DataItem.Descricao")%><span class="label  pull-right">
                                                                            <button class="btn btn-block btn-primary">Download</button></span></a>
                                                                        <span class="product-description"></span>
                                                                    </div>
                                                                    </td>
                                                                </tr>
                                                                <!-- /.item -->
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                  




                                        </tbody>
                                        <tfoot>
                            
                                        </tfoot>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->


            <div class="control-sidebar-bg"></div>
        </div>
        









</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
<!-- ./wrapper -->

        <!-- jQuery 2.1.4 -->
        <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
        <!-- Bootstrap 3.3.5 -->
        <script src="bootstrap/js/bootstrap.min.js"></script>

        <!-- DataTables -->
        <script src="plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
        <!-- SlimScroll -->
        <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="plugins/fastclick/fastclick.min.js"></script>
        <!-- AdminLTE App -->
        <script src="dist/js/app.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="dist/js/demo.js"></script>
        <!-- page script -->
       <script>
           $(function () {
               $("#example2").DataTable();
               $('#example3').DataTable({
                   "paging": true,
                   "lengthChange": false,
                   "searching": false,
                   "ordering": true,
                   "info": true,
                   "autoWidth": false
               });


               $("#example1").dataTable({
                   "bJQueryUI": true,
                   "order": [[1, "desc"]],
                   "oLanguage": {
                       "sProcessing": "Processando...",
                       "sLengthMenu": "Mostrar _MENU_ registros",
                       "sZeroRecords": "Não foram encontrados resultados",
                       "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                       "sInfoEmpty": "Mostrando de 0 até 0 de 0 registros",
                       "sInfoFiltered": "",
                       "sInfoPostFix": "",
                       "sSearch": "Buscar:",
                       "sUrl": "",
                       "oPaginate": {
                           "sFirst": "Primeiro",
                           "sPrevious": "Anterior",
                           "sNext": "Seguinte",
                           "sLast": "Último"
                       }
                   }
               })
           });
       </script>
    </body>
</asp:Content>
