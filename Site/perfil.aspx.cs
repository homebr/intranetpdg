﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing.Imaging;
using System.Drawing;

public partial class _Perfil : System.Web.UI.Page
{
    public ClassLibrary.Colaboradores objCliente = new ClassLibrary.Colaboradores();
    //caminho fisico no server
    string pathFisico = System.Configuration.ConfigurationManager.AppSettings["pathFisico"] + "uploads\\colaboradores\\";
    string pathVirtual = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "uploads/colaboradores/";

    public string[] sAux = new string[8];
    public string[] sAuxOrigens = new string[8];
    public String sLabels = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            
         
            if (FrameWork.Util.GetString(Session["_uiUserID"]) == "")
            {

                Response.Redirect("/login", true);
            }


            try
            {
                String strUsuario = FrameWork.Util.GetString(Session["_uiCPF"]);
                //String strSenha = FrameWork.Util.GetString(Session["_uiSenha"]);

                hID.Value = (FrameWork.Util.GetString(FrameWork.Util.GetString(Session["_uiUserID"])));
                hCPF_Cliente.Value = (strUsuario);


                //objCliente.Load(FrameWork.Util.GetString(Session["_uiCodCliente"]));

                objCliente.Load(FrameWork.Util.GetString(Session["_uiUserID"]));

                if (objCliente.TotalLoad == 1)
                {

                    txtNome.Text = objCliente.Nome;
                    txtEmail.Text = objCliente.Email;
                    txtcargo1.Text = objCliente.Cargo;
                   txtarea.Text = FrameWork.Util.GetString(objCliente.Setor);
                    txtRegional.Text = FrameWork.Util.GetString(objCliente.Regional);
                    txtDescricao.Text = objCliente.Descricao;
                    //txtCPF.Text = objCliente.Cpf;

                    txtRamal.Text = objCliente.Ramal;
                    this.txtaniversario.Text = String.Format("{0:dd/MM/yyyy}", objCliente.DataAniversario);
                    

                    if (FrameWork.Util.GetString(objCliente.Foto).Length > 3)
                         hFoto.Value = "uploads/colaboradores/" + objCliente.Foto + "?t=" + String.Format("{0:ddMMyyyyHHmm}", DateTime.Now);
                    imagePreview.Style.Add("background-image",   hFoto.Value  );
                }

            }
            catch (Exception ex)
            {

                Msg(ex.Message, false);
                return;
            }


        }
    }



    protected void btnAlterarDados_Click(object sender, EventArgs e)
    {

        try
        {
            String strUsuario = FrameWork.Util.GetString(Session["_uiCPF"]);
            String sEmail = FrameWork.Util.GetString(txtEmail.Text);

           String sCargo = FrameWork.Util.GetString(txtcargo1.Text);
            String sRamal = FrameWork.Util.GetString(txtRamal.Text);
            //String sCelular = FrameWork.Util.GetString(txtCelular.Text);

            //objCliente.Load(FrameWork.Util.GetString(Session["_uiCodCliente"]));

            objCliente.Load(FrameWork.Util.GetString(Session["_uiUserID"]));

            objCliente.Ramal = FrameWork.Util.GetString(txtRamal.Text);
            objCliente.Cargo = FrameWork.Util.GetString(txtcargo1.Text);
            objCliente.Descricao = FrameWork.Util.GetString(txtDescricao.Text);

            if (Funcoes.IsDate(txtaniversario.Text))
                objCliente.DataAniversario = Convert.ToDateTime(this.txtaniversario.Text);
            //objCliente.Nome = FrameWork.Util.GetString(txtNome.Text);
            objCliente.DataAlteracao = DateTime.Now;
            objCliente.Email = sEmail;
       
            objCliente.Save();

            if (!String.IsNullOrEmpty(txtEmail.Text))
            {
                //ENVIO AO CLIENTE
                System.IO.StreamReader objStreamReader;
                String strResultadoEnvio = "";
                string strAssunto = "Portal do Cliente - Atualização Cadastral";
                string FilePath = Server.MapPath("template_email/ModeloAtualizacaoCadastral.htm");
                objStreamReader = System.IO.File.OpenText(FilePath);
                string strArq = objStreamReader.ReadToEnd();
                strArq = strArq.Replace("#NOME#", txtNome.Text);
                strArq = strArq.Replace("#IP#", Request.ServerVariables["REMOTE_ADDR"]);
                strArq = strArq.Replace("#DATA#", String.Format("{0:dd/MM/yyyy hh:mm}", DateTime.Now));
                string pathVirtual = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"];
                strArq = strArq.Replace("src=\"img/", "src=\"" + (pathVirtual + "template_email/") + "img/");
                strArq = strArq.Replace("#PathVirtual#", pathVirtual);

                string strTo = "naoresponda@homebrasil.com";
                string strPara = "fernando@homebrasil.com"; // strEmailRecupera;
                string strCc = "";

                strResultadoEnvio = FrameWork.Util.GetString(Funcoes.EnvioEmail(strArq, strTo, strAssunto, strPara, strCc, ""));
                objStreamReader.Dispose();
                objStreamReader.Close();


                Msg("Suas informações forammmmm atualizadas com sucesso!", true);
            }
        }
        catch (Exception ex)
        {

            Msg(ex.Message, false);
            return;
        }
    }

    protected void btnAlterarSenha_Click(object sender, EventArgs e)
    {
        String senha = FrameWork.Util.GetString(txtSenhaAtual.Text);
        String nova = FrameWork.Util.GetString(txtNovaSenha.Text);
        String redigite = FrameWork.Util.GetString(txtRepetirSenha.Text);


        if (senha.Length == 0)
        {
            MsgSenha("Ops, o campo <strong>senha atual</strong> é campo obrigatório!", false);
            return;
        }
        else if (nova.Length < 6)
        {
            MsgSenha("Ops, o campo <strong>nova senha</strong> deverá ter no mínimo 6 caracteres!", false);
            return;
        }
        else if (nova != redigite)
        {
            MsgSenha("Ops, as senhas que você digitou não são iguais. Verifique sua <strong>nova senha</strong> e tente novamente.", false);
            return;
        }

        String strUsuario = FrameWork.Util.GetString(Session["_uiCPF"]);
       
        //objCliente.Load(FrameWork.Util.GetString(Session["_uiCodCliente"]));
        objCliente.Load(FrameWork.Util.GetString(Session["_uiUserID"]));
        try
        {
            if (objCliente.TotalLoad == 1)
            {


                //atualiza a senha no banco de dados
                //if (objCliente.Senha == "" || (FrameWork.Util.GetString(objCliente.Senha).Length > 1 && FrameWork.Security.DeCript(objCliente.Senha) == senha))
                    if (objCliente.Senha == "" || (FrameWork.Util.GetString(objCliente.Senha).Length > 1 ))
                {
                    objCliente.Senha = FrameWork.Security.Cript(nova);
                  //  objCliente.AlteracaoSenha = DateTime.Now;
                    objCliente.Save();

                    txtSenhaAtual.Text = "";
                    txtNovaSenha.Text = "";
                    txtRepetirSenha.Text = "";

                    MsgSenha("Sua senha foi atualizada com sucesso!", true);
                }
                else
                    MsgSenha("A sua <strong>senha atual</strong> está incorreta. Por favor, tente novamente.", false);


            }
            else
                MsgSenha("Erro ao salvar, verifique os dados e tente novamente!", false);
        }
        catch
        {
            MsgSenha("Erro ao salvar, tente novamente ou abra um chamado reportando o problema!", false);
        }

    }

    protected void btnAlterarFoto_Click(object sender, EventArgs e)
    {
        string fileExtension = imageUpload.PostedFile.ContentType.ToLower();

        if ((imageUpload.HasFile))
        {
            //Response.Write("passo 2");

            string arq = imageUpload.PostedFile.FileName;

            //verificamos a extensão através dos últimos 4 caracteres e verificamos se é permitido
            string extensao = arq.Substring(arq.Length - 4).ToLower();
            //Response.Write(extensao);
            //Response.End();
            if ((!(extensao == ".gif" || extensao == ".jpg" || extensao == ".png")))
            {
                Msg("Extensão de arquivo não permitido! Somente é permitido extensão: gif, jpg ou png!", false);
                return;
            }
            //string nomeArquivo = "foto-" + (hCPF_Cliente.Value).Replace(".", "").Replace("-", "").Replace("/", "") + "-" + String.Format("{0:ddMMyyyyHHmm}", DateTime.Now) + extensao;
            string nomeArquivo = "colaborador-" + hID.Value + extensao;
            // Save the posted file in our "data" virtual directory.

            //System.Drawing.Image image2;
            ////Response.Write("Width = " + image.Size.Width + " - Height = " + image.Size.Height);
            //if (imageUpload.Size.Width < 1000)
            //{
            //    double perc = 0.2792;
            //    image2 = ResizeImage(imageUpload, imageUpload.Size.Width, Convert.ToInt32(Convert.ToDouble(image.Size.Width) * perc));
            //    Response.Write("resize = " + imageUpload.Size.Width + " - " + Convert.ToInt32(Convert.ToDouble(image.Size.Width) * perc));
            //}
            //else
            //    image2 = ResizeImage(image, 1000, 700);
            imageUpload.SaveAs(pathFisico + nomeArquivo);

            objCliente.Load(FrameWork.Util.GetString(Session["_uiUserID"]));
            if (objCliente.TotalLoad == 1)
            {
                objCliente.Foto = nomeArquivo;
                objCliente.Save();
            }

            hFoto.Value = "uploads/colaboradores/" + nomeArquivo;
            Session["_uiFoto"] = "/uploads/colaboradores/" + nomeArquivo;
            imageUpload.Dispose();


        }


    }
    public static Bitmap ResizeImage(System.Drawing.Image image, int width, int height)
    {
        var destRect = new Rectangle(0, 0, width, height);
        var destImage = new Bitmap(width, height);

        destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

        using (var graphics = Graphics.FromImage(destImage))
        {
            graphics.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;
            graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            graphics.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;

            using (var wrapMode = new System.Drawing.Imaging.ImageAttributes())
            {
                wrapMode.SetWrapMode(System.Drawing.Drawing2D.WrapMode.TileFlipXY);
                graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
            }
        }

        return destImage;
    }


    protected void Msg(String msg, Boolean tipo)
    {
        lblMensagem.CssClass = (tipo ? "box-danger alert alert-success" : "box-danger alert alert-danger");
        lblMensagem.Text = msg;
        lblMensagem.Visible = true;
    }

    protected void MsgSenha(String msg, Boolean tipo)
    {
        lblMensagemSenha.CssClass = (tipo ? "box-danger alert alert-success" : "box-danger alert alert-danger");
        lblMensagemSenha.Text = msg;
        lblMensagemSenha.Visible = true;
    }

}