﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="faq.aspx.cs" Inherits="faq" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script>
        $(document).ready(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_flat-yellow',
                radioClass: 'iradio_flat-yellow'
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="Server">
    <div class="container">
        	<h2 class="titulo-geral">
                    <asp:Literal ID="ltlTitulo" runat="server"></asp:Literal><span></span></h2>
                <p><asp:Literal ID="ltlConteudo" runat="server"></asp:Literal>
                </p>


        <div class="menu-faq">
            <asp:HyperLink ID="btnFinanciamento" CssClass="col-xs-2 col-xs-15 item-faq i-financiamento" NavigateUrl="/faq/financiamento" runat="server"><span class="arrow-down "></span></asp:HyperLink>
            <asp:HyperLink ID="btnPagamentos" CssClass="col-xs-2 col-xs-15 item-faq i-pagamentos" NavigateUrl="/faq/pagamentos" runat="server"><span class="arrow-down "></span></asp:HyperLink>
            <asp:HyperLink ID="btnContratos" CssClass="col-xs-2 col-xs-15 item-faq i-contratos" NavigateUrl="/faq/contratos" runat="server"><span class="arrow-down "></span></asp:HyperLink>
            <asp:HyperLink ID="btnObras" CssClass="col-xs-2 col-xs-15 item-faq i-obras" NavigateUrl="/faq/obras" runat="server"><span class="arrow-down "></span></asp:HyperLink>
            <asp:HyperLink ID="btnAssistenciaTecnica" CssClass="col-xs-2 col-xs-15 item-faq i-assitenciatecnica" NavigateUrl="/faq/assitenciatecnica" runat="server"><span class="arrow-down "></span></asp:HyperLink>
        </div>

        <div class="barra-busca">
            <div class="col-xs-12 col-sm-6 col-md-9 padding-none"><span class="divisor"></span></div>
            <div class="col-xs-12 col-sm-6 col-md-3 padding-none">
                <form runat="server" id="form1" class="form-search" style="padding: 0; width: 100%;" defaultbutton="btnBusca" >
                    <asp:TextBox ID="txtPalavra" MaxLength="50" runat="server" CssClass="input-medium search-query campo-busca" placeholder="Palavra-chave"></asp:TextBox>
                    <asp:LinkButton ID="btnBusca" CssClass="btn-busca" runat="server" OnClick="btnBusca_Click"><i class="fa fa-search" aria-hidden="true"></i></asp:LinkButton>
                </form>
            </div>
        </div>

    </div>
    <div class="container">


        <asp:Repeater ID="rptFaq" runat="server">
            <ItemTemplate>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href='<%# "#collapse" + (Container.ItemIndex+1).ToString() %>'><%# (Container.ItemIndex+1).ToString() + ". " + DataBinder.Eval(Container, "DataItem.Titulo") %> <i class="fa fa-plus" aria-hidden="true"></i></a>
                        </h4>
                    </div>
                    <div id='<%# "collapse" + (Container.ItemIndex+1).ToString() %>' class='<%# "panel-collapse collapse" + (Container.ItemIndex == 0?" in":"") %>'>
                        <div class="panel-body">
                            <p>
                                <%# DataBinder.Eval(Container, "DataItem.Texto") %>
                            </p>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>


    </div>


    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="Server">

    <script>
        $(document).ready(function () {
            $(".panel-heading").click(function () {
                if ($(this).find("i").hasClass("fa-plus")) {
                    $(this).find("i").removeClass("fa-plus");
                    $(this).find("i").addClass("fa-minus");
                }
                else {
                    $(this).find("i").addClass("fa-plus");
                    $(this).find("i").removeClass("fa-minus");
                }
            });
        });
    </script>
</asp:Content>

