﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


public partial class MasterPage : System.Web.UI.MasterPage
{

    public ClassLibrary.Colaboradores objCliente = new ClassLibrary.Colaboradores();
    protected void Page_Load(object sender, EventArgs e)
    {
        CarregaMenu();

        if (!this.IsPostBack)
        {


            if (FrameWork.Util.GetString(Session["_uiUserID"]) == "")
            {

                Response.Redirect("/login", true);
            }


            try
            {
                

                //hID.Value = (FrameWork.Util.GetString(FrameWork.Util.GetString(Session["_uiUserID"])));
              


                //objCliente.Load(FrameWork.Util.GetString(Session["_uiCodCliente"]));

                objCliente.Load(FrameWork.Util.GetString(Session["_uiUserID"]));

                if (objCliente.TotalLoad == 1)
                {

                    lblNome.Text = objCliente.Nome;
                    lblnome1.Text = objCliente.Nome;
                    //txtEmail.Text = objCliente.Email;
                    lblcargo.Text = objCliente.Cargo;
                    ////txtCPF.Text = objCliente.Cpf;

                    //txtRamal.Text = objCliente.Ramal;
                    ////txtaniversario.Text = objCliente.DataAniversario;

                    if (FrameWork.Util.GetString(objCliente.Foto).Length > 3) {
                        imgmenu.ImageUrl = "uploads/colaboradores/" + objCliente.Foto + "?t=" + String.Format("{0:ddMMyyyyHHmm}", DateTime.Now);
                    imgtopo.ImageUrl = "uploads/colaboradores/" + objCliente.Foto + "?t=" + String.Format("{0:ddMMyyyyHHmm}", DateTime.Now);
                    imgdescricao.ImageUrl = "uploads/colaboradores/" + objCliente.Foto + "?t=" + String.Format("{0:ddMMyyyyHHmm}", DateTime.Now);
                    }
                    //imagePreview.Style.Add("background-image", hFoto.Value);
                }

            }
            catch (Exception ex)
            {

               // Msg(ex.Message, false);
                return;
            }


        }

    }



    public void CarregaMenu()
    {

        string pathVirtual = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "uploads/colaboradores/";
        String strUsuario = FrameWork.Util.GetString(Session["_uiUser"]);

        string strSql = "SELECT * from formularios WHERE  (TipoFormulario = 'I') AND (Status = 'A')  ";
        //strSql += "and Convert(char(2),dataAniversario,101) = '" + String.Format("{0:MM}", DateTime.Now) + "' ";
        //if (grupo == "4")

        //{
        //    strSql += " and acesso in (0,1)";
        //}
        //if (grupo == "6")

        //{
        //    strSql += " and acesso in (0,2)";
        //}

        strSql += " order by sequencia ";
        //Response.Write(strSql);
        DataTable dt = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSql);
        rptMenu.DataSource = dt;
        rptMenu.DataBind();

        if (dt.Rows.Count > 0)
        {


            //pnlComprovantes.Visible = false;
            // ltlTextoEndereco.Text = "Aguardando validação manualmente na PDG!";

        }





    }
}
