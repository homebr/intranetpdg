﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class dados_bancarios : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {

            if (FrameWork.Util.GetString(Session["_uiCodCliente"]) == "")
            {
                Response.Redirect("/login");
            }
        }
    }
}