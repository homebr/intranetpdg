﻿$(document).ready(function () {
    qtd = $(".listar-notificacoes").html().trim();
    if (qtd == "" || qtd == "-1")
        $(".notificacoes").hide();

});

function lido(id,obj) {
    if (id > 0) {
        data = {
            id: id,
            modulo: "notificacao-lida"
        };

        $.post('/ws-form.ashx', data,
            function (msg) {
                if (msg['response'] == "ok") {
                    $(obj).parent().parent().remove();
                    qtd = Number($(".notificacoes span").text());
                    if (qtd <= 0)
                        qtd = 0;
                    else
                        qtd--;
                    $(".notificacoes span").text(qtd);
                    if (qtd == 0) 
                        $(".notificacoes").hide();
                }
            }
        );
    }
}