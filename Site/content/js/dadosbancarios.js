﻿var arrMonth = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];

$(document).ready(function () {
    $.getJSON('/content/js/bancos.json', function (data) {
        var items = [];
        var options = '<option value="0">Escolha um Banco</option>';
        $.each(data, function (key, val) {
            options += '<option value="' + val.code + '">' + val.code + ' - ' + val.name + '</option>';
        });
        $("#ddlBancos").html(options);

        $(".select-bancos").select2();

    });

    $(".btncontasalva").click(function () {
        $(this).addClass("ativo");
        $(".btnnovaconta").removeClass("ativo");

        CarregaContas();

        $("#novaconta").hide();
        $("#contasalva").show();
    });

    $(".btnnovaconta").click(function () {
        $(this).addClass("ativo");
        $(".btncontasalva").removeClass("ativo");

        $("#novaconta").show();
        $("#contasalva").hide();
    });

    $(".btn-send-criarconta").bind("click", function () {
        $(".alert-criarconta").show();


        var banco = $('#ddlBancos');
        //$("#first").select2('val');
        var tipo = $('#ddlTipo');
        var agencia = $('#txtCB_Agencia');
        var conta = $('#txtCB_Conta');
        var apelido = $('#txtCB_Apelido');

        //console.log("#ddlBancos" + $("#ddlBancos option:selected").val());

        if ($("#ddlBancos option:selected").val() == '0') {
            banco.css('border', '1px solid red');
            banco.focus();
            $(".alert-criarconta").html('Favor selecionar o Banco!');
            $(".alert-criarconta").addClass("alert-danger");
            return false;
        } else {
            banco.css('border', '1px solid #818181');
        }
        if (agencia.val() == '') {
            agencia.css('border', '1px solid red');
            agencia.focus();
            $(".alert-criarconta").html('Favor preencher a Agência!');
            $(".alert-criarconta").addClass("alert-danger");
            return false;
        } else {
            agencia.css('border', '1px solid #818181');
        }
        if (conta.val() == '') {
            conta.css('border', '1px solid red');
            conta.focus();
            $(".alert-criarconta").html('Favor preencher a Conta!');
            $(".alert-criarconta").addClass("alert-danger");
            return false;
        } else {
            conta.css('border', '1px solid #818181');
        }

        cpf = $("#hcpf").val();
        sconta = $("#ddlBancos option:selected").text() + " - " + $("#ddlTipo option:selected").text() + " - " + agencia.val() + " - " + conta.val();
        $("#modal-mensagem .doc-cpf").html(cpf);
        $("#modal-mensagem .doc-conta").html(sconta);
        $("#modal-mensagem").modal("show");


        return false;
    });


    $(".btn-aceito-criarconta").bind("click", function () {
        $(".alert-criarconta").show();
        $(".alert-criarconta").html('Enviando ... ');
        $(".alert-criarconta").addClass("alert-success");

        var banco = $('#ddlBancos');
        var tipo = $('#ddlTipo');
        var agencia = $('#txtCB_Agencia');
        var conta = $('#txtCB_Conta');
        var apelido = $('#txtCB_Apelido');

        if ($("#ddlBancos option:selected").val() == '0') {
            banco.css('border', '1px solid red');
            banco.focus();
            $(".alert-criarconta").html('Favor selecionar o Banco!');
            $(".alert-criarconta").addClass("alert-danger");
            return false;
        } else {
            banco.css('border', '1px solid #818181');
        }
        if (agencia.val() == '') {
            agencia.css('border', '1px solid red');
            agencia.focus();
            $(".alert-criarconta").html('Favor preencher a Agência!');
            $(".alert-criarconta").addClass("alert-danger");
            return false;
        } else {
            agencia.css('border', '1px solid #818181');
        }
        if (conta.val() == '') {
            conta.css('border', '1px solid red');
            conta.focus();
            $(".alert-criarconta").html('Favor preencher a Conta!');
            $(".alert-criarconta").addClass("alert-danger");
            return false;
        } else {
            conta.css('border', '1px solid #818181');
        }

        data = {
            banco: $("#ddlBancos option:selected").text(),
            numerobanco: $("#ddlBancos option:selected").val(),
            tipo: $("#ddlTipo option:selected").val(),
            agencia: agencia.val(),
            conta: conta.val(),
            apelido: apelido.val(),
            modulo: "criarconta"
        };


        $("#modal-mensagem").modal("hide");


        objAlerta = "alert-criarconta";
        $.post('ws-form.ashx', data,
            function (msg) {
                $("." + objAlerta).html(msg['message']);
                if (msg['response'] == "ok") {
                    $("." + objAlerta).removeClass("alert-danger");
                    $("." + objAlerta).addClass("alert-success");

                    //salva log mensagem
                    mensagem = $("#modal-mensagem .modal-body").html();
                    $.post('ws-form.ashx', { modulo: "LogMensagens", titulo: "Alerta de criação de conta", mensagem: mensagem});

                    var banco = $('#ddlBancos');
                    var tipo = $('#ddlTipo');
                    var agencia = $('#txtCB_Agencia');
                    var conta = $('#txtCB_Conta');
                    var apelido = $('#txtCB_Apelido');

                    if ($(".alert-criarconta").hasClass("alert-success")) {
                        agencia.val('');
                        conta.val('');
                        apelido.val('');
                    }

                    setTimeout(function () {
                        $(".btncontasalva").click();
                    }, 3000);
                }
                else {
                    $("." + objAlerta).removeClass("alert-success");
                    $("." + objAlerta).addClass("alert-danger");
                }
            }
        );

        return false;
    });
    

    $(".btn-send-vincular").bind("click", function () {
        $(".alert-vincularcontas").show();
        //$(".alert-vincularcontas").html('Enviando ... ');
        //$(".alert-vincularcontas").addClass("alert-success");

        if (typeof $('input[name="chkConta"]:checked').attr("id") == 'undefined') {
            $(".alert-vincularcontas").html('Favor selecionar uma Conta Bancária!');
            $(".alert-vincularcontas").addClass("alert-danger");
            return false;
        }

        id = $('input[name="chkConta"]:checked').attr("id").replace("chkConta_","");

        sHoteis = "";
        sNomeHoteis = "";
        $('.grid-hoteis input[type=checkbox]').each(function () {
            if (this.checked) {
                if (sHoteis.length > 0) {
                    sHoteis += ",";
                    sNomeHoteis += ",";
                }
                sHoteis += $(this).attr("id").replace("chkHotel_", "");
                sNomeHoteis += $(this).parent().parent().parent().find("h5").text();
            }
        });

        if (sHoteis.length == 0) {
            $(".alert-vincularcontas").html('Favor selecionar um ou mais Hotéis!');
            $(".alert-vincularcontas").addClass("alert-danger");
            return false;
        }

        objC = $('input[name="chkConta"]:checked').parent().parent().parent();
        sconta = $(objC).find("div.col-4 span").html();
        sconta += " - " + $(objC).find("div.col-2:eq(0) li").html() + " - " + $(objC).find("div.col-2:eq(1) li").html() + " - " + $(objC).find("div.col-2:eq(2) li").html();
        $("#modal-mensagem-vincular .doc-hoteis").html(replaceAll(sNomeHoteis, ",", "<br/>"));
        $("#modal-mensagem-vincular .doc-conta").html(sconta);

        var dHoje = new Date();
        dia = dHoje.getDate();
        if (dia > 20)
            dHoje = new Date(dHoje.setMonth(dHoje.getMonth() + 2));
        else
            dHoje = new Date(dHoje.setMonth(dHoje.getMonth() + 1));

        sdata = arrMonth[dHoje.getMonth()] + " de " + dHoje.getFullYear();
        $("#modal-mensagem-vincular .doc-data").html(sdata);
        $("#modal-mensagem-vincular").modal("show");

        return false;
    });

    $(".btn-aceito-vincular").bind("click", function () {
        $(".alert-vincularcontas").show();
        $(".alert-vincularcontas").html('Enviando ... ');
        $(".alert-vincularcontas").addClass("alert-success");

        if (typeof $('input[name="chkConta"]:checked').attr("id") == 'undefined') {
            $(".alert-vincularcontas").html('Favor selecionar uma Conta Bancária!');
            $(".alert-vincularcontas").addClass("alert-danger");
            return false;
        }

        id = ($('input[name="chkConta"]:checked').attr("id").replace("chkConta_", ""));

        sHoteis = "";
        $('.grid-hoteis input[type=checkbox]').each(function () {
            if (this.checked) {
                if (sHoteis.length > 0)
                    sHoteis += ",";
                sHoteis += $(this).attr("id").replace("chkHotel_", "");

            }
        });

        if (sHoteis.length == 0) {
            $(".alert-vincularcontas").html('Favor selecionar um ou mais Hotéis!');
            $(".alert-vincularcontas").addClass("alert-danger");
            return false;
        }

        data = {
            banco: id,
            hoteis: sHoteis,
            modulo: "vincular"
        };

        objAlerta = "alert-vincularcontas";

        $("#modal-mensagem-vincular").modal("hide");

        $.post('ws-form.ashx', data,
            function (msg) {
                $("." + objAlerta).html(msg['message']);
                if (msg['response'] == "ok") {
                    $("." + objAlerta).removeClass("alert-danger");
                    $("." + objAlerta).addClass("alert-success");


                    //salva log mensagem
                    mensagem = $("#modal-mensagem-vincular .modal-body").html();
                    $.post('ws-form.ashx', { modulo: "LogMensagens", titulo: "Alerta de vinculação de conta", mensagem: mensagem });

                    setTimeout(function () {
                        CarregaContas();
                        CarregaHoteis();
                        $("." + objAlerta).html('');
                        $("." + objAlerta).removeClass("alert-success");
                    }, 3000);
                }
                else {
                    $("." + objAlerta).removeClass("alert-success");
                    $("." + objAlerta).addClass("alert-danger");
                }
            }
        );

        return false;
    });

    $(".btn-aceito-vincular-a").bind("click", function () {
        $(".alert-vincularcontas").show();
        $(".alert-vincularcontas").html('Enviando ... ');
        $(".alert-vincularcontas").addClass("alert-success");

        if (typeof $('input[name="chkConta"]:checked').attr("id") == 'undefined') {
            $(".alert-vincularcontas").html('Favor selecionar uma Conta Bancária!');
            $(".alert-vincularcontas").addClass("alert-danger");
            return false;
        }

        idconta = $('input[name="chkConta"]:checked').attr("id").replace("chkConta_", "");
        id = $('#hIDContaVinculada').val();


        data = {
            id: id,
            idconta: idconta,
            modulo: "alterarconta"
        };

        objAlerta = "alert-vincularcontas";

        $("#modal-mensagem-vincular-a").modal("hide");

        $.post('ws-form.ashx', data,
            function (msg) {
                $("." + objAlerta).html(msg['message']);
                if (msg['response'] == "ok") {
                    $("." + objAlerta).removeClass("alert-danger");
                    $("." + objAlerta).addClass("alert-success");


                    //salva log mensagem
                    mensagem = $("#modal-mensagem-vincular-a .modal-body").html();
                    $.post('ws-form.ashx', { modulo: "LogMensagens", titulo: "Alerta de alteração de conta", mensagem: mensagem });

                    CarregaHoteis();

                    setTimeout(function () {
                        $(".alert-vincularcontas").html('');
                        $(".alert-vincularcontas").removeClass("alert-success");
                    }, 3000);
                }
                else {
                    $("." + objAlerta).removeClass("alert-success");
                    $("." + objAlerta).addClass("alert-danger");
                }
            }
        );

        return false;
    });


    CarregaContas();
    CarregaHoteis();
});

function CarregaContas() {


    data = {
        modulo: "listacontas"
    };


    $.post('/ws-form.ashx', data,
        function (result) {

            items = JSON.parse(result);
            sHtml = "";
            items.forEach(function (item) {
                sHtml += "<div class=\"row\"> \r\n ";
                sHtml += "  <div class=\"col-1\"> \r\n ";
                sHtml += "      <div class=\"radio\"><input type=\"radio\" id=\"chkConta_" + item["ID"] + "\" name=\"chkConta\" class=\"magic-radio\" ><label for=\"chkConta_" + item["ID"] + "\"></label></div> \r\n ";
                sHtml += "  </div > \r\n ";
                sHtml += "  <div class=\"col-4 col-md-3\" > \r\n ";
                sHtml += "  <li> <span>" + item["NumeroBanco"] + " - " + item["Banco"] + "<br><b> " + item["Apelido"] + " </b></span></li > \r\n ";
                sHtml += "  </div > \r\n ";
                sHtml += "  <div class=\"col-2\" > \r\n ";
                sHtml += "  <li>" + (item["Tipo"] == 2 ? "Poupança" : "Corrente") + "</li> \r\n ";
                sHtml += "  </div > \r\n ";
                sHtml += "  <div class=\"col-2\" > \r\n ";
                sHtml += "  <li>" + item["Agencia"] + "</li > \r\n ";
                sHtml += "  </div > \r\n ";
                sHtml += "  <div class=\"col-2 col-md-3\" > \r\n ";
                sHtml += "  <li>" + item["Conta"] + "</li > \r\n ";
                sHtml += "  </div > \r\n ";
                sHtml += "  <div class=\"col-1 editarcontas\" > \r\n ";
                if (Number(item["Qtd"]) == 0)
                    sHtml += "  <a href=\"javascript:void(0);\" onclick=\"removerConta(this," + item["ID"] + ");\"><i class=\"far fa-trash-alt\" ></i ></a> \r\n ";
                sHtml += "  </div > \r\n ";
                sHtml += "  </div > \r\n ";
            });

            $(".grid-contas").html(sHtml);



        }
    );
}

function CarregaHoteis() {


    data = {
        modulo: "listahoteis"
    };


    $.post('/ws-form.ashx', data,
        function (result) {

            items = JSON.parse(result);
            sHtml = "";
            sHtml2 = "";
            items.forEach(function (item) {

                if (item["IDContaBancaria"] == 0) {
                    sHtml += "<div class=\"row\"> \r\n ";
                    sHtml += "  <div class=\"col-1\"> \r\n ";
                    sHtml += "      <div class=\"checkbox\"><input type=\"checkbox\" id=\"chkHotel_" + item["IDEmpreendimento"] + "\" name=\"chkHotel_" + item["IDEmpreendimento"] + "\" class=\"magic-checkbox\" ><label for=\"chkHotel_" + item["IDEmpreendimento"] + "\"></label></div> \r\n ";
                    sHtml += "  </div > \r\n ";
                    sHtml += "  <div class=\"col-10\" > \r\n ";
                    sHtml += "      <h5>" + item["Empreendimento"] + "</h5> \r\n ";
                    sHtml += "  </div > \r\n ";
                    sHtml += "</div > \r\n ";
                }
                else {
                    sHtml2 += "<div class=\"box-dashboard\"> \r\n ";
                    sHtml2 += "  <div class=\"row\"> \r\n ";
                    sHtml2 += "     <div class=\"col-7 col-sm-7 col-md-12 col-lg-12 col-xl-7\"> \r\n ";
                    sHtml2 += "  <h4>" + item["Empreendimento"] + "</h4> \r\n ";
                    sHtml2 += "  <li>" + item["Apelido"] + "</li> \r\n ";
                    sHtml2 += "  <li>" + item["NumeroBanco"] + " - " + item["Banco"] + "</li> \r\n ";
                    sHtml2 += "  <li>Agencia:<b>" + item["Agencia"] + "</b></li> \r\n ";
                    sHtml2 += "  <li>Conta " + (item["Tipo"] == 2 ? "Poupança" : "Corrente") + ":<b>" + item["Conta"] + "</b></li> \r\n ";
                    sHtml2 += "     </div > \r\n ";
                    sHtml2 += "     <div class=\"col-5 col-sm-5 col-md-12 col-lg-12 col-xl-5 text-center\"> \r\n ";
                    sHtml2 += "  <img src=\"" + item["Logo"] + "\" width=\"90\" height=\"80\" /> \r\n ";
                    //sHtml2 += "  <a class=\"btn-geral\" href=\"javascript:void(0);\" onclick=\"DesvincularHotel(" + item["ID"] + ");\">desvincular</a></div> \r\n ";
                    sHtml2 += "  <a class=\"btn-geral\" href=\"javascript:void(0);\" onclick=\"AlterarConta(" + item["ID"] + ",this);\">alterar</a></div> \r\n ";
                    sHtml2 += "     </div > \r\n ";
                    sHtml2 += "  </div > \r\n ";
                    sHtml2 += "</div > \r\n ";
                }
            });

            $(".grid-hoteis").html(sHtml);
            $(".grid-dadosbancarios").html(sHtml2);


        }
    );
}

function AlterarConta(id,obj) {

    if (typeof $('input[name="chkConta"]:checked').attr("id") == 'undefined') {
        $(".alert-vincularcontas").html('Favor selecionar uma Conta Bancária!');
        $(".alert-vincularcontas").addClass("alert-danger");
        alert('Favor selecionar uma Conta Bancária!');
        return false;
    }

    $('#hIDContaVinculada').val(id);
    idconta = $('input[name="chkConta"]:checked').attr("id").replace("chkConta_", "");

    objC = $('input[name="chkConta"]:checked').parent().parent().parent();
    sconta = $(objC).find("div.col-4 span").html();
    sconta += " - " + $(objC).find("div.col-2:eq(0) li").html() + " - " + $(objC).find("div.col-2:eq(1) li").html() + " - " + $(objC).find("div.col-2:eq(2) li").html();
    $("#modal-mensagem-vincular-a .doc-hoteis").html($(obj).parent().parent().find("div h4").html());
    $("#modal-mensagem-vincular-a .doc-conta").html(sconta);

    var dHoje = new Date();
    dia = dHoje.getDate();
    if (dia > 20)
        dHoje = new Date(dHoje.setMonth(dHoje.getMonth() + 2));
    else
        dHoje = new Date(dHoje.setMonth(dHoje.getMonth() + 1));

    sdata = arrMonth[dHoje.getMonth()] + " de " + dHoje.getFullYear();
    $("#modal-mensagem-vincular-a .doc-data").html(sdata);
    $("#modal-mensagem-vincular-a").modal("show");

    return false;

    //if (confirm('Alterar a conta deste Hotel?')) {

    //    data = {
    //        id: id,
    //        idconta: idconta,
    //        modulo: "alterarconta"
    //    };

    //    $.post('/ws-form.ashx', data,
    //        function (msg) {
    //            $(".alert-vincularcontas").html(msg['message']);
    //            if (msg['response'] == "ok") {
    //                $(".alert-vincularcontas").removeClass("alert-danger");
    //                $(".alert-vincularcontas").addClass("alert-success");

    //                CarregaHoteis();

    //                setTimeout(function () {
    //                    $(".alert-vincularcontas").html('');
    //                    $(".alert-vincularcontas").removeClass("alert-success");
    //                }, 3000);

    //            }
    //        }
    //    );

    //}
}


//function DesvincularHotel(id) {
//    if (confirm('Desvincular este Hotel?')) {

//        data = {
//            id: id,
//            modulo: "desvincular"
//        };

//        $.post('/ws-form.ashx', data,
//            function (msg) {
//                $(".alert-vincularcontas").html(msg['message']);
//                if (msg['response'] == "ok") {
//                    $(".alert-vincularcontas").removeClass("alert-danger");
//                    $(".alert-vincularcontas").addClass("alert-success");

//                    CarregaHoteis();

//                    setTimeout(function () {
//                        $(".alert-vincularcontas").html('');
//                        $(".alert-vincularcontas").removeClass("alert-success");
//                    }, 3000);

//                }
//            }
//        );

//    }
//}

function removerConta(obj, id) {
    if (confirm('Remover está Conta?')) {

        data = {
            id: id,
            modulo: "removerconta"
        };

        $.post('/ws-form.ashx', data,
            function (msg) {
                $(".alert-gridcontas").html(msg['message']);
                if (msg['response'] == "ok") {
                    $(".alert-gridcontas").removeClass("alert-danger");
                    $(".alert-gridcontas").addClass("alert-success");
                    $(obj).parent().parent().remove();
                }
                else {
                    $(".alert-gridcontas").removeClass("alert-success");
                    $(".alert-gridcontas").addClass("alert-danger");
                }
            }
        );

    }
}

function escapeRegExp(string) {
    return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
}
function replaceAll(str, term, replacement) {
    return str.replace(new RegExp(escapeRegExp(term), 'g'), replacement);
}