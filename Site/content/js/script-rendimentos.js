﻿jQuery(document).ready(function () {

    var date = new Date();
    date.setDate(date.getMonth() - 1);
    var iAno = date.getFullYear();
    var iAnoAnterior = date.getFullYear() - 1;

    //DADOS 
    var monthsShort = ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"];
    var colors = ['#FF0000', '#C0C0C0', '#1c1c1c', '#6A5ACD', '#FF8C00', '#B0C4DE', '#B0E0E6', '#00b5f7', '#EEE8AA', '#EEDD82', '#CDB7B5', '#DA70D6', '#FFA500', '#ffb805'];

    //-------------------- TAB 1 - GRAFICO DONUT  --------------

    var iMeses = 12;
    var iLinha = 0;
    var iMesAtual = 0;


    

    //-------------------- TAB 2 - GRAFICO ADR  --------------
    iLinha = 0;

    objOcc = $("#tabela_rendimentos tbody");
    aux_data = "[";
    $("#tabela_rendimentos tbody tr").each(function (index) {
        mesano = $(this).find("td:eq(0)").data("value");
       // console.log("rendimento=" + $(this).find("td:eq(1)").text());
        rendimento = parseFloat($(this).find("td:eq(1)").text().replace("R$ ", "").replace(".", "").replace(",", "."));
        //console.log("rendimento2=" + rendimento);
        data_pagamento = $(this).find("td:eq(2)").text();

        if (aux_data.length > 3)
            aux_data += ", ";
        aux_data += "{\"mes\": \"" + monthsShort[Number(mesano)-1] + "\"";
        if (rendimento > 0)
            aux_data += ", \"r1\": " + rendimento;
        aux_data += "}";

     });
    aux_data += "]";

    //console.log(aux_data);

    data_bar_valor = JSON.parse(aux_data);

    letras_bar = ['r1'];
    labels_bar = ['Rendimentos'];

    i_ymax = 0;
    $.each(data_bar_valor, function (i, val) {
        if (typeof val.r1 != "undefined" && Number(val.r1) > i_ymax)
            i_ymax = Number(val.r1);

    });
    i_ymax_valor = (Math.round((Number(i_ymax) + 20) / 10) * 10);

    $("#grafico-rendimento").html(""); //destroy
    grafico_rendimento = Morris.Line({
        element: 'grafico-rendimento',
        data: data_bar_valor,
        xkey: 'mes',
        ykeys: letras_bar,
        ymax: i_ymax_valor,
        labels: labels_bar,
        gridEnabled: true,
        gridLineColor: 'rgba(0,0,0,.1)',
        gridTextColor: '#8f9ea6',
        gridTextSize: '10px',
        lineColors: colors,
        xLabelColors: colors,
        xLabelMargin: 2,
        lineWidth: 2,
        preUnits: "$",
        parseTime: false,
        resize: true,
        hideHover: true
    });


    //for (x = 0; x < 3; x++) {
    //    value_1 = $(objOcc).find("tr:eq(" + x + ") td:eq(13)").text();
    //    legendItem = $('<span class="item"></span>').prepend('<br><span>$' + value_1 + '</span>');
    //    legendItem.find('span')
    //        .css('backgroundColor', colors[x]);
    //    $('#grafico-adr-legenda').append(legendItem);
    //}


   


});
