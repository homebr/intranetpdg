﻿var periodo = 'ano';

jQuery(document).ready(function () {

	//redefinir altura dos boxes
	/*var largura = $(window).width();
	if(largura <= 600){
		//var altura = $(window).height();
		//altura = altura - 100;
		$(".morris-line").css("height", "200px");
	}
	else if(largura <= 1500){
		//var altura = $(window).height();
		//altura = altura - 250;
		$(".morris-line").css("height", "270px");
	}
	else{
		//var altura = $(window).height();
		//altura = altura - 430;
		$(".morris-line").css("height", "200px");
	}*/
	
	
    CarregaGraficos('ano');
});


$(".trocaPeriodo").click(function () {
    $(this).parent().find(".btn-geral").removeClass("active");
    $(this).addClass("active");
    periodo = $(this).data("value");
    CarregaGraficos(periodo);
    if ($(".btn-performance").hasClass("ativo"))
        CarregaAbaGrafico5();
});

//DADOS 
var monthsLabel = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];
var monthsShort = ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"];
var colors = ['#FF0000', '#C0C0C0', '#1c1c1c', '#6A5ACD', '#FF8C00', '#B0C4DE', '#B0E0E6', '#00b5f7', '#EEE8AA', '#EEDD82', '#CDB7B5', '#DA70D6', '#FFA500', '#ffb805'];

var iMeses = 12;
var iLinha = 0;
var id = $("#" + campo + "hCodEmpreendimento").val();
var data_bar_valor = "";

function CarregaGraficos(periodo) {


    //-------------------- TAB 1 - GRAFICO DONUT  --------------

    objOcc = $("#tabela_ocupacao tbody");




    //speriodo = "";
    //if (periodo == "trimestre")
    //    speriodo = "Trimestre";
    //else if (periodo == "semestre")
    //    speriodo = "Semestre";
    //else if (periodo == "ano")
    //    speriodo = now.getFullYear();
    //else
        speriodo = monthsLabel[periodoSetado.getMonth()] + " de " + periodoSetado.getFullYear();


    $("#titulo-periodo").text(speriodo);



    //consulta banco
    /* DADOS DE OCUPAÇÃO */
    $.post("wsDashboard.ashx", { modulo: 'dados_ocupacao', id: id, periodo: periodo },
        function (result) {
            data_bar_valor = JSON.parse(result);
            //console.log(result);
            CarregaGrafico1(result);
        });

    //data_bar_valor = [{ "mes": "Jan", "r1": 76, "r2": 89, "r3": 88 }, { "mes": "Fev", "r1": 69, "r2": 67, "r3": 74 }, { "mes": "Mar", "r1": 68, "r2": 77, "r3": 68 }, { "mes": "Abr", "r1": 63, "r2": 68, "r3": 69 }, { "mes": "Mai", "r2": 67, "r3": 62 }, { "mes": "Jun", "r2": 64, "r3": 61 }, { "mes": "Jul", "r2": 75, "r3": 69 }, { "mes": "Ago", "r2": 68, "r3": 58 }, { "mes": "Set", "r2": 68, "r3": 60 }, { "mes": "Out", "r2": 69, "r3": 54 }, { "mes": "Nov", "r2": 68, "r3": 66 }, { "mes": "Dez", "r2": 65, "r3": 60 }];
    var letras_bar = ['r1', 'r2', 'r3'];
    var labels_bar = ['Occ ' + now.getFullYear() + ' (Real)', 'Occ ' + now.getFullYear() + ' (Orçado)', 'Occ ' + (now.getFullYear()-1) + ''];
    var i_ymax_valor = 100;


    function CarregaGrafico1() {
        i_ymax = 0;
        //$.each(data_bar_valor, function (i, val) {
        //    if (typeof val.r1 != "undefined" && Number(val.r1) > i_ymax)
        //        i_ymax = Number(val.r1);
        //    if (typeof val.r2 != "undefined" && Number(val.r2) > i_ymax)
        //        i_ymax = Number(val.r2);
        //    if (typeof val.r3 != "undefined" && Number(val.r3) > i_ymax)
        //        i_ymax = Number(val.r3);

        //});
        i_ymax_valor = 100; //(Math.round((Number(i_ymax) + 20) / 10) * 10);
        //console.log("i_ymax=" + i_ymax);
        labels_bar = ['Occ ' + now.getFullYear() + ' (Real)', 'Occ ' + now.getFullYear() + ' (Orçado)', 'Occ ' + (now.getFullYear() - 1) + ''];

        $("#grafico-ocupacao").html(""); //destroy
        grafico_ocupacao = Morris.Line({
            element: 'grafico-ocupacao',
            data: data_bar_valor,
            xkey: 'mes',
            ykeys: letras_bar,
            ymax: i_ymax_valor,
            labels: labels_bar,
            gridEnabled: true,
            gridLineColor: 'rgba(0,0,0,.1)',
            gridTextColor: '#8f9ea6',
            gridTextSize: '10px',
            lineColors: colors,
            xLabelColors: colors,
            xLabelMargin: 2,
            lineWidth: 2,
            postUnits: "%",
            parseTime: false,
            resize: true,
            hideHover: true
        });

        //value_1 = $(objOcc).find("tr:eq(0) td:eq(12)").text().replace("%", "");
        //value_2 = $(objOcc).find("tr:eq(1) td:eq(12)").text().replace("%", "");
        //value_3 = $(objOcc).find("tr:eq(2) td:eq(12)").text().replace("%", "");

        //for (x = 0; x < 3; x++) {
        //    value_1 = $(objOcc).find("tr:eq(" + x + ") td:eq(13)").text();
        //    var legendItem = $('<span class="item"></span>').prepend('<br><span>' + value_1 + '</span>');
        //    legendItem.find('span')
        //        .css('backgroundColor', colors[x]);
        //    $('#grafico-ocupacao-legenda').append(legendItem);
        //}
    }
    //CarregaGrafico1();

    //-------------------- TAB 2 - GRAFICO ADR  --------------
    iLinha = 0;

    objOcc = $("#tabela_adr tbody");

    //data_bar_valor = [{ "mes": "Jan", "r2": 155 }, { "mes": "Fev", "r2": 151 }, { "mes": "Mar", "r2": 167 }, { "mes": "Abr", "r1": 134, "r2": 155 }, { "mes": "Mai", "r2": 151 }, { "mes": "Jun", "r2": 153 }, { "mes": "Jul", "r2": 153 }, { "mes": "Ago", "r2": 155 }, { "mes": "Set", "r2": 153 }, { "mes": "Out", "r2": 153 }, { "mes": "Nov", "r2": 151 }, { "mes": "Dez", "r2": 159 }];

    //consulta banco
    /* DADOS DE OCUPAÇÃO */
    $.post("wsDashboard.ashx", { modulo: 'dados_adr', id: id, periodo: periodo },
        function (result) {
            data_bar_valor = JSON.parse(result);
            //console.log(result);
            CarregaGrafico2(data_bar_valor);
        });


    letras_bar = ['r1', 'r2', 'r3'];
    labels_bar = ['ADR ' + now.getFullYear() + ' (Real)', 'ADR ' + now.getFullYear() + ' (Orçado)', 'ADR ' + (now.getFullYear()-1) + ''];
    i_ymax_valor = 200;

    function CarregaGrafico2(data_bar_valor) {

        i_ymax = 0;
        $.each(data_bar_valor, function (i, val) {
            if (typeof val.r1 != "undefined" && Number(val.r1) > i_ymax)
                i_ymax = Number(val.r1);
            if (typeof val.r2 != "undefined" && Number(val.r2) > i_ymax)
                i_ymax = Number(val.r2);
            if (typeof val.r3 != "undefined" && Number(val.r3) > i_ymax)
                i_ymax = Number(val.r3);

        });
        i_ymax_valor = (Math.round((Number(i_ymax) + 20) / 10) * 10);
        labels_bar = ['ADR ' + now.getFullYear() + ' (Real)', 'ADR ' + now.getFullYear() + ' (Orçado)', 'ADR ' + (now.getFullYear() - 1) + ''];

        $("#grafico-adr").html(""); //destroy
        grafico_adr = Morris.Line({
            element: 'grafico-adr',
            data: data_bar_valor,
            xkey: 'mes',
            ykeys: letras_bar,
            ymax: i_ymax_valor,
            labels: labels_bar,
            gridEnabled: true,
            gridLineColor: 'rgba(0,0,0,.1)',
            gridTextColor: '#8f9ea6',
            gridTextSize: '10px',
            lineColors: colors,
            xLabelColors: colors,
            xLabelMargin: 2,
            lineWidth: 2,
            preUnits: "$",
            parseTime: false,
            resize: true,
            hideHover: true
        });

        //value_1 = $(objOcc).find("tr:eq(0) td:eq(12)").text().replace("%", "");
        //value_2 = $(objOcc).find("tr:eq(1) td:eq(12)").text().replace("%", "");
        //value_3 = $(objOcc).find("tr:eq(2) td:eq(12)").text().replace("%", "");

        //for (x = 0; x < 3; x++) {
        //    value_1 = $(objOcc).find("tr:eq(" + x + ") td:eq(13)").text();
        //    legendItem = $('<span class="item"></span>').prepend('<br><span>' + value_1 + '</span>');
        //    legendItem.find('span')
        //        .css('backgroundColor', colors[x]);
        //    $('#grafico-adr-legenda').append(legendItem);
        //}
    }
    CarregaGrafico2();

    //-------------------- TAB 3 - GRAFICO REVPAR  -------------- 
    iLinha = 0;

    objOcc = $("#tabela_RevPAR tbody");

    //data_bar_valor = [{"mes":"Jan","r1":134,"r2":138},{"mes":"Fev","r1":112,"r2":101},{"mes":"Mar","r1":130,"r2":129},{"mes":"Abr","r1":102,"r2":105},{"mes":"Mai","r2":101},{"mes":"Jun","r2":98},{"mes":"Jul","r2":115},{"mes":"Ago","r2":105},{"mes":"Set","r2":104},{"mes":"Out","r2":106},{"mes":"Nov","r2":103},{"mes":"Dez","r2":103}];

    //consulta banco
    /* DADOS DE OCUPAÇÃO */
    $.post("wsDashboard.ashx", { modulo: 'dados_revpar', id: id, periodo: periodo },
        function (result) {
            data_bar_valor = JSON.parse(result);
            //console.log(result);
            CarregaGrafico3(data_bar_valor);
        });


    letras_bar = ['r1', 'r2', 'r3'];
    labels_bar = ['RevPAR ' + now.getFullYear() + ' (Real)', 'RevPAR ' + now.getFullYear() + ' (Orçado)', 'RevPAR ' + (now.getFullYear()-1) + ''];
    i_ymax_valor = 200;

    function CarregaGrafico3(data_bar_valor) {

        i_ymax = 0;
        $.each(data_bar_valor, function (i, val) {
            if (typeof val.r1 != "undefined" && Number(val.r1) > i_ymax)
                i_ymax = Number(val.r1);
            if (typeof val.r2 != "undefined" && Number(val.r2) > i_ymax)
                i_ymax = Number(val.r2);
            if (typeof val.r3 != "undefined" && Number(val.r3) > i_ymax)
                i_ymax = Number(val.r3);

        });
        i_ymax_valor = (Math.round((Number(i_ymax) + 20) / 10) * 10);
        labels_bar = ['RevPAR ' + now.getFullYear() + ' (Real)', 'RevPAR ' + now.getFullYear() + ' (Orçado)', 'RevPAR ' + (now.getFullYear() - 1) + ''];

        $("#grafico-RevPAR").html(""); //destroy
        grafico_revpar = Morris.Line({
            element: 'grafico-RevPAR',
            data: data_bar_valor,
            xkey: 'mes',
            ykeys: letras_bar,
            ymax: i_ymax_valor,
            ymin: 0,
            labels: labels_bar,
            gridEnabled: true,
            gridLineColor: 'rgba(0,0,0,.1)',
            gridTextColor: '#8f9ea6',
            gridTextSize: '10px',
            lineColors: colors,
            xLabelColors: colors,
            xLabelMargin: 2,
            lineWidth: 2,
            preUnits: "$",
            parseTime: false,
            resize: true,
            hideHover: true
        });

        //value_1 = $(objOcc).find("tr:eq(0) td:eq(12)").text().replace("%", "");
        //value_2 = $(objOcc).find("tr:eq(1) td:eq(12)").text().replace("%", "");
        //value_3 = $(objOcc).find("tr:eq(2) td:eq(12)").text().replace("%", "");

        //for (x = 0; x < 3; x++) {
        //    value_1 = $(objOcc).find("tr:eq(" + x + ") td:eq(13)").text();
        //    legendItem = $('<span class="item"></span>').prepend('<br><span>' + value_1 + '</span>');
        //    legendItem.find('span')
        //        .css('backgroundColor', colors[x]);
        //    $('#grafico-RevPAR-legenda').append(legendItem);
        //}

        //$(".morris-line path[stroke=#c0c0c0]").attr("stroke-dasharray", "4");
        //$(".morris-line path[stroke=#1c1c1c]").attr("stroke-dasharray", "4");
    }
    //CarregaGrafico3();





    CarregaAbaGrafico5();


    setTimeout(function () {
        $('.morris-line svg path[stroke="#c0c0c0"]').attr("stroke-dasharray", "4");
        $('.morris-line svg path[stroke="#1c1c1c"]').attr("stroke-dasharray", "4");
    }, 1000);
}


//gráfico de pizza - Despesas
function CarregaAbaGrafico4() {
    //data = [{ "label": "Restauração", "value": "14" }, { "label": "Pessoal", "value": "42" }, { "label": "Utilidades", "value": "11" }, { "label": "Manutenção", "value": "6" }, { "label": "Comissões Agências", "value": "5" }, { "label": "Outras Desp.Op.", "value": 22 }];
    $.post("wsDashboard.ashx", { modulo: 'dados_despesas', id: id, periodo: periodo },
        function (result) {
            data_donut_valor = JSON.parse(result);
            //console.log(result);
            CarregaGrafico4(data_donut_valor);
        });
}

function CarregaGrafico4(data_donut_valor) {
	
	
    colors2 = ['#7874B5', '#602376', '#9A054A', '#ED700A', '#D3E3EA', '#BBCEA4'];

    //console.log(data_donut_valor);

    sortJson(data_donut_valor, "value", "int", false);

    //console.log(data_donut_valor2);
	
    $("#grafico-despesas").html(""); //destroy
    grafico_receitas = Morris.Donut({
        element: 'grafico-despesas',
        data: data_donut_valor,
        colors: colors2,
        formatter: function (y, data) { return y + '%' },
        resize: true
    });

    $('#grafico-despesas-legenda').html('');
    grafico_receitas.options.data.forEach(function (label, i) {
        var legendItem = $('<span></span>').text(label['label'] + " ( " + label['value'] + "% )").prepend('<br><span>&nbsp;</span>');
        legendItem.find('span')
            .css('backgroundColor', grafico_receitas.options.colors[i])
            .css('width', '15px')
            .css('height', '15px')
            .css('display', 'inline-block')
            .css('margin', '3px 3px 0 3px');
        $('#grafico-despesas-legenda').append(legendItem);
    });
}

function sortJson(element, prop, propType, asc) {
    switch (propType) {
        case "int":
            element = element.sort(function (a, b) {
                if (asc) {
                    return (parseInt(a[prop]) > parseInt(b[prop])) ? 1 : ((parseInt(a[prop]) < parseInt(b[prop])) ? -1 : 0);
                } else {
                    return (parseInt(b[prop]) > parseInt(a[prop])) ? 1 : ((parseInt(b[prop]) < parseInt(a[prop])) ? -1 : 0);
                }
            });
            break;
        default:
            element = element.sort(function (a, b) {
                if (asc) {
                    return (a[prop].toLowerCase() > b[prop].toLowerCase()) ? 1 : ((a[prop].toLowerCase() < b[prop].toLowerCase()) ? -1 : 0);
                } else {
                    return (b[prop].toLowerCase() > a[prop].toLowerCase()) ? 1 : ((b[prop].toLowerCase() < a[prop].toLowerCase()) ? -1 : 0);
                }
            });
    }
}

/* ANÁLISE DE PERFORMANCE */
function CarregaAbaGrafico5() {
    $.post("wsDashboard.ashx", { modulo: 'dados_performance', id: id, periodo: periodo },
        function (result) {
            data_bar_valor = JSON.parse(result);
            //console.log(result);
            CarregaGrafico5(data_bar_valor);
        });
}

letras_bar = ['r1', 'r2', 'r3'];
i_ymax_valor = 200;
function CarregaGrafico5(data_bar_valor) {

    i_ymax = 0;
    $.each(data_bar_valor, function (i, val) {
        if (typeof val.r1 != "undefined" && Number(val.r1) > i_ymax)
            i_ymax = Number(val.r1);
        if (typeof val.r2 != "undefined" && Number(val.r2) > i_ymax)
            i_ymax = Number(val.r2);
        if (typeof val.r3 != "undefined" && Number(val.r3) > i_ymax)
            i_ymax = Number(val.r3);

    });
    i_ymax_valor = Math.round((Number(i_ymax) + 20) / 1000) * 1000;

    if (typeof $(".box-dashboard .btn-despesas").html() == "undefined") {
        labels_bar_performance = ['Tot Receita Bruta', 'Tot Despesas Oper', 'GOP'];
        $(".grafico-performance .legenda-rodape .texto:eq(2)").html('GOP');
    }
    else
        labels_bar_performance = ['Tot Receita Bruta', 'Tot Despesas Oper', 'EBITDAR'];


    $("#grafico-performance").html(""); //destroy
    grafico_performance = Morris.Line({
        element: 'grafico-performance',
        data: data_bar_valor,
        xkey: 'mes',
        ykeys: letras_bar,
        ymax: i_ymax_valor,
        ymin: 1000,
        smooth: true,
        labels: labels_bar_performance,
        axes: true,
        gridEnabled: true,
        gridLineColor: 'rgba(0,0,0,.1)',
        gridTextColor: '#8f9ea6',
        gridTextSize: '10px',
        lineColors: colors,
        xLabelColors: colors,
        xLabelMargin: 2,
        lineWidth: 2,
        preUnits: "$",
        parseTime: false,
        resize: true,
        hideHover: true
    });
    $("#grafico-performance").show();

    setTimeout(function () {
        $('.morris-line svg path[stroke="#c0c0c0"]').attr("stroke-dasharray", "4");
        $('.morris-line svg path[stroke="#1c1c1c"]').attr("stroke-dasharray", "4");
    }, 1000);
}

//RENDIMENTOS
function CarregaRendimentos() {
    $.post("wsDashboard.ashx", { modulo: 'dados_rendimentos', id: id, periodo: periodo },
        function (result) {
            if (result.length > 1) {
                rendimentos_valor = JSON.parse(result);
                //console.log(result);
                //CarregaGrafico4(data_donut_valor);
                srendimento = "";
                $(".box-dashboard.rendimento h2").html(rendimentos_valor[0].distribuicao);
                $(".box-dashboard.rendimento h4").html(rendimentos_valor[0].data_pagamento);
            }
            else {
                $(".box-dashboard.rendimento h2").html("");
                $(".box-dashboard.rendimento h4").html("");
            }
        });

}
CarregaRendimentos();