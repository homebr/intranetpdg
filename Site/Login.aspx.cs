﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.IO;



public partial class Login : System.Web.UI.Page
{
    String strCampo = "ctl00$ContentPlaceHolder1$";
    public String sBox = "";

    protected void Page_Load(object sender, System.EventArgs e)
    {

        if (!IsPostBack)
        {
            //if (FrameWork.Util.GetString(Session["_uiReferer"]).Length > 0)
            //    hUrl.Value = FrameWork.Util.GetString(Session["_uiReferer"]);
            //else
            //    hUrl.Value = FrameWork.Util.GetString(Request.UrlReferrer);

            Session.Abandon();

        }
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {

        lblMensagem.Text = "";
        if (string.IsNullOrEmpty(Request.Form["email"]))
        {
            Msg("Favor preencher o campo E-mail!", false);
            return;
        }
        if (string.IsNullOrEmpty(Request.Form["senha"]))
        {
            Msg("Favor preencher o campo Senha!", false);
            return;
        }

        if (!string.IsNullOrEmpty(Request.Form["email"]))
        {
            string strUsuario = FrameWork.Util.GetString(Request.Form["email"]).Trim();
            string strSenha = FrameWork.Util.GetString(Request.Form["senha"]).Trim();
            if (FrameWork.Util.GetString(strUsuario) == "")
            {
                Msg("Favor preencher o campo Email!", false);
                return;
            }
            if (FrameWork.Util.GetString(strSenha) == "")
            {
                Msg("Favor preencher o campo Senha!", false);
                return;
            }

            ClassLibrary.Colaboradores cli = new ClassLibrary.Colaboradores();
            cli.LoadWhere("loginrede=@user AND ativo = 1 ", new FrameWork.DataObject.ParameterItem("@user", strUsuario)); 
  //          cli.LoadWhere("loginacesso=@user AND SituacaoLogin = 'A' and grupo in  (0,5) ", new FrameWork.DataObject.ParameterItem("@user", strUsuario)); //



            try
            {
                if (cli.TotalLoad == 1)
                {
                   //string senha1  = FrameWork.WebControls.WebSecurity.DeCript(cli.Senha);

                   // Response.Write(cli.Senha +  "<br>" + strSenha);
                   // Response.End();
                    //Checa senha
                    if (strSenha.Length == 0)
                    {

                        Msg("Favor inserir a senha!", false);
                        return;
                    }
                    

                    if (strSenha != FrameWork.Security.DeCript(cli.Senha)) //FrameWork.WebControls.WebSecurity.DeCript
                    {
                        Msg("Login ou Senha inválida!", false);
                        return;
                    }

                    Session["_uiUserID"] = cli.ID;
                    Session["_uiUser"] = cli.LoginRede;
                    Session["_uiNome"] = cli.Nome;
                    Session["_uiEmail"] = cli.Email;
                    //Session["_uiGruip"] = cli.Grupo;
                   
                    ////if (!cli.Admin)
                    ////    Session["_uiRights"] = FrameWork.Util.GetString(FrameWork.DataObject.DataSource.DefaulDataSource.Execute("SELECT Acessos FROM GrupoUsuarios WHERE IDGrupo=" + cli.Grupo));

                    //cli.UltimoAcesso = DateTime.Now;
                   //cli.SessionID = this.Page.Session.SessionID;
                   cli.Save();


                    // Grava o cookie se foi marcado para lembrar
                    if (FrameWork.Util.GetString(Request.Form["chkConectado"]) == "on")
                    {
                        HttpCookie cookie = new HttpCookie("_PDGarq_login", cli.LoginRede);
                        cookie.Expires = DateTime.Now.AddMonths(6);
                        Response.Cookies.Add(cookie);
                    }

                    //INSERT INTO[dbo].[Log_Acessos]([IDUsuario],[loginrede],[IP],[DataAcesso])     VALUES
                    ////Log de Acesso
                    String strSql = "insert into Log_Acessos (DataAcesso,IDUsuario,loginrede,IP) ";
                    strSql += "values (getdate(),";
                    strSql += FrameWork.Util.GetInt(Session["_uiUserID"]) + ",'";
                    strSql += FrameWork.Util.GetString(Session["_uiUser"]) + "','";
                    strSql += FrameWork.Util.GetString(Request.ServerVariables["REMOTE_ADDR"]) + "')";
                    FrameWork.DataObject.DataSource.DefaulDataSource.Execute(strSql);

                    //redireciona
                    //if (FrameWork.Util.GetString(hUrl.Value).Length > 1)
                    //    Response.Redirect(hUrl.Value, true);
                    //else
                    // Response.Write("passou");
                    // Response.End();
                    // Response.Redirect("/inicio.html", true);
                    Response.Redirect("/default.aspx", true);

                 


                }
                else
                {
                    Msg("Login ou Senha inválida!", false);

                }
            }
            catch
            {
            }
        }
        else
        {
            Session.Abandon();
            Session.RemoveAll();
            Session.Clear();
        }
    }

    protected void btnLembrete_Click(object sender, EventArgs e)
    {
       // Response.Write("tstest");
        //Response.End();
        lblMensagem.Text = "";
        sBox = "lembrete";
        if (string.IsNullOrEmpty(Request.Form["emailrecuperar"]))
        {
            Msg("Favor preencher o campo Email!", false);
            return;
        }


        if (!string.IsNullOrEmpty(Request.Form["emailrecuperar"]))
        {
            string strEmail = FrameWork.Util.GetString(Request.Form["emailrecuperar"]).Trim();
            if (FrameWork.Util.GetString(strEmail) == "")
            {
                Msg("Favor preencher o campo Email!", false);
                return;
            }

            ClassLibrary.Colaboradores cli = new ClassLibrary.Colaboradores();
            cli.LoadWhere("email=@user AND Ativo = 1 ", new FrameWork.DataObject.ParameterItem("@user", strEmail)); //

            try
            {
                if (cli.TotalLoad == 1)
                {
                    // Response.Write("tstest");
                    //Response.End();
                    string strDestinatarios = "";
                    string strAssunto = "";
                    string FilePath = "";
                    System.IO.StreamReader objStreamReader;
                    string strArq;
                    String strResultadoEnvio = "";

                    strDestinatarios = cli.Email;
                    strAssunto = "Esqueci a Senha";

                    FilePath = Server.MapPath("template_email/ModeloLembreteSenha.htm");
                    objStreamReader = System.IO.File.OpenText(FilePath);
                    strArq = objStreamReader.ReadToEnd();

                    strArq = strArq.Replace("#Nome", cli.Nome.Substring(0));
                    strArq = strArq.Replace("#Login", (cli.LoginRede));
                    strArq = strArq.Replace("#Senha", FrameWork.Security.DeCript(cli.Senha));
                    strArq = strArq.Replace("#Email", cli.Email);
                    strArq = strArq.Replace("#IP", Request.ServerVariables["REMOTE_ADDR"]);
                    strArq = strArq.Replace("#DATA", String.Format("{0:d}", DateTime.Now));
                    string pathVirtual = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "template_email/";
                    strArq = strArq.Replace("src=\"img/", "src=\"" + pathVirtual + "template/img/");
                    strArq = strArq.Replace("#PathVirtual#", pathVirtual);
                    string strTo = "naoresponda@homebrasil.com";
                    string strPara = cli.Email; // strEmailRecupera;
                    string strCc = "";

                    //strResultadoEnvio = FrameWork.Util.GetString(Funcoes.EnvioEmail(strArq, "naoresponda@homebrasil.com", strAssunto, strDestinatarios, "fernando@homebrasil.com", ""));
                    strResultadoEnvio = FrameWork.Util.GetString(Funcoes.EnvioEmail(strArq, strTo, strAssunto, strPara, strCc, "fernando@homebrasil.com"));

                    Msg("Foi enviado ao email cadastrado a senha atual!", true);

                    objStreamReader.Dispose();
                    objStreamReader.Close();


                    //ENVIO AO CLIENTE
                   // System.IO.StreamReader objStreamReader;
                    //String strResultadoEnvio = "";
//string strAssunto = "Portal do Cliente - Atualização Cadastral";
                    //string FilePath = Server.MapPath("template_email/ModeloAtualizacaoCadastral.htm");
                    //objStreamReader = System.IO.File.OpenText(FilePath);
                    //string strArq = objStreamReader.ReadToEnd();
                    //strArq = strArq.Replace("#NOME#", txtNome.Text);
                    //strArq = strArq.Replace("#IP#", Request.ServerVariables["REMOTE_ADDR"]);
                    //strArq = strArq.Replace("#DATA#", String.Format("{0:dd/MM/yyyy hh:mm}", DateTime.Now));
                    //string pathVirtual = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"];
                    //strArq = strArq.Replace("src=\"img/", "src=\"" + (pathVirtual + "template_email/") + "img/");
                    //strArq = strArq.Replace("#PathVirtual#", pathVirtual);

                    //string strTo = "naoresponda@homebrasil.com";
                    //string strPara = "fernando@homebrasil.com"; // strEmailRecupera;
                    //string strCc = "";

                    //strResultadoEnvio = FrameWork.Util.GetString(Funcoes.EnvioEmail(strArq, strTo, strAssunto, strPara, strCc, ""));
                    //objStreamReader.Dispose();
                    //objStreamReader.Close();


                    //Msg("Suas informações forammmmm atualizadas com sucesso!", true);

                    return;

                }
                else
                {
                    Msg("E-mail inválido!", false);

                }
            }
            catch
            {
            }
        }
        else
        {
            Session.Abandon();
            Session.RemoveAll();
            Session.Clear();
        }
    }

    protected void Msg(String msg, Boolean tipo)
    {
        lblMensagem.CssClass = (tipo ? "box-danger alert alert-success" : "box-danger alert alert-danger");
        lblMensagem.Text = msg;
        lblMensagem.Visible = true;
    }


    protected void btnEsqueci_Click(object sender, EventArgs e)
    {
        PanelLogin.Visible = false;
        PanelRecuperarSenha.Visible = true;
    }

    protected void BTNVOLTAR_Click(object sender, EventArgs e)
    {
        PanelLogin.Visible = true;
        PanelRecuperarSenha.Visible = false;
    }
    
}



//public partial class Login : System.Web.UI.Page
//{

//    public String sBox = "";

//    protected void Page_Load(object sender, EventArgs e)
//    {

//        if (!this.IsPostBack)
//        {
//            if (FrameWork.Util.GetString(Session["_uiReferer"]).Length > 0)
//                hUrl.Value = FrameWork.Util.GetString(Session["_uiReferer"]);
//            else
//                hUrl.Value = FrameWork.Util.GetString(Request.UrlReferrer);


//            //login por url do az
//            //exemplo: http://pdgpc/login?login=16963920206op0001&senha=0B8AD16AE33301F4913B1EC35D88408B
//            if (!String.IsNullOrEmpty(Request["login"]) && !String.IsNullOrEmpty(Request["senha"]))
//            {

//                //Session.Abandon();
//                LimparSession();

//                String strUsuario = FrameWork.Util.GetString(Request["login"]);
//                String strSenha = FrameWork.Util.GetString(Request["senha"]);

//                try
//                {

//                    AZPortal.ServiceSoapClient envio = new AZPortal.ServiceSoapClient("ServiceSoap1", Config.wsAZ);
//                    AZPortal.Auth auth = new AZPortal.Auth();
//                    auth.TokenAutn = "integracaoPortal";

//                    //1 -AUTENTICA CLIENTE - RETORNA STRING
//                    String strAutentica = envio.autenticaCliente(auth, strUsuario, strSenha);

//                    if (strAutentica == "autentica=0")
//                    {
//                        Msg("Login ou Senha inválida!", false);
//                        return;
//                    }
//                    else
//                    {
//                        // grava as primeiras sessoes
//                        String[] arrAutentica = strAutentica.Split(',');
//                        Session["_uiCodCliente"] = FrameWork.Util.GetString(arrAutentica[1]).Split(':')[1];
//                        Session["_uiNomeCliente"] = FrameWork.Util.GetString(arrAutentica[2]).Split(':')[1];
//                        Session["_uiCPF"] = strUsuario;
//                        Session["_uiSenha"] = strSenha;
//                        Session["_uiFinanceiro"] = FrameWork.Util.GetString(FrameWork.DataObject.DataSource.DefaulDataSource.Execute("SELECT CASE WHEN Financeiro = 1 THEN 'I' ELSE 'A' END AS Status from Clientes WHERE CodCliente='" + FrameWork.Util.GetString(Session["_uiCodCliente"]) + "'"));

//                        //consulta os contratos
//                        DataTable dtContrato = envio.getContratosPDG(auth, strUsuario, strSenha);
//                        if (dtContrato.Rows.Count == 0)
//                        {
//                            Msg("Contrato não localizado!", false);
//                            return;
//                        }

//                        if (FrameWork.Util.GetString(Request["login-url"]).Length > 0)
//                            Response.Redirect("/?l=" + Request["login-url"], false);
//                        else
//                            Response.Redirect("/", false);

//                    }
//                }
//                catch (Exception ex)
//                {
//                    Msg(ex.Message, false);
//                    return;
//                }
//            }
//            else
//                Session.Abandon();


//            if (!this.IsPostBack)
//            {
//                //Redirecionamento
//                //HttpContext context = HttpContext.Current;
//                //if (!context.Request.IsSecureConnection)
//                //{
//                //    UriBuilder secureUrl = new UriBuilder(context.Request.Url);
//                //    secureUrl.Scheme = "https";
//                //    secureUrl.Port = 443;
//                //    context.Response.Redirect(secureUrl.ToString(), false);
//                //}

              // RegistroVisitas v = new RegistroVisitas();
//                //v.Make(this.Context);
//            }

//        }


//    }

//    protected void btnLogin_Click(object sender, EventArgs e)
//    {
//        lblMensagem.Text = "";
//        if (string.IsNullOrEmpty(Request.Form["cpf"]))
//        {
//            Msg("Favor preencher o campo CPF / CNPJ!", false);
//            return;
//        }
//        else if (string.IsNullOrEmpty(Request.Form["senha"]))
//        {
//            Msg("Favor preencher o campo Senha!", false);
//            return;
//        }
//        else
//        {
//            //System.Data.DataSet userDS = new System.Data.DataSet();
//            //userDS = GetUser(Request.Form["txtUsuario"], (Request.Form["txtSenha"]));
//            //Session["_uiUser"] = Request.Form["txtUsuario"];
//            string strUsuario = FrameWork.Util.GetString(Request.Form["cpf"]).Trim().Replace(".", "").Replace("-","");
//            string strSenha = FrameWork.Util.GetString(Request.Form["senha"]).Trim();
//            if (FrameWork.Util.GetString(strUsuario) == "")
//            {
//                Msg("Favor preencher o campo CPF / CNPJ!", false);
//                return;
//            }
//            else if (FrameWork.Util.GetString(strSenha) == "")
//            {
//                Msg("Favor preencher o campo Senha!", false);
//                return;
//            }

//            if (FrameWork.Util.GetString(strUsuario).Length == 10)
//                strUsuario = "0" + strUsuario;

//            try
//            {

//                AZPortal.ServiceSoapClient envio = new AZPortal.ServiceSoapClient("ServiceSoap1", Config.wsAZ);
//                AZPortal.Auth auth = new AZPortal.Auth();
//                auth.TokenAutn = "integracaoPortal";

//                //1 -AUTENTICA CLIENTE - RETORNA STRING
//                String strAutentica = envio.autenticaCliente(auth, strUsuario, strSenha);
//                //autentica=1,codCliente:138921,nome:CLAUDETE CHOMEN CASTANHA HONORATO
//                //"03039600940", "teste@123" Senha: 89d54a7

//                //"10547239823", "3824a3e" da34a9c

//                if (strAutentica == "autentica=0")
//                {
//                    String sDados = ""; // strUsuario + " - " + strSenha;
//                    Msg("Login ou Senha inválida!" + sDados, false);
//                    return;
//                }
//                else
//                {


//                    // grava as primeiras sessoes
//                    String[] arrAutentica = strAutentica.Split(',');
//                    Session["_uiCodCliente"] = FrameWork.Util.GetString(arrAutentica[1]).Split(':')[1];
//                    Session["_uiNomeCliente"] = FrameWork.Util.GetString(arrAutentica[2]).Split(':')[1];
//                    Session["_uiCPF"] = strUsuario;
//                    Session["_uiSenha"] = strSenha;
//                    Session["_uiFinanceiro"] = FrameWork.Util.GetString(FrameWork.DataObject.DataSource.DefaulDataSource.Execute("SELECT CASE WHEN Financeiro = 1 THEN 'I' ELSE 'A' END AS Status from Clientes WHERE CodCliente='" + FrameWork.Util.GetString(Session["_uiCodCliente"]) + "'"));
//                    //Response.Write("passo 2");
//                    //consulta os contratos
//                    DataTable dtContrato = envio.getContratosPDG(auth, strUsuario, strSenha);
//                    if (dtContrato.Rows.Count == 0)
//                    {
//                        Msg("Contrato não localizado!", false);
//                        return;
//                    }


//                    // Grava o cookie se foi marcado para lembrar
//                    if (FrameWork.Util.GetString(Request.Form["chkConectado"]) == "on")
//                    {
//                        HttpCookie cookie = new HttpCookie("_PDG_pc_login", strUsuario);
//                        cookie.Expires = DateTime.Now.AddMonths(6);
//                        Response.Cookies.Add(cookie);
//                    }


//                    //Log de Acesso
//                    String strSql = "insert into Log_Acessos (CodCliente,Cpf,IP) ";
//                    strSql += "values ('" + Session["_uiCodCliente"] + "','" + strUsuario + "','" + Request.ServerVariables["REMOTE_HOST"] + "')";
//                    FrameWork.DataObject.DataSource.DefaulDataSource.Execute(strSql);


//                    Response.Redirect("/", true);

//                }

//            }
//            catch (Exception ex)
//            {

//                Msg(ex.Message, false);
//                return;
//            }


//        }

//    }

//    protected void btnLembrete_Click(object sender, EventArgs e)
//    {
//        lblMensagem.Text = "";
//        sBox = "lembrete";
//        if (string.IsNullOrEmpty(Request.Form["lembrete_cpf"]))
//        {
//            Msg("Favor preencher o campo CPF / CNPJ!", false);
//            return;
//        }
//        else if (string.IsNullOrEmpty(Request.Form["lembrete_email"]))
//        {
//            Msg("Favor preencher o campo Email!", false);
//            return;
//        }
//        else
//        {


//            string strUsuario = FrameWork.Util.GetString(Request.Form["lembrete_cpf"]).Trim();
//            string strEmail = FrameWork.Util.GetString(Request.Form["lembrete_email"]).Trim();
//            if (FrameWork.Util.GetString(strUsuario) == "")
//            {
//                Msg("Favor preencher o campo CPF / CNPJ!", false);
//                return;
//            }
//            else if (FrameWork.Util.GetString(strEmail) == "")
//            {
//                Msg("Favor preencher o campo Email!", false);
//                return;
//            }

//            try
//            {

//                AZPortal.ServiceSoapClient envio = new AZPortal.ServiceSoapClient("ServiceSoap1", Config.wsAZ);
//                AZPortal.Auth auth = new AZPortal.Auth();
//                auth.TokenAutn = "integracaoPortal";


//                //1 -AUTENTICA CLIENTE - RETORNA STRING
//                String sRetorno = envio.recuperarSenha(auth, strUsuario);

//                //Response.Write("sRetorno: " + sRetorno);
//                //senha:2df7a6a;email:teste@aztronic.com.br
//                //return;
//                //Response.Write(sRetorno);
//                //autentica=1,codCliente:138921,nome:CLAUDETE CHOMEN CASTANHA HONORATO
//                //"03039600940", "teste@123" sRetorno: senha:f349f99;email:teste@aztronic.com.br
//                //"10547239823", "home1010"

//                if (sRetorno == "autentica=0")
//                {
//                    Msg("Login ou Senha inválida!", false);
//                    return;
//                }
//                else
//                {



//                    String[] arrAutentica = sRetorno.Split(';');
//                    String strSenha = FrameWork.Util.GetString(arrAutentica[0]).Split(':')[1];
//                    String strEmailRecupera = FrameWork.Util.GetString(arrAutentica[1]).Split(':')[1];

//                    if (FrameWork.Util.GetString(Request["debug"]) == "true")
//                    {
//                        Response.Write("<!-- " + sRetorno + " -->");
//                        Msg("Modo de debug!", false);
//                        return;
//                    }
//                    else if (strEmail.Trim() != strEmailRecupera.Trim())
//                    {
//                        Msg("E-mail não confere com o cadastrado! ", false);
//                        Response.Write("<!-- " + strEmail.Trim() + " != " + strEmailRecupera.Trim() + " -->");
//                        return;
//                    }


//                    try
//                    {

//                        System.IO.StreamReader objStreamReader;
//                        String strResultadoEnvio = "";

//                        string strAssunto = "Portal do Cliente - Solicitação de senha";

//                        string FilePath = Server.MapPath("template_email/ModeloLembreteSenha.htm");
//                        objStreamReader = System.IO.File.OpenText(FilePath);
//                        string strArq = objStreamReader.ReadToEnd();

//                        strArq = strArq.Replace("#CPF#", strUsuario);
//                        strArq = strArq.Replace("#Senha#", strSenha);
//                        //strArq = strArq.Replace("#Email", strEmail);
//                        strArq = strArq.Replace("#IP#", Request.ServerVariables["REMOTE_ADDR"]);
//                        strArq = strArq.Replace("#DATACADASTRO#", String.Format("{0:dd/MM/yyyy hh:mm}", DateTime.Now));

//                        string pathVirtual = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "template_email/";
//                        strArq = strArq.Replace("src=\"img/", "src=\"" + pathVirtual + "img/");

//                        strResultadoEnvio = FrameWork.Util.GetString(Funcoes.EnvioEmail(strArq, "interacoes@pdg.com.br", strAssunto, strEmail, "", "oliveira@homebrasil.com"));

//                        Msg("Foi enviado ao email cadastrado a senha atual!", true);

//                        objStreamReader.Dispose();
//                        objStreamReader.Close();

//                        return;


//                    }
//                    catch (Exception ex)
//                    {

//                        Msg(ex.Message, false);
//                        return;
//                    }

//                }

//            }
//            catch (Exception ex)
//            {

//                Msg(ex.Message, false);  //Msg("Login ou Senha inválida ou sem comunicação!", false); //Msg(ex.Message, false);
//                return;
//            }




//        }
//    }

//    protected void LimparSession()
//    {
//        Session["_uiCodCliente"] = "";
//        Session["_uiNomeCliente"] = "";
//        Session["_uiCPF"] = "";
//        Session["_uiSenha"] = "";
//        Session["seqContrato"] = "";
//    }

//    protected void Msg(String msg, Boolean tipo)
//    {
//        lblMensagem.CssClass = (tipo ? "box-danger alert alert-success" : "box-danger alert alert-danger");
//        lblMensagem.Text = msg;
//        lblMensagem.Visible = true;
//    }

//}