﻿<%@ WebHandler Language="C#" Class="ws_form" %>

using System;
using System.Web;
using System.Web.Script.Serialization;
using System.IO;
using System.Web.SessionState;
using System.Web.Script.Serialization;
using System.Data;




public class ws_form : IHttpHandler, IReadOnlySessionState
{

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "application/json";

        string json = "";

        // verifica login do usuário
        if (FrameWork.Util.GetInt(context.Session["_uiCodCliente"]) > 0)
        {
            json = "{ \"response\": \"error\",\"message\": \"Sua sessão expirou! <a href='/'>Clique aqui</a> para fazer o login!\" }";
            context.Response.Write(json);
            return;
        }

        string modulo = FrameWork.Util.GetString(context.Request["modulo"]);



        if (modulo == "listafeed")
        {
            context.Response.ContentType = "text/plain";
            //string pathVirtual = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "uploads/documentos/";
            int decValor = 0;
            int decValor1 = 0;
            string strSql = @"select top 40 class,tipo,idtipo,id,titulo,data,chamada,conteudo,imagem,datainclusao,ImgLink,video,link,Link1  
            from (
            select '' as class,'Evento' as tipo,1 as idtipo,id,titulo,data,chamada,conteudo,imagem,datainclusao,CASE    WHEN len(imagem) > 0 THEN ('/uploads/imgeventos/' +imagem)   ELSE('') END AS ImgLink,video,link,('/eventosView?id=' +convert(char,(id))) as Link1  from Eventos where ativo =1 and destaque = 1 union all ";
            strSql += " select '' as class,'PDG Comunica' as tipo,2 as idtipo,id, titulo,data,chamada,conteudo,imagem,datainclusao,CASE    WHEN len(imagem) > 0 THEN ('/uploads/imgpdgcomunica/' +imagem)   ELSE('') END AS ImgLink,video,link,('/comunicadosView?id=' +convert(char,(id))) as Link1  from PDGComunica where ativo = 1 and destaque = 1 union all ";
            strSql += " select '' as class,'PDG em casa' as tipo,3 as idtipo, id,titulo,data,chamada,conteudo,imagem,datainclusao,CASE    WHEN len(imagem) > 0 THEN ('/uploads/imgpdgemcasa/' +imagem)   ELSE('') END AS ImgLink,video,link,('/PDGemcasaView?id=' +convert(char,(id))) as Link1 from Pdgemcasa where ativo = 1 and destaque = 1 union all ";
            strSql += " select '' as class,'Treinamento' as tipo,4 as idtipo, id,titulo,data,chamada,conteudo,imagem,datainclusao,CASE    WHEN len(imagem) > 0 THEN ('/uploads/imgtreinamento/' +imagem)   ELSE('') END AS ImgLink,video,link,('/treinamentoView?id=' +convert(char,(id))) as Link1  from treinamentos where ativo = 1 and destaque = 1 union all  ";
            strSql += " select '' as class,'PDG Integração' as tipo,5 as idtipo, id,titulo,data,chamada,conteudo,imagem,datainclusao,CASE    WHEN len(imagem) > 0 THEN ('/uploads/imgintegracaopdg/' +imagem)   ELSE('') END AS ImgLink,video,link,('/pdgintegracaoView?id=' +convert(char,(id))) as Link1  from integracaopdg where ativo = 1 and destaque = 1 union all ";
            strSql += " select '' as class,'PDG Análises e estudos' as tipo,6 as idtipo, id,titulo,data,chamada,conteudo,imagem,datainclusao,CASE    WHEN len(imagem) > 0 THEN ('/uploads/imgestudospdg/' +imagem)   ELSE('') END AS ImgLink,video,link,('/pdgestudosView?id=' +convert(char,(id))) as Link1  from estudospdg where ativo = 1 and destaque = 1 ";
            strSql += ") as novo order by data desc,datainclusao desc ";


            DataTable dsDados = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSql);

            json = "[";
            foreach (DataRow item in dsDados.Rows)
            {
                //Qtd

                string strSql2 = @"select count(*) as qtd from comentarios where idtipofeed =" + FrameWork.Util.GetString(item["idtipo"]) + " and idFeed = " + FrameWork.Util.GetString(item["id"]) + " and (liberado = 1 or idcolaborador=" + FrameWork.Util.GetInt(context.Session["_uiUserID"]) + ")";
                // strSql2 += "where ativo=1 and idempreendimento=" + iEmpreendimento + " and idmodelo=6 and Ano = '" + iAno + "'  order by sequencia ";
                DataTable dtQtd = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSql2);

                foreach (DataRow item1 in dtQtd.Rows)
                {

                    decValor = FrameWork.Util.GetInt(item1["qtd"]);

                }


                //curtida

                string strSql3 = @"select count(*) as qtd2 from curtidas where idtipofeed =" + FrameWork.Util.GetString(item["idtipo"]) + " and idFeed = " + FrameWork.Util.GetString(item["id"]);
                // strSql2 += "where ativo=1 and idempreendimento=" + iEmpreendimento + " and idmodelo=6 and Ano = '" + iAno + "'  order by sequencia ";
                DataTable dtQtd1 = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSql3);

                foreach (DataRow item2 in dtQtd1.Rows)
                {

                    decValor1 = FrameWork.Util.GetInt(item2["qtd2"]);

                }
                if (json.Length > 3)
                    json += ",";

                json += "{";
                json += "\"id\": \"" + FrameWork.Util.GetString(item["ID"]) + "\", ";
                json += "\"qtd\": \"" + FrameWork.Util.GetString(decValor) + "\", ";
                json += "\"qtd2\": \"" + FrameWork.Util.GetString(decValor1) + "\", ";
                json += "\"titulo\": \"" + FrameWork.Util.GetString(item["titulo"]) + "\", ";
                json += "\"data\": \"" + String.Format("{0:dd/MM/yyyy}", item["data"]) + "\", ";
                json += "\"chamada\": \"" + FrameWork.Util.GetString(item["chamada"]) + "\", ";
                //json += "\"conteudo\": \"" + FrameWork.Util.GetString(item["conteudo"]) + "\", ";
                json += "\"imagem\": \"" + FrameWork.Util.GetString(item["imagem"]) + "\", ";
                json += "\"idtipo\": \"" + FrameWork.Util.GetString(item["idtipo"]) + "\", ";
                json += "\"idcolaborador\": \"" + FrameWork.Util.GetInt(context.Session["_uiUserID"]) + "\", ";
                json += "\"tipo\": \"" + FrameWork.Util.GetString(item["tipo"]) + "\", ";
                json += "\"class\": \"" + FrameWork.Util.GetString(item["class"]) + "\", ";
                json += "\"imglink\": \"" + FrameWork.Util.GetString(item["imglink"]) + "\", ";
                json += "\"video\": \"" + FrameWork.Util.GetString(item["video"]) + "\", ";
                json += "\"link\": \"" + FrameWork.Util.GetString(item["link"]) + "\", ";
                json += "\"link1\": \"" + FrameWork.Util.GetString(item["link1"]) + "\" ";
                json += "}";
            }
            json += "]";

            //id,titulo,data,chamada,conteudo,imagem,idtipo,tipo,class,imglink,video,link1

        }
        else if (modulo == "criarcomentario")
        {
            //inserir
            ClassLibrary.Comentarios objCB = new ClassLibrary.Comentarios();
            // objCB.IDCliente = FrameWork.Util.GetInt(context.Session["_uiCodCliente"]);

            objCB.idFeed = FrameWork.Util.GetInt(context.Request["idfeed"]);
            objCB.IdTipoFeed = FrameWork.Util.GetInt(context.Request["idtipofeed"]);
            objCB.idColaborador = FrameWork.Util.GetInt(context.Request["idcolaborardor"]);
            objCB.Comentario = FrameWork.Util.GetString(context.Request["comentario"]);
            objCB.Liberado = true;
            objCB.DataInclusao = DateTime.Now;
            objCB.Save();


            json = "{ \"response\": \"ok\",\"message\": \"Comentário adicionado com sucesso!\" }";
        }
        else if (modulo == "listartipocurtidas")
        {
            context.Response.ContentType = "text/plain";

            int id = FrameWork.Util.GetInt(context.Request["id"]);
            int idtipo = FrameWork.Util.GetInt(context.Request["idtipo"]);
            // int idcolaborador = FrameWork.Util.GetInt(context.Session["_uiUserID"]);



            //Tipos de curtida




            String strSql = @"select count(c.idtipocurtida) as soma , c.idtipocurtida, tc.TipoCurtida,tc.icone 
                                    from curtidas c inner join TipoCurtida tc on tc.id = c.IdTipoCurtida 
                                    WHERE c.idfeed=" + id + " and c.idtipofeed=" + idtipo + " " +
                                    "group by c.idtipocurtida,tc.tipocurtida,tc.icone ";
            DataTable dsDados = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSql);

            strSql = @"Select c.idtipocurtida, tc.TipoCurtida,tc.icone , UPPER(co.nome) as nome
                                    from curtidas c 
                                    inner join TipoCurtida tc on tc.id = c.IdTipoCurtida
                                    inner join Colaboradores co on co.ID = c.idColaborador
                                      WHERE c.idfeed=" + id + " and c.idtipofeed=" + idtipo;
            DataTable dsDados2 = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSql);


            string sjson = "";
            foreach (DataRow item in dsDados.Rows)
            {
                if (sjson.Length > 3)
                    sjson += ",";
                //23 Jan 5:39 pm
                sjson += "{";
                sjson += "\"Soma\": \"" + FrameWork.Util.GetString(item["soma"]) + "\", ";
                sjson += "\"Icone\": \"" + FrameWork.Util.GetString(item["icone"]) + "\" ";
                sjson += "}";
            }
            string sjson2 = "";
            foreach (DataRow item in dsDados2.Rows)
            {
                if (sjson2.Length > 3)
                    sjson2 += ",";
                //23 Jan 5:39 pm
                sjson2 += "{";
                sjson2 += "\"nome\": \"" + FrameWork.Util.GetString(item["nome"]) + "\", ";
                sjson2 += "\"Icone\": \"" + FrameWork.Util.GetString(item["icone"]) + "\" ";
                sjson2 += "}";
            }

            json += "{\"listasoma\": [" + sjson + "], \"listanomes\": [" + sjson2 + "] }";

        }
        else if (modulo == "criarcurtida")
        {
            int idFeed = FrameWork.Util.GetInt(context.Request["idfeed"]);
            int IdTipoFeed = FrameWork.Util.GetInt(context.Request["idtipofeed"]);
            int idColaborador = FrameWork.Util.GetInt(context.Request["idcolaborardor"]);
            //insere o item do pedido
            ClassLibrary.Curtidas objItem = new ClassLibrary.Curtidas();
            objItem.LoadWhere("idfeed=" + idFeed + " and idtipofeed=" + IdTipoFeed + " and idcolaborador=" + idColaborador);
            if (objItem.TotalLoad == 0)
            {
                //inserir
                ClassLibrary.Curtidas objCB = new ClassLibrary.Curtidas();
                // objCB.IDCliente = FrameWork.Util.GetInt(context.Session["_uiCodCliente"]);

                objCB.idFeed = FrameWork.Util.GetInt(context.Request["idfeed"]);
                objCB.IdTipoFeed = FrameWork.Util.GetInt(context.Request["idtipofeed"]);
                objCB.idColaborador = FrameWork.Util.GetInt(context.Request["idcolaborardor"]);
                objCB.IdTipoCurtida = FrameWork.Util.GetInt(context.Request["tipo"]);
                objCB.Curtido = true;
                objCB.DataInclusao = DateTime.Now;
                objCB.Save();
                json = "{ \"response\": \"ok\",\"message\": \"Comentário adicionado com sucesso!\" }";
            }
            else
            {
                //string strSqlAux1 = "NomeEntregador ='" + entregador + "'";
                string strSql1 = "update curtidas set IdTipoCurtida =" + FrameWork.Util.GetInt(context.Request["tipo"]) + " where idfeed=" + idFeed + " and idtipofeed =" + IdTipoFeed + " and idColaborador=  " + idColaborador;
                FrameWork.DataObject.DataSource.DefaulDataSource.Execute(strSql1);
            }




        }

        else if (modulo == "removercurtida")
        {

            int idFeed = FrameWork.Util.GetInt(context.Request["idfeed"]);
            int IdTipoFeed = FrameWork.Util.GetInt(context.Request["idtipofeed"]);
            int idColaborador = FrameWork.Util.GetInt(context.Request["idcolaborardor"]);


            //string strSqlAux1 = "NomeEntregador ='" + entregador + "'";
            string strSql1 = "delete curtidas where idfeed=" + idFeed + " and idtipofeed =" + IdTipoFeed + " and idColaborador=  " + idColaborador;
            FrameWork.DataObject.DataSource.DefaulDataSource.Execute(strSql1);

            json = "{ \"response\": \"ok\",\"message\": \"Comentário adicionado com sucesso!\" }";
        }
        else if (modulo == "listarcomentarios")
        {
            context.Response.ContentType = "text/plain";

            string id = FrameWork.Util.GetString(context.Request["id"]);
            string idtipo = FrameWork.Util.GetString(context.Request["idtipo"]);
            int idcolaborador = FrameWork.Util.GetInt(context.Session["_uiUserID"]);

            String strSql = @"SELECT c.id, c.idFeed, c.idColaborador, co.Nome, co.Foto, c.IdTipoFeed, c.Comentario, c.DataInclusao, c.Liberado  
                FROM Comentarios AS c INNER JOIN Colaboradores AS co ON co.ID = c.idColaborador 
                WHERE c.idfeed=" + id + " and c.idtipofeed=" + idtipo + " and (liberado = 1 or idcolaborador=" + FrameWork.Util.GetInt(context.Session["_uiUserID"]) + ")" +
                " ORDER BY c.DataInclusao DESC";
            DataTable dsDados = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSql);

            json = "[";
            foreach (DataRow item in dsDados.Rows)
            {
                if (json.Length > 3)
                    json += ",";
                //23 Jan 5:39 pm
                json += "{";
                json += "\"ID\": \"" + FrameWork.Util.GetString(item["ID"]) + "\", ";
                json += "\"IDFeed\": \"" + FrameWork.Util.GetString(item["idFeed"]) + "\", ";
                json += "\"IDColaborador\": \"" + FrameWork.Util.GetInt(context.Session["_uiUserID"]) + "\", ";
                json += "\"Comentario\": \"" + FrameWork.Util.GetString(item["Comentario"]) + "\", ";
                json += "\"Data\": \"" + String.Format("{0:dd MMM HH:mm t}", item["DataInclusao"]) + "\", ";
                json += "\"Nome\": \"" + FrameWork.Util.GetString(item["Nome"]) + "\", ";
                json += "\"Foto\": \"" + FrameWork.Util.GetString(item["Foto"]) + "\" ";
                //json += "\"Tipo\": \"" + FrameWork.Util.GetString(item["Tipo"]) + "\", ";
                //json += "\"Qtd\": \"" + FrameWork.Util.GetString(item["Qtd"]) + "\" ";
                json += "}";
            }
            json += "]";

        }
        else if (modulo == "removerconta")
        {
            int id = FrameWork.Util.GetInt(context.Request["id"]);
            if (id > 0)
            {
                //ClassLibrary.ContasBancarias objCB = new ClassLibrary.ContasBancarias();
                //objCB.Load(id);
                //objCB.excluido = true;
                //objCB.Usuario = FrameWork.Util.GetString(context.Session["_uiCPF"]);
                //objCB.DataAlteracao = DateTime.Now;
                //objCB.Save();
                //json = "{ \"response\": \"ok\",\"message\": \"Conta removida com sucesso!\" }";
            }
            else
            {

            }
        }

        else if (modulo == "vincular")
        {
            int id = FrameWork.Util.GetInt(context.Request["banco"]);
            string hoteis = FrameWork.Util.GetString(context.Request["hoteis"]);
            if (id > 0 && hoteis.Length > 0)
            {
                string[] arr = hoteis.Split(',');

                for (int x = 0; x < arr.Length; x++)
                {
                    //ClassLibrary.Clientes_ContasBancarias objCB = new ClassLibrary.Clientes_ContasBancarias();
                    //objCB.IDContaBancaria = id;
                    //objCB.IDEmpreendimento = FrameWork.Util.GetInt(arr[x].Trim());
                    //objCB.Usuario = FrameWork.Util.GetString(context.Session["_uiCPF"]);
                    //objCB.DataInclusao = DateTime.Now;
                    //objCB.Save();
                }

                json = "{ \"response\": \"ok\",\"message\": \"Vinculado com sucesso!\" }";
            }
            else
            {
                json = "{ \"response\": \"erro\",\"message\": \"Vinculado não realizado!\" }";
            }
        }
        else if (modulo == "alterarconta")
        {
            int id = FrameWork.Util.GetInt(context.Request["id"]);
            int idconta = FrameWork.Util.GetInt(context.Request["idconta"]);
            if (idconta > 0 && id > 0)
            {
                //ClassLibrary.Clientes_ContasBancarias objCB = new ClassLibrary.Clientes_ContasBancarias();
                //objCB.Load(id);
                //objCB.IDContaBancaria = idconta;
                //objCB.Usuario = FrameWork.Util.GetString(context.Session["_uiCPF"]);
                //objCB.DataAlteracao = DateTime.Now;
                //objCB.Save();
                //json = "{ \"response\": \"ok\",\"message\": \"Alteração da conta feita com sucesso!\" }";
            }
            else
            {

            }
        }
        else if (modulo == "desvincular")
        {
            int id = FrameWork.Util.GetInt(context.Request["id"]);
            if (id > 0)
            {
                //ClassLibrary.Clientes_ContasBancarias objCB = new ClassLibrary.Clientes_ContasBancarias();
                //objCB.Load(id);
                //objCB.excluido = true;
                //objCB.Usuario = FrameWork.Util.GetString(context.Session["_uiCPF"]);
                //objCB.DataAlteracao = DateTime.Now;
                //objCB.Save();
                //json = "{ \"response\": \"ok\",\"message\": \"Hotel desvinculado com sucesso!\" }";
            }
            else
            {

            }
        }
        else if (modulo == "notificacao-lida")
        {
            int id = FrameWork.Util.GetInt(context.Request["id"]);
            if (id > 0)
            {
                //ClassLibrary.Notificacoes_Visualizacao objNV = new ClassLibrary.Notificacoes_Visualizacao();
                //objNV.IDNotificacao = id;
                //objNV.IDCliente = FrameWork.Util.GetInt(context.Session["_uiCodCliente"]);
                //objNV.DataInclusao = DateTime.Now;
                //objNV.Save();
                //json = "{ \"response\": \"ok\",\"message\": \"notificação visualiza com sucesso!\" }";
                //context.Session["_uiNotificacoes"] = "-1";
                //context.Session["_uiQtdNotificacoes"] = "";

            }
            else
            {
                json = "{ \"response\": \"erro\",\"message\": \"id incorreto!\" }";
            }
        }
        else if (modulo == "LogMensagens")
        {
            string titulo = FrameWork.Util.GetString(context.Request["titulo"]);
            string mensagem = FrameWork.Util.GetString(context.Request["mensagem"]);

            //grava log da mensagem exibida e aceita
            //ClassLibrary.Log_Mensagens objLog = new ClassLibrary.Log_Mensagens();
            //objLog.AddLog(FrameWork.Util.GetInt(context.Session["_uiCodCliente"]),titulo,mensagem);
        }

        context.Response.Write(json);
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}