﻿using System;
using System.Data;

public partial class faq : System.Web.UI.Page
{
    public ClassLibrary.Paginas objPaginas = new ClassLibrary.Paginas();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {

            objPaginas.Load(3);

            if (objPaginas.TotalLoad == 1)
            {

                ltlTitulo.Text = objPaginas.Titulo;
                ltlConteudo.Text = objPaginas.Conteudo;

            }
            Carrega();
        }
       


    }

    public void Carrega()
    {
        String sCat = "Financiamento";
        string strSql = @"SELECT *  FROM Faq WHERE Situacao='A' and Categoria=@cat ";
        if (FrameWork.Util.GetString(txtPalavra.Text).Length>2)
            strSql += "and (Titulo like @palavra or Texto like @palavra)  ";
        strSql += " order by Posicao ";

        //verifica a categoria na url
        String[] arrUrl = Request.Url.ToString().Replace("http://", "").Split('/');
        if (arrUrl.Length > 2)
            sCat = arrUrl[arrUrl.Length - 1];

        if (sCat == "assitenciatecnica")
        {
            btnAssistenciaTecnica.CssClass += " ativo";
            sCat = "Assistência Técnica";
        }
        else if (sCat == "contratos")
            btnContratos.CssClass += " ativo";
        else if (sCat == "obras")
            btnObras.CssClass += " ativo";
        else if (sCat == "pagamentos")
            btnPagamentos.CssClass += " ativo";
        else
            btnFinanciamento.CssClass += " ativo";


        DataTable dt = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSql,new FrameWork.DataObject.ParameterItem("@cat", sCat), new FrameWork.DataObject.ParameterItem("@palavra", "%" + FrameWork.Util.GetString(txtPalavra.Text) + "%"));

        rptFaq.DataSource = dt;
        rptFaq.DataBind();

    }

    protected void btnBusca_Click(object sender, EventArgs e)
    {
        Carrega();
    }
}