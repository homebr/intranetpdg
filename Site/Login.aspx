﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" ViewStateMode="Disabled" Inherits="Login" %>

<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Bem-vindo a Intranet PDG</title>


     <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>
  <link href="dist/css/bootstrap.css" rel="stylesheet">
  <link href="dist/css/bootstrap-theme.css" rel="stylesheet">
  <link href="dist/css/font-awesome.css" rel="stylesheet">
  <link href="dist/css/animate.css" rel="stylesheet">
  <link href="dist/css/time-circles.css" rel="stylesheet">
  <link href="dist/css/hauze-coming-soon.css" rel="stylesheet">  
  <meta name="google-site-verification" content="tf0V9rRefne2jCpKRaaoPHgeB-_8571CDY7MqkPPzMk" />
  <%--  ---------------------%>

    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link href="skins/all.css?v=1.0.2" rel="stylesheet">
    <link href="skins/flat/yellow.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
 <%--   <link href="css/login.css" rel="stylesheet">--%>
    <script src="js/jquery-1.12.4.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="skins/icheck.js?v=1.0.2"></script>
    <script>
        $(document).ready(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_flat-yellow',
                radioClass: 'iradio_flat-yellow'
            });

            $(window).resize(function () {
                var height = $(document).height();
                $(".box-login").css("height", height);
            });

            var height = $(document).height();
            $(".box-login").css("height", height);
        });
    </script>


</head>

<body>


    <div class="box-login col-md-12">
        <div class="col-md-12">
    <div class="container-fluid">
        <div class="row">

            <div class="col-sm-6 col-md-4 col-lg-4 overlay scrollpoint" data-animation="fadeInLeft" data-delay="1.3s">
                <div class="rodape"> Copyright © 2021 PDG. Todos os direitos reservados.<br>by Homebrasil</div>
                <div class="row">
                  

                        <div class="col-sm-4 col-md-4 col-lg-3 logo" style="float: right; margin-top: 30px;">
                            <img src="dist/img/logo-pdg.png" alt="PDG" class="" width="100%" />
                            <span class="span-logo"><img src="dist/img/intranet.png" height="100%" alt="" /></span>
                        </div>
                      <form id="form1" class="login" action="login" runat="server">
                        <h2><!--Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vestibulum consequat risus.--></h2>
                    <asp:Panel ID="PanelLogin" runat="server" Visible="true">
                        <div class="login-box-body">
                            <p class="login-box-msg"></p>
                         
                                <div class="form-group has-feedback">
                                    <input type="text" class="form-control" id="email" placeholder="Usuário" name="email">
                                </div>
                                <div class="form-group has-feedback">
                                    <input type="password" class="form-control" id="senha" placeholder="Senha" name="senha">
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                             <asp:Button ID="btnLogin" class="btn btn-primary btn-block btn-flat" runat="server" Text="ENTRAR" OnClick="btnLogin_Click" />
                                    <%--    <input type="submit" class="btn btn-primary btn-block btn-flat" name="button" id="button" value="Entrar" />--%>
                                    </div><!-- /.col -->
                                   <%-- <div class="col-xs-6 link">[ REGISTRE-SE ]</div>--%>
                                        <div class="col-xs-6 link">
                                        <label>
                                            <input type="checkbox" id="chkConectado" name="chkConectado">
                                            Mantenha-me conectado
                                        </label>
                                    &nbsp;</div>
                                    <div class="col-xs-6 link2">
                                            <asp:Button ID="btnEsqueci" runat="server" Text="[ ESQUECI MINHA SENHA ]" OnClick="btnEsqueci_Click" /></div>
                                   <div class="col-md-12">
                        <asp:Label ID="lblMensagem" runat="server" CssClass="box-danger alert alert-danger" Style="display: block" Visible="false" />
                    </div>
                                </div>
                          


                        </div><!-- /.login-box-body -->
                        </asp:Panel>
                     <asp:Panel ID="PanelRecuperarSenha" runat="server" Visible="false">
                        <div class="login-box-forgot">
                            <p class="login-box-msg"></p>
                          
                                <div class="form-group has-feedback">
                                    <input type="text" class="form-control" id="emailrecuperar" placeholder="Informe seu email!!!" name="emailrecuperar">
                                </div>
                              
                                <div class="row">
                                    <div class="col-xs-12">
                                             <asp:Button ID="btnLembrete" class="btn btn-primary btn-block btn-flat" runat="server" Text="Recuperar Senha" OnClick="btnLembrete_Click"  />
                                    <%--    <input type="submit" class="btn btn-primary btn-block btn-flat" name="button" id="button" value="Entrar" />--%>
                                    </div><!-- /.col -->
                                   <%-- <div class="col-xs-6 link">[ REGISTRE-SE ]</div>--%>
                                        <div class="col-xs-6 link">
                                      
                                    &nbsp;&nbsp;</div>
                                    <div class="col-xs-6 link2">
                                                             <asp:Button ID="BTNVOLTAR" runat="server" Text="[ VOLTAR TELA DE LOGIN ]" OnClick="BTNVOLTAR_Click" /></div></div>
                                   <div class="col-md-12">
                        <asp:Label ID="Label1" runat="server" CssClass="box-danger alert alert-danger" Style="display: block" Visible="false" />
                    </div>
                                </div>
                          


                        </div><!-- /.login-box-body -->
                        </asp:Panel>
                                 </form>     
                    </div>
                </div>
            </div>
        </div>
    
        


        
        </div>

    </div>





    <script src="/js/jQuery-Cookie/jquery.cookie.min.js"></script>
       <script src="dist/js/jquery.js"></script>
    <script src="dist/js/bootstrap.js"></script>
    <script src="dist/js/jquery.backstretch.min.js"></script>
    <script src="dist/js/waypoints.min.js"></script>    
    <script src="dist/js/time-circles.js"></script> 
    <script>


        function createFormNotified() {
            var formElement = "";
            formElement += '<div id="content-popover-be-notified" class="popover-be-notified-custom-content">'
            formElement += '<form class="form-inline form-be-notified">'
            formElement += '<div class="form-group">'
            formElement += '<input type="email" class="form-control" id="email-field" name="email-field" placeholder="seu.nome@exemplo.com">'
            formElement += '</div>'
            formElement += '<button type="submit" class="btn-notified"><i class="fa fa-send"></i></button>'
            formElement += '<div class="loading"></div>'
            formElement += '<div class="alert alert-email small alert-danger">E-mail inválido!</div>'
            formElement += '<div class="alert small alert-danger">Erro ao criar alerta.</div>'
            formElement += '<div class="alert small alert-success">Alerta criado!</div>'
            formElement += '</form>'
            formElement += '</div>';
            return formElement;
        }




        $(document).ready(function () {

            $.backstretch([
                "dist/img/01.png"
            ], { duration: 6000, fade: 700 });

            $('.scrollpoint').each(function (index) {
                $(this).waypoint(function () {
                    var animation = $(this).attr("data-animation");
                    var delay = $(this).attr("data-delay");
                    $(this).addClass('active');
                    if (delay != 0) {
                        $(this).css("-webkit-animation-delay", delay)
                            .css("-moz-animation-delay", delay)
                            .css("-ms-animation-delay", delay)
                            .css("-o-animation-delay", delay)
                            .css("animation-delay", delay);
                    }
                    $(this).addClass('animated ' + animation);
                }
                    , { offset: '100%' }).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                        $(this).removeClass("scrollpoint").removeClass("active").removeClass("animated");
                    });
            });

            $('[data-toggle="popover-be-notified"]').popover({
                html: true,
                trigger: 'click',
                placement: 'top',
                container: 'body',
                content: createFormNotified
            });

            $('body').on('click', function (e) {
                $('[data-toggle="popover-be-notified"]').each(function () {
                    if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                        $(this).popover('hide');
                    }
                });
            });

            $("#DateCountdown").TimeCircles({
                "animation": "smooth",
                "bg_width": 0.3,
                "fg_width": 0.02,
                "text_size": 0.1,
                "circle_bg_color": "#FFF",
                "time": {
                    "Days": {
                        "text": "Dias",
                        "color": "#ed4b0c",
                        "show": true
                    },
                    "Hours": {
                        "text": "Horas",
                        "color": "#ed4b0c",
                        "show": true
                    },
                    "Minutes": {
                        "text": "Minutos",
                        "color": "#ed4b0c",
                        "show": true
                    },
                    "Seconds": {
                        "text": "Segundos",
                        "color": "#ed4b0c",
                        "show": false
                    }
                }
            });

            fResponsive();
            $(window).resize(function () {
                fResponsive();
            });

            function fResponsive() {
                var screenWidth = $(window).width();
                if (screenWidth <= 767) {
                    $('.social').removeClass("animated");
                    $('.social').removeClass("fadeInDown");
                    $('[data-toggle="tooltip"]').tooltip('destroy');
                    $('.social').insertAfter($('.be-notified'));
                    $('[data-toggle="tooltip"]').tooltip({
                        placement: 'top',
                        container: 'body'
                    });
                } else {
                    $('.social').addClass("animated");
                    $('.social').addClass("fadeInDown");
                    $('[data-toggle="tooltip"]').tooltip('destroy');
                    $('.social').insertBefore($('.container-fluid'));
                    $('[data-toggle="tooltip"]').tooltip({
                        placement: 'left',
                        container: 'body'
                    });
                }

                if (screenWidth <= 550) {
                    $('.time_circles h4').css("font-size", "10px")
                }
            }

            $(document).on('submit', ".form-be-notified", function (event) {
                event.preventDefault();
                event.stopPropagation();

                var email = $('#email-field');
                if (email.val() == '') {
                    $('.popover-be-notified-custom-content .alert-email').show();
                    return false;
                } else {
                    $('.popover-be-notified-custom-content .alert-email').hide();
                }

                var dados = $(this).serialize();

                $.ajax({
                    type: "POST",
                    url: "grava.php",
                    data: dados,
                    success: function (data) {
                        $('.alert-success').show();
                        $('.popover-be-notified-custom-content .alert-email').hide();
                    },
                    error: function () {
                        $('.alert-danger').show();
                        $('.popover-be-notified-custom-content .alert-email').hide();
                    },
                    beforeSend: function () {
                        $(".popover-be-notified-custom-content .loading").show();
                        $(".popover-be-notified-custom-content .btn-notified").hide();
                    },
                    complete: function () {
                        $(".popover-be-notified-custom-content .loading").hide();
                        $(".popover-be-notified-custom-content .btn-notified").show();
                    }
                });

                return false;
            });


        });

        function SomenteNumero(e) {
            var tecla = (window.event) ? event.keyCode : e.which;
            if ((tecla > 47 && tecla < 58)) return true;
            else {
                if (tecla == 8 || tecla == 0) return true;
                else return false;
            }
        }

        $(document).ready(function () {

            $('input[type=text][name=cpf]').tooltip({
                placement: "right",
                trigger: "focus"
            });
            $('.dropup').hover(function () {
                $('.dropdown-toggle', this).trigger('click');
            });

            $('.forgot-pass').click(function (event) {
                $('.login-box-body').removeClass('visible animated fadeInUp');
                $('.login-box-forgot').addClass('visible animated fadeInUp');
            });

            $('.pass-reset').click(function (event) {
                $('.login-box-body').addClass('visible animated fadeInUp');
                $('.login-box-forgot').removeClass('visible animated fadeInUp');
            });


                <% if (sBox == "lembrete")
        { %>
                $('.login-box-body').removeClass('visible animated fadeInUp');
                $('.login-box-forgot').addClass('visible animated fadeInUp');
                <% } %>


                //Checa o cookie se foi marcado para lembrar
                if ($.cookie("_PDG_pc_login") != "undefined") {
                    $("#cpf").val($.cookie("_PDG_pc_login"));
                    $("#chkConectado").prop('checked', true);
                    $(".icheckbox_flat-yellow").addClass("checked");
                }


            });

    </script>
            <!-- Google Analytics-->
    <script>
            //(function (i, s, o, g, r, a, m) {
            //    i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
            //        (i[r].q = i[r].q || []).push(arguments)
            //    }, i[r].l = 1 * new Date(); a = s.createElement(o),
            //    m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
            //})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
            //ga('create', 'UA-83123671-1', 'auto');
            //ga('send', 'pageview');
        </script>
    	<style>
        .login-box-body {
			margin: auto;
			width: 90%;
        }
		.form-control {
			font-size: 15px;
			height: 42px;
		}
		.form-group.has-feedback {
			width: 100%;
		}
		.logo {
			background: #FFF;
			padding: 0 !important;
			max-width: inherit !important;
			margin-right: -15px;
		}
		body{
			overflow: hidden;
		}
		.form-group:focus{
			border: 1px solid #ca4f1f;
		}
		.btn-flat {
			background: #f1a400;
			width: 100%;
			padding: 16px;
			border-radius: 0;
			border: 0 !important;
			font-size: 20px;
			text-transform: uppercase;
			margin-top: 21px;
		}
		.form-control {
			font-size: 19px;
			height: 42px;
			background: none;
			color: #8c80a0 !important;
			border-top: 1px solid #8c80a0 !important;
			border: 0;
			border-radius: 0 !important;
		}
		#email {
			background: url(dist/img/icon-login.png) no-repeat 0 12px;
			padding: 33px 55px 24px 55px;
			margin-top: 92px;
		}
        	#emailrecuperar {
			background: url(dist/img/icon-login.png) no-repeat 0 12px;
			padding: 33px 55px 24px 55px;
			margin-top: 92px;
		}
		#senha{
			background: url(dist/img/icon-senha.png) no-repeat 0 12px;
			padding: 33px 55px 34px 55px;
			border-bottom: 1px solid #8c80a0 !important;
		}
		.span-logo {
			position: absolute;
			left: 100%;
			top: 0;
			height: 100%;
		}
		.rodape{
			background: #3e3e3e;
			width: 100%;
			bottom: 0;
			left: 0;
			padding: 20px;
			position: absolute;
			font-family: Arial;
			font-size: 12px;
			color: #999999;
			text-align: center;
		}
		.link{
			font-size: 12px;
			color: #999999;
			text-align: left;
			margin-top: 20px;
		}
		.link2{
			font-size: 12px;
			color: #999999;
			text-align: right;
			margin-top: 20px;
		}
    </style>
</body>
</html>
