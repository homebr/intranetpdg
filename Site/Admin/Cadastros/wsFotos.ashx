﻿<%@ WebHandler Language="C#" Class="wsFotos" %>

using System;
using System.Web;
using FrameWork.WebControls;
using FrameWork.DataObject;
using ClassLibrary;
using System.Web.SessionState;

public class wsFotos : IHttpHandler, IRequiresSessionState {

    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";

        string acao = FrameWork.Util.GetString(context.Request["acao"]);

        //inserir

        if (acao == "edit")
        {
            ClassLibrary.Fotos foto = new ClassLibrary.Fotos();
            foto.Load(FrameWork.Util.GetInt(context.Request["id"]));
            if (foto.TotalLoad == 1)
            {
                foto.Descricao = FrameWork.Util.GetString(context.Request["desc"]);
                foto.Save();

                context.Response.Write("ok");
            }
        }


    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}