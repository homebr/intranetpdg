﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="CategoriasEditar.aspx.cs" Inherits="Cadastros_CategoriasEditar" ValidateRequest="false" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <!-- ANIMATE -->
    <link rel="stylesheet" type="text/css" href="/Admin/css/animatecss/animate.min.css" />
    <!-- COLORBOX -->
    <link rel="stylesheet" type="text/css" href="/Admin/js/colorbox/colorbox.min.css" />

    <link rel="stylesheet" href="/Admin/css/jquery-ui-1.8.4.custom.css" />

    <!-- BOOTSTRAP SWITCH -->
    <link rel="stylesheet" type="text/css" href="/Admin/js/bootstrap-switch/bootstrap-switch.min.css" />
    <link rel="stylesheet" type="text/css" href="/Admin/css/themes/default.css" id="skin-switcher">

        <!-- Select2 -->
    <link rel="stylesheet" href="/Admin/plugins/select2/select2.min.css">


    <link href="/Admin/components/imgareaselect/css/imgareaselect-default.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="/Admin/css/jquery.awesome-cropper.css">
    <script src="https://cdn.ckeditor.com/4.7.3/standard-all/ckeditor.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="content-header">
        <h1>Categorias
           
                <small>Edição</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i>Home</a></li>
            <li>Categorias</li>
            <li class="active">Edição</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <asp:Label ID="lblMensagem" runat="server" CssClass="box-danger alert alert-danger" Style="display: block" Visible="false"></asp:Label>

            <div class="col-md-12">
                <div class="box box-primary">

                    <form id="form1" runat="server">
                        <div class="box-body">

                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label class="obrigatorio">Nome da Categoria:</label>
                                            <asp:TextBox ID="txtNome" CssClass="form-control" required="required" runat="server" MaxLength="100" Width="100%" placeholder="Nome completo"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-sm-6">
                                            <label>Categoria:</label>
                                            <select id="ddlCategorias" name="ddlCategorias" class="js-example-basic-single form-control" style="width: 100%">
                                                <asp:Literal ID="ltlCategorias" runat="server"></asp:Literal>
                                            </select>                                    
                                        </div>
                                        <div class="col-sm-1">
                                            <label>Posição:</label>
                                            <asp:TextBox ID="txtPosicao" runat="server" CssClass="form-control" MaxLength="10" Width="70px"></asp:TextBox>
                                        </div>
<%--                                        <div class="col-sm-1">
                                            <label>Destaque:</label>
                                            <div>
                                                <div class="make-switch switch-small">
                                                    <asp:CheckBox ID="chkDestaque" runat="server" />
                                                </div>
                                            </div>
                                        </div>--%>
                                        <div class="col-sm-2">
                                            <label>Ativo:</label>
                                            <div>
                                                <div class="make-switch switch-small" data-on="success" data-off="danger" data-on-label="<i class='fa fa-check icon-white'></i>" data-off-label="<i class='fa fa-times'></i>">
                                                    <asp:CheckBox ID="chkAtivo" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                    
                                            <br />
                                                <asp:Literal ID="lblQtd" runat="server" Visible="false" Text=""></asp:Literal>
                                        </div>
                                    </div>
                                </div>
                                <div class="separator"></div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-sm-2">
                                            <label for="ContentPlaceHolder1_lblDataInclusao">Data Inclusão:</label>
                                            <asp:Label ID="lblDataInclusao" runat="server" Text=""></asp:Label>
                                        </div>
                                        <div class="col-sm-2">
                                            <label for="ContentPlaceHolder1_lblDataAlteracao">Data Alteração:</label>
                                            <asp:Label ID="lblDataAlteracao" runat="server" Text=""></asp:Label>
                                        </div>
                                        <div class="col-sm-5">
                                            <label for="ContentPlaceHolder1_lblUsuario">Usuário:</label>
                                            <asp:Label ID="lblUsuario" runat="server" Text=""></asp:Label>
                                        </div>

                                    </div>
                                </div>

                                    <div class="form-group">
                                        <asp:Literal ID="ltlLOG" runat="server"></asp:Literal>
                                    </div>

                            </div>
                        <div class="box-footer">
                                <asp:Button ID="btnSalvar" runat="server" Text="SALVAR" CssClass="btn btn-success  fa-check-circle" OnClick="btnSalvar_Click" />
                                <asp:Button ID="btnExcluir" runat="server" Text="EXCLUIR" CssClass="btn btn-warning fa-delicious" Visible="false" OnClick="btnExcluir_Click" />

                                <asp:Button ID="btnVoltar" runat="server" Text="VOLTAR" CssClass="btn fa-reply" PostBackUrl="Categorias" />

                                <br />
                                <br />
                                <asp:HiddenField ID="hOperacao" runat="server" />
                                <asp:HiddenField ID="hID" runat="server" />
                                <asp:HiddenField ID="hUrlReferrer" runat="server" />
                                <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                            </div>
                    </form>

                </div>

                <asp:Panel ID="pnlFotos" Visible="true" runat="server">
                    <p>

                        <!-- GALERIA -->
                        <div id="filter-items" class="row">

                            <div class="col-md-3 category_1 item">
                                <div class="filter-content">
                                    <asp:Image ID="Image1" runat="server" CssClass="img-responsive" />
                                    <div class="hover-content">
                                        <a class="btn btn-warning hover-link colorbox-button" href='<%=(strImage) %>' title=''>
                                            <i class="fa fa-search-plus fa-1x"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /GALERIA -->
                    </p>
                </asp:Panel>
            </div>

                <div class="footer-tools">
                    <span class="go-top">
                        <i class="fa fa-chevron-up"></i>Top
                    </span>
                </div>



            </div>

    </section>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="Server">
    <!-- DATE RANGE PICKER -->
    <script src="/Admin/js/bootstrap-daterangepicker/moment.min.js"></script>

    <script src="/Admin/js/bootstrap-daterangepicker/daterangepicker.min.js"></script>
    <!-- SLIMSCROLL -->
    <%--    <script type="text/javascript" src="/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.min.js"></script>
    <script type="text/javascript" src="/js/jQuery-slimScroll-1.3.0/slimScrollHorizontal.min.js"></script>--%>
    <!-- BLOCK UI -->
    <script type="text/javascript" src="/Admin/js/jQuery-BlockUI/jquery.blockUI.min.js"></script>
    <!-- BOOTSTRAP SWITCH -->
    <script type="text/javascript" src="/Admin/js/bootstrap-switch/bootstrap-switch.min.js"></script>
    <!-- ISOTOPE -->
    <script type="text/javascript" src="/Admin/js/isotope/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="/Admin/js/isotope/imagesloaded.pkgd.min.js"></script>
    <!-- COLORBOX -->
    <script type="text/javascript" src="/Admin/js/colorbox/jquery.colorbox.min.js"></script>


    <script type="text/jscript" src="/Admin/js/jquery.maskedinput-1.3.1.js"></script>

        <!-- Select2 -->
    <script src="/Admin/plugins/select2/select2.full.min.js"></script>

        <!-- bootstrap color picker -->
    <script src="/Admin/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
    <!-- bootstrap time picker -->
    <script src="/Admin/plugins/timepicker/bootstrap-timepicker.min.js"></script>


    <script type="text/jscript">

        var campo = "ContentPlaceHolder1_";

        $(function () {
            $(".accordion-toggle").click(function () {
                var $this = $(this);
                if ($this.hasClass('icon-chevron-up')) {
                    $this.removeClass('icon-chevron-up').addClass('icon-chevron-down');
                } else {
                    $this.removeClass('icon-chevron-down').addClass('icon-chevron-up');
                }
            });
            return false;
        });


    </script>
    <script src="/Admin/components/imgareaselect/scripts/jquery.imgareaselect.js"></script>
    <script src="/Admin/build/jquery.awesome-cropper.js"></script>
    <script>
        $(document).ready(function () {
            //$('#hfileDestaque').awesomeCropper(
            //{ width: 1980, height: 325, debug: true }
            //);

        });
    </script>


</asp:Content>



