﻿using Microsoft.VisualBasic;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Data.SqlClient;
using FrameWork.WebControls;
using FrameWork.DataObject;
using System.Reflection;
using System.IO;
using System.Drawing;
using ClassLibrary;

partial class DestaquesVitrine : System.Web.UI.Page
{

    //utilizando a propriedade name do hidden
    string pathVirtual = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "uploads/fotos/";
    ClassLibrary.Destaques objDestaque = new ClassLibrary.Destaques();


    protected void Page_Load(object sender, System.EventArgs e)
    {
        //    If Not (Me.Request.IsLocal Or FrameWork.Util.GetString(Session["_uiGrup"]) = "AD" Or Funcoes.VeCheckUserRight(14, "O")) Then
        //        Funcoes.SkinMessage("Sem Acesso", Me.Page)
        //    End If
        if (!this.IsPostBack)
        {
            txtPosicao.Attributes.Add("type", "number");
            txtPosicao.Attributes.Add("min", "1");
            txtPosicao.Attributes.Add("max", "99");

            if (this.Request["id"] != null)
                this.hID.Value = FrameWork.Util.GetString(this.Request["id"]);

            //CARREGA DADOS
            Carrega();
        }


    }



    public void Carrega()
    {
        //OPCAO DE EDICAO PARA VISUALIZAR OS DADOS
        if (FrameWork.Util.GetInt(hID.Value) > 0)
        {

            objDestaque.Load(hID.Value);
            if (objDestaque.TotalLoad > 0)
            {

                txtTitulo.Text = objDestaque.Titulo;
                txtPosicao.Text = FrameWork.Util.GetString(objDestaque.Posicao);
                txtURL.Text = FrameWork.Util.GetString(objDestaque.URL);
                chkAtivo.Checked = (Convert.ToBoolean(objDestaque.Ativo));

                lblUsuario.Text = FrameWork.Util.GetString(objDestaque.Usuario);
                lblDataInclusao.Text = FrameWork.Util.GetString(objDestaque.DataInclusao);
                lblDataAlteracao.Text = FrameWork.Util.GetString(objDestaque.DataAlteracao);

                hOperacao.Value = "UPDATE";

                btnExcluir.Visible = true;
            }
        }

    }




    protected void btnSalvar_Click(object sender, EventArgs e)
    {

        //validação de campos
        if (FrameWork.Util.GetString(txtTitulo.Text) == "")
        {
            Msg("Favor inserir um título!", false);
            txtTitulo.Focus();
            return;
        }


        if (FrameWork.Util.GetInt(hID.Value) > 0)
        {
            //Comandos de Atualização do Empreendimento
            objDestaque.Load(hID.Value);
            objDestaque.DataAlteracao = DateTime.Now;

        }
        else
        {
            hOperacao.Value = "INSERT";
            objDestaque.DataInclusao = DateTime.Now;
        }


        objDestaque.Titulo = this.txtTitulo.Text;
        objDestaque.URL = this.txtURL.Text;
        
        objDestaque.Ativo = chkAtivo.Checked;
        objDestaque.Posicao = FrameWork.Util.GetInt(txtPosicao.Text);
        objDestaque.Usuario = (string)this.Session["_uiUser"];


        try
        {
            objDestaque.Save();
            this.hID.Value = objDestaque.ID.ToString();

            hOperacao.Value = "UPDATE";
            Msg("Salvo com sucesso!", true);
            //this.abaFotos.Visible = true;

        }
        catch (Exception ex)
        {
            Msg(ex.Message, false);
        }


    }

    protected void btnExcluir_Click(object sender, EventArgs e)
    {

        try
        {
            objDestaque.Load(hID.Value);
            objDestaque.Delete();
            Response.Redirect("Destaques");
        }
        catch (Exception ex)
        {
            Msg(ex.Message, false);
        }
    }


    public static Bitmap ResizeImage(System.Drawing.Image image, int width, int height)
    {
        var destRect = new Rectangle(0, 0, width, height);
        var destImage = new Bitmap(width, height);

        destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

        using (var graphics = Graphics.FromImage(destImage))
        {
            graphics.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;
            graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            graphics.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;

            using (var wrapMode = new System.Drawing.Imaging.ImageAttributes())
            {
                wrapMode.SetWrapMode(System.Drawing.Drawing2D.WrapMode.TileFlipXY);
                graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
            }
        }

        return destImage;
    }


    protected void Msg(String msg, Boolean tipo)
    {
        lblMensagem.CssClass = (tipo ? "box-danger alert alert-success" : "box-danger alert alert-danger");
        lblMensagem.Text = msg;
        lblMensagem.Visible = true;
    }


}
