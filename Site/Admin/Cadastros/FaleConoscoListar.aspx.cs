﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClassLibrary;
using FrameWork.WebControls;
using FrameWork.DataObject;


public partial class FaleConosco_Listar : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            //txtDataInicial.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddMonths(-6));
            //txtDataFinal.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            txtDataInicial.Text = FrameWork.Util.GetString(Session["Noticias_DataInicial"]);
            txtDataFinal.Text = FrameWork.Util.GetString(Session["Noticias_DataFinal"]);

        }
        Carrega();
    }

    public void Carrega()
    {

        String strSQL = "SELECT        ID, Nome, Email, Cargo, Assunto, Mensagem, DataInclusao, XmlLog FROM    FaleConosco f where 1=1  ";
        if (FrameWork.Util.GetString(TextBox1.Text).ToString().Trim() != "")
        {
            strSQL += " and (f.nome LIKE '%" + TextBox1.Text + "%' or f.mensagem LIKE '%" + TextBox1.Text + "%') ";
            Session["Noticias_TextBox1"] = TextBox1.Text;
        }
        if (Funcoes.IsDate(txtDataInicial.Text))
        {
            //FlagFiltro = True
            strSQL += " and Convert(char(8),f.datainclusao,112) >= '" + String.Format("{0:yyyyMMdd}", Convert.ToDateTime(txtDataInicial.Text)) + "'  ";
        }
        if (Funcoes.IsDate(txtDataFinal.Text))
        {
            //FlagFiltro = True
            strSQL += " and Convert(char(8),f.datainclusao,112) <= '" + String.Format("{0:yyyyMMdd}", Convert.ToDateTime(txtDataFinal.Text)) + "'  ";
        }
  

        strSQL += " ORDER BY f.ID DESC ";

        //Response.Write(strSQL);


        DataTable dt = new DataTable();
        dt = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSQL, new ParameterItem("@text", TextBox1.Text));



        Repeater1.DataSource = dt;
        Repeater1.DataBind();



    }
}

