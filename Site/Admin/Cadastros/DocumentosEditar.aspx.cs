﻿using System;
using System.IO;
using System.Drawing;
using System.Data;
using FrameWork;
using FrameWork.DataObject;
using FrameWork.WebControls;

public partial class Cadastros_DocumentosEditar : System.Web.UI.Page
{
    ClassLibrary.Documentos objCases = new ClassLibrary.Documentos();
    public String strImage = "";
    string pathVirtual = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "uploads/documentos/";
    String pathFisico = System.Configuration.ConfigurationManager.AppSettings["pathFisico"] + "uploads\\documentos\\";


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(this.IsPostBack))
        {

                  if (this.Request["id"] != null)
                this.hID.Value = FrameWork.Util.GetString(this.Request["id"]);
            // CarregaPasta();
            CarregaSetor();
            Carrega();
           

        }
    }


    public void CarregaSetor()
    {

        String strSql = "SELECT ID,Setor ,'1' as cat FROM Area  union all SELECT 0 AS ID,'[SELECIONE O SETOR]' as area,'0' as cat order by cat,setor  ";
        ddlarea.DataSource = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSql);
        ddlarea.DataValueField = "ID";
        ddlarea.DataTextField = "setor";
        ddlarea.DataBind();

    }
    public void Carrega()
    {
        //OPCAO DE EDICAO PARA VISUALIZAR OS DADOS
        if (FrameWork.Util.GetInt(hID.Value) > 0)
        {
            objCases.Load(this.hID.Value);

            //REGISTRO LOCALIZADO ENTAO APENAS EDITA DADOS DO FORMULARIO
            if (objCases.TotalLoad == 1)
            {

                txtTitulo.Text = objCases.Descricao;
                // txtDescricao.Text = FrameWork.Util.GetString(objCases.Descricao);


                if (ddlarea.Items.FindByValue(FrameWork.Util.GetString(objCases.Area)) != null)
                    ddlarea.SelectedValue = FrameWork.Util.GetString(objCases.Area);



                if (ddltipo.Items.FindByValue(FrameWork.Util.GetString(objCases.Tipo)) != null)
                    ddltipo.SelectedValue = FrameWork.Util.GetString(objCases.Tipo);
                //if (ddlPasta.Items.FindByValue(FrameWork.Util.GetString(objCases.Pasta)) != null)
                //    ddlPasta.SelectedValue = FrameWork.Util.GetString(objCases.Pasta);
                // if (ddlSegmento.Items.FindByText(FrameWork.Util.GetString(objCases.Segmento)) != null)
                //ddlSegmento.SelectedValue = FrameWork.Util.GetString(objCases.Segmento);


                if (FrameWork.Util.GetString(objCases.Arquivo).Length > 1)
                {
                  //  strImage = pathVirtual + FrameWork.Util.GetString(objCases.Arquivo);
                   // this.Image1.ImageUrl = strImage+"?t=" + String.Format("{0:yyyyMMddHHmm}",DateTime.Now);
                }

              
                chkAtivo.Checked = (Convert.ToBoolean(objCases.Ativo));

                //lblUsuario.Text = FrameWork.Util.GetString(objCases.Usuario);
                txtData.Text = FrameWork.Util.GetString(objCases.Data);
                lblDataInclusao.Text = FrameWork.Util.GetString(objCases.DataInclusao);
                lblDataAlteracao.Text = FrameWork.Util.GetString(objCases.DataAlteracao);

                hOperacao.Value = "UPDATE";

                btnExcluir.Visible = true;

                // CarregaFotos();
                string pathVirtual = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "uploads/documentos/";
                String strUsuario = FrameWork.Util.GetString(Session["_uiUser"]);

                string strSql = "SELECT *,('" + pathVirtual + "' + Arquivo) as Link FROM Documentos where id=   " + FrameWork.Util.GetInt(hID.Value);
                DataTable dt = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSql);
                Repeater1.DataSource = dt;
                Repeater1.DataBind();
            }
            else
            {
                //SENAO ACHOU O CODIGO EM QUESTAO É INSERÇÃO

            }

        }
        else
        {
            //ZERA TODOS OS CAMPOS DO FORMULARIO

        }
    }
    


    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        //validação de campos
        if (FrameWork.Util.GetString(txtTitulo.Text) == "")
        {
            Msg("Favor inserir um Título!", false);
            txtTitulo.Focus();
            return;
        }

        if (FrameWork.Util.GetString(txtTitulo.Text) == "Selecione")
        {
            Msg("Favor inserir o tipo do Arquivo!", false);
            ddltipo.Focus();
            return;
        }


        if (FrameWork.Util.GetInt(hID.Value) > 0)
        {
            //Comandos de Atualização do Empreendimento
            objCases.Load(hID.Value);
            objCases.DataAlteracao = DateTime.Now;

        }
        else
        {
            hOperacao.Value = "INSERT";
            objCases.DataInclusao = DateTime.Now;
        }


        //salva os valores
        objCases.Area = Convert.ToInt32(this.ddlarea.SelectedValue);
        objCases.Tipo = Convert.ToString(this.ddltipo.SelectedValue);
        //objCases.Pasta = Convert.ToInt32(this.ddlPasta.SelectedValue);
        //objCases.Segmento = (ddlSegmento.SelectedValue);

        objCases.Descricao = this.txtTitulo.Text;
        objCases.Data = Convert.ToDateTime(this.txtData.Text);
        objCases.Ativo = chkAtivo.Checked;
       // objCases.Sequencia = FrameWork.Util.GetInt(txtPosicao.Text);
       // objCases.Usuario = (string)this.Session["_uiUser"];
 
  

        try
        {
            objCases.Save();
            this.hID.Value = objCases.ID.ToString();

            hOperacao.Value = "UPDATE";
            Msg("Salvo com sucesso!", true);

        }
        catch (Exception ex)
        {
            Msg(ex.Message, false);
        }

        try
        {


            if ((txtArquivo.HasFile))
            {

                //inicializar as variáveis
                string arq = txtArquivo.PostedFile.FileName;
                string fileExtension = txtArquivo.PostedFile.ContentType.ToLower();

                if (!string.IsNullOrEmpty(arq))
                {


                    fileExtension = Path.GetExtension(arq);
                    string str_image = "" + Funcoes.RemoveAcentos(FrameWork.Util.GetString(txtTitulo.Text).Replace(" ", " ").ToLower()) +  "_" + hID.Value  + fileExtension;  // + "_" + FrameWork.Security.FromBase64(String.Format("{0:ddMMyyyyHHmm}", DateTime.Now))

                    //string str_image = "documento-" + hID.Value + fileExtension.ToLower();

                    //verificamos a extensão através dos últimos 4 caracteres e verificamos se é permitido
                    //if ((!(fileExtension == ".gif" || fileExtension == ".jpg" || fileExtension == ".png")))
                    //{
                    //    Msg("Extensão de arquivo não permitido! Somente é permitido extensão: gif, jpg ou png!", false);
                    //    return;
                    //}


                    //tamanho maximo do upload em kb
                    double permitido = 2000;
                    //identificamos o tamanho do arquivo
                    double tamanho = 0;
                    tamanho = Convert.ToDouble(txtArquivo.PostedFile.ContentLength) / 1024;
                    //if ((tamanho > permitido))
                    //{
                    //    Msg("Tamanho Máximo permitido é de " + permitido + " kb", false);
                    //    return;
                    //}

                    //caminho fisico no server
                    string pathPDFs = System.Configuration.ConfigurationManager.AppSettings["pathFisico"] + "uploads\\documentos\\";

                    txtArquivo.SaveAs(pathPDFs + str_image);

                    String strSql = "UPDATE Documentos SET Arquivo = '" + str_image + "' WHERE ID=" + hID.Value;
                    FrameWork.DataObject.DataSource.DefaulDataSource.Execute(strSql);

                    txtArquivo.Dispose();

                   // strImage = pathVirtual + str_image;
                    //this.Image1.ImageUrl = strImage + "?t=" + String.Format("{0:yyyyMMddHHmm}", DateTime.Now);
                }

            }
         


            

        }
        catch (Exception ex)
        {
            Msg(ex.Message, false);
            return;
        }
        Response.Redirect("Documentos");

    }

    protected void btnExcluir_Click(object sender, EventArgs e)
    {

        try
        {
            objCases.Load(hID.Value);
            objCases.Delete();
            Response.Redirect("Cases");
        }
        catch (Exception ex)
        {
            Msg(ex.Message, false);
        }
    }

    protected void Msg(String msg, Boolean tipo)
    {
        lblMensagem.CssClass = (tipo ? "box-danger alert alert-success" : "box-danger alert alert-danger");
        lblMensagem.Text = msg;
        lblMensagem.Visible = true;
    }

    public static Bitmap ResizeImage(Image image, int width, int height)
    {
        var destRect = new Rectangle(0, 0, width, height);
        var destImage = new Bitmap(width, height);

        destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

        using (var graphics = Graphics.FromImage(destImage))
        {
            graphics.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;
            graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            graphics.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;

            using (var wrapMode = new System.Drawing.Imaging.ImageAttributes())
            {
                wrapMode.SetWrapMode(System.Drawing.Drawing2D.WrapMode.TileFlipXY);
                graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
            }
        }

        return destImage;
    }
}