﻿<%@ WebHandler Language="C#" Class="FileUpload" %>

using System;
using System.Web;
using System.IO;
using System.Web.SessionState;
using System.Drawing;


public class FileUpload : IHttpHandler, IRequiresSessionState
{

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";
        System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");

        string dirFullPath = System.Configuration.ConfigurationManager.AppSettings["pathFisico"] + "uploads\\fotos\\";


        int id = FrameWork.Util.GetInt(context.Request["id"]);
        string modulo = FrameWork.Util.GetString(context.Request["modulo"]);

        if (modulo == "cases")
        {
            string[] files;
            int numFiles;

            dirFullPath = dirFullPath.Replace("fotos","cases");
            files = System.IO.Directory.GetFiles(dirFullPath);
            numFiles = files.Length;
            numFiles = numFiles + 1;


            string str_image = "";

            foreach (string s in context.Request.Files)
            {
                HttpPostedFile file = context.Request.Files[s];
                string fileName = file.FileName;
                string fileExtension = file.ContentType.ToLower();

                if (!string.IsNullOrEmpty(fileName))
                {
                    fileExtension = Path.GetExtension(fileName);
                    if ((!(fileExtension == ".gif" || fileExtension == ".jpg" || fileExtension == ".png")))
                    {
                        throw new System.InvalidOperationException("Extensão de arquivo não permitido! Somente é permitido extensão: gif, jpg ou png!");
                    }


                    str_image = id + "_" + numFiles.ToString() + fileExtension.ToLower();
                    string pathToSave_100 = dirFullPath + str_image;
                    file.SaveAs(pathToSave_100);

                    string strSql = "insert into Fotos (CaseID,Posicao,FileName,FileNameOriginal) ";
                    strSql += " values (" + id + "," + numFiles.ToString() + ",'" + str_image.ToString() + "','" + fileName.ToString() + "') ";
                    FrameWork.DataObject.DataSource.DefaulDataSource.Execute(strSql);


                    //verifica orientação e rotação da foto e salva corretamente se necessário
                    try
                    {
                        using (var image = Image.FromFile(pathToSave_100))
                        {
                            foreach (var prop in image.PropertyItems)
                            {
                                if (prop.Id == 0x112)
                                {
                                    image.RotateFlip(OrientationToFlipType(prop.Value[0]));
                                    image.Save(pathToSave_100);
                                }
                            }
                        }

                    }
                    catch
                    {

                    }


                }
            }
            context.Response.Write(str_image);

        }

    }


    private static RotateFlipType OrientationToFlipType(int orientation)
    {
        switch (orientation)
        {
            case 1:
                return RotateFlipType.RotateNoneFlipNone;
                break;
            case 2:
                return RotateFlipType.RotateNoneFlipX;
                break;
            case 3:
                return RotateFlipType.Rotate180FlipNone;
                break;
            case 4:
                return RotateFlipType.Rotate180FlipX;
                break;
            case 5:
                return RotateFlipType.Rotate90FlipX;
                break;
            case 6:
                return RotateFlipType.Rotate90FlipNone;
                break;
            case 7:
                return RotateFlipType.Rotate270FlipX;
                break;
            case 8:
                return RotateFlipType.Rotate270FlipNone;
                break;
            default:
                return RotateFlipType.RotateNoneFlipNone;
        }
    }


    //private System.Drawing.Bitmap rotateImage90(System.Drawing.Bitmap b)
    //{
    //    System.Drawing.Bitmap returnBitmap = new System.Drawing.Bitmap(b.Height, b.Width);
    //    System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(returnBitmap);
    //    g.TranslateTransform((float)b.Width / 2, (float)b.Height / 2);
    //    g.RotateTransform(90);
    //    g.TranslateTransform(-(float)b.Width / 2, -(float)b.Height / 2);
    //    g.DrawImage(b, new System.Drawing.Point(0, 0));
    //    return returnBitmap;
    //}


    public bool IsReusable
    {
        get
        {
            return false;
        }
    }



}