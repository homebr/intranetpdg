﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClassLibrary;
using FrameWork.WebControls;
using FrameWork.DataObject;

public partial class Cadastros_Documentos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(this.IsPostBack))
        {
          
            txtDataInicial.Text = FrameWork.Util.GetString(Session["Noticias_DataInicial"]);
            txtDataFinal.Text = FrameWork.Util.GetString(Session["Noticias_DataFinal"]);
        }
        
        Carrega();
    }

    
    public void Carrega()
    {
       
        string pathVirtual = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "uploads/documentos/";
        String strUsuario = FrameWork.Util.GetString(Session["_uiUser"]);

        string strSql = "SELECT *,('" + pathVirtual + "' + c.Arquivo) as Link, a.setor as setorempresa  FROM Documentos c   left join area a on a.id = c.area    where 1=1  ";
        if (FrameWork.Util.GetString(TextBox1.Text).ToString().Trim() != "")
        {
            strSql += " and (c.descricao LIKE '%" + TextBox1.Text + "%' ) ";
            //Session["Noticias_TextBox1"] = TextBox1.Text;
        }
      

      
        if (Funcoes.IsDate(txtDataInicial.Text))
        {
            //FlagFiltro = True
            strSql += " and Convert(char(8),c.data,112) >= '" + String.Format("{0:yyyyMMdd}", Convert.ToDateTime(txtDataInicial.Text)) + "'  ";
        }
        if (Funcoes.IsDate(txtDataFinal.Text))
        {
            //FlagFiltro = True
            strSql += " and Convert(char(8),c.data,112) <= '" + String.Format("{0:yyyyMMdd}", Convert.ToDateTime(txtDataFinal.Text)) + "'  ";
        }
        strSql += " ORDER BY c.data desc";
       // Response.Write(strSql);
       // Response.End();
        DataTable dt = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSql);
        Repeater1.DataSource = dt;
        Repeater1.DataBind();

        if (dt.Rows.Count > 0)
        {


            //pnlComprovantes.Visible = false;
            // ltlTextoEndereco.Text = "Aguardando validação manualmente na PDG!";

        }

        



    }
}