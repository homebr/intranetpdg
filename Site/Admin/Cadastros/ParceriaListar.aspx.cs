﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClassLibrary;
using FrameWork.WebControls;
using FrameWork.DataObject;

public partial class Cadastros_ParceriaListar : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Carrega();
    }


    public void Carrega()
    {

        string pathPDFs = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "uploads/logoParceria/";

        String strSQL = "SELECT  *,('" + pathPDFs + "' + Logo) as logo FROM Parceria WHERE 1=1  ";
        if (FrameWork.Util.GetString(TextBox1.Text).ToString().Trim() != "")
            strSQL += " and (nome LIKE @text) ";

        strSQL += " ORDER BY Sequencia  ";

        //Response.Write(strSQL);


        DataTable dt = new DataTable();
        dt = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSQL, new ParameterItem("@text", TextBox1.Text));



        Repeater1.DataSource = dt;
        Repeater1.DataBind();



    }
}