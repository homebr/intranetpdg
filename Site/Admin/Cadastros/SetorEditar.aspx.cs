﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using FrameWork.DataObject;
using FrameWork.WebControls;
using FrameWork;
using ClassLibrary;
using System.Drawing.Imaging;
using System.Drawing;

public partial class Setor_Editar : System.Web.UI.Page
{

    ClassLibrary.Area objNoticias = new ClassLibrary.Area();
    public String strImage = "";
    //caminho fisico no server
    string pathFisico = System.Configuration.ConfigurationManager.AppSettings["pathFisico"] + "uploads\\ImgEventos\\";
    string pathVirtual = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "uploads/ImgEventos/";


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(this.IsPostBack))
        {
       

            //ID DO EMPREENDIMENTO
            //SE NAO HA NADA DENTRO DO CONTROLE HIDDEN PEGA VIA REQUEST
            if (this.Request["id"] != null)
                this.hID.Value = Convert.ToString(this.Request["id"]);

            //CARREGA DADOS
           // CarregaCategorias();
            CarregaEventos();

            //this.txtPreco.Attributes.Add("onkeypress", "formatar_mascara(this,'########,##')");
        }
    }

    public void CarregaEventos()
    {
        //OPCAO DE EDICAO PARA VISUALIZAR OS DADOS
        if (hID.Value != "")
        {
            objNoticias.Load(this.hID.Value);
            string strSql = "SELECT * FROM Area " +
                            "WHERE ID = " + this.hID.Value;

            DataTable dt = new DataTable();
            dt = DataSource.DefaulDataSource.Select(strSql);

            //REGISTRO LOCALIZADO ENTAO APENAS EDITA DADOS DO FORMULARIO
            if (dt.Rows.Count > 0)
            {

                //coloca os dados para edição
                hID.Value = dt.Rows[0]["ID"].ToString();
                this.lblNoticiaID.Text = dt.Rows[0]["ID"].ToString();


                this.txtArea.Text = FrameWork.Util.GetString(dt.Rows[0]["Setor"]);
              
                chkAtivo.Checked = (Convert.ToBoolean(dt.Rows[0]["Ativo"]));
              
                lblDataInclusao.Text = Convert.ToString(dt.Rows[0]["DataInclusao"]);
                lblDataAlteracao.Text = Convert.ToString(dt.Rows[0]["DataAlteracao"]);
           


                hOperacao.Value = "UPDATE";

                // CarregaFotos();

            }
            else
            {
                //SENAO ACHOU O CODIGO EM QUESTAO É INSERÇÃO
                // btnNovaNoticia_Click(null, null);

            }
        }
        else
        {
            //ZERA TODOS OS CAMPOS DO FORMULARIO
            //btnNovaNoticia_Click(null, null);

        }
    }

    //public void CarregaCategorias()
    //{
    //    string strSql = "SELECT ID,Categoria FROM CategoriaNoticia where Ativo=1  ";
    //    ddlCategoria.DataSource = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSql);
    //    ddlCategoria.DataTextField = "Categoria";
    //    ddlCategoria.DataValueField = "ID";
    //    ddlCategoria.DataBind();
    //}


    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        //validação de campos
        if (FrameWork.Util.GetString(txtArea.Text) == "")
        {
            Msg("Favor inserir um Título!", false);
            txtArea.Focus();
            return;
        }


        if (hOperacao.Value == "UPDATE")
        {
            //Comandos de Atualização do Empreendimento
            objNoticias.LoadWhere("ID=@ID", new ParameterItem("@ID", hID.Value));
            objNoticias.DataAlteracao = DateTime.Now;

        }
        else
            objNoticias.DataInclusao = DateTime.Now;

       // objNoticias.Tipo = Convert.ToInt32(this.ddlTipo.SelectedValue);
        objNoticias.Setor = this.txtArea.Text;
  
        //objNoticias.Fonte = txtFonte.Text;
        objNoticias.Ativo = (chkAtivo.Checked);
 
        objNoticias.Usuario = (string)this.Session["_uiUser"];

       

        try
        {
            objNoticias.Save();
            this.hID.Value = objNoticias.ID.ToString();

            //ATUALIZA A TELA 
            //this.Image1.ImageUrl = FrameWork.Util.GetString(objBanners.Imagem"]);
            //IMAGENS 





            hOperacao.Value = "UPDATE";
            Msg("Salvo com sucesso!", true);
            //this.abaFotos.Visible = true;
         
        }
        catch (Exception ex)
        {
            Msg(ex.Message, false);
        }
    }
       

    protected void btnExcluir_Click(object sender, EventArgs e)
    {
        try
        {
            //Comandos de excluir do Empreendimento
            objNoticias.LoadWhere("ID=@ID", new ParameterItem("@ID", hID.Value));
            objNoticias.Delete();

            Response.Redirect("setorListar");

        }
        catch (Exception ex)
        {

            Msg(ex.Message, false);
        }
    }

    protected void btnNovaNoticia_Click(object sender, EventArgs e)
    {
        ////ZERA TODOS OS CAMPOS DO FORMULARIO

        //ddlSessao.SelectedIndex = 0;

        this.lblNoticiaID.Text = "";
        this.txtArea.Text = "";

       // this.txtAutor.Text = "";
        //this.txtAutorFoto.Text = "";
        //txtPagina.Text = "";

        lblNoticiaID.Text = "";


        lblDataInclusao.Text = "";
        lblDataAlteracao.Text = "";
        lblUsuario.Text = "";

        this.hOperacao.Value = "INSERT";
        this.hID.Value = "";

       // btnNovaNoticia.Visible = false;
       // abaFotos.Visible = false;

        btnExcluir.Visible = false;

    }

    protected void Msg(String msg,Boolean tipo)
    {
        lblMensagem.CssClass = (tipo ? "box-danger alert alert-success" : "box-danger alert alert-danger");
        lblMensagem.Text = msg;
        lblMensagem.Visible = true;
    }


}