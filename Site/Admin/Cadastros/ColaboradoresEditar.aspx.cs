﻿using System;
using System.IO;

public partial class Cadastros_ColaboradoresEditar : System.Web.UI.Page
{
    ClassLibrary.Colaboradores objColaboradores = new ClassLibrary.Colaboradores();
    public String strImage = "";
    string pathVirtual = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "uploads/colaboradores/";


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(this.IsPostBack))
        {

            //txtPosicao.Attributes.Add("type", "number");
            //txtPosicao.Attributes.Add("min", "1");
            //txtPosicao.Attributes.Add("max", "99");

            //CarregaDepartamentos();

            ////Direitos do Usuário
            //if (!this.UserInfo.Admin && !CheckUserRight(101, "C"))
            //{
            //    Response.Redirect("Default.aspx");
            //}
            //if (!this.UserInfo.Admin && !CheckUserRight(101, "I"))
            //{
            //    this.btnNovaNoticia.Visible = false;
            //}
            //if (!this.UserInfo.Admin && !CheckUserRight(101, "E"))
            //{
            //    this.btnSalvar.Visible = false;
            //}

            //ID DO EMPREENDIMENTO
            //SE NAO HA NADA DENTRO DO CONTROLE HIDDEN PEGA VIA REQUEST
            if (this.Request["id"] != null)
                this.hID.Value = FrameWork.Util.GetString(this.Request["id"]);

            //CARREGA DADOS
            CarregaSetor();
            CarregaRegional();
            Carrega();

        }
    }

    public void Carrega()
    {
        //OPCAO DE EDICAO PARA VISUALIZAR OS DADOS
        if (FrameWork.Util.GetInt(hID.Value) > 0)
        {
            objColaboradores.Load(this.hID.Value);

            //REGISTRO LOCALIZADO ENTAO APENAS EDITA DADOS DO FORMULARIO
            if (objColaboradores.TotalLoad == 1)
            {

                txtNome.Text = objColaboradores.Nome;
                txtCargo.Text = objColaboradores.Cargo;
                txtDescricao.Text = FrameWork.Util.GetString(objColaboradores.Descricao);
               // txtFormacao.Text = FrameWork.Util.GetString(objColaboradores.Formacao);
                txtCPF.Text = FrameWork.Util.GetString(objColaboradores.Cpf);
                txtloginRede.Text = FrameWork.Util.GetString(objColaboradores.LoginRede);
                txtRamal.Text = FrameWork.Util.GetString(objColaboradores.Ramal);
                txtSenha.Text = FrameWork.Security.DeCript(objColaboradores.Senha);
                txtaniversario.Text = String.Format("{0:dd/MM/yyyy}", objColaboradores.DataAniversario);
                txtAdmissao.Text = String.Format("{0:dd/MM/yyyy}", objColaboradores.DataAdmissao);
                if (ddlsetor.Items.FindByValue(FrameWork.Util.GetString(objColaboradores.Setor)) != null)
                    ddlsetor.SelectedValue = FrameWork.Util.GetString(objColaboradores.Setor);

                if (ddlregional.Items.FindByValue(FrameWork.Util.GetString(objColaboradores.Regional)) != null)
                    ddlregional.SelectedValue = FrameWork.Util.GetString(objColaboradores.Regional);

                txtEmail.Text = FrameWork.Util.GetString(objColaboradores.Email);
                txtLinkedin.Text = FrameWork.Util.GetString(objColaboradores.Linkedin);

                if (FrameWork.Util.GetString(objColaboradores.Foto).Length > 1)
                {
                    strImage = pathVirtual + FrameWork.Util.GetString(objColaboradores.Foto);
                    this.Image1.ImageUrl = strImage+"?t=" + String.Format("{0:yyyyMMddHHmm}",DateTime.Now);
                }


                //txtPosicao.Text = FrameWork.Util.GetString(objColaboradores.Sequencia);
                chkAtivo.Checked = (Convert.ToBoolean(objColaboradores.Ativo));
                //if (this.ddlSituacao.Items.FindByValue(objColaboradores.Situacao) != null)
                //    this.ddlSituacao.SelectedValue = FrameWork.Util.GetString(objColaboradores.Situacao);

                lblUsuario.Text = FrameWork.Util.GetString(objColaboradores.Usuario);
                lblDataInclusao.Text = FrameWork.Util.GetString(objColaboradores.DataInclusao);
                lblDataAlteracao.Text = FrameWork.Util.GetString(objColaboradores.DataAlteracao);
             

                hOperacao.Value = "UPDATE";

                btnExcluir.Visible = true;


            }
            else
            {
                //SENAO ACHOU O CODIGO EM QUESTAO É INSERÇÃO

            }
        }
        else
        {
            //ZERA TODOS OS CAMPOS DO FORMULARIO

        }
    }

    //public void CarregaDepartamentos()
    //{
    //    string strSql = "SELECT * FROM Departamentos where Situacao='A'  ";
    //    ddlDepartamento.DataSource = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSql);
    //    ddlDepartamento.DataTextField = "Nome";
    //    ddlDepartamento.DataValueField = "ID";
    //    ddlDepartamento.DataBind();

    //}
    public void CarregaRegional()
    {

        String strSql = "SELECT ID,RegionalNome ,'1' as cat FROM Regional  union all SELECT 0 AS ID,'[SELECIONE A REGIONAL]' as regionalnome,'0' as cat order by cat,regionalnome  ";
        ddlregional.DataSource = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSql);
        ddlregional.DataValueField = "ID";
        ddlregional.DataTextField = "RegionalNome";
        ddlregional.DataBind();

    }
    public void CarregaSetor()
    {

        String strSql = "SELECT ID,Setor ,'1' as cat FROM Area  union all SELECT 0 AS ID,'[SELECIONE O SETOR]' as area,'0' as cat order by cat,setor  ";
        ddlsetor.DataSource = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSql);
        ddlsetor.DataValueField = "ID";
        ddlsetor.DataTextField = "setor";
        ddlsetor.DataBind();

    }
    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        //validação de campos
        if (FrameWork.Util.GetString(txtNome.Text) == "")
        {
            Msg("Favor inserir um Nome!", false);
            txtNome.Focus();
            return;
        }

        if (FrameWork.Util.GetString(txtSenha.Text) == "")
        {
            Msg("Favor inserir uma Senha para acesso a Intranet!", false);
            txtSenha.Focus();
            return;
        }

        if (FrameWork.Util.GetInt(hID.Value) > 0)
        {
            //Comandos de Atualização do Empreendimento
            objColaboradores.Load(hID.Value);
            objColaboradores.DataAlteracao = DateTime.Now;

        }
        else
        {
            hOperacao.Value = "INSERT";
            objColaboradores.DataInclusao = DateTime.Now;
        }


        //salva os valores
        //objColaboradores.SessaoID = Convert.ToInt32(this.ddlSessao.SelectedValue);
        //objColaboradores.EditorialID = Convert.ToInt32(this.ddlEditorial.SelectedValue);

        objColaboradores.Nome = this.txtNome.Text;
        objColaboradores.Descricao = txtDescricao.Text;
        //objColaboradores.Formacao = txtFormacao.Text;
        objColaboradores.Cargo = (txtCargo.Text);
        objColaboradores.Linkedin  = (txtLinkedin.Text);
        objColaboradores.Senha = FrameWork.Security.Cript(txtSenha.Text);

        objColaboradores.Setor = FrameWork.Util.GetInt(ddlsetor.SelectedValue);
        objColaboradores.Regional = FrameWork.Util.GetInt(ddlregional.SelectedValue);
        objColaboradores.Email = (txtEmail.Text);
        objColaboradores.Ativo = chkAtivo.Checked;
       objColaboradores.Cpf = txtCPF.Text;
       objColaboradores.LoginRede = txtloginRede.Text;
         objColaboradores.Ramal = txtRamal.Text;
        //objColaboradores.DataAniversario = FrameWork.Util.(txtaniversario.Text);
        if (Funcoes.IsDate(txtaniversario.Text))
        {
            objColaboradores.DataAniversario = Convert.ToDateTime(this.txtaniversario.Text);
        }
        if (Funcoes.IsDate(txtAdmissao.Text))
        {
            objColaboradores.DataAdmissao = Convert.ToDateTime(this.txtAdmissao.Text);
        }
        //objColaboradores.Sequencia = FrameWork.Util.GetInt(txtPosicao.Text);
        objColaboradores.Usuario = (string)this.Session["_uiUser"];


        try
        {
            objColaboradores.Save();
            this.hID.Value = objColaboradores.ID.ToString();

            hOperacao.Value = "UPDATE";
            Msg("Salvo com sucesso!", true);
            //this.abaFotos.Visible = true;

        }
        catch (Exception ex)
        {
            Msg(ex.Message, false);
        }

        try
        {


            if ((txtArquivo.HasFile))
            {

                //inicializar as variáveis
                string arq = txtArquivo.PostedFile.FileName;
                string fileExtension = txtArquivo.PostedFile.ContentType.ToLower();

                if (!string.IsNullOrEmpty(arq))
                {


                    fileExtension = Path.GetExtension(arq);
                    string str_image = "colaborador-" + hID.Value + fileExtension.ToLower();

                    //verificamos a extensão através dos últimos 4 caracteres e verificamos se é permitido
                    if ((!(fileExtension == ".gif" || fileExtension == ".jpg" || fileExtension == ".png")))
                    {
                        Msg("Extensão de arquivo não permitido! Somente é permitido extensão: gif, jpg ou png!", false);
                        return;
                    }


                    //tamanho maximo do upload em kb
                    double permitido = 2000;
                    //identificamos o tamanho do arquivo
                    double tamanho = 0;
                    tamanho = Convert.ToDouble(txtArquivo.PostedFile.ContentLength) / 1024;
                    if ((tamanho > permitido))
                    {
                        Msg("Tamanho Máximo permitido é de " + permitido + " kb", false);
                        return;
                    }

                    //caminho fisico no server
                    string pathPDFs = System.Configuration.ConfigurationManager.AppSettings["pathFisico"] + "uploads\\colaboradores\\";

                    txtArquivo.SaveAs(pathPDFs + str_image);

                    String strSql = "UPDATE Colaboradores SET Foto = '" + str_image + "' WHERE ID=" + hID.Value;
                    FrameWork.DataObject.DataSource.DefaulDataSource.Execute(strSql);

                    txtArquivo.Dispose();

                    strImage = pathVirtual + str_image;
                    this.Image1.ImageUrl = strImage + "?t=" + String.Format("{0:yyyyMMddHHmm}", DateTime.Now);
                }

            }


        }
        catch (Exception ex)
        {
            Msg(ex.Message, false);
            return;
        }


    }

    protected void btnExcluir_Click(object sender, EventArgs e)
    {

        try
        {
            objColaboradores.Load(hID.Value);
            objColaboradores.Delete();
            Response.Redirect("Colaboradores");
        }
        catch (Exception ex)
        {
            Msg(ex.Message, false);
        }
    }

    protected void Msg(String msg, Boolean tipo)
    {
        lblMensagem.CssClass = (tipo ? "box-danger alert alert-success" : "box-danger alert alert-danger");
        lblMensagem.Text = msg;
        lblMensagem.Visible = true;
    }

}