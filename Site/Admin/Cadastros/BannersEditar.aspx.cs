﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using FrameWork.DataObject;
using FrameWork.WebControls;
using FrameWork;
using ClassLibrary;
using System.Drawing.Imaging;
using System.Drawing;


public partial class BannersEditar : System.Web.UI.Page
{

    public ClassLibrary.Banners objBanners = new ClassLibrary.Banners();
    public String strImage = "";
    //caminho fisico no server
    string pathFisico = System.Configuration.ConfigurationManager.AppSettings["pathFisico"] + "uploads\\banners\\";
    string pathVirtual = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "uploads/banners/";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(this.IsPostBack))
        {

            txtPosicao.Attributes.Add("type","number");
            txtPosicao.Attributes.Add("min", "1");
            txtPosicao.Attributes.Add("max", "99");

            ////Direitos do Usuário
            //if (!this.UserInfo.Admin && !CheckUserRight(101, "C"))
            //{
            //    Response.Redirect("Default.aspx");
            //}
            //if (!this.UserInfo.Admin && !CheckUserRight(101, "I"))
            //{
            //    this.btnNovaNoticia.Visible = false;
            //}
            //if (!this.UserInfo.Admin && !CheckUserRight(101, "E"))
            //{
            //    this.btnSalvar.Visible = false;
            //}

            //ID DO EMPREENDIMENTO
            //SE NAO HA NADA DENTRO DO CONTROLE HIDDEN PEGA VIA REQUEST
            if (this.Request["id"] != null)
                this.hID.Value = FrameWork.Util.GetString(this.Request["id"]);

            //CARREGA DADOS
            Carrega();

        }
    }


    public void Carrega()
    {
        //OPCAO DE EDICAO PARA VISUALIZAR OS DADOS
        if (hID.Value != "")
        {
            objBanners.Load(this.hID.Value);

            //REGISTRO LOCALIZADO ENTAO APENAS EDITA DADOS DO FORMULARIO
            if (objBanners.TotalLoad == 1)
            {


                this.txtTitulo.Text = objBanners.Titulo;
                this.txtFrase.Text = objBanners.Frase;


                this.txtLink.Text = objBanners.Link;


                this.txtDescricao.Text = objBanners.Descricao;

                this.txtPosicao.Text = FrameWork.Util.GetString(objBanners.Posicao);
                this.txtDataInicial.Text = String.Format("{0:dd/MM/yyyy}", objBanners.DataInicial);
                this.txtDataFinal.Text = String.Format("{0:dd/MM/yyyy}", objBanners.DataFinal);

                

                if (FrameWork.Util.GetString(objBanners.Imagem).Length > 0)
                {

                    strImage = pathVirtual + FrameWork.Util.GetString(objBanners.Imagem) + "?t=" + String.Format("{0:ddMmyyyyhhmm}", DateTime.Now);
                    this.Image1.ImageUrl = strImage;

                    strImage = pathVirtual + FrameWork.Util.GetString(objBanners.Imagem_iPad) + "?t=" + String.Format("{0:ddMmyyyyhhmm}", DateTime.Now);
                    this.Image2.ImageUrl = strImage;

                    strImage = pathVirtual + FrameWork.Util.GetString(objBanners.Imagem_Mobile) + "?t=" + String.Format("{0:ddMmyyyyhhmm}", DateTime.Now);
                    this.Image3.ImageUrl = strImage;

                    try
                    {
                        // redimenciona para iPad
                        if (FrameWork.Util.GetString(objBanners.Imagem_iPad).Length == 0)
                        {

                            
                            String pathToSave_100 = pathFisico + FrameWork.Util.GetString(objBanners.Imagem);
                            if (System.IO.File.Exists(pathToSave_100))
                            {
                                using (var vimage = System.Drawing.Image.FromFile(pathToSave_100))
                                {
                                    //salva em 900x335
                                    int swidth = 900;
                                    int sheight = 335;

                                    if (vimage.Width > vimage.Height)
                                    {
                                        decimal fator = Convert.ToDecimal(Convert.ToDecimal(swidth) / vimage.Width);
                                        sheight = Convert.ToInt16(vimage.Height * fator);
                                        if (sheight == 0)
                                            sheight = 900;
                                    }
                                    else
                                    {
                                        decimal fator = Convert.ToDecimal(sheight / vimage.Height);
                                        swidth = Convert.ToInt16(vimage.Width * fator);
                                        if (swidth == 0)
                                            swidth = 335;

                                    }

                                    System.Drawing.Image image2 = ResizeImage(vimage, swidth, sheight);
                                    string nomeArquivo = FrameWork.Util.GetString(objBanners.Imagem).Replace(".jpg", "-ipad.jpg").Replace(".gif", "-ipad.gif").Replace(".png", "-ipad.png");
                                    image2.Save(pathFisico + nomeArquivo, System.Drawing.Imaging.ImageFormat.Jpeg);
                                    objBanners.Imagem_iPad = nomeArquivo;

                                    String strSql = "update Banners set Imagem_iPad='" + nomeArquivo + "' where IDBanner=" + hID.Value;
                                    FrameWork.DataObject.DataSource.DefaulDataSource.Execute(strSql);


                                }
                            }
                        }
                        else
                            this.Image2.ImageUrl = pathVirtual + FrameWork.Util.GetString(objBanners.Imagem_iPad);

                        // crop e redimenciona para Mobile
                        if (FrameWork.Util.GetString(objBanners.Imagem_Mobile).Length == 0)
                        {

                            String pathToSave_100 = pathFisico + FrameWork.Util.GetString(objBanners.Imagem);
                            if (System.IO.File.Exists(pathToSave_100))
                            {
                                using (var vimage = System.Drawing.Image.FromFile(pathToSave_100))
                                {

                                    //salva em 1014x400 para aproveitar a altura toda da imagem
                                    int swidth = 1020;
                                    int sheight = 410;

                                    if (vimage.Width > vimage.Height)
                                    {
                                        decimal fator = Convert.ToDecimal(Convert.ToDecimal(swidth) / vimage.Width);
                                        sheight = Convert.ToInt16(vimage.Height * fator);
                                        if (sheight == 0)
                                            sheight = 1014;
                                    }
                                    else
                                    {
                                        decimal fator = Convert.ToDecimal(sheight / vimage.Height);
                                        swidth = Convert.ToInt16(vimage.Width * fator);
                                        if (swidth == 0)
                                            swidth = 400;

                                    }

                                    System.Drawing.Image image3 = ResizeImage(vimage, swidth, sheight);

                                    //crop para mobile

                                    Bitmap croppedBitmap = new Bitmap(image3);
                                    croppedBitmap = croppedBitmap.Clone(
                                                new Rectangle(((1014 / 2) - (500 / 2)), 0, 500, 400),
                                                System.Drawing.Imaging.PixelFormat.DontCare);

                                    String nomeArquivo = FrameWork.Util.GetString(objBanners.Imagem).Replace(".jpg", "-mobile.jpg").Replace(".gif", "-mobile.gif").Replace(".png", "-mobile.png");
                                    croppedBitmap.Save(pathFisico + nomeArquivo, System.Drawing.Imaging.ImageFormat.Jpeg);
                                    objBanners.Imagem_Mobile = nomeArquivo;

                                    String strSql = "update Banners set Imagem_Mobile='" + nomeArquivo + "' where IDBanner=" + hID.Value;
                                    FrameWork.DataObject.DataSource.DefaulDataSource.Execute(strSql);

                                }
                            }
                        }
                        else
                            this.Image3.ImageUrl = pathVirtual + FrameWork.Util.GetString(objBanners.Imagem_Mobile);

                    }
                    catch (Exception e)
                    {
                        //Msg(e.Message, false);
                    }

                }

                //chkIndiferente.Checked = Convert.ToBoolean(objBanners.Indeterminado);
                chkAtivo.Checked = (Convert.ToBoolean(objBanners.Ativo));

                if (ddlTipoLink.Items.FindByValue(FrameWork.Util.GetString(objBanners.TipoLink)) != null)
                {
                    this.ddlTipoLink.SelectedValue = FrameWork.Util.GetString(objBanners.TipoLink);
                }

                //this.txtPagina.Text = FrameWork.Util.GetString(objBanners.pagina);
                lblUsuario.Text  = FrameWork.Util.GetString(objBanners.Usuario);
                lblDataInclusao.Text = FrameWork.Util.GetString(objBanners.DataInclusao);
                lblDataAlteracao.Text = FrameWork.Util.GetString(objBanners.DataAlteracao);

                hOperacao.Value = "UPDATE";

                btnExcluir.Visible = true;
                // abaFotos.Visible = true;
                //CarregaFotos();

            }

        }

    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        //validação de campos
        if (FrameWork.Util.GetString(txtTitulo.Text) == "")
        {
            Msg("Favor inserir um Título!", false);
            txtTitulo.Focus();
            return;
        }


        if (hOperacao.Value == "UPDATE")
        {
            //Comandos de Atualização do Empreendimento
            objBanners.LoadWhere("IDBanner=@ID", new ParameterItem("@ID", hID.Value));
            objBanners.DataAlteracao = DateTime.Now;

        }
        else
            objBanners.DataInclusao = DateTime.Now;

        objBanners.Titulo = this.txtTitulo.Text;

        objBanners.Frase = txtFrase.Text;

        objBanners.Posicao = FrameWork.Util.GetInt(txtPosicao.Text);

        objBanners.TipoLink = ddlTipoLink.SelectedValue;

        objBanners.Link = this.txtLink.Text;

        objBanners.Descricao = this.txtDescricao.Text;

        //Response.Write("chkIndiferente=" + Request["chkIndiferente"]);

        objBanners.Indeterminado = (FrameWork.Util.GetString(Request["chkIndiferente"])=="on");
        objBanners.Ativo = (chkAtivo.Checked);


        if (Funcoes.IsDate(txtDataInicial.Text))
            objBanners.DataInicial = Convert.ToDateTime(this.txtDataInicial.Text);
        if (Funcoes.IsDate(txtDataFinal.Text))
            objBanners.DataFinal = Convert.ToDateTime(this.txtDataFinal.Text);


        objBanners.DataAlteracao = DateTime.Now;
        objBanners.Usuario = (string)this.Session["_uiUser"];


        try
        {
            objBanners.Save();
            this.hID.Value = objBanners.IDBanner.ToString();

            hOperacao.Value = "UPDATE";
            Msg("Salvo com sucesso!", true);
            //this.abaFotos.Visible = true;

        }
        catch (Exception ex)
        {
            //this.pnlEmp.Visible = false;
            //if (ex.ErrorCode.ToString() == "-2146232060")
            //    this.DisplayMessage = " ID da Notícia já existente! ";
            //else
            //    this.DisplayMessage = ex.ErrorCode.ToString() + " | " + ex.InnerException.ToString();

            Msg(ex.Message, false);
        }

        //ATUALIZA A TELA 
        //this.Image1.ImageUrl = FrameWork.Util.GetString(objBanners.Imagem"]);
        //IMAGENS 
        try
        {



            if ((fileImagem.HasFile))
            {

                //inicializar as variáveis
                string arq = fileImagem.PostedFile.FileName;

                //verificamos a extensão através dos últimos 4 caracteres e verificamos se é permitido
                string extensao = arq.Substring(arq.Length - 4).ToLower();
                if ((!(extensao == ".gif" || extensao == ".jpg" || extensao == ".png")))
                {
                    Msg("Extensão de arquivo não permitido! Somente é permitido extensão: gif, jpg ou png!", false);
                    return;
                }

                //If Not "pdf,jpg,gif,png".Contains(Right(Trim(FileUpload1.FileName), 3).ToLower) Then
                //    Msg("Tipo de arquivo não permitido! Inclua apenas arquivos com a extensão informada!", Me)
                //    Exit Sub
                //End If


                //tamanho maximo do upload em kb
                double permitido = 2000;
                //identificamos o tamanho do arquivo
                double tamanho = 0;
                tamanho = Convert.ToDouble(fileImagem.PostedFile.ContentLength) / 1024;
                if ((tamanho > permitido))
                {
                    Msg("Tamanho Máximo permitido é de " + permitido + " kb", false);
                    return;
                }

                System.IO.Stream inStream = fileImagem.PostedFile.InputStream;
                System.Drawing.Image img = System.Drawing.Image.FromStream(inStream);

                int fonteLargura = img.Size.Width;     //armazena a largura original da imagem origem
                int fonteAltura = img.Size.Height;   //armazena a altura original da imagem origem
                int origemX = 1980;        //eixo x da imagem origem
                int origemY = 1000;        //eixo y da imagem origem
                //if ((fonteLargura != origemX))  // || (fonteAltura != origemY)
                //{
                //    Msg("Largura e Altura da imagem atual tem " + fonteLargura + " x " + fonteAltura + " px e deverá ser de 1980 x 780 px. Favor redimencionar a imagem corretamente!", false);
                //    return;
                //}


                string nomeArquivo = "banner-" + hID.Value + extensao;
                //string nomeArquivo = txtArquivo.FileName.ToString();

                fileImagem.SaveAs(pathFisico + nomeArquivo);

                //objBanners.Imagem = nomeArquivo;
                String strSql = "UPDATE Banners SET Imagem = '" + nomeArquivo + "' WHERE IDBanner=" + hID.Value;
                FrameWork.DataObject.DataSource.DefaulDataSource.Execute(strSql);

                fileImagem.Dispose();

                strImage = pathVirtual + nomeArquivo;
                this.Image1.ImageUrl = strImage;

                pnlFotos.Visible = true;

            }

            if ((fileImagem_iPad.HasFile))
            {

                //inicializar as variáveis
                string arq = fileImagem_iPad.PostedFile.FileName;

                //verificamos a extensão através dos últimos 4 caracteres e verificamos se é permitido
                string extensao = arq.Substring(arq.Length - 4).ToLower();
                if ((!(extensao == ".gif" || extensao == ".jpg" || extensao == ".png")))
                {
                    Msg("Extensão de arquivo não permitido! Somente é permitido extensão: gif, jpg ou png!", false);
                    return;
                }

                //If Not "pdf,jpg,gif,png".Contains(Right(Trim(FileUpload1.FileName), 3).ToLower) Then
                //    Msg("Tipo de arquivo não permitido! Inclua apenas arquivos com a extensão informada!", Me)
                //    Exit Sub
                //End If


                //tamanho maximo do upload em kb
                double permitido = 2000;
                //identificamos o tamanho do arquivo
                double tamanho = 0;
                tamanho = Convert.ToDouble(fileImagem_iPad.PostedFile.ContentLength) / 1024;
                if ((tamanho > permitido))
                {
                    Msg("Tamanho Máximo permitido é de " + permitido + " kb", false);
                    return;
                }

                System.IO.Stream inStream = fileImagem_iPad.PostedFile.InputStream;
                System.Drawing.Image img = System.Drawing.Image.FromStream(inStream);

                int fonteLargura = img.Size.Width;     //armazena a largura original da imagem origem
                int fonteAltura = img.Size.Height;   //armazena a altura original da imagem origem
                int origemX = 1980;        //eixo x da imagem origem
                int origemY = 780;        //eixo y da imagem origem
                //if ((fonteLargura != origemX) || (fonteAltura != origemY))
                //{
                //    Msg("Largura e Altura da imagem atual tem " + fonteLargura + " x " + fonteAltura + " px e deverá ser de 1980 x 780 px. Favor redimencionar a imagem corretamente!", false);
                //    return;
                //}


                string nomeArquivo = "banner-" + hID.Value + "-ipad" + extensao;
                //string nomeArquivo = txtArquivo.FileName.ToString();

                fileImagem_iPad.SaveAs(pathFisico + nomeArquivo);

                //objBanners.Imagem = nomeArquivo;
                String strSql = "UPDATE Banners SET Imagem_iPad = '" + nomeArquivo + "' WHERE IDBanner=" + hID.Value;
                FrameWork.DataObject.DataSource.DefaulDataSource.Execute(strSql);

                fileImagem_iPad.Dispose();

                strImage = pathVirtual + nomeArquivo;
                this.Image2.ImageUrl = strImage;


            }

            if ((fileImagem_Mobile.HasFile))
            {

                //inicializar as variáveis
                string arq = fileImagem_Mobile.PostedFile.FileName;

                //verificamos a extensão através dos últimos 4 caracteres e verificamos se é permitido
                string extensao = arq.Substring(arq.Length - 4).ToLower();
                if ((!(extensao == ".gif" || extensao == ".jpg" || extensao == ".png")))
                {
                    Msg("Extensão de arquivo não permitido! Somente é permitido extensão: gif, jpg ou png!", false);
                    return;
                }

                //If Not "pdf,jpg,gif,png".Contains(Right(Trim(FileUpload1.FileName), 3).ToLower) Then
                //    Msg("Tipo de arquivo não permitido! Inclua apenas arquivos com a extensão informada!", Me)
                //    Exit Sub
                //End If


                //tamanho maximo do upload em kb
                double permitido = 2000;
                //identificamos o tamanho do arquivo
                double tamanho = 0;
                tamanho = Convert.ToDouble(fileImagem_Mobile.PostedFile.ContentLength) / 1024;
                if ((tamanho > permitido))
                {
                    Msg("Tamanho Máximo permitido é de " + permitido + " kb", false);
                    return;
                }

                System.IO.Stream inStream = fileImagem_Mobile.PostedFile.InputStream;
                System.Drawing.Image img = System.Drawing.Image.FromStream(inStream);

                int fonteLargura = img.Size.Width;     //armazena a largura original da imagem origem
                int fonteAltura = img.Size.Height;   //armazena a altura original da imagem origem
                int origemX = 1980;        //eixo x da imagem origem
                int origemY = 780;        //eixo y da imagem origem
                //if ((fonteLargura != origemX) || (fonteAltura != origemY))
                //{
                //    Msg("Largura e Altura da imagem atual tem " + fonteLargura + " x " + fonteAltura + " px e deverá ser de 1980 x 780 px. Favor redimencionar a imagem corretamente!", false);
                //    return;
                //}


                string nomeArquivo = "banner-" + hID.Value + "-mobile" + extensao;
                //string nomeArquivo = txtArquivo.FileName.ToString();

                fileImagem_Mobile.SaveAs(pathFisico + nomeArquivo);

                //objBanners.Imagem = nomeArquivo;
                String strSql = "UPDATE Banners SET Imagem_Mobile = '" + nomeArquivo + "' WHERE IDBanner=" + hID.Value;
                FrameWork.DataObject.DataSource.DefaulDataSource.Execute(strSql);

                fileImagem_Mobile.Dispose();

                strImage = pathVirtual + nomeArquivo;
                this.Image3.ImageUrl = strImage;


            }


        }
        catch (Exception ex)
        {
            Msg(ex.Message, false);
        }


    }

    protected void btnExcluir_Click(object sender, EventArgs e)
    {


        try
        {
            //Comandos de excluir do Empreendimento
            objBanners.LoadWhere("IDBanner=@ID", new ParameterItem("@ID", hID.Value));
            objBanners.Delete();

            Response.Redirect("BannersLista");

        }
        catch (Exception ex)
        {
            //this.pnlEmp.Visible = false;
            //if (ex.ErrorCode.ToString() == "-2146232060")
            //    this.DisplayMessage = " ID da Notícia já existente! ";
            //else
            //    this.DisplayMessage = ex.ErrorCode.ToString() + " | " + ex.InnerException.ToString();

            Msg(ex.Message, false);
        }





    }
    protected void Msg(String msg, Boolean tipo)
    {
        lblMensagem.CssClass = (tipo ? "box-danger alert alert-success" : "box-danger alert alert-danger");
        lblMensagem.Text = msg;
        lblMensagem.Visible = true;
    }

    public static string resizeImageAndSave(string imagePath, int largura, int altura, string prefixo)
    {
        System.Drawing.Image fullSizeImg = System.Drawing.Image.FromFile(imagePath);
        var thumbnailImg = new Bitmap(largura, altura);
        var thumbGraph = Graphics.FromImage(thumbnailImg);
        thumbGraph.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
        thumbGraph.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
        thumbGraph.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
        var imageRectangle = new Rectangle(0, 0, largura, altura);
        thumbGraph.DrawImage(fullSizeImg, imageRectangle);
        string targetPath = imagePath.Replace(Path.GetFileNameWithoutExtension(imagePath), Path.GetFileNameWithoutExtension(imagePath) + prefixo);
        thumbnailImg.Save(targetPath, System.Drawing.Imaging.ImageFormat.Jpeg);
        thumbnailImg.Dispose();
        return targetPath;
    }

    public static Bitmap ResizeImage(System.Drawing.Image image, int width, int height)
    {
        var destRect = new Rectangle(0, 0, width, height);
        var destImage = new Bitmap(width, height);

        destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

        using (var graphics = Graphics.FromImage(destImage))
        {
            graphics.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;
            graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            graphics.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;

            using (var wrapMode = new System.Drawing.Imaging.ImageAttributes())
            {
                wrapMode.SetWrapMode(System.Drawing.Drawing2D.WrapMode.TileFlipXY);
                graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
            }
        }

        return destImage;
    }

    public System.Drawing.Image Crop(string img, int width, int height, int x, int y)
    {
        try
        {
            System.Drawing.Image image = System.Drawing.Image.FromFile(img);
            Bitmap bmp = new Bitmap(width, height); //, PixelFormat.Format24bppRgb
            //bmp.SetResolution(80, 60);

            Graphics gfx = Graphics.FromImage(bmp);
            //gfx.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            //gfx.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            //gfx.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
            //gfx.DrawImage(image, new Rectangle(0, 0, width, height), x, y, width, height, GraphicsUnit.Pixel);
            var crop = new Rectangle(0, y, width, height);
            var dest = new Rectangle(0, 0, width, height);

            //gfx.DrawImage(image, new Rectangle(0, 0, width, height), GraphicsUnit.Point);
            // Dispose to free up resources
            image.Dispose();
            bmp.Dispose();
            gfx.Dispose();

            return bmp;
        }
        catch (Exception ex)
        {
            Msg(ex.Message, false);
            return null;
        }
    }

}