﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="GaleriasEditar.aspx.cs" Inherits="Cadastros_GaleriasEditar" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <%--    <link href="/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet" type="text/css" />--%>
    <%--    <link rel="stylesheet" href="/css/jquery.ui.all.css">--%>
    <link href="/admin/css/jquery-fancybox.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="/admin/css/responsive.css">
    <!-- DATE RANGE PICKER -->
    <%--    <link rel="stylesheet" type="text/css" href="/js/bootstrap-daterangepicker/daterangepicker-bs3.css" />--%>
    <!-- ANIMATE -->
    <link rel="stylesheet" type="text/css" href="/admin/css/animatecss/animate.min.css" />
    <!-- COLORBOX -->
    <link rel="stylesheet" type="text/css" href="/admin/js/colorbox/colorbox.min.css" />
    <link rel="stylesheet" href="/admin/css/jquery-ui-1.8.4.custom.css" />

    <!-- BOOTSTRAP SWITCH -->
    <link rel="stylesheet" type="text/css" href="/admin/js/bootstrap-switch/bootstrap-switch.min.css" />
    <link rel="stylesheet" type="text/css" href="/admin/css/themes/default.css" id="skin-switcher">

    <!-- JQUERY UI-->
    <link rel="stylesheet" type="text/css" href="/admin/js/jquery-ui-1.10.3.custom/css/custom-theme/jquery-ui-1.10.3.custom.min.css" />


    <!-- DROPZONE -->
    <link rel="stylesheet" type="text/css" href="/admin/js/dropzone/dropzone.min.css" />


    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="/admin/plugins/iCheck/all.css">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="/admin/plugins/colorpicker/bootstrap-colorpicker.min.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="/admin/plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="/admin/plugins/select2/select2.min.css">


    <link href="/admin/components/imgareaselect/css/imgareaselect-default.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="/admin/css/jquery.awesome-cropper.css">


    <style type="text/css">
        .container-narrow {
            margin: 150px auto 50px auto;
            max-width: 728px;
        }

        .border-white canvas {
            border: 1px solid #ddd;
            border-radius: 4px;
            padding: 4px;
        }

        div.inputnotes div {
            padding: 3px;
            padding-left: 4px;
            padding-top: 4px;
            margin-top: 2px;
        }

            div.inputnotes div.note {
                color: #fff;
                background: #333;
            }

            div.inputnotes div.warning {
                color: #fff;
                background: #f03;
            }

        input:focus {
            background-color: #316AC5;
            color: #FFFFFF;
        }

        input:hover {
            background-color: #FFFFFF;
            color: #000000;
        }

        .image-grid img {
            width: 312px;
            height: 200px;
        }
        .foto-descricao-oculto{
            display:none;
        }
        .imgPrincipal img {
            margin: 0 auto 0 auto;
            max-height:150px;
        }

    </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <section class="content-header">
        <h1>Empreendimentos
           
                <small>Edição</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i>Home</a></li>
            <li>Empreendimento</li>
            <li class="active">Edição</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">

            <asp:Label ID="lblMensagem" runat="server" Text="<img src='/images/alert.gif' /> &nbsp; Verificar se o cliente já está cadastrado pelo cpf antes de iniciar o cadastramento!<br />" Font-Bold="true" ForeColor="Red" Visible="false"></asp:Label>

            <div class="col-md-12">
                <div class="box box-primary">

                    <div class="box-header with-border">
                        <div class="box-header with-border">
                            <h3 class="box-title">
                                <asp:Label ID="lblNomeImovel" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label></h3>
                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                    </div>
                    <form id="form1" runat="server">

                        <div class="box-body">
                            <div class="tabbable header-tabs">
                                <ul class="nav nav-tabs">
                                    <li><a href="EmpreendimentosEditar?id=<%=hID.Value %>#box_tab1"><i class="fa fa-bars"></i><span class="hidden-inline-mobile">&nbsp;Cadastro</span></a></li>
                                    <li><a href="EmpreendimentosEditar?id=<%=hID.Value %>#box_tab2"><i class="fa fa-laptop"></i><span class="hidden-inline-mobile">&nbsp;Informações</span></a></li>
                                    <li><a href="EmpreendimentosEditar?id=<%=hID.Value %>#box_tab3"><i class="fa fa-sun"></i><span class="hidden-inline-mobile">&nbsp;Destaques</span></a></li>
                                    <li><a href="EmpreendimentosEditar?id=<%=hID.Value %>#box_tab4"><i class="fa fa-sun"></i><span class="hidden-inline-mobile">&nbsp;Diferenciais</span></a></li>
                                    <li><a href="EmpreendimentosEditar?id=<%=hID.Value %>#box_tab5"><i class="fa fa-book"></i><span class="hidden-inline-mobile">&nbsp;Características</span></a></li>
                                    <li><a href="EmpreendimentosEditar?id=<%=hID.Value %>#box_tab6"><i class="fa fa-bullhorn"></i><span class="hidden-inline-mobile">&nbsp;Detalhes</span></a></li>
                                    <li><a href="EmpreendimentosEditar?id=<%=hID.Value %>#box_tab7"><i class="fa fa-map-marked"></i><span class="hidden-inline-mobile">&nbsp;Bairro</span></a></li>
                                    <li><a href="EmpreendimentosEditar?id=<%=hID.Value %>#box_tab8"><i class="fa fa-html5"></i><span class="hidden-inline-mobile"> SEO</span></a></li>
                                    <li><a href="EmpreendimentosEditar?id=<%=hID.Value %>#box_tab9"><i class="fa fa-file"></i><span class="hidden-inline-mobile">&nbsp;Documentos</span></a></li>
                                    <li class="active"><a href="#" data-toggle="tab"><i class="fa fa-file-image-o"></i><span class="hidden-inline-mobile"> Galeria</span></a></li>
                                    <li><a href="EmpreendimentosEditar?id=<%=hID.Value %>#box_tab10"><i class="fa fa-file"></i><span class="hidden-inline-camera">&nbsp;Câmeras</span></a></li>
                                    <li><a href="EstagioObra?id=<%=hID.Value %>"><i class="fa fa-file-build"></i><span class="hidden-inline-mobile">&nbsp;Estágio de Obra</span></a></li>
                                </ul>
                                <div class="tab-content">
                                    <!-- Aba Cadastro -->
                                    <div class="tab-pane fade in active" id="box_tab6">

                                        <div class="form-group">
                                            <div class="col-sm-2">
                                                <br />
                                                <asp:Image ID="Image1" runat="server" Visible="true" CssClass="img-responsive thumbnail" ImageUrl="/admin/img/img-logo.png" Height="85px" />
                                            </div>
                                            <div class="col-sm-3">
                                                <label for="ContentPlaceHolder1_Logo">Logo:</label>
                                                <asp:FileUpload ID="fileLogo" CssClass="form-control" runat="server" Width="100%"></asp:FileUpload>
                                                <small>Upload (Gif/Jpg/Png) tamanho: 200 x 125 px</small>
                                            </div>
                                            <div class="col-sm-2 col-sm-offset-2">
                                                <label>Destaque:</label>
                                                <asp:Image ID="ImageDestaque" runat="server" Visible="true" CssClass="img-responsive thumbnail" ImageUrl="/admin/img/img-destaque.png" Height="100px" />
                                            </div>
                                            <div class="col-sm-3">
                                                <asp:FileUpload ID="fileDestaque" CssClass="form-control" runat="server" Width="100%"></asp:FileUpload>
                                                <small>Upload (Gif/Jpg/Png) tamanho: 750 x 620 px</small>
                                            </div>
                                        </div>
                                        <div style="clear: both;"></div>
                                        <div class="col-sm-12">
                                            <div class="separator"></div>
                                        </div>
                                        <div class="form-group" style="height: 100px;">
                                            <h3>Principal:</h3><br />
                                            <div class="col-sm-4">
                                                <label>Padrão:</label>
                                                <input id="hfilePrincipal" type="hidden" name="filePrincipal[image]">
                                                <small>Upload (Gif/Jpg/Png) tamanho: 1980 x 500 px</small>
                                            </div>
                                            <div class="col-sm-3 col-sm-offset-1">
                                                <label>Tablet:</label>
                                                <input id="hfilePrincipal_Tablet" type="hidden" name="filePrincipal_Tablet[image]">
                                                <small>Upload (Gif/Jpg/Png) tamanho: 900 x 355 px</small>
                                            </div>
                                            <div class="col-sm-3 col-sm-offset-1">
                                                <label>Mobile:</label>
                                                <input id="hfilePrincipal_Mobile" type="hidden" name="filePrincipal_Mobile[image]">
                                                <small>Upload (Gif/Jpg/Png) tamanho: 500 x 500 px</small>
                                            </div>
                                        </div>
                                        <div class="form-group" style="height: 100px;">
                                            <div class="col-sm-4">
                                                <asp:Image ID="ImagePrincipal" runat="server" Visible="false" CssClass="img-responsive thumbnail" ImageUrl="/admin/img/img-principal.png"  />
                                            </div>
                                            <div class="col-sm-4 imgPrincipal">
                                                <asp:Image ID="ImagePrincipal_Tablet" runat="server" Visible="false" CssClass="img-responsive thumbnail" ImageUrl="/admin/img/img-principal.png"  />
                                            </div>
                                            <div class="col-sm-4 imgPrincipal">
                                                <asp:Image ID="ImagePrincipal_Mobile" runat="server" Visible="false" CssClass="img-responsive thumbnail" ImageUrl="/admin/img/img-principal.png"  />
                                            </div>
                                        </div>
                                        <div style="clear: both;"></div>
                                        <div class="col-sm-12">
                                            <div class="separator"></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <label>Título:</label>
                                                <asp:TextBox ID="txtDestaque_Titulo" CssClass="form-control" runat="server" MaxLength="100" Width="100%"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-4">
                                                <label>Ícone Texto:</label>
                                                <asp:TextBox ID="txtDestaque_IconeTexto" CssClass="form-control" runat="server" MaxLength="30" Width="100%"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-2">
                                                <label>Ícone Class:</label>
                                                <asp:TextBox ID="txtDestaque_IconeClass" CssClass="form-control" runat="server" MaxLength="30" Width="100%"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-12">
                                                <label>Frase:</label>
                                                <asp:TextBox ID="txtDestaque_Frase" CssClass="form-control" runat="server" MaxLength="250" Width="100%"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-12">
                                                <label>Descrição:</label>
                                                <asp:TextBox ID="txtDestaque_Descricao" CssClass="form-control" TextMode="MultiLine" Height="100" runat="server" Width="100%"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div style="clear: both;"></div>

                                        <p>&nbsp;</p>

                                        <p>&nbsp;</p>
                                    </div>

                                </div>

                            </div>

                        </div>
                        <div class="box-footer">

                            <asp:LinkButton ID="btnSalvar" runat="server" CssClass="btn btn-success col-sm-1" OnClick="btnSalvar_Click"><i class="fa fa-save fa-1x"></i> SALVAR</asp:LinkButton>&nbsp;&nbsp;
                            <asp:LinkButton ID="btnVoltar" runat="server" CssClass="btn btn-primary" PostBackUrl="EmpreendimentosListar.aspx"><i class="fa fa-arrow-left fa-1x"></i> VOLTAR</asp:LinkButton>

                            <asp:HiddenField ID="hOperacao" runat="server" />
                            <asp:HiddenField ID="hID" runat="server" />
                            <asp:HiddenField ID="hUrlReferrer" runat="server" />
                            <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                        </div>

                    </form>

                </div>
            </div>
        </div>


        <!-- /.row -->
    </section>
    <!-- /.content -->



    <section class="content">




        <br />
        <p>
            <asp:Panel ID="pnlUpload" Visible="false" runat="server">
                <!-- DROPZONE -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BOX -->
                        <div class="box box-info border blue">
                            <div class="box-header with-border">
                                <h3 class="box-title"><i class="fa fa-cloud-download"></i>Upload Fotos - tamanho máximo 2mb</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                </div>
                            </div>
                            <div class="box-body" style="width: 100%;">
                                <form action="FileUpload.ashx?id=<% Response.Write(hID.Value); %>" class="dropzone" id="my-awesome-dropzone">
                                    <div style="float: left; width: 20%;">
                                        <div class="form-horizontal ">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Categoria: </label>
                                                <div class="col-md-8">
                                                    <select id="ddlCategorias" name="ddlCategorias" class="form-control">
                                                        <%=FrameWork.Util.GetString(Session["sCategorias"]) %>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="fallback" style="width: 80%; float: right;">
                                        <input name="file" id="file" type="file" multiple />
                                    </div>

                                </form>
                            </div>
                        </div>
                        <!-- /BOX -->
                    </div>
                </div>
                <!-- /DROPZONE -->
                <a href="GaleriasEditar?id=<% Response.Write(hID.Value); %>" class="btn btn-danger"><i class="fa fa-refresh"></i>ATUALIZA</a>

            </asp:Panel>
        </p>
        <br />


            <!-- GALERIA -->
            <div class="row">
                <div class="col-md-12">
                    <div class="image-filters">
                        <span>Filtrar imagens |</span>&nbsp;&nbsp;
                            <%=FrameWork.Util.GetString(Session["sCategorias_filtros"]) %>
                            <a title="Todos" class="filter" data-filter="all"><i></i>Todos</a>
                    </div>

                    <ul class="image-grid">
                        <asp:Repeater ID="rptFotos" runat="server">
                            <ItemTemplate>
                                <li class='<%# "mix " + FrameWork.Util.GetString(DataBinder.Eval(Container, "DataItem.Categoria")).ToLower() %>' data-categoria='<%# FrameWork.Util.GetString(DataBinder.Eval(Container, "DataItem.IDCategoria")) %>' style='background-image: url(<%# FrameWork.Util.GetString(DataBinder.Eval(Container, "DataItem.FileNameFull")).Replace(FrameWork.Util.GetString(DataBinder.Eval(Container, "DataItem.FileName")),FrameWork.Util.GetString(DataBinder.Eval(Container, "DataItem.FileName_600x600"))) %>);' title='<%# DataBinder.Eval(Container, "DataItem.Descricao")%>'>
                                    <div class="hover-options">
                                        <a class="remove-img btn btn-danger hover-link" data-value='<%# DataBinder.Eval(Container, "DataItem.ImagemID")%>'>
                                            <i class="fa fa-trash-o fa-1x"></i>
                                        </a>
                                        <a class="edit-img btn btn-warning hover-link" data-value='<%# DataBinder.Eval(Container, "DataItem.ImagemID")%>' data-toggle="modal" data-target="#editImagem">
                                            <i class="fa fa-edit fa-1x"></i>
                                        </a>
                                        <a class="btn btn-success hover-link colorbox-button cboxElement " href='<%# FrameWork.Util.GetString(DataBinder.Eval(Container, "DataItem.FileNameFull")) %>' title='<%# DataBinder.Eval(Container, "DataItem.Descricao")%>'>
                                            <i class="fa fa-search-plus fa-1x"></i>
                                        </a>
                                        <div class="foto-descricao-oculto"><%# FrameWork.Util.GetString(DataBinder.Eval(Container, "DataItem.Conteudo")) %></div>
                                    </div>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </div>
            </div>
            <!-- /GALERIA -->

            <div class="modal" id="editImagem">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Editar Imagem</h4>
                        </div>
                        <form id="modal-edit-foto" name="modal-edit-foto">
                            <div class="modal-body">
                                <div class="form-horizontal ">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Categoria: </label>
                                        <div class="col-md-8">
                                                <select id="ddlCategorias_modal" name="ddlCategorias_modal" class="form-control">
                                                    <%=FrameWork.Util.GetString(Session["sCategorias"]) %>
                                                </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label>Título:</label>
                                        <input id="txtDescricao" name="txtDescricao" class="form-control" maxlength="50" placeholder="Título da Foto" />
                                        <span class="error-span"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label>Conteúdo:</label>
                                        <textarea id="txtConteudo" name="txtConteudo" class="form-control" ></textarea>
                                        <span class="error-span"></span>
                                    </div>
                                </div>

                                <p>
                                    &nbsp;
                                    
                                </p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-success btn-foto-edit">Salvar</button>
                            </div>
                            <input type="hidden" id="hFotoEdit" />
                        </form>

                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->



        <div class="footer-tools">
            <span class="go-top">
                <i class="fa fa-chevron-up"></i>Top
            </span>
        </div>

    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">

    <!-- JQUERY UI-->
    <%--    <link rel="stylesheet" type="text/css" href="/js/jquery-ui-1.10.3.custom/css/custom-theme/jquery-ui-1.10.3.custom.min.css" />--%>

    <!-- DATE RANGE PICKER -->
    <%--    <script src="/js/bootstrap-daterangepicker/moment.min.js"></script>
    <script src="/js/bootstrap-daterangepicker/daterangepicker.min.js"></script>--%>

    <!-- Select2 -->
    <script src="/admin/plugins/select2/select2.full.min.js"></script>

    <!-- bootstrap color picker -->
    <script src="/admin/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
    <!-- bootstrap time picker -->
    <script src="/admin/plugins/timepicker/bootstrap-timepicker.min.js"></script>

    <!-- iCheck 1.0.1 -->
    <script src="/admin/plugins/iCheck/icheck.min.js"></script>

    <%-- <!-- SLIMSCROLL -->
    <script type="text/javascript" src="/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.min.js"></script>
    <script type="text/javascript" src="/js/jQuery-slimScroll-1.3.0/slimScrollHorizontal.min.js"></script>
    <!-- BLOCK UI -->
    <script type="text/javascript" src="/js/jQuery-BlockUI/jquery.blockUI.min.js"></script>    --%>
    <!-- BOOTSTRAP SWITCH -->
    <script type="text/javascript" src="/admin/js/bootstrap-switch/bootstrap-switch.min.js"></script>

    <!-- ISOTOPE -->
    <script type="text/javascript" src="/admin/js/isotope/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="/admin/js/isotope/imagesloaded.pkgd.min.js"></script>
    <!-- COLORBOX -->
    <script type="text/javascript" src="/admin/js/colorbox/jquery.colorbox.min.js"></script>

    <!-- KNOB -->
    <script type="text/javascript" src="/admin/js/jQuery-Knob/js/jquery.knob.min.js"></script>


    <!-- DROPZONE -->
    <script type="text/javascript" src="/admin/js/dropzone/dropzone.min.js"></script>

    <script src="/admin/js/jquery-fancybox-pack.js"></script>

    <script src="/admin/js/jquery.mixitup.js"></script>

    <script src="/admin/ckeditor/ckeditor.js?t=C6HH5UF" type="text/javascript"></script>

    <script>
        $(document).ready(function () {

            CKEDITOR.replace('txtConteudo', {
                toolbar: [
                    { name: 'document', items: ['Source', '-', 'NewPage', 'Preview', '-', 'Templates'] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
                    ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'],			// Defines toolbar group without name.
                    '/',																					// Line break - next group will be placed in new line.
                    { name: 'basicstyles', items: ['Bold', 'Italic'] }
                ]});

            var campo = "ContentPlaceHolder1_";


            //$(".dz-remove").click(function () {
            //    alert("dz-remove");
            //    e = $(this).parent();

            //    alert(e.html());
            //});

            $(".remove-img").click(function () {
                if (confirm("Confirma a exclusão da foto?")) {
                    $.post("/Admin/FotosRemover.ashx?id=" + $(this).data('value'), function (data) {
                    });
                    e = $(this).parent().parent();

                    //alert(e.html());
                    e.hide();
                }
            });

            $('.image-grid').mixItUp();

            //$(".image-fancybox").fancybox({
            //    padding: 2
            //});

            $(".edit-img").click(function () {

                var $this = $(this);

                var id = $(this).attr('data-value');
                $("#modal-edit-foto #hFotoEdit").val(id);

                var stitle = $(this).parent().parent().attr("title");
                $("#modal-edit-foto #txtDescricao").val(stitle);

                var sconteudo = $(this).parent().find(".foto-descricao-oculto");
                //$("#modal-edit-foto #txtConteudo").val(sconteudo.html());
                CKEDITOR.instances.txtConteudo.setData(sconteudo.html());
                

                var sclass = $(this).parent().parent().data('categoria');

                //console.log("sclass=" + sclass);

                $("#ddlCategorias_modal").val(sclass).change();

                //if (sclass == "plantas")
                //    $('input:radio[class=inputradio][id=categoria2]').prop('checked', true);
                //else if (sclass == "decorados")
                //    $('input:radio[class=inputradio][id=categoria9]').prop('checked', true);
                //else
                //    $('input:radio[class=inputradio][id=categoria1]').prop('checked', true);


            });

            //foto edit
            $(".btn-foto-edit").click(function () {
                var result = 0;
                var id = $('#modal-edit-foto #hFotoEdit').val();
                var sCategoria = $('#ddlCategorias_modal').val();
                var stringCategoria = $('#ddlCategorias_modal option:selected').text();
                var sDescricao = $('#modal-edit-foto #txtDescricao').val();
                var sConteudo = CKEDITOR.instances.txtConteudo.getData(); //$('#modal-edit-foto #txtConteudo').text();
                //console.log("id=" + id);
                //console.log("sCategoria=" + sCategoria);
                //console.log("sDescricao=" + sDescricao);

                data = {
                    acao: 'edit',
                    id: id,
                    cat: sCategoria,
                    desc: sDescricao,
                    conteudo: sConteudo
                };

                $.post("wsFotos.ashx", data, function (result) {

                    console.log("result=" + result);


                    e = $(".image-grid .edit-img[data-value=" + id + "]");
                    z = e.parent().parent();
                    z.attr("title", sDescricao);

                    //ajustar a categoria da foto
                    var sclass = z.attr("class").replace('mix ', '').replace(' ui-sortable-handle', '');
                    var vclass = "";


                    if (vclass != sclass) {
                        z.attr("class", "mix " + stringCategoria.toLowerCase() + " ui-sortable-handle");
                    }

                    $('#editImagem').modal('toggle');

                });

                return false;
            });


        });



        //ordernação das imagens
        $(function () {
            $(".image-grid").sortable({
                placeholder: "highlight",
                connectWith: ".image-grid .mix",
                start: function (event, ui) {
                    ui.item.toggleClass("highlight");
                },
                stop: function (event, ui) {
                    var slides = [];
                    var items = [];

                    ui.item.toggleClass("highlight");

                    $(".image-grid .mix .remove-img").each(function (index, element) {
                        slides[index] = index + 1;
                        items[index] = parseInt($(element).attr('data-value'));

                        //alert("pos=" + slides[index] + " - item=" + items[index]);
                    });

                    console.log(slides, items);
                    $.post('GaleriaOrdenacao.ashx',
                    { 'item_id': items.join(','), 'positions': slides.join(',') }, function (data) { }, 'json');


                }
            });

            $(".image-grid").disableSelection();




        });



    </script>


    <script type="text/javascript">

        var campo = "ContentPlaceHolder1_";

        ////$(function () {
        ////    $(".accordion-toggle").click(function () {
        ////        var $this = $(this);
        ////        if ($this.hasClass('icon-chevron-up')) {
        ////            $this.removeClass('icon-chevron-up').addClass('icon-chevron-down');
        ////        } else {
        ////            $this.removeClass('icon-chevron-down').addClass('icon-chevron-up');
        ////        }
        ////    });
        ////    return false;
        ////});

    </script>
    <!-- CUSTOM SCRIPT -->
    <%--    <script src="/js/script.js"></script>--%>
    <script>
        jQuery(document).ready(function () {


            // cache container
            var $container = $('#filter-items');
            // initialize isotope after image loaded
            $container.imagesLoaded(function () {
                $container.isotope({
                    // options...
                });

                // filter items when filter link is clicked
                $('#filter-controls a').click(function () {
                    var selector = $(this).attr('data-filter');
                    $container.isotope({ filter: selector });
                    return false;
                });
                // filter on smaller screens
                $("#e1").change(function () {
                    var selector = $(this).find(":selected").val();
                    $container.isotope({ filter: selector });
                    return false;
                });
            });

            function handleIsotopeStretch() {
                var width = $(window).width();
                if (width < 768) {
                    $('#filter-items .item').addClass('width-100');
                }
                else {
                    $('#filter-items .item').removeClass('width-100');
                }
            }
            handleIsotopeStretch();
            /* On Resize show menu on desktop if hidden */
            jQuery(window).resize(function () {
                handleIsotopeStretch();
            });



            $('.filter-content').hover(function () {
                var hoverContent = $(this).children('.hover-content');
                hoverContent.removeClass('fadeOut').addClass('animated fadeIn').show();
            }, function () {
                var hoverContent = $(this).children('.hover-content');
                hoverContent.removeClass('fadeIn').addClass('fadeOut');
            });

            $('.colorbox-button').colorbox({ rel: 'colorbox-button', maxWidth: '95%', maxHeight: '95%' });
            /* Colorbox resize function */
            var resizeTimer;
            function resizeColorBox() {
                if (resizeTimer) clearTimeout(resizeTimer);
                resizeTimer = setTimeout(function () {
                    var myWidth = 442, percentageWidth = .95;
                    if (jQuery('#cboxOverlay').is(':visible')) {
                        $.colorbox.resize({ width: ($(window).width() > (myWidth + 20)) ? myWidth : Math.round($(window).width() * percentageWidth) });
                        $('.cboxPhoto').css({
                            width: $('#cboxLoadedContent').innerWidth(),
                            height: 'auto'
                        });
                        $('#cboxLoadedContent').height($('.cboxPhoto').height());
                        $.colorbox.resize();
                    }
                }, 300)
            }


            // Resize Colorbox when resizing window or changing mobile device orientation
            jQuery(window).resize(resizeColorBox);
            window.addEventListener("orientationchange", resizeColorBox, false);

        });
    </script>

    <script src="/admin/components/imgareaselect/scripts/jquery.imgareaselect.js"></script>
    <script src="/admin/build/jquery.awesome-cropper.js"></script>
    <script>
        $(document).ready(function () {
            $('#hfilePrincipal').awesomeCropper(
            { width: 1980, height: 500, debug: true }
            );
            $('#hfilePrincipal_Tablet').awesomeCropper(
            { width: 900, height: 355, debug: true }
            );
            $('#hfilePrincipal_Mobile').awesomeCropper(
            { width: 500, height: 500, debug: true }
            );


        });


    </script>

    <!-- /JAVASCRIPTS -->
    <style>
        /*.image-fancybox img {
            width: 435px !important;
        }*/
    </style>
</asp:Content>

