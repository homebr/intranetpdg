﻿<%@ WebHandler Language="C#" Class="FileUploadNoticias" %>

using System;
using System.Web;
using System.IO;


public class FileUploadNoticias : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";

        String strLog = "inicio do código - ";
        //context.Response.Write(strLog);
        //return;

        string dirFullPath = System.Configuration.ConfigurationManager.AppSettings["pathFisico"] + "uploads\\fotos\\";
        string[] files;
        int numFiles;

        int id = FrameWork.Util.GetInt(context.Request["id"]);

         strLog += (" id=" + id + " - ");

        files = System.IO.Directory.GetFiles(dirFullPath); // + id + "*"
        numFiles = files.Length;
        numFiles = numFiles + 1;


        string str_image = "";

        strLog += (" files=" + files + " - ");

        foreach (string s in context.Request.Files)
        {

            HttpPostedFile file = context.Request.Files[s];
            //  int fileSizeInBytes = file.ContentLength;
            string fileName = file.FileName;
            string fileExtension = file.ContentType.ToLower();

            if (!string.IsNullOrEmpty(fileName))
            {
                fileExtension = Path.GetExtension(fileName);
                str_image = "noticia_" + id + "_" + numFiles.ToString() + fileExtension.ToLower();
                strLog += (" str_image=" + str_image + " - ");
                if (FrameWork.Util.GetInt(FrameWork.DataObject.DataSource.DefaulDataSource.Execute("Select count(*) as qtd from Fotos where NoticiaID=" + id + " and FileName='" + str_image + "'")) == 0)
                {

                    string pathToSave_100 = dirFullPath + str_image;
                    file.SaveAs(pathToSave_100);
                    string strSql = "insert into Fotos (NoticiaID,Posicao,FileName) values (" + id + "," + numFiles.ToString() + ",'" + str_image.ToString() + "') ";
                    FrameWork.DataObject.DataSource.DefaulDataSource.Execute(strSql);
                    strLog += " strSql=" + strSql + " - ";
                }
                else
                {
                    str_image = "Erro: Permitido apenas uma Foto";
                    return;
                }
            }
        }
        //context.Response.Write(strLog);
        
        context.Response.Write(str_image);
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}