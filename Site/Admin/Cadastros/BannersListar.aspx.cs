﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClassLibrary;
using FrameWork.WebControls;
using FrameWork.DataObject;

public partial class BannersListar : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            //txtDataInicial.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddMonths(-6));
            //txtDataFinal.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);

            TextBox1.Text = FrameWork.Util.GetString(Session["Banners_TextBox1"]);

            txtDataInicial.Text = FrameWork.Util.GetString(Session["Banners_DataInicial"]);
            txtDataFinal.Text = FrameWork.Util.GetString(Session["Banners_DataFinal"]);

            if (FrameWork.Util.GetString(Session["Banners_Ativo"]) == "A")
                rdbAtivos_A.Checked = true;
            else if (FrameWork.Util.GetString(Session["Banners_Ativo"]) == "N")
                rdbAtivos_N.Checked = true;
            else
                rdbAmbos.Checked = true;
        }
        Carrega();
    }

    public void Carrega()
    {

        string pathVirtual = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "uploads/banners/";

        String strSQL = "SELECT  IDBanner,Titulo,Link,Posicao,DataInicial,DataFinal,Ativo,('" + pathVirtual + "' + Imagem) as Imagem FROM Banners WHERE 1=1  ";
        if (FrameWork.Util.GetString(TextBox1.Text).ToString().Trim() != "")
        {
            strSQL += " and (Titulo LIKE @text) ";
            Session["Banners_TextBox1"] = TextBox1.Text;
        }
        else
            Session["Banners_TextBox1"] = "";

        if (rdbAtivos_A.Checked)
        {
            strSQL += " and (Ativo = 1) ";
            Session["Banners_Ativo"] = "A";
        }
        else if (rdbAtivos_N.Checked)
        {
            strSQL += " and (Ativo = 0) ";
            Session["Banners_Ativo"] = "N";
        }
        else
            Session["Banners_Ativo"] = "";

        if (Funcoes.IsDate(txtDataInicial.Text))
        {
            //FlagFiltro = True
            strSQL += " and Convert(char(8),datainclusao,112) >= '" + String.Format("{0:yyyyMMdd}", Convert.ToDateTime(txtDataInicial.Text)) + "'  ";
        }
        if (Funcoes.IsDate(txtDataFinal.Text))
        {
            //FlagFiltro = True
            strSQL += " and Convert(char(8),datainclusao,112) <= '" + String.Format("{0:yyyyMMdd}", Convert.ToDateTime(txtDataFinal.Text)) + "'  ";
        }

        strSQL += " ORDER BY posicao  ";

        //Response.Write(strSQL);


        DataTable dt = new DataTable();
        dt = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSQL, new ParameterItem("@text", TextBox1.Text));



        Repeater1.DataSource = dt;
        Repeater1.DataBind();



    }
}