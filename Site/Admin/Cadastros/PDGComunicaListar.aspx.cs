﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClassLibrary;
using FrameWork.WebControls;
using FrameWork.DataObject;


public partial class PDGComunica_Listar : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            //txtDataInicial.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddMonths(-6));
            //txtDataFinal.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            txtDataInicial.Text = FrameWork.Util.GetString(Session["Noticias_DataInicial"]);
            txtDataFinal.Text = FrameWork.Util.GetString(Session["Noticias_DataFinal"]);

            if (FrameWork.Util.GetString(Session["Noticias_Ativo"]) == "A")
                rdbAtivos_A.Checked = true;
            else if (FrameWork.Util.GetString(Session["Noticias_Ativo"]) == "N")
                rdbAtivos_N.Checked = true;
            else
                rdbAmbos.Checked = true;
        }
        Carrega();
    }

    public void Carrega()
    {

        String strSQL = "SELECT  n.id,n.Titulo,n.Ativo, n.Data,n.destaque  FROM PDGComunica n where 1=1  ";
        if (FrameWork.Util.GetString(TextBox1.Text).ToString().Trim() != "")
        {
            strSQL += " and (n.Titulo LIKE '%" + TextBox1.Text + "%' or n.Conteudo LIKE '%" + TextBox1.Text + "%') ";
            Session["Noticias_TextBox1"] = TextBox1.Text;
        }
        if (Funcoes.IsDate(txtDataInicial.Text))
        {
            //FlagFiltro = True
            strSQL += " and Convert(char(8),n.datainclusao,112) >= '" + String.Format("{0:yyyyMMdd}", Convert.ToDateTime(txtDataInicial.Text)) + "'  ";
        }
        if (Funcoes.IsDate(txtDataFinal.Text))
        {
            //FlagFiltro = True
            strSQL += " and Convert(char(8),n.datainclusao,112) <= '" + String.Format("{0:yyyyMMdd}", Convert.ToDateTime(txtDataFinal.Text)) + "'  ";
        }
        if (rdbAtivos_A.Checked)
        {
            strSQL += " and (n.Ativo = 1) ";
            Session["Noticias_Ativo"] = "A";
        }
        else if (rdbAtivos_N.Checked)
        {
            strSQL += " and (n.Ativo = 0) ";
            Session["Noticias_Ativo"] = "N";
        }
        else
            Session["Noticias_Ativo"] = "";

        strSQL += " ORDER BY n.ID DESC ";

        //Response.Write(strSQL);


        DataTable dt = new DataTable();
        dt = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSQL, new ParameterItem("@text", TextBox1.Text));



        Repeater1.DataSource = dt;
        Repeater1.DataBind();



    }
}

