﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="DestaquesEditar.aspx.cs" Inherits="DestaquesVitrine" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

  <!--Switchery [ OPTIONAL ]-->
    <link href="/admin/content/plugins/switchery/switchery.min.css" rel="stylesheet">
    <link href="/admin/content/css/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet" type="text/css" />

    <!--Ion Icons [ OPTIONAL ]-->
    <link href="/admin/content/plugins/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="/admin/content/plugins/themify-icons/themify-icons.min.css" rel="stylesheet">
    <script src="/admin/content/js/demo/icons.js"></script>


    <link href="/admin/content/plugins/spectrum/spectrum.css" rel="stylesheet">



    <!-- ANIMATE -->
    <link rel="stylesheet" type="text/css" href="/admin/content/css/animatecss/animate.min.css" />
    <!-- COLORBOX -->
    <link rel="stylesheet" type="text/css" href="/admin/content/js/colorbox/colorbox.min.css" />


    <!-- BOOTSTRAP SWITCH -->
    <link rel="stylesheet" type="text/css" href="/admin/content/js/bootstrap-switch/bootstrap-switch.min.css" />
    <link rel="stylesheet" type="text/css" href="/admin/content/css/themes/default.css" id="skin-switcher">

    <!-- JQUERY UI-->
    <link rel="stylesheet" type="text/css" href="/admin/content/js/jquery-ui-1.10.3.custom/css/custom-theme/jquery-ui-1.10.3.custom.min.css" />

    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="/admin/content/plugins/iCheck/all.css">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="/admin/content/plugins/colorpicker/bootstrap-colorpicker.min.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="/admin/content/plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="/admin/content/plugins/select2/select2.min.css">


    <!--Magic Checkbox [ OPTIONAL ]-->
    <link href="/admin/content/plugins/magic-check/css/magic-check.min.css" rel="stylesheet">


    <style type="text/css">
        div.inputnotes div {
            padding: 3px;
            padding-left: 4px;
            padding-top: 4px;
            margin-top: 2px;
        }

            div.inputnotes div.note {
                color: #fff;
                background: #333;
            }

            div.inputnotes div.warning {
                color: #fff;
                background: #f03;
            }

        input:focus {
            background-color: #316AC5;
            color: #FFFFFF;
        }

        input:hover {
            background-color: #FFFFFF;
            color: #000000;
        }
        .btn-edit
        {
            background-color: orange !important;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!--Page Title-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <div id="page-title">
        <h1 class="page-header text-overflow">Destaques Home</h1>

    </div>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End page title-->

    <!--Breadcrumb-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <ol class="breadcrumb">
        <li><a href="/">Home</a></li>
        <li class="active">Destaques Home</li>
        <li>Editar</li>
    </ol>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End breadcrumb-->




        <!-- Main content -->
        <div id="page-content">

            <form id="form1" runat="server">
                <asp:Label ID="lblMensagem" runat="server" Text="" Font-Bold="true" ForeColor="Red" Visible="false"></asp:Label>

                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Dados</h3>
                    </div>
                    <div class="panel-body">

                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>Título:<span class="required">*</span></label>
                                <asp:TextBox ID="txtTitulo" CssClass="form-control" required="required" runat="server" MaxLength="80" Width="100%" placeholder="Título"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>URL do Link:</label>
                                <asp:TextBox ID="txtURL" CssClass="form-control" runat="server" MaxLength="250" Width="100%" placeholder="URL do link"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2">
                                <label>Ativar:</label>
                                <div>
                                    <div class="make-switch switch-small" data-on="success" data-off="danger" data-on-label="<i class='fa fa-check icon-white'></i>" data-off-label="<i class='fa fa-times'></i>">
                                        <asp:CheckBox ID="chkAtivo" runat="server" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <label>Posição:</label>
                                <asp:TextBox ID="txtPosicao" CssClass="form-control" runat="server" MaxLength="2" Width="60px" Value="1"></asp:TextBox>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-4">
                                <label for="ContentPlaceHolder1_lblDataInclusao">Data Inclusão:</label>
                                <asp:Label ID="lblDataInclusao" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="col-sm-4">
                                <label for="ContentPlaceHolder1_lblDataAlteracao">Data Alteração:</label>
                                <asp:Label ID="lblDataAlteracao" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="col-sm-4">
                                <label for="ContentPlaceHolder1_lblUsuario">Usuário:</label>
                                <asp:Label ID="lblUsuario" runat="server" Text=""></asp:Label>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Literal ID="ltlLOG" runat="server"></asp:Literal>
                        </div>


                    </div>
                    <div class="panel-footer text-right">
                        <asp:LinkButton ID="btnSalvar" runat="server" CssClass="btn btn-success col-sm-1" OnClick="btnSalvar_Click"><i class="fa fa-save fa-1x"></i> SALVAR</asp:LinkButton>&nbsp;&nbsp;
                        <asp:Button ID="btnExcluir" runat="server" Text="EXCLUIR" CssClass="btn btn-warning fa-delicious" Visible="false" OnClientClick="return confirm('Confirma exclusão deste registro?');" OnClick="btnExcluir_Click" />&nbsp;&nbsp;
                        <asp:Button ID="btnVoltar" runat="server" Text="VOLTAR" CssClass="btn fa-reply" PostBackUrl="Destaques" />

                        <asp:HiddenField ID="hOperacao" runat="server" />
                        <asp:HiddenField ID="hID" runat="server" />
                        <asp:HiddenField ID="hUrlReferrer" runat="server" />
                        <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                    </div>

                </div>

            </form>
            <br />

        </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="Server">

   <!-- JQUERY UI-->
    <link rel="stylesheet" type="text/css" href="/admin/content/js/jquery-ui-1.10.3.custom/css/custom-theme/jquery-ui-1.10.3.custom.min.css" />

    <!-- DATE RANGE PICKER -->
    <script src="/admin/content/js/bootstrap-daterangepicker/moment.min.js"></script>
    <script src="/admin/content/js/bootstrap-daterangepicker/daterangepicker.min.js"></script>


    <!-- bootstrap color picker -->
    <script src="/admin/content/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
    <!-- bootstrap time picker -->
    <script src="/admin/content/plugins/timepicker/bootstrap-timepicker.min.js"></script>

    <!-- iCheck 1.0.1 -->
    <script src="/admin/content/plugins/iCheck/icheck.min.js"></script>


    <!-- BOOTSTRAP SWITCH -->
    <script type="text/javascript" src="/admin/content/js/bootstrap-switch/bootstrap-switch.min.js"></script>

    <!-- ISOTOPE -->
    <script type="text/javascript" src="/admin/content/js/isotope/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="/admin/content/js/isotope/imagesloaded.pkgd.min.js"></script>
    <!-- COLORBOX -->
    <script type="text/javascript" src="/admin/content/js/colorbox/jquery.colorbox.min.js"></script>

    <!-- KNOB -->
    <script type="text/javascript" src="/admin/content/js/jQuery-Knob/js/jquery.knob.min.js"></script>


    <script type="text/jscript" src="/admin/content/js/jquery.maskedinput-1.3.1.js"></script>


    <script type="text/jscript">

        var campo = '<%=hID.ClientID.ToString().Replace("hID","")%>';


    </script>


</asp:Content>

