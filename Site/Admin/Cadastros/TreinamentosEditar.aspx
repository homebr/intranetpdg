﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="TreinamentosEditar.aspx.cs" Inherits="Treinamentos_Editar" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <!--Switchery [ OPTIONAL ]-->
    <link href="/admin/content/plugins/switchery/switchery.min.css" rel="stylesheet">
    <link href="/admin/content/css/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet" type="text/css" />

    <!--Ion Icons [ OPTIONAL ]-->
    <link href="/admin/content/plugins/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="/admin/content/plugins/themify-icons/themify-icons.min.css" rel="stylesheet">
    <script src="/admin/content/js/demo/icons.js"></script>


    <link href="/admin/content/plugins/spectrum/spectrum.css" rel="stylesheet">



    <!-- ANIMATE -->
    <link rel="stylesheet" type="text/css" href="/admin/content/css/animatecss/animate.min.css" />
    <!-- COLORBOX -->
    <link rel="stylesheet" type="text/css" href="/admin/content/js/colorbox/colorbox.min.css" />


    <!-- BOOTSTRAP SWITCH -->
    <link rel="stylesheet" type="text/css" href="/admin/content/js/bootstrap-switch/bootstrap-switch.min.css" />
    <link rel="stylesheet" type="text/css" href="/admin/content/css/themes/default.css" id="skin-switcher">

    <!-- JQUERY UI-->
    <link rel="stylesheet" type="text/css" href="/admin/content/js/jquery-ui-1.10.3.custom/css/custom-theme/jquery-ui-1.10.3.custom.min.css" />

    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="/admin/content/plugins/iCheck/all.css">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="/admin/content/plugins/colorpicker/bootstrap-colorpicker.min.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="/admin/content/plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="/admin/content/plugins/select2/select2.min.css">

    <script src="https://cdn.ckeditor.com/4.7.3/standard-all/ckeditor.js"></script>

    <!--Magic Checkbox [ OPTIONAL ]-->
    <link href="/admin/content/plugins/magic-check/css/magic-check.min.css" rel="stylesheet">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!--Page Title-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <div id="page-title">
        <h1 class="page-header text-overflow">Treinamentos</h1>

    </div>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End page title-->

    <!--Breadcrumb-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <ol class="breadcrumb">
        <li><a href="/">Home</a></li>
        <li class="active">Treinamentos</li>
        <li>Editar</li>
    </ol>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End breadcrumb-->



    <div id="page-content">
        <form id="form1" runat="server">

            <asp:Label ID="lblMensagem" runat="server" CssClass="box-danger alert alert-danger" Style="display: block" Visible="false"></asp:Label>


            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Dados</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="ContentPlaceHolder1_Nome">ID:</label>
                            <asp:Label ID="lblNoticiaID" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>Treinamento:<span class="required">*</span></label>
                                <asp:TextBox ID="txtTitulo" CssClass="form-control" required="required" runat="server" MaxLength="140" Width="100%" placeholder="Título do Treinamento"></asp:TextBox>
                            </div>
                            <%--<div class="col-sm-2">
                                <label>Categoria:</label>
                                <asp:DropDownList ID="ddlCategoria" CssClass="form-control" runat="server"></asp:DropDownList>
                            </div>--%>
                           <%-- <div class="col-sm-2">
                                <label>Tipo:</label>
                                <asp:DropDownList ID="ddlTipo" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="1">Texto</asp:ListItem>
                                    <asp:ListItem Value="2">Texto + Imagem</asp:ListItem>
                                    <asp:ListItem Value="3">Texto + Video</asp:ListItem>
                                </asp:DropDownList>
                            </div>--%>
                        </div>
                    </div>
                 <%--   <div class="form-group">
                        <div class="col-sm-12">
                            <label for="ContentPlaceHolder1_DocIdentidade">Chamada:</label>
                            <asp:TextBox CssClass="form-control" TextMode="MultiLine" ID="txtChamada" runat="server" Height="50" Width="100%"></asp:TextBox>
                        </div>
                    </div>--%>

                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="ContentPlaceHolder1_DocIdentidade">Conteúdo:</label>
                            <asp:TextBox CssClass="form-control" TextMode="MultiLine" ID="txtConteudo" runat="server" Height="250" Width="100%"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-4">
                            <label for="ContentPlaceHolder1_Endereco">Data:</label>
                            <asp:TextBox ID="txtData" CssClass="form-control datepicker" runat="server" required="required" MaxLength="15" Width="100px" placeholder="dd/mm/yyyy"></asp:TextBox>
                        </div>
                           <%--  <div class="col-sm-8">
                                    <label for="Nome">Visível para:<span class="required">*</span></label>
                                      <asp:DropDownList ID="ddlSetor" CssClass="form-control" required="required" runat="server">
                                          <asp:ListItem Value="0">Todos</asp:ListItem>
                                          <asp:ListItem Value="1" >Conselho de Administração</asp:ListItem>
                                          <asp:ListItem Value="2">Conselho Fiscal</asp:ListItem>
                                      </asp:DropDownList>
                                </div>--%>
                    </div>

                    <div class="col-sm-12">
                        <div class="separator"></div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label>Ativar:</label>
                            <div>
                                <div class="make-switch switch-small" data-on="success" data-off="danger" data-on-label="<i class='fa fa-check icon-white'></i>" data-off-label="<i class='fa fa-times'></i>">
                                    <asp:CheckBox ID="chkAtivo" runat="server" />
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label>Aparecer no Feed:</label>
                            <div>
                                <div class="make-switch switch-small" data-on="success" data-off="danger" data-on-label="<i class='fa fa-check icon-white'></i>" data-off-label="<i class='fa fa-times'></i>">
                                    <asp:CheckBox ID="chkDestaque" runat="server" />
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label for="ContentPlaceHolder1_DocIdentidade">Video:</label>
                            <asp:TextBox CssClass="form-control" ID="txtvideo" runat="server" MaxLength="140" Width="100%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="separator"></div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-4">
                            <label>Imagem:</label>
                            <asp:FileUpload ID="ImageDestaque" CssClass="form-control" runat="server" Width="100%"></asp:FileUpload>
                            <small>Upload (Gif/Jpg/Png) formato: 1980 x 1000</small>
                        </div>
                      <%--  <div class="col-sm-4">
                            <label>Imagem iPad:</label>
                            <asp:FileUpload ID="ImageDestaque_iPad" CssClass="form-control" runat="server" Width="100%"></asp:FileUpload>
                            <small>Upload (Gif/Jpg/Png) formato: 900 x 355</small>
                        </div>
                        <div class="col-sm-4">
                            <label>Imagem Mobile:</label>
                            <asp:FileUpload ID="ImageDestaque_Mobile" CssClass="form-control" runat="server" Width="100%"></asp:FileUpload>
                            <small>Upload (Gif/Jpg/Png) formato: 500 x 350</small>
                        </div>--%>
                    </div>
                    <div class="col-sm-12">
                        <div class="separator"></div>
                    </div>

                   <%-- <div class="form-group">
                        <div class="col-sm-12">
                            <div class="col-sm-6">
                                <label for="ContentPlaceHolder1_Autor">Autor:</label>
                                <asp:TextBox ID="txtAutor" CssClass="form-control" runat="server" MaxLength="50" Width="100%"></asp:TextBox>
                            </div>
                            <div class="col-sm-6">
                                <label for="ContentPlaceHolder1_Autor">Fonte:</label>
                                <asp:TextBox ID="txtFonte" CssClass="form-control" runat="server" MaxLength="50" Width="100%"></asp:TextBox>
                            </div>
                        </div>
                    </div>--%>
               <%--     <div class="col-sm-12">
                        <div class="separator"></div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-4">
                            <asp:Image ID="Image4" runat="server" CssClass="img-responsive" />
                        </div>
                        <div class="col-sm-8">
                            <label>Foto do Autor:</label>
                            <asp:FileUpload ID="ImagemAutor" CssClass="form-control" runat="server" Width="100%"></asp:FileUpload>
                            <small>Upload (Gif/Jpg/Png) formato: 980 x 500</small>
                        </div>

                    </div>--%>



                    <div class="col-sm-12">
                        <div class="separator"></div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-4">
                            <label for="lblDataInclusao">Data Inclusão:</label>
                            <asp:Label ID="lblDataInclusao" runat="server" Text=""></asp:Label>
                        </div>
                        <div class="col-sm-4">
                            <label for="lblDataAlteracao">Data Alteração:</label>
                            <asp:Label ID="lblDataAlteracao" runat="server" Text=""></asp:Label>
                        </div>
                        <div class="col-sm-4">
                            <label for="lblUsuario">Usuário:</label>
                            <asp:Label ID="lblUsuario" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Literal ID="ltlLOG" runat="server"></asp:Literal>
                    </div>

                </div>
                <div class="panel-footer text-right">
                    <asp:LinkButton ID="btnSalvar" runat="server" CssClass="btn btn-success col-sm-1" OnClick="btnSalvar_Click"><i class="fa fa-save fa-1x"></i> SALVAR</asp:LinkButton>&nbsp;&nbsp;

                   <asp:LinkButton ID="btnExcluir" runat="server" CssClass="btn btn-warning col-sm-1" OnClick="btnExcluir_Click" Visible="true"><i class="fa fa-trash fa-1x"></i> EXCLUIR</asp:LinkButton>&nbsp;&nbsp;
                    <asp:Button ID="btnVoltar" runat="server" Text="VOLTAR" CssClass="btn fa-reply" PostBackUrl="TreinamentosListar.aspx" />

                    <br />
                    <br />
                    <asp:HiddenField ID="hOperacao" runat="server" />
                    <asp:HiddenField ID="hID" runat="server" />
                    <asp:HiddenField ID="hUrlReferrer" runat="server" />
                    <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                </div>
            </div>


        </form>

    </div>

    <asp:Panel ID="pnlFotos" Visible="true" runat="server">
        <p>

            <!-- GALERIA -->
            <div id="filter-items" class="row">

                <div class="col-md-3 category_1 item">
                    <div class="filter-content">
                        <asp:Image ID="Image1" runat="server" CssClass="img-responsive" />
                    </div>
                </div>
                <div class="col-md-2 category_1 item">
                    <div class="filter-content">
                        <asp:Image ID="Image2" runat="server" CssClass="img-responsive" />
                    </div>
                </div>
                <div class="col-md-1 category_1 item">
                    <div class="filter-content">
                        <asp:Image ID="Image3" runat="server" CssClass="img-responsive" />
                    </div>
                </div>
            </div>
            <!-- /GALERIA -->
        </p>
    </asp:Panel>



</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="Server">
    <!-- JQUERY UI-->
    <link rel="stylesheet" type="text/css" href="/admin/content/js/jquery-ui-1.10.3.custom/css/custom-theme/jquery-ui-1.10.3.custom.min.css" />

    <!-- DATE RANGE PICKER -->
    <script src="/admin/content/js/bootstrap-daterangepicker/moment.min.js"></script>
    <script src="/admin/content/js/bootstrap-daterangepicker/daterangepicker.min.js"></script>


    <!-- bootstrap color picker -->
    <script src="/admin/content/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
    <!-- bootstrap time picker -->
    <script src="/admin/content/plugins/timepicker/bootstrap-timepicker.min.js"></script>

    <!-- iCheck 1.0.1 -->
    <script src="/admin/content/plugins/iCheck/icheck.min.js"></script>


    <!-- BOOTSTRAP SWITCH -->
    <script type="text/javascript" src="/admin/content/js/bootstrap-switch/bootstrap-switch.min.js"></script>

    <!-- ISOTOPE -->
    <script type="text/javascript" src="/admin/content/js/isotope/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="/admin/content/js/isotope/imagesloaded.pkgd.min.js"></script>
    <!-- COLORBOX -->
    <script type="text/javascript" src="/admin/content/js/colorbox/jquery.colorbox.min.js"></script>

    <!-- KNOB -->
    <script type="text/javascript" src="/admin/content/js/jQuery-Knob/js/jquery.knob.min.js"></script>


    <script type="text/jscript" src="/admin/content/js/jquery.maskedinput-1.3.1.js"></script>

    <script type="text/jscript">

        var campo = '<%=hID.ClientID.ToString().Replace("hID","")%>';


        $(document).ready(function () {
            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });

            obj = $("#" + campo + "chkIndiferente").parent();
            obj2 = $(obj).find(".bootstrap-switch-handle-off");
            $(obj2).removeClass('bootstrap-switch-default').addClass('bootstrap-switch-danger');

        });



        $(document).ready(function () {
            $("#" + campo + "txtDataInicial").mask("99/99/9999");
            $("#" + campo + "txtDataFinal").mask("99/99/9999");

            //$(".datepicker").datepicker({
            //    changeMonth: true,
            //    changeYear: true,
            //    dateFormat: 'dd/mm/yy',
            //    dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado', 'Domingo'],
            //    dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
            //    dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
            //    monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            //    monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez']
            //});

        });


        $(function () {
            $(".accordion-toggle").click(function () {
                var $this = $(this);
                if ($this.hasClass('icon-chevron-up')) {
                    $this.removeClass('icon-chevron-up').addClass('icon-chevron-down');
                } else {
                    $this.removeClass('icon-chevron-down').addClass('icon-chevron-up');
                }
            });
            return false;
        });

        jQuery(document).ready(function () {

            //$('#hfileDestaque').awesomeCropper(
            //    { width: 1110, height: 310, debug: true }
            //);
            //$('#hfilePequena').awesomeCropper(
            //    { width: 500, height: 350, debug: true }
            //);



            // cache container
            var $container = $('#filter-items');
            // initialize isotope after image loaded
            $container.imagesLoaded(function () {
                $container.isotope({
                    // options...
                });

                // filter items when filter link is clicked
                $('#filter-controls a').click(function () {
                    var selector = $(this).attr('data-filter');
                    $container.isotope({ filter: selector });
                    return false;
                });
                // filter on smaller screens
                $("#e1").change(function () {
                    var selector = $(this).find(":selected").val();
                    $container.isotope({ filter: selector });
                    return false;
                });
            });

            function handleIsotopeStretch() {
                var width = $(window).width();
                if (width < 768) {
                    $('#filter-items .item').addClass('width-100');
                }
                else {
                    $('#filter-items .item').removeClass('width-100');
                }
            }
            handleIsotopeStretch();
            /* On Resize show menu on desktop if hidden */
            jQuery(window).resize(function () {
                handleIsotopeStretch();
            });



            $('.filter-content').hover(function () {
                var hoverContent = $(this).children('.hover-content');
                hoverContent.removeClass('fadeOut').addClass('animated fadeIn').show();
            }, function () {
                var hoverContent = $(this).children('.hover-content');
                hoverContent.removeClass('fadeIn').addClass('fadeOut');
            });

            $('.colorbox-button').colorbox({ rel: 'colorbox-button', maxWidth: '95%', maxHeight: '95%' });
            /* Colorbox resize function */
            var resizeTimer;
            function resizeColorBox() {
                if (resizeTimer) clearTimeout(resizeTimer);
                resizeTimer = setTimeout(function () {
                    var myWidth = 442, percentageWidth = .95;
                    if (jQuery('#cboxOverlay').is(':visible')) {
                        $.colorbox.resize({ width: ($(window).width() > (myWidth + 20)) ? myWidth : Math.round($(window).width() * percentageWidth) });
                        $('.cboxPhoto').css({
                            width: $('#cboxLoadedContent').innerWidth(),
                            height: 'auto'
                        });
                        $('#cboxLoadedContent').height($('.cboxPhoto').height());
                        $.colorbox.resize();
                    }
                }, 300)
            }


            // Resize Colorbox when resizing window or changing mobile device orientation
            jQuery(window).resize(resizeColorBox);
            window.addEventListener("orientationchange", resizeColorBox, false);

        });


        CKEDITOR.replace('<%=txtConteudo.ClientID %>', {
            language: 'br',
            filebrowserBrowseUrl: 'Upload.ashx',
            filebrowserImageBrowseUrl: 'Upload.ashx',
            filebrowserUploadUrl: 'Upload.ashx',
            filebrowserImageUploadUrl: 'Upload.ashx',
            toolbar: [
                { name: 'document', groups: ['mode', 'document', 'doctools'], items: ['Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates'] },
                { name: 'clipboard', groups: ['clipboard', 'undo'], items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'] },
                { name: 'editing', groups: ['find', 'selection', 'spellchecker'], items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt'] },
                { name: 'forms', items: ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'] },
                { name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat'] },
                { name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language'] },
                { name: 'links', items: ['Link', 'Unlink', 'Anchor'] },
                { name: 'insert', items: ['Image', 'Table', 'HorizontalRule', 'Smiley'] },
                { name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize'] },
                { name: 'colors', items: ['TextColor', 'BGColor'] },
                { name: 'tools', items: ['Maximize', 'ShowBlocks'] },
                { name: 'others', items: ['-'] }

            ]
        });
        //var charCount = document.getElementById("ctl00_ContentPlaceHolder1_txtConteudo");
        //CKEDITOR.instances.ctl00_ContentPlaceHolder1_txtConteudo.on("key", function (event)
        //{
        //    var curVal = charCount.value.length; //get current value
        //     charCount.innerHTML = curVal; //update the span to show the current number of characters
        //});
    </script>

</asp:Content>

