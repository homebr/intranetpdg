﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClassLibrary;
using FrameWork.WebControls;
using FrameWork.DataObject;

public partial class Cadastros_Comentarios : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)


    {

        if (this.Request["id"] != null)
        {

            int id = Convert.ToInt32(this.Request["id"]);
            Boolean liberado = Convert.ToBoolean(this.Request["liberado"]);




            //update libera comentario
            if (liberado == false)
            {
                String strSql = "update comentarios set liberado = 1 where  id=" + id;
                FrameWork.DataObject.DataSource.DefaulDataSource.Execute(strSql);
            }
           else
            { 
                String strSql = "update comentarios set liberado = 0 where  id=" + id;
            FrameWork.DataObject.DataSource.DefaulDataSource.Execute(strSql);
            }
            Response.Redirect("comentarios", false);
        }
        Carrega();
    }


    public void Carrega()
    {

        string pathPDFs = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "uploads/colaboradores/";

        String strSQL = "SELECT  c.id, c.idFeed, c.idColaborador,co.nome, c.IdTipoFeed,t.Feed, c.Comentario, c.DataInclusao, c.Liberado, ";
        strSQL += "CASE WHEN c.idtipofeed = 1  THEN e.titulo WHEN c.idtipofeed = 2  THEN pc.titulo WHEN c.idtipofeed = 3  THEN pe.titulo WHEN c.idtipofeed = 4  THEN tr.titulo END as titulofeed ";
        strSQL += "FROM Comentarios c";
        strSQL += " inner join Colaboradores co on co.ID = c.idColaborador";
        strSQL += " inner join TabFeed t on t.id = c.IdTipoFeed";
        strSQL += " left join eventos e on e.id = c.idfeed";
        strSQL += " left join PDGComunica pc on pc.id = c.idfeed";
        strSQL += "  left join pdgemcasa pe on pe.id = c.idfeed ";
        strSQL += " left join Treinamentos tr on tr.id = c.idfeed where 1=1 ";
        if (FrameWork.Util.GetString(TextBox1.Text).ToString().Trim() != "")
            strSQL += " and (c.Comentario LIKE @text) ";
        if (rdbAtivos_A.Checked)
        {
            strSQL += " and (c.Liberado = 1) ";
            Session["Banners_Ativo"] = "A";
        }
        else if (rdbAtivos_N.Checked)
        {
            strSQL += " and (c.Liberado = 0) ";
            Session["Banners_Ativo"] = "N";
        }
        else
            Session["Banners_Ativo"] = "";

        strSQL += " ORDER BY ID desc ";

        //Response.Write(strSQL);


        DataTable dt = new DataTable();
        dt = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSQL, new ParameterItem("@text", TextBox1.Text));



        Repeater1.DataSource = dt;
        Repeater1.DataBind();



    }
}