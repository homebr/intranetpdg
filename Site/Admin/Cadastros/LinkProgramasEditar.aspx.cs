﻿using System;
using System.IO;

public partial class Cadastros_LinkProgramasEditar : System.Web.UI.Page
{
    ClassLibrary.LinkProgramas objLinkProgramas = new ClassLibrary.LinkProgramas();
    public String strImage = "";
    string pathVirtual = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "uploads/logoProgramas/";


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(this.IsPostBack))
        {

            //txtPosicao.Attributes.Add("type", "number");
            //txtPosicao.Attributes.Add("min", "1");
            //txtPosicao.Attributes.Add("max", "99");

            //CarregaDepartamentos();

            ////Direitos do Usuário
            //if (!this.UserInfo.Admin && !CheckUserRight(101, "C"))
            //{
            //    Response.Redirect("Default.aspx");
            //}
            //if (!this.UserInfo.Admin && !CheckUserRight(101, "I"))
            //{
            //    this.btnNovaNoticia.Visible = false;
            //}
            //if (!this.UserInfo.Admin && !CheckUserRight(101, "E"))
            //{
            //    this.btnSalvar.Visible = false;
            //}

            //ID DO EMPREENDIMENTO
            //SE NAO HA NADA DENTRO DO CONTROLE HIDDEN PEGA VIA REQUEST
            if (this.Request["id"] != null)
                this.hID.Value = FrameWork.Util.GetString(this.Request["id"]);

            //CARREGA DADOS
            Carrega();

        }
    }

    public void Carrega()
    {
        //OPCAO DE EDICAO PARA VISUALIZAR OS DADOS
        if (FrameWork.Util.GetInt(hID.Value) > 0)
        {
            objLinkProgramas.Load(this.hID.Value);

            //REGISTRO LOCALIZADO ENTAO APENAS EDITA DADOS DO FORMULARIO
            if (objLinkProgramas.TotalLoad == 1)
            {

                txtNome.Text = objLinkProgramas.Nome;
                txtLink.Text = objLinkProgramas.Link;
                txtdescricao.Text = objLinkProgramas.Descricao;
                txtsequencia.Text = FrameWork.Util.GetString(objLinkProgramas.Sequencia);
                TxtClass.Text = FrameWork.Util.GetString(objLinkProgramas.Class);
            

                if (FrameWork.Util.GetString(objLinkProgramas.Logo).Length > 1)
                {
                    strImage = pathVirtual + FrameWork.Util.GetString(objLinkProgramas.Logo);
                    this.Image1.ImageUrl = strImage+"?t=" + String.Format("{0:yyyyMMddHHmm}",DateTime.Now);
                }


                //txtPosicao.Text = FrameWork.Util.GetString(objColaboradores.Sequencia);
                chkAtivo.Checked = (Convert.ToBoolean(objLinkProgramas.Ativo));
                //if (this.ddlSituacao.Items.FindByValue(objColaboradores.Situacao) != null)
                //    this.ddlSituacao.SelectedValue = FrameWork.Util.GetString(objColaboradores.Situacao);

              
             

                hOperacao.Value = "UPDATE";

                btnExcluir.Visible = true;


            }
            else
            {
                //SENAO ACHOU O CODIGO EM QUESTAO É INSERÇÃO

            }
        }
        else
        {
            //ZERA TODOS OS CAMPOS DO FORMULARIO

        }
    }

    //public void CarregaDepartamentos()
    //{
    //    string strSql = "SELECT * FROM Departamentos where Situacao='A'  ";
    //    ddlDepartamento.DataSource = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSql);
    //    ddlDepartamento.DataTextField = "Nome";
    //    ddlDepartamento.DataValueField = "ID";
    //    ddlDepartamento.DataBind();

    //}

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        //validação de campos
        if (FrameWork.Util.GetString(txtNome.Text) == "")
        {
            Msg("Favor inserir um Nome!", false);
            txtNome.Focus();
            return;
        }


        if (FrameWork.Util.GetInt(hID.Value) > 0)
        {
            //Comandos de Atualização do Empreendimento
            objLinkProgramas.Load(hID.Value);
           // objLinkProgramas.DataAlteracao = DateTime.Now;

        }
        else
        {
            hOperacao.Value = "INSERT";
           // objLinkProgramas.DataInclusao = DateTime.Now;
        }


        //salva os valores
        //objColaboradores.SessaoID = Convert.ToInt32(this.ddlSessao.SelectedValue);
        //objColaboradores.EditorialID = Convert.ToInt32(this.ddlEditorial.SelectedValue);

        objLinkProgramas.Nome = this.txtNome.Text;
        objLinkProgramas.Link = this.txtLink.Text;
        objLinkProgramas.Descricao = this.txtdescricao.Text;
        objLinkProgramas.Sequencia = Convert.ToInt32(txtsequencia.Text);
        objLinkProgramas.Class = this.TxtClass.Text;
             objLinkProgramas.Ativo = chkAtivo.Checked;
     
        


        try
        {
            objLinkProgramas.Save();
            this.hID.Value = objLinkProgramas.ID.ToString();

            hOperacao.Value = "UPDATE";
            Msg("Salvo com sucesso!", true);
            //this.abaFotos.Visible = true;

        }
        catch (Exception ex)
        {
            Msg(ex.Message, false);
        }

        try
        {


            if ((txtArquivo.HasFile))
            {

                //inicializar as variáveis
                string arq = txtArquivo.PostedFile.FileName;
                string fileExtension = txtArquivo.PostedFile.ContentType.ToLower();

                if (!string.IsNullOrEmpty(arq))
                {


                    fileExtension = Path.GetExtension(arq);
                    string str_image = "linkprogramas-" + hID.Value + fileExtension.ToLower();

                    //verificamos a extensão através dos últimos 4 caracteres e verificamos se é permitido
                    if ((!(fileExtension == ".gif" || fileExtension == ".jpg" || fileExtension == ".png")))
                    {
                        Msg("Extensão de arquivo não permitido! Somente é permitido extensão: gif, jpg ou png!", false);
                        return;
                    }


                    //tamanho maximo do upload em kb
                    double permitido = 2000;
                    //identificamos o tamanho do arquivo
                    double tamanho = 0;
                    tamanho = Convert.ToDouble(txtArquivo.PostedFile.ContentLength) / 1024;
                    if ((tamanho > permitido))
                    {
                        Msg("Tamanho Máximo permitido é de " + permitido + " kb", false);
                        return;
                    }

                    //caminho fisico no server
                    string pathPDFs = System.Configuration.ConfigurationManager.AppSettings["pathFisico"] + "uploads\\logoProgramas\\";

                    txtArquivo.SaveAs(pathPDFs + str_image);

                    String strSql = "UPDATE linkprogramas SET logo = '" + str_image + "' WHERE ID=" + hID.Value;
                    FrameWork.DataObject.DataSource.DefaulDataSource.Execute(strSql);

                    txtArquivo.Dispose();

                    strImage = pathVirtual + str_image;
                    this.Image1.ImageUrl = strImage + "?t=" + String.Format("{0:yyyyMMddHHmm}", DateTime.Now);
                }

            }


        }
        catch (Exception ex)
        {
            Msg(ex.Message, false);
            return;
        }


    }

    protected void btnExcluir_Click(object sender, EventArgs e)
    {

        try
        {
            objLinkProgramas.Load(hID.Value);
            objLinkProgramas.Delete();
            Response.Redirect("Colaboradores");
        }
        catch (Exception ex)
        {
            Msg(ex.Message, false);
        }
    }

    protected void Msg(String msg, Boolean tipo)
    {
        lblMensagem.CssClass = (tipo ? "box-danger alert alert-success" : "box-danger alert alert-danger");
        lblMensagem.Text = msg;
        lblMensagem.Visible = true;
    }

}