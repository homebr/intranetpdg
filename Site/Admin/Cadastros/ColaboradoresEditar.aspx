﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="ColaboradoresEditar.aspx.cs" Inherits="Cadastros_ColaboradoresEditar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <!--Switchery [ OPTIONAL ]-->
    <link href="/admin/content/plugins/switchery/switchery.min.css" rel="stylesheet">
    <link href="/admin/content/css/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet" type="text/css" />

    <!--Ion Icons [ OPTIONAL ]-->
    <link href="/admin/content/plugins/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="/admin/content/plugins/themify-icons/themify-icons.min.css" rel="stylesheet">
    <script src="/admin/content/js/demo/icons.js"></script>


    <link href="/admin/content/plugins/spectrum/spectrum.css" rel="stylesheet">



    <!-- ANIMATE -->
    <link rel="stylesheet" type="text/css" href="/admin/content/css/animatecss/animate.min.css" />
    <!-- COLORBOX -->
    <link rel="stylesheet" type="text/css" href="/admin/content/js/colorbox/colorbox.min.css" />


    <!-- BOOTSTRAP SWITCH -->
    <link rel="stylesheet" type="text/css" href="/admin/content/js/bootstrap-switch/bootstrap-switch.min.css" />
    <link rel="stylesheet" type="text/css" href="/admin/content/css/themes/default.css" id="skin-switcher">

    <!-- JQUERY UI-->
    <link rel="stylesheet" type="text/css" href="/admin/content/js/jquery-ui-1.10.3.custom/css/custom-theme/jquery-ui-1.10.3.custom.min.css" />

    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="/admin/content/plugins/iCheck/all.css">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="/admin/content/plugins/colorpicker/bootstrap-colorpicker.min.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="/admin/content/plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="/admin/content/plugins/select2/select2.min.css">


    <!--Magic Checkbox [ OPTIONAL ]-->
    <link href="/admin/content/plugins/magic-check/css/magic-check.min.css" rel="stylesheet">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!--Page Title-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <div id="page-title">
        <h1 class="page-header text-overflow">Colaboradores</h1>

    </div>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End page title-->

    <!--Breadcrumb-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <ol class="breadcrumb">
        <li><a href="/">Home</a></li>
        <li class="active">Colaboradores</li>
        <li>Editar</li>
    </ol>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End breadcrumb-->


    <!-- Main content -->
    <div id="page-content">
        <form id="form1" runat="server">


            <asp:Label ID="lblMensagem" runat="server" CssClass="box-danger alert alert-danger" Style="display: block" Visible="false"></asp:Label>

            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Dados</h3>
                </div>
                <div class="panel-body">


                    <div class="form-group">
                        <div class="col-sm-6">
                            <label for="ContentPlaceHolder1_Nome">Nome:<span class="required">*</span></label>
                            <asp:TextBox ID="txtNome" CssClass="form-control" required="required" runat="server" MaxLength="80" Width="100%" placeholder="Nome do Colaborador"></asp:TextBox>
                        </div>
                        <div class="col-sm-4">
                            <label for="ContentPlaceHolder1_CPF">CPF:<span class="required">*</span></label>
                            <asp:TextBox ID="txtCPF" CssClass="form-control" required="required" runat="server" MaxLength="80" Width="100%" placeholder="CPF do Colaborador"></asp:TextBox>
                        </div>
                        <div class="col-sm-2">
                            <div id="trDataInicial" class="form-inline">
                            <label for="ContentPlaceHolder1_Aniv">Data Anivesario:<span class="required">*</span></label>
                            <asp:TextBox ID="txtaniversario" CssClass="form-control mask-data" required="required" runat="server" MaxLength="80" Width="80%" placeholder="dd/mm/yyyy"></asp:TextBox>
                        </div></div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="ContentPlaceHolder1_Cargo">Cargo:</label>
                            <asp:TextBox ID="txtCargo" CssClass="form-control" runat="server" MaxLength="50" Width="100%" placeholder="Cargo do Colaborador"></asp:TextBox>
                        </div>
                        <div class="col-sm-3">
                            <label for="ContentPlaceHolder1_Cargo">Área:</label>
                            <asp:DropDownList ID="ddlsetor" CssClass="form-control" runat="server"></asp:DropDownList>
                        </div>
                         <div class="col-sm-3">
                            <label for="ContentPlaceHolder1_Regional">Regional:</label>
                            <asp:DropDownList ID="ddlregional" CssClass="form-control" runat="server"></asp:DropDownList>
                        </div>
                        <div class="col-sm-1">
                            <label for="ContentPlaceHolder1_Ramal">Ramal:</label>
                            <asp:TextBox ID="txtRamal" CssClass="form-control" runat="server" MaxLength="50" Width="100%" placeholder="Ramal do Colaborador"></asp:TextBox>
                        </div>
                        <div class="col-sm-2">
                            <div id="trDataInicial" class="form-inline">
                                <label for="ContentPlaceHolder1_Aniv">Data Admissão:<span class="required">*</span></label>
                                  <asp:TextBox ID="txtAdmissao" CssClass="form-control mask-data" required="required" runat="server" MaxLength="80" Width="80%" placeholder="dd/mm/yyyy"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="ContentPlaceHolder1_Chamada">Descrição Pessoal:</label>
                            <asp:TextBox ID="txtDescricao" CssClass="form-control" runat="server" Height="100px" TextMode="MultiLine" Width="100%" placeholder=""></asp:TextBox>

                        </div>
                    </div>

                <%--    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="ContentPlaceHolder1_Chamada">Formação Pessoal:</label>
                            <asp:TextBox ID="txtFormacao" CssClass="form-control" runat="server" Height="100px" TextMode="MultiLine" Width="100%" placeholder=""></asp:TextBox>

                        </div>
                    </div>--%>


                    <div class="col-sm-12">
                        <div class="separator"></div>
                    </div>


                    <div class="form-group">
                        <div class="col-sm-4">
                            <label for="ContentPlaceHolder1_URL">E-mail:</label>
                            <asp:TextBox ID="txtEmail" CssClass="form-control" runat="server" MaxLength="150" Width="100%" placeholder="E-mail do Colaborador">></asp:TextBox>
                        </div>
                        <div class="col-sm-4">
                            <label for="ContentPlaceHolder1_URL">Login de Rede:</label>
                            <asp:TextBox ID="txtloginRede" CssClass="form-control" runat="server" MaxLength="150" Width="100%" placeholder="Login de rede do Colaborador">></asp:TextBox>
                        </div>
                        <div class="col-sm-4">
                            <label for="ContentPlaceHolder1_URL">Senha:</label>
                            <asp:TextBox ID="txtSenha" CssClass="form-control" runat="server" MaxLength="150" Width="100%" placeholder="Senha do Colaborador">></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>Linkedin:</label>
                            <asp:TextBox ID="txtLinkedin" CssClass="form-control" runat="server" MaxLength="150" Width="100%" placeholder="url do linkedin"></asp:TextBox>
                        </div>
                    </div>






                    <div class="form-group">
                        <div class="col-sm-2">
                            <label>Ativar:</label>
                            <div>
                                <div class="make-switch switch-small" data-on="success" data-off="danger" data-on-label="<i class='fa fa-check icon-white'></i>" data-off-label="<i class='fa fa-times'></i>">
                                    <asp:CheckBox ID="chkAtivo" runat="server" />
                                </div>
                            </div>
                        </div>
                        <%-- <div class="col-sm-1">
                            <label>Posição:</label>
                            <asp:TextBox ID="txtPosicao" CssClass="form-control" runat="server" MaxLength="2" Width="60px" Value="1"></asp:TextBox>
                        </div>--%>
                    </div>


                    <div style="clear: both;"></div>
                    <div class="col-sm-12">
                        <div class="separator"></div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>Foto:</label>
                            <asp:FileUpload ID="txtArquivo" CssClass="form-control" runat="server" Width="100%"></asp:FileUpload>
                            <small>Upload (Gif/Jpg/Png) formato: 414 x 414</small>
                        </div>
                    </div>
                    <div style="clear: both;"></div>
                    <div class="col-sm-12">
                        <div class="separator"></div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-4">
                            <label for="ContentPlaceHolder1_lblDataInclusao">Data Inclusão:</label>
                            <asp:Label ID="lblDataInclusao" runat="server" Text=""></asp:Label>
                        </div>
                        <div class="col-sm-4">
                            <label for="ContentPlaceHolder1_lblDataAlteracao">Data Alteração:</label>
                            <asp:Label ID="lblDataAlteracao" runat="server" Text=""></asp:Label>
                        </div>
                        <div class="col-sm-4">
                            <label for="ContentPlaceHolder1_lblUsuario">Usuário:</label>
                            <asp:Label ID="lblUsuario" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Literal ID="ltlLOG" runat="server"></asp:Literal>
                    </div>


                </div>
                <div class="panel-footer text-right">
                    <asp:LinkButton ID="btnSalvar" runat="server" CssClass="btn btn-success col-sm-1" OnClick="btnSalvar_Click"><i class="fa fa-save fa-1x"></i> SALVAR</asp:LinkButton>&nbsp;&nbsp;
                            <asp:Button ID="btnExcluir" runat="server" Text="EXCLUIR" CssClass="btn btn-warning fa-delicious" Visible="false" OnClientClick="return confirm('Confirma exclusão deste registro?');" OnClick="btnExcluir_Click" />&nbsp;&nbsp;

                            <asp:Button ID="btnVoltar" runat="server" Text="VOLTAR" CssClass="btn fa-reply" PostBackUrl="Colaboradores" />

                    <br />
                    <br />
                    <asp:HiddenField ID="hOperacao" runat="server" />
                    <asp:HiddenField ID="hID" runat="server" />
                    <asp:HiddenField ID="hUrlReferrer" runat="server" />
                    <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                </div>
            </div>


        </form>



        <asp:Panel ID="pnlFotos" Visible="true" runat="server">
            <p>

                <!-- GALERIA -->
                <div id="filter-items" class="row">

                    <div class="col-md-3 category_1 item">
                        <div class="filter-content">
                            <asp:Image ID="Image1" runat="server" CssClass="img-responsive" />
                            <div class="hover-content">
                                <a class="btn btn-warning hover-link colorbox-button" href='<%=(strImage) %>' title=''>
                                    <i class="fa fa-search-plus fa-1x"></i>
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /GALERIA -->
            </p>
        </asp:Panel>




    </div>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="Server">
    <!-- JQUERY UI-->
    <link rel="stylesheet" type="text/css" href="/admin/content/js/jquery-ui-1.10.3.custom/css/custom-theme/jquery-ui-1.10.3.custom.min.css" />

    <!-- DATE RANGE PICKER -->
    <script src="/admin/content/js/bootstrap-daterangepicker/moment.min.js"></script>
    <script src="/admin/content/js/bootstrap-daterangepicker/daterangepicker.min.js"></script>

    <!-- bootstrap color picker -->
    <script src="/admin/content/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
    <!-- bootstrap time picker -->
    <script src="/admin/content/plugins/timepicker/bootstrap-timepicker.min.js"></script>

    <!-- iCheck 1.0.1 -->
    <script src="/admin/content/plugins/iCheck/icheck.min.js"></script>


    <!-- BOOTSTRAP SWITCH -->
    <script type="text/javascript" src="/admin/content/js/bootstrap-switch/bootstrap-switch.min.js"></script>

    <!-- ISOTOPE -->
    <script type="text/javascript" src="/admin/content/js/isotope/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="/admin/content/js/isotope/imagesloaded.pkgd.min.js"></script>
    <!-- COLORBOX -->
    <script type="text/javascript" src="/admin/content/js/colorbox/jquery.colorbox.min.js"></script>

    <!-- KNOB -->
    <script type="text/javascript" src="/admin/content/js/jQuery-Knob/js/jquery.knob.min.js"></script>


    <script type="text/jscript" src="/admin/content/js/jquery.maskedinput-1.3.1.js"></script>


    <script type="text/jscript">

        var campo = '<%=hID.ClientID.ToString().Replace("hID","")%>';


        $(document).ready(function () {
            $(".mask-data").mask("99/99/9999");

            $(".mask-data").datepicker({
                showOn: "button",
                buttonImage: "/admin/content/img/Calendar_scheduleHS.png",
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd/mm/yy',
                dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado', 'Domingo'],
                dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
                dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
                monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez']
            });
        });
    </script>

</asp:Content>





