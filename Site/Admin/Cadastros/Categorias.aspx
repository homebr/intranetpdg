﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="Categorias.aspx.cs" Inherits="Cadastros_Categorias" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!-- JQUERY UI-->
    <link rel="stylesheet" type="text/css" href="/js/jquery-ui-1.10.3.custom/css/custom-theme/jquery-ui-1.10.3.custom.min.css" />
    <!-- DATE RANGE PICKER -->
    <link rel="stylesheet" type="text/css" href="/js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
    <!-- DATA TABLES -->
    <link rel="stylesheet" type="text/css" href="/js/datatables/media/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" type="text/css" href="/js/datatables/media/assets/css/datatables.min.css" />
    <link rel="stylesheet" type="text/css" href="/js/datatables/extras/TableTools/media/css/TableTools.min.css" />
    <!-- DATE PICKER -->
    <link rel="stylesheet" type="text/css" href="/js/datepicker/themes/default.min.css" />
    <link rel="stylesheet" type="text/css" href="/js/datepicker/themes/default.date.min.css" />
    <link rel="stylesheet" type="text/css" href="/js/datepicker/themes/default.time.min.css" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <section class="content-header">
        <h1>Categorias
           
                <small>Listagem</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i>Home</a></li>
            <li>Categorias</li>
            <li class="active">Listagem</li>
        </ol>
    </section>

    <form id="form1" runat="server">
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <!-- BOX -->
                    <div class="box border blue">
                        <div class="box-body">

                            <div class="form-group">
                                <div class="col-md-3">
                                    <asp:TextBox ID="TextBox1" runat="server" Width="100%" CssClass="form-control" placehorder="Adicione o setença de sua busca"></asp:TextBox>
                                </div>
                                <div class="col-md-9">
                                    <asp:Button ID="btnBuscar" CssClass="btn btn-success fa-search-plus" Text="BUSCAR" runat="server" />
                                </div>

                            </div>
                        </div>


                    </div>
                </div>
            </div>

            <!-- DATA TABLES -->
            <div class="row">
                <div class="col-md-12">
                    <!-- BOX -->
                    <div class="box border blue">
                        <div class="box-title">
                            <h4><i class="fa fa-table"></i>Listagem</h4>
                            <div class="tools hidden-xs">
                                <a href="javascript:;" class="collapse">
                                    <i class="fa fa-chevron-up"></i>
                                </a>

                            </div>
                        </div>
                        <div class="box-body">

                            <table id="datatable1" cellpadding="0" cellspacing="0" border="0" class="datatable table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Pai</th>
                                        <th>Categoria</th>
                                        <th class="center">Posição</th>
                                        <th class="center">Ativo</th>
                                        <th class="center">Qtde</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <asp:Repeater ID="Repeater1" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# DataBinder.Eval(Container, "DataItem.Pai")%></td>
                                                <td class="tdBlack"><a href='<%# "CategoriasEditar?id=" + DataBinder.Eval(Container, "DataItem.IDCategoria")%>'><%# DataBinder.Eval(Container, "DataItem.Categoria")%></a></td>
                                                <td class="center"><%# DataBinder.Eval(Container, "DataItem.Posicao")%></td>
                                                <td class="center"><%# (Convert.ToBoolean(DataBinder.Eval(Container, "DataItem.Ativo"))?"<i class=\"fa fa-check\"></i>":"") %></td>
                                                <td class="center"><span class='label label-<%# (FrameWork.Util.GetInt(DataBinder.Eval(Container, "DataItem.Qtd"))>0?"success":"warning")%>'><%# DataBinder.Eval(Container, "DataItem.Qtd")%> artigos vinculados</span></td>
                                                <td class="center"><a class="btn btn-danger" href='<%# "CategoriasEditar?id=" + DataBinder.Eval(Container, "DataItem.IDCategoria")%>'>Ver</a></td>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /BOX -->
                </div>
            </div>
            <!-- /DATA TABLES -->

            <p>
                <a class="btn btn-danger" href="CategoriasEditar">NOVO</a>
            </p>
        </section>
    </form>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">

    <!-- Data tables -->
    <script type="text/javascript" src="/js/plugins/datatables/js/jquery.dataTables.js"></script>


    <script type="text/javascript">
        $(document).ready(function () {
            oTable = $('#example').dataTable({
                "bPaginate": true,
                "bJQueryUI": true,
                "sPaginationType": "full_numbers",
                "iDisplayLength": "50"
            });
        });


    </script>


</asp:Content>



