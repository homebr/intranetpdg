﻿<%@ WebHandler Language="C#" Class="GaleriaOrdenacao" %>

using System;
using System.Web;
using FrameWork.WebControls;
using FrameWork.DataObject;
using ClassLibrary;

public class GaleriaOrdenacao : IHttpHandler {

    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";

        string[] item_id = FrameWork.Util.GetString(context.Request["item_id"]).Split(',');
        string[] positions = FrameWork.Util.GetString(context.Request["positions"]).Split(',');

        string strSql = "";
        for (int n=0;n<item_id.Length;n++)
        {
            strSql += "update Fotos set posicao="+positions[n]+" where FotoID="+item_id[n]+" \r";
        }

        FrameWork.DataObject.DataSource.DefaulDataSource.Execute(strSql);

       context.Response.Write("ok");
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}