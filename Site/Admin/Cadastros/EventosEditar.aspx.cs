﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using FrameWork.DataObject;
using FrameWork.WebControls;
using FrameWork;
using ClassLibrary;
using System.Drawing.Imaging;
using System.Drawing;

public partial class Eventos_Editar : System.Web.UI.Page
{

    ClassLibrary.Eventos objNoticias = new ClassLibrary.Eventos();
    public String strImage = "";
    //caminho fisico no server
    string pathFisico = System.Configuration.ConfigurationManager.AppSettings["pathFisico"] + "uploads\\ImgEventos\\";
    string pathVirtual = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "uploads/ImgEventos/";


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(this.IsPostBack))
        {
            //CarregaSessoes();
            //CarregaEditoriais();

            ////Direitos do Usuário
            //if (!this.UserInfo.Admin && !CheckUserRight(101, "C"))
            //{
            //    Response.Redirect("Default.aspx");
            //}
            //if (!this.UserInfo.Admin && !CheckUserRight(101, "I"))
            //{
            //    this.btnNovaNoticia.Visible = false;
            //}
            //if (!this.UserInfo.Admin && !CheckUserRight(101, "E"))
            //{
            //    this.btnSalvar.Visible = false;
            //}

            //ID DO EMPREENDIMENTO
            //SE NAO HA NADA DENTRO DO CONTROLE HIDDEN PEGA VIA REQUEST
            if (this.Request["id"] != null)
                this.hID.Value = Convert.ToString(this.Request["id"]);

            //CARREGA DADOS
           // CarregaCategorias();
            CarregaEventos();

            //this.txtPreco.Attributes.Add("onkeypress", "formatar_mascara(this,'########,##')");
        }
    }

    public void CarregaEventos()
    {
        //OPCAO DE EDICAO PARA VISUALIZAR OS DADOS
        if (hID.Value != "")
        {
            objNoticias.Load(this.hID.Value);
            string strSql = "SELECT * FROM Eventos " +
                            "WHERE ID = " + this.hID.Value;

            DataTable dt = new DataTable();
            dt = DataSource.DefaulDataSource.Select(strSql);

            //REGISTRO LOCALIZADO ENTAO APENAS EDITA DADOS DO FORMULARIO
            if (dt.Rows.Count > 0)
            {

                //coloca os dados para edição
                hID.Value = dt.Rows[0]["ID"].ToString();
                this.lblNoticiaID.Text = dt.Rows[0]["ID"].ToString();


                this.txtTitulo.Text = FrameWork.Util.GetString(dt.Rows[0]["Titulo"]);
                //this.txtChamada.Text = FrameWork.Util.GetString(dt.Rows[0]["Chamada"]);
                //this.txtAutor.Text = FrameWork.Util.GetString(dt.Rows[0]["Autor"]);
                //this.txtFonte.Text = FrameWork.Util.GetString(dt.Rows[0]["Fonte"]);
               // txtChamada.Text = FrameWork.Util.GetString(dt.Rows[0]["Chamada"]);
                txtConteudo.Text = FrameWork.Util.GetString(dt.Rows[0]["Conteudo"]);
                txtvideo.Text = FrameWork.Util.GetString(dt.Rows[0]["Video"]);
                this.txtData.Text = String.Format("{0:dd/MM/yyyy}", dt.Rows[0]["Data"]);
                //ddlSetor.SelectedValue = dt.Rows[0]["Acesso"].ToString();
                //this.ddlSituacao.SelectedValue = Convert.ToString(dt.Rows[0]["Situacao"]);
                chkAtivo.Checked = (Convert.ToBoolean(dt.Rows[0]["Ativo"]));
                chkDestaque.Checked = Convert.ToBoolean(dt.Rows[0]["Destaque"]);

                //this.txtPosicao.Text = FrameWork.Util.GetString(dt.Rows[0]["Posicao"]);

               // if (ddlCategoria.Items.FindByText(FrameWork.Util.GetString(dt.Rows[0]["Categoria"])) != null)
               //     ddlCategoria.SelectedValue = FrameWork.Util.GetString(dt.Rows[0]["Categoria"]);

               // if (ddlTipo.Items.FindByValue(FrameWork.Util.GetString(dt.Rows[0]["Tipo"])) != null)
                //    ddlTipo.SelectedValue = FrameWork.Util.GetString(dt.Rows[0]["Tipo"]);

                lblDataInclusao.Text = Convert.ToString(dt.Rows[0]["DataInclusao"]);
                lblDataAlteracao.Text = Convert.ToString(dt.Rows[0]["DataAlteracao"]);
                //if (FrameWork.Util.GetString(dt.Rows[0]["FotoAutor"]).Length > 0)
                //{
                //    strImage = pathVirtual + FrameWork.Util.GetString(dt.Rows[0]["FotoAutor"]) + "?t=" + String.Format("{0:ddMmyyyyhhmm}", DateTime.Now);
                //    this.Image4.ImageUrl = strImage;
                //}



                if (FrameWork.Util.GetString(dt.Rows[0]["Imagem"]).Length > 0)
                {

                    strImage = pathVirtual + FrameWork.Util.GetString(dt.Rows[0]["Imagem"]) + "?t=" + String.Format("{0:ddMmyyyyhhmm}", DateTime.Now);
                    this.Image1.ImageUrl = strImage;

                    //strImage = pathVirtual + FrameWork.Util.GetString(dt.Rows[0]["Imagem_Ipad"]);
                    //this.Image2.ImageUrl = strImage;

                    //strImage = pathVirtual + FrameWork.Util.GetString(dt.Rows[0]["Imagem_Mobile"]);
                    //this.Image3.ImageUrl = strImage;

                }


                hOperacao.Value = "UPDATE";

                pnlFotos.Visible = true;
                // CarregaFotos();

            }
            else
            {
                //SENAO ACHOU O CODIGO EM QUESTAO É INSERÇÃO
                // btnNovaNoticia_Click(null, null);

            }
        }
        else
        {
            //ZERA TODOS OS CAMPOS DO FORMULARIO
            //btnNovaNoticia_Click(null, null);

        }
    }

    //public void CarregaCategorias()
    //{
    //    string strSql = "SELECT ID,Categoria FROM CategoriaNoticia where Ativo=1  ";
    //    ddlCategoria.DataSource = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSql);
    //    ddlCategoria.DataTextField = "Categoria";
    //    ddlCategoria.DataValueField = "ID";
    //    ddlCategoria.DataBind();
    //}


    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        //validação de campos
        if (FrameWork.Util.GetString(txtTitulo.Text) == "")
        {
            Msg("Favor inserir um Título!", false);
            txtTitulo.Focus();
            return;
        }


        if (hOperacao.Value == "UPDATE")
        {
            //Comandos de Atualização do Empreendimento
            objNoticias.LoadWhere("ID=@ID", new ParameterItem("@ID", hID.Value));
            objNoticias.DataAlteracao = DateTime.Now;

        }
        else
            objNoticias.DataInclusao = DateTime.Now;

       // objNoticias.Tipo = Convert.ToInt32(this.ddlTipo.SelectedValue);
        objNoticias.Titulo = this.txtTitulo.Text;
        //objNoticias.Acesso = Convert.ToInt32(this.ddlSetor.SelectedValue);
        objNoticias.Conteudo = txtConteudo.Text;
       // objNoticias.Chamada = txtChamada.Text;

        //objNoticias.Categoria = FrameWork.Util.GetInt(ddlCategoria.SelectedValue);


        if (Funcoes.IsDate(txtData.Text))
            objNoticias.Data = Convert.ToDateTime(this.txtData.Text);
        //objNoticias.Autor = txtAutor.Text;
        //objNoticias.Fonte = txtFonte.Text;
        objNoticias.Ativo = (chkAtivo.Checked);
        objNoticias.Destaque = (chkDestaque.Checked);
        objNoticias.Usuario = (string)this.Session["_uiUser"];

        //caso nao tenha pagina criada, gera atraves do titulo
        //String pagina = txtSku.Text;
        //if (pagina.Trim().Length == 0)
        //{
        //    pagina = WebUtil.GetPageName(txtTitulo.Text);
        //    //title.Trim().ToLower().Replace(" ", "-")
        //    if ((pagina.Length > 70))
        //    {
        //        pagina = pagina.Substring(0, 69);
        //    }
        //    int intQtdPagina = FrameWork.Util.GetInt(FrameWork.DataObject.DataSource.DefaulDataSource.Execute("Select count(*) from Noticias where sku = '" + pagina + "' "));
        //    if (intQtdPagina > 0)
        //    {
        //        pagina += "-" + System.DateTime.Now.Day + System.DateTime.Now.Month + System.DateTime.Now.Year + System.DateTime.Now.Hour + System.DateTime.Now.Minute;
        //        if ((pagina.Length > 70))
        //        {
        //            pagina = WebUtil.GetPageName(pagina).Substring(0, 30) + "-" + System.DateTime.Now.Day + System.DateTime.Now.Month + System.DateTime.Now.Year + System.DateTime.Now.Hour + System.DateTime.Now.Minute;
        //        }
        //    }
        //    txtSku.Text = pagina;
        //}
        objNoticias.Video = txtvideo.Text;

        try
        {
            objNoticias.Save();
            this.hID.Value = objNoticias.ID.ToString();

            //ATUALIZA A TELA 
            //this.Image1.ImageUrl = FrameWork.Util.GetString(objBanners.Imagem"]);
            //IMAGENS 




            if ((ImageDestaque.HasFile))
            {

                //inicializar as variáveis
                string arq = ImageDestaque.PostedFile.FileName;

                //verificamos a extensão através dos últimos 4 caracteres e verificamos se é permitido
                string extensao = arq.Substring(arq.Length - 4).ToLower();
                if ((!(extensao == ".gif" || extensao == ".jpg" || extensao == ".png")))
                {
                    Msg("Extensão de arquivo não permitido! Somente é permitido extensão: gif, jpg ou png!", false);
                    return;
                }

                //If Not "pdf,jpg,gif,png".Contains(Right(Trim(FileUpload1.FileName), 3).ToLower) Then
                //    Msg("Tipo de arquivo não permitido! Inclua apenas arquivos com a extensão informada!", Me)
                //    Exit Sub
                //End If


                //tamanho maximo do upload em kb
                double permitido = 2000;
                //identificamos o tamanho do arquivo
                double tamanho = 0;
                tamanho = Convert.ToDouble(ImageDestaque.PostedFile.ContentLength) / 1024;
                if ((tamanho > permitido))
                {
                    Msg("Tamanho Máximo permitido é de " + permitido + " kb", false);
                    return;
                }

                System.IO.Stream inStream = ImageDestaque.PostedFile.InputStream;
                System.Drawing.Image img = System.Drawing.Image.FromStream(inStream);

                int fonteLargura = img.Size.Width;     //armazena a largura original da imagem origem
                int fonteAltura = img.Size.Height;   //armazena a altura original da imagem origem
                int origemX = 1980;        //eixo x da imagem origem
                int origemY = 1000;        //eixo y da imagem origem
                                           //if ((fonteLargura != origemX))  // || (fonteAltura != origemY)
                                           //{
                                           //    Msg("Largura e Altura da imagem atual tem " + fonteLargura + " x " + fonteAltura + " px e deverá ser de 1980 x 780 px. Favor redimencionar a imagem corretamente!", false);
                                           //    return;
                                           //}


                string nomeArquivo = "ImgEventos-" + hID.Value + extensao;
                //string nomeArquivo = txtArquivo.FileName.ToString();

                ImageDestaque.SaveAs(pathFisico + nomeArquivo);

                //objBanners.Imagem = nomeArquivo;
                String strSql = "UPDATE Eventos SET Imagem = '" + nomeArquivo + "' WHERE ID=" + hID.Value;
                FrameWork.DataObject.DataSource.DefaulDataSource.Execute(strSql);

                ImageDestaque.Dispose();

                strImage = pathVirtual + nomeArquivo;
                this.Image1.ImageUrl = strImage;

                pnlFotos.Visible = true;

            }


            hOperacao.Value = "UPDATE";
            Msg("Salvo com sucesso!", true);
            //this.abaFotos.Visible = true;
            pnlFotos.Visible = true;
        }
        catch (Exception ex)
        {
            Msg(ex.Message, false);
        }
    }
       

    protected void btnExcluir_Click(object sender, EventArgs e)
    {
        try
        {
            //Comandos de excluir do Empreendimento
            objNoticias.LoadWhere("ID=@ID", new ParameterItem("@ID", hID.Value));
            objNoticias.Delete();

            Response.Redirect("EventosListar");

        }
        catch (Exception ex)
        {

            Msg(ex.Message, false);
        }
    }

    //public void CarregaFotos()
    //{
    //    string strPath = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "uploads/fotos/";
    //    string strSql = "SELECT ImagemID,Posicao,('" + strPath + "' + FileName) as FileName FROM Fotos where NoticiaID=" + hID.Value + " order by posicao ";


    //    DataTable dt = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSql);

    //    rptFotos.DataSource = dt;
    //    rptFotos.DataBind();

    //    //if (dt.Rows.Count > 0)
    //    //    pnlUpload.Visible = false;

    //    //Response.Write(strSql)
    //}

    // BOTAO NOVO // ZERA OS CAMPOS 
    protected void btnNovaNoticia_Click(object sender, EventArgs e)
    {
        ////ZERA TODOS OS CAMPOS DO FORMULARIO

        //ddlSessao.SelectedIndex = 0;

        this.lblNoticiaID.Text = "";
        this.txtTitulo.Text = "";

        txtConteudo.Text = "";
        txtData.Text = "";
       // this.txtAutor.Text = "";
        //this.txtAutorFoto.Text = "";
        //txtPagina.Text = "";

        lblNoticiaID.Text = "";


        lblDataInclusao.Text = "";
        lblDataAlteracao.Text = "";
        lblUsuario.Text = "";

        this.hOperacao.Value = "INSERT";
        this.hID.Value = "";

       // btnNovaNoticia.Visible = false;
       // abaFotos.Visible = false;

        btnExcluir.Visible = false;

    }

    protected void Msg(String msg,Boolean tipo)
    {
        lblMensagem.CssClass = (tipo ? "box-danger alert alert-success" : "box-danger alert alert-danger");
        lblMensagem.Text = msg;
        lblMensagem.Visible = true;
    }
    public static string resizeImageAndSave(string imagePath, int largura, int altura, string prefixo)
    {
        System.Drawing.Image fullSizeImg = System.Drawing.Image.FromFile(imagePath);
        var thumbnailImg = new Bitmap(largura, altura);
        var thumbGraph = Graphics.FromImage(thumbnailImg);
        thumbGraph.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
        thumbGraph.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
        thumbGraph.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
        var imageRectangle = new Rectangle(0, 0, largura, altura);
        thumbGraph.DrawImage(fullSizeImg, imageRectangle);
        string targetPath = imagePath.Replace(Path.GetFileNameWithoutExtension(imagePath), Path.GetFileNameWithoutExtension(imagePath) + prefixo);
        thumbnailImg.Save(targetPath, System.Drawing.Imaging.ImageFormat.Jpeg);
        thumbnailImg.Dispose();
        return targetPath;
    }

    public static Bitmap ResizeImage(System.Drawing.Image image, int width, int height)
    {
        var destRect = new Rectangle(0, 0, width, height);
        var destImage = new Bitmap(width, height);

        destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

        using (var graphics = Graphics.FromImage(destImage))
        {
            graphics.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;
            graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            graphics.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;

            using (var wrapMode = new System.Drawing.Imaging.ImageAttributes())
            {
                wrapMode.SetWrapMode(System.Drawing.Drawing2D.WrapMode.TileFlipXY);
                graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
            }
        }

        return destImage;
    }

    public System.Drawing.Image Crop(string img, int width, int height, int x, int y)
    {
        try
        {
            System.Drawing.Image image = System.Drawing.Image.FromFile(img);
            Bitmap bmp = new Bitmap(width, height); //, PixelFormat.Format24bppRgb
            //bmp.SetResolution(80, 60);

            Graphics gfx = Graphics.FromImage(bmp);
            //gfx.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            //gfx.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            //gfx.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
            //gfx.DrawImage(image, new Rectangle(0, 0, width, height), x, y, width, height, GraphicsUnit.Pixel);
            var crop = new Rectangle(0, y, width, height);
            var dest = new Rectangle(0, 0, width, height);

            //gfx.DrawImage(image, new Rectangle(0, 0, width, height), GraphicsUnit.Point);
            // Dispose to free up resources
            image.Dispose();
            bmp.Dispose();
            gfx.Dispose();

            return bmp;
        }
        catch (Exception ex)
        {
            Msg(ex.Message, false);
            return null;
        }
    }

}