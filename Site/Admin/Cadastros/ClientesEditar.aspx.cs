﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using FrameWork.DataObject;
using FrameWork.WebControls;
using FrameWork;
using ClassLibrary;
using System.Drawing.Imaging;
using System.Drawing;


public partial class BannersEditar : System.Web.UI.Page
{

    public ClassLibrary.Clientes objClientes = new ClassLibrary.Clientes();
    public String strImage = "";
    //caminho fisico no server
    string pathFisico = System.Configuration.ConfigurationManager.AppSettings["pathFisico"] + "uploads\\logos\\";
    string pathVirtual = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "uploads/logos/";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(this.IsPostBack))
        {

            //txtPosicao.Attributes.Add("type","number");
            //txtPosicao.Attributes.Add("min", "1");
            //txtPosicao.Attributes.Add("max", "99");

            ////Direitos do Usuário
            //if (!this.UserInfo.Admin && !CheckUserRight(101, "C"))
            //{
            //    Response.Redirect("Default.aspx");
            //}
            //if (!this.UserInfo.Admin && !CheckUserRight(101, "I"))
            //{
            //    this.btnNovaNoticia.Visible = false;
            //}
            //if (!this.UserInfo.Admin && !CheckUserRight(101, "E"))
            //{
            //    this.btnSalvar.Visible = false;
            //}

            //ID DO EMPREENDIMENTO
            //SE NAO HA NADA DENTRO DO CONTROLE HIDDEN PEGA VIA REQUEST
            if (this.Request["id"] != null)
                this.hID.Value = FrameWork.Util.GetString(this.Request["id"]);

            //CARREGA DADOS
            Carrega();

        }
    }


    public void Carrega()
    {
        //OPCAO DE EDICAO PARA VISUALIZAR OS DADOS
        if (hID.Value != "")
        {
            objClientes.Load(this.hID.Value);

            //REGISTRO LOCALIZADO ENTAO APENAS EDITA DADOS DO FORMULARIO
            if (objClientes.TotalLoad == 1)
            {


                this.txtTitulo.Text = objClientes.Nome;
                //this.txtFrase.Text = objBanners.Frase;


               // this.txtLink.Text = objBanners.Link;


               //this.txtDescricao.Text = objBanners.Descricao;
                
               //this.txtPosicao.Text = FrameWork.Util.GetString(objBanners.Posicao);
               // this.txtDataInicial.Text = String.Format("{0:dd/MM/yyyy}", objBanners.DataInicial);
                //this.txtDataFinal.Text = String.Format("{0:dd/MM/yyyy}", objBanners.DataFinal);

                

                if (FrameWork.Util.GetString(objClientes.Logo).Length > 0)
                {

                    strImage = pathVirtual + FrameWork.Util.GetString(objClientes.Logo) + "?t=" + String.Format("{0:ddMmyyyyhhmm}", DateTime.Now);
                    this.Image1.ImageUrl = strImage;

                  

                    try
                    {
                      

                    }
                    catch (Exception e)
                    {
                        //Msg(e.Message, false);
                    }

                }

                //chkIndiferente.Checked = Convert.ToBoolean(objBanners.Indeterminado);
                chkAtivo.Checked = (Convert.ToBoolean(objClientes.Ativo));


                //this.txtPagina.Text = FrameWork.Util.GetString(objBanners.pagina);
                lblUsuario.Text  = FrameWork.Util.GetString(objClientes.Usuario);
                lblDataInclusao.Text = FrameWork.Util.GetString(objClientes.DataInclusao);
                lblDataAlteracao.Text = FrameWork.Util.GetString(objClientes.DataAlteracao);

                hOperacao.Value = "UPDATE";

                btnExcluir.Visible = true;
                // abaFotos.Visible = true;
                //CarregaFotos();

            }

        }

    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        //validação de campos
        if (FrameWork.Util.GetString(txtTitulo.Text) == "")
        {
            Msg("Favor inserir um Nome!", false);
            txtTitulo.Focus();
            return;
        }


        if (hOperacao.Value == "UPDATE")
        {
            //Comandos de Atualização do Empreendimento
            //objClientes.LoadWhere("ID=@ID", new ParameterItem("@ID", hID.Value));
            objClientes.Load(hID.Value);
            objClientes.DataAlteracao = DateTime.Now;

        }
        else
            objClientes.DataInclusao = DateTime.Now;

        objClientes.Nome = this.txtTitulo.Text;



        //Response.Write("chkIndiferente=" + Request["chkIndiferente"]);

        
        objClientes.Ativo = (chkAtivo.Checked);

        objClientes.DataAlteracao = DateTime.Now;
        objClientes.Usuario = (string)this.Session["_uiUser"];


        try
        {
            objClientes.Save();
            this.hID.Value = objClientes.ID.ToString();

            hOperacao.Value = "UPDATE";
            Msg("Salvo com sucesso!", true);
            //this.abaFotos.Visible = true;

        }
        catch (Exception ex)
        {
            //this.pnlEmp.Visible = false;
            //if (ex.ErrorCode.ToString() == "-2146232060")
            //    this.DisplayMessage = " ID da Notícia já existente! ";
            //else
            //    this.DisplayMessage = ex.ErrorCode.ToString() + " | " + ex.InnerException.ToString();

            Msg(ex.Message, false);
        }

        //ATUALIZA A TELA 
        //this.Image1.ImageUrl = FrameWork.Util.GetString(objBanners.Imagem"]);
        //IMAGENS 
        try
        {



            if ((fileImagem.HasFile))
            {

                //inicializar as variáveis
                string arq = fileImagem.PostedFile.FileName;

                //verificamos a extensão através dos últimos 4 caracteres e verificamos se é permitido
                string extensao = arq.Substring(arq.Length - 4).ToLower();
                if ((!(extensao == ".gif" || extensao == ".jpg" || extensao == ".png")))
                {
                    Msg("Extensão de arquivo não permitido! Somente é permitido extensão: gif, jpg ou png!", false);
                    return;
                }

                //If Not "pdf,jpg,gif,png".Contains(Right(Trim(FileUpload1.FileName), 3).ToLower) Then
                //    Msg("Tipo de arquivo não permitido! Inclua apenas arquivos com a extensão informada!", Me)
                //    Exit Sub
                //End If


                //tamanho maximo do upload em kb
                double permitido = 2000;
                //identificamos o tamanho do arquivo
                double tamanho = 0;
                tamanho = Convert.ToDouble(fileImagem.PostedFile.ContentLength) / 1024;
                if ((tamanho > permitido))
                {
                    Msg("Tamanho Máximo permitido é de " + permitido + " kb", false);
                    return;
                }

                System.IO.Stream inStream = fileImagem.PostedFile.InputStream;
                System.Drawing.Image img = System.Drawing.Image.FromStream(inStream);

                int fonteLargura = img.Size.Width;     //armazena a largura original da imagem origem
                int fonteAltura = img.Size.Height;   //armazena a altura original da imagem origem
                int origemX = 1980;        //eixo x da imagem origem
                int origemY = 1000;        //eixo y da imagem origem
                //if ((fonteLargura != origemX))  // || (fonteAltura != origemY)
                //{
                //    Msg("Largura e Altura da imagem atual tem " + fonteLargura + " x " + fonteAltura + " px e deverá ser de 1980 x 780 px. Favor redimencionar a imagem corretamente!", false);
                //    return;
                //}


                string nomeArquivo = "logo-" + hID.Value + extensao;
                //string nomeArquivo = txtArquivo.FileName.ToString();

                fileImagem.SaveAs(pathFisico + nomeArquivo);

                //objBanners.Imagem = nomeArquivo;
                String strSql = "UPDATE clientes SET logo = '" + nomeArquivo + "' WHERE id=" + hID.Value;
                FrameWork.DataObject.DataSource.DefaulDataSource.Execute(strSql);

                fileImagem.Dispose();

                strImage = pathVirtual + nomeArquivo;
                this.Image1.ImageUrl = strImage;

                pnlFotos.Visible = true;

            }



        }
        catch (Exception ex)
        {
            Msg(ex.Message, false);
        }


    }

    protected void btnExcluir_Click(object sender, EventArgs e)
    {


        try
        {
            //Comandos de excluir do Empreendimento
            objClientes.LoadWhere("ID=@ID", new ParameterItem("@ID", hID.Value));
            objClientes.Delete();

            Response.Redirect("ClientesListar");

        }
        catch (Exception ex)
        {
            //this.pnlEmp.Visible = false;
            //if (ex.ErrorCode.ToString() == "-2146232060")
            //    this.DisplayMessage = " ID da Notícia já existente! ";
            //else
            //    this.DisplayMessage = ex.ErrorCode.ToString() + " | " + ex.InnerException.ToString();

            Msg(ex.Message, false);
        }





    }
    protected void Msg(String msg, Boolean tipo)
    {
        lblMensagem.CssClass = (tipo ? "box-danger alert alert-success" : "box-danger alert alert-danger");
        lblMensagem.Text = msg;
        lblMensagem.Visible = true;
    }

    public static string resizeImageAndSave(string imagePath, int largura, int altura, string prefixo)
    {
        System.Drawing.Image fullSizeImg = System.Drawing.Image.FromFile(imagePath);
        var thumbnailImg = new Bitmap(largura, altura);
        var thumbGraph = Graphics.FromImage(thumbnailImg);
        thumbGraph.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
        thumbGraph.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
        thumbGraph.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
        var imageRectangle = new Rectangle(0, 0, largura, altura);
        thumbGraph.DrawImage(fullSizeImg, imageRectangle);
        string targetPath = imagePath.Replace(Path.GetFileNameWithoutExtension(imagePath), Path.GetFileNameWithoutExtension(imagePath) + prefixo);
        thumbnailImg.Save(targetPath, System.Drawing.Imaging.ImageFormat.Jpeg);
        thumbnailImg.Dispose();
        return targetPath;
    }

    public static Bitmap ResizeImage(System.Drawing.Image image, int width, int height)
    {
        var destRect = new Rectangle(0, 0, width, height);
        var destImage = new Bitmap(width, height);

        destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

        using (var graphics = Graphics.FromImage(destImage))
        {
            graphics.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;
            graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            graphics.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;

            using (var wrapMode = new System.Drawing.Imaging.ImageAttributes())
            {
                wrapMode.SetWrapMode(System.Drawing.Drawing2D.WrapMode.TileFlipXY);
                graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
            }
        }

        return destImage;
    }

    public System.Drawing.Image Crop(string img, int width, int height, int x, int y)
    {
        try
        {
            System.Drawing.Image image = System.Drawing.Image.FromFile(img);
            Bitmap bmp = new Bitmap(width, height); //, PixelFormat.Format24bppRgb
            //bmp.SetResolution(80, 60);

            Graphics gfx = Graphics.FromImage(bmp);
            //gfx.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            //gfx.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            //gfx.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
            //gfx.DrawImage(image, new Rectangle(0, 0, width, height), x, y, width, height, GraphicsUnit.Pixel);
            var crop = new Rectangle(0, y, width, height);
            var dest = new Rectangle(0, 0, width, height);

            //gfx.DrawImage(image, new Rectangle(0, 0, width, height), GraphicsUnit.Point);
            // Dispose to free up resources
            image.Dispose();
            bmp.Dispose();
            gfx.Dispose();

            return bmp;
        }
        catch (Exception ex)
        {
            Msg(ex.Message, false);
            return null;
        }
    }

}