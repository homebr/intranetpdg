﻿using System;
using System.Data;
using System.IO;
using System.Drawing;

public partial class Cadastros_GaleriasEditar : System.Web.UI.Page
{
    string cep;
    string Sql;
    string sexo;
    string SexoConjuge;
    string sErro;
    int n;

    //utilizando a propriedade name do hidden
    string campo = "ctl00$ContentPlaceHolder1$";
    

    ClassLibrary.Imoveis produto = new ClassLibrary.Imoveis();



    protected void Page_Load(object sender, EventArgs e)
    {
		//    If Not (Me.Request.IsLocal Or FrameWork.Util.GetString(Session["_uiGrup"]) = "AD" Or Funcoes.VeCheckUserRight(14, "O")) Then
		//        Funcoes.SkinMessage("Sem Acesso", Me.Page)
		//    End If

		if (!this.IsPostBack) {

			if (FrameWork.Util.GetInt(Request["id"]) > 0) {
				hID.Value = Request["id"];
				Carrega();
			} else {
				hOperacao.Value = "INSERT";
			}


            CarregaCategorias();
		}

	}




	public void Carrega()
	{
		produto.Load(hID.Value);

		if (produto.TotalLoad == 1) {

            lblNomeImovel.Text = produto.Nome;

            if ((FrameWork.Util.GetString(produto.Logo).Length > 1)) {
				Image1.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "/uploads/fotos/" + produto.Logo + "?t=" + String.Format("{0:ddMMyyyyHHmm}", DateTime.Now);
				Image1.Visible = true;


			}

            if ((FrameWork.Util.GetString(produto.FotoDestaque).Length > 1))
                ImageDestaque.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "/uploads/fotos/" + produto.FotoDestaque + "?t=" + String.Format("{0:ddMMyyyyHHmm}", DateTime.Now);

            if ((FrameWork.Util.GetString(produto.FotoPrincipal).Length > 1))
            {
                ImagePrincipal.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "/uploads/fotos/" + produto.FotoPrincipal + "?t=" + String.Format("{0:ddMMyyyyHHmm}", DateTime.Now);
                ImagePrincipal.Visible = true;
            }
            if ((FrameWork.Util.GetString(produto.FotoPrincipal_Tablet).Length > 1))
            {
                ImagePrincipal_Tablet.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "/uploads/fotos/" + produto.FotoPrincipal_Tablet + "?t=" + String.Format("{0:ddMMyyyyHHmm}", DateTime.Now);
                ImagePrincipal_Tablet.Visible = true;
            }
            if ((FrameWork.Util.GetString(produto.FotoPrincipal_Mobile).Length > 1))
            {
                ImagePrincipal_Mobile.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "/uploads/fotos/" + produto.FotoPrincipal_Mobile + "?t=" + String.Format("{0:ddMMyyyyHHmm}", DateTime.Now);
                ImagePrincipal_Mobile.Visible = true;
            }

            CarregaFotos();
            pnlUpload.Visible = true;

            txtDestaque_Titulo.Text = produto.Destaque_Titulo;
            txtDestaque_Frase.Text = produto.Destaque_Frase;
            txtDestaque_Descricao.Text = produto.Destaque_Descricao;
            txtDestaque_IconeTexto.Text = produto.Destaque_IconeTexto;
            txtDestaque_IconeClass.Text = produto.Destaque_IconeClass;

        }

	}


    public void CarregaFotos()
    {
        string strPath = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "uploads/fotos/";

        string strSql = @"SELECT i.ImagemID,('" + strPath + "' + i.FileName) as FileNameFull,('" + strPath + "' + i.FileName) as FileNameFull,i.FileName,i.FileName_600x600,i.Descricao,c.Categoria,i.Categoria as IDCategoria,i.Conteudo ";
        strSql += @"FROM ImovelImagens i 
                    left join Categorias c on c.id = i.categoria 
                    WHERE   idimovel=" + hID.Value + " order by Posicao ";

        DataTable dt = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSql);

        foreach (DataRow item in dt.Rows)
        {

            try
            {
                if (FrameWork.Util.GetString(item["FileName_600x600"]).Length == 0)
                {
                    String pathFisico = System.Configuration.ConfigurationManager.AppSettings["pathFisico"] + "uploads\\fotos\\";
                    String pathToSave_100 = pathFisico + FrameWork.Util.GetString(item["FileName"]);
                    if (System.IO.File.Exists(pathToSave_100))
                    {
                        using (var vimage = Image.FromFile(pathToSave_100))
                        {
                            //salva em 600x600
                            int swidth = 600;
                            int sheight = 600;

                            if (vimage.Width > vimage.Height)
                            {
                                decimal fator = Convert.ToDecimal(Convert.ToDecimal(swidth) / vimage.Width);
                                sheight = Convert.ToInt16(vimage.Height * fator);
                                if (sheight == 0)
                                    sheight = 600;
                            }
                            else
                            {
                                decimal fator = Convert.ToDecimal(Convert.ToDecimal(sheight) / vimage.Height);
                                swidth = Convert.ToInt16(vimage.Width * fator);
                                if (swidth == 0)
                                    swidth = 600;

                            }

                            System.Drawing.Image image2 = ResizeImage(vimage, swidth, sheight);
                            string nomeArquivo = FrameWork.Util.GetString(item["FileName"]).Replace(".jpg", "-600x600.jpg").Replace(".gif", "-600x600.gif").Replace(".png", "-600x600.png");
                            image2.Save(pathFisico + nomeArquivo, System.Drawing.Imaging.ImageFormat.Jpeg);
                            item["FileName_600x600"] = nomeArquivo;

                            strSql = "update ImovelImagens set FileName_600x600='" + nomeArquivo + "' where IDImovel=" + hID.Value + " and ImagemID=" + FrameWork.Util.GetString(item["ImagemID"]);
                            FrameWork.DataObject.DataSource.DefaulDataSource.Execute(strSql);

                        }
                    }
                }
            }
            catch
            {

            }

        }


        rptFotos.DataSource = dt;
        rptFotos.DataBind();

        //Response.Write(strSql)
    }

    public void CarregaCategorias()
    {

        if (FrameWork.Util.GetString(Session["sCategorias"]).Length == 0)
        {
            string strSql = @"SELECT ID,Categoria ";
            strSql += @"FROM Categorias 
                    WHERE  Ativo=1  order by Ordem,Categoria ";

            DataTable dt = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSql);

            String sCategorias = "";
            String sCategorias_filtros = "";
            foreach (DataRow item in dt.Rows)
            {

                sCategorias += "<option value='" + item["ID"] + "'>" + item["Categoria"] + "</option> \n";
                sCategorias_filtros += "<a title=\"" + item["Categoria"] + "\" class=\"filter\" data-filter=\"." + FrameWork.Util.GetString(item["Categoria"]).ToLower().Replace(" ","-") + "\"><i></i>" + FrameWork.Util.GetString(item["Categoria"]) + "</a>&nbsp;&nbsp; \n";
                //

            }
            Session["sCategorias"] = sCategorias;
            Session["sCategorias_filtros"] = sCategorias_filtros;
        }

    }


    protected void btnSalvar_Click(object sender, EventArgs e)
	{


        //string[] keys = Request.Form.AllKeys;
        //for (int i = 0; i < keys.Length; i++)
        //{
        //    Response.Write(keys[i] + ": " + Request.Form[keys[i]] + "<br>");
        //}

        try
        {
            //caminho fisico no server
            string pathPDFs = System.Configuration.ConfigurationManager.AppSettings["pathFisico"] + "uploads\\fotos\\";

            //foreach (string s in Request.Files)
            //{
            //    HttpPostedFile file = Request.Files[s];
            //    //  int fileSizeInBytes = file.ContentLength;
            //    string fileName = file.FileName;
            //    string fileExtension = file.ContentType.ToLower();
            //    Response.Write(fileName + ": " + fileExtension + "<br>");
            //}


            ClassLibrary.Imoveis produto = new ClassLibrary.Imoveis();
            if (FrameWork.Util.GetInt(hID.Value) > 0)
            {
                produto.Load(hID.Value);
                produto.Destaque_Titulo = txtDestaque_Titulo.Text;
                produto.Destaque_Frase = txtDestaque_Frase.Text;
                produto.Destaque_Descricao = txtDestaque_Descricao.Text;
                produto.Destaque_IconeTexto = txtDestaque_IconeTexto.Text;
                produto.Destaque_IconeClass = txtDestaque_IconeClass.Text;
                produto.Save();
            }



            if (FrameWork.Util.GetString(Request.Form["filePrincipal[image]"]).Length > 1)
            {
                string strData = FrameWork.Util.GetString(Request.Form["filePrincipal[image]"]).Split(',')[1];
                byte[] imageBytes = Convert.FromBase64String(strData);
                MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
                ms.Write(imageBytes, 0, imageBytes.Length);
                System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
                System.Drawing.Image image2 = ResizeImage(image, 1980, 500);

                string nomeArquivo = "foto-principal-" + hID.Value + ".jpg";
                image2.Save(pathPDFs + nomeArquivo, System.Drawing.Imaging.ImageFormat.Jpeg);

                String strSql = "UPDATE Imoveis SET FotoPrincipal = '" + nomeArquivo + "' WHERE IDImovel=" + hID.Value;
                FrameWork.DataObject.DataSource.DefaulDataSource.Execute(strSql);

                image.Dispose();
                image2.Dispose();

                ImagePrincipal.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "uploads/fotos/" + nomeArquivo + "?t=" + String.Format("{0:ddMMyyyyHHmm}",DateTime.Now);
                ImagePrincipal.Visible = true;
            }
            if (FrameWork.Util.GetString(Request.Form["filePrincipal_Tablet[image]"]).Length > 1)
            {
                string strData = FrameWork.Util.GetString(Request.Form["filePrincipal_Tablet[image]"]).Split(',')[1];
                byte[] imageBytes = Convert.FromBase64String(strData);
                MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
                ms.Write(imageBytes, 0, imageBytes.Length);
                System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
                System.Drawing.Image image2 = ResizeImage(image, 900, 355);

                string nomeArquivo = "foto-principal-tablet-" + hID.Value + ".jpg";
                image2.Save(pathPDFs + nomeArquivo, System.Drawing.Imaging.ImageFormat.Jpeg);

                String strSql = "UPDATE Imoveis SET FotoPrincipal_Tablet = '" + nomeArquivo + "' WHERE IDImovel=" + hID.Value;
                FrameWork.DataObject.DataSource.DefaulDataSource.Execute(strSql);

                image.Dispose();
                image2.Dispose();

                ImagePrincipal_Tablet.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "uploads/fotos/" + nomeArquivo + "?t=" + String.Format("{0:ddMMyyyyHHmm}", DateTime.Now);
                ImagePrincipal_Tablet.Visible = true;
            }
            if (FrameWork.Util.GetString(Request.Form["filePrincipal_Mobile[image]"]).Length > 1)
            {
                string strData = FrameWork.Util.GetString(Request.Form["filePrincipal_Mobile[image]"]).Split(',')[1];
                byte[] imageBytes = Convert.FromBase64String(strData);
                MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
                ms.Write(imageBytes, 0, imageBytes.Length);
                System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
                System.Drawing.Image image2 = ResizeImage(image, 500, 500);

                string nomeArquivo = "foto-principal-mobile-" + hID.Value + ".jpg";
                image2.Save(pathPDFs + nomeArquivo, System.Drawing.Imaging.ImageFormat.Jpeg);

                String strSql = "UPDATE Imoveis SET FotoPrincipal_Mobile = '" + nomeArquivo + "' WHERE IDImovel=" + hID.Value;
                FrameWork.DataObject.DataSource.DefaulDataSource.Execute(strSql);

                image.Dispose();
                image2.Dispose();

                ImagePrincipal_Mobile.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "uploads/fotos/" + nomeArquivo + "?t=" + String.Format("{0:ddMMyyyyHHmm}", DateTime.Now);
                ImagePrincipal_Mobile.Visible = true;
            }

            if ((fileLogo.HasFile))
            {

                //inicializar as variáveis
                string arq = fileLogo.PostedFile.FileName;

                //Response.Write("arq=" + arq + "<br/>");
                //verificamos a extensão através dos últimos 4 caracteres e verificamos se é permitido
                string extensao = arq.Substring(arq.Length - 4).ToLower();
                if ((!(extensao == ".gif" || extensao == ".jpg" || extensao == ".png")))
                {
                    Msg("Extensão de arquivo não permitido! Somente é permitido extensão: gif, jpg ou png!", false);
                    return;
                }


                //tamanho maximo do upload em kb
                double permitido = 2000;
                //identificamos o tamanho do arquivo
                double tamanho = 0;
                tamanho = Convert.ToDouble(fileLogo.PostedFile.ContentLength) / 1024;
                //Response.Write("tamanho=" + tamanho + "<br/>");


                if ((tamanho > permitido))
                {
                    Msg("Tamanho Máximo permitido é de " + permitido + " kb", false);
                    return;
                }

		        produto.Load(hID.Value);
                string nomeArquivo = "logo-" + produto.sku + extensao;//fileLogo.FileName.ToString();

                //Response.Write(pathPDFs + nomeArquivo);

                fileLogo.SaveAs(pathPDFs + nomeArquivo);
                // produto.Logo = nomeArquivo;

                String strSql = "UPDATE Imoveis SET Logo = '" + nomeArquivo + "' WHERE IDImovel=" + hID.Value;
                FrameWork.DataObject.DataSource.DefaulDataSource.Execute(strSql);


                fileLogo.Dispose();

                Image1.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "uploads/fotos/" + nomeArquivo + "?t=" + String.Format("{0:ddMMyyyyHHmm}", DateTime.Now);
                Image1.Visible = true;

            }


            //fileDestaque
            if ((fileDestaque.HasFile))
            {

                //inicializar as variáveis
                string arq = fileDestaque.PostedFile.FileName;

                //Response.Write("arq=" + arq + "<br/>");
                //verificamos a extensão através dos últimos 4 caracteres e verificamos se é permitido
                string extensao = arq.Substring(arq.Length - 4).ToLower();
                if ((!(extensao == ".gif" || extensao == ".jpg" || extensao == ".png")))
                {
                    Msg("Extensão de arquivo não permitido! Somente é permitido extensão: gif, jpg ou png!", false);
                    return;
                }


                //tamanho maximo do upload em kb
                double permitido = 2000;
                //identificamos o tamanho do arquivo
                double tamanho = 0;
                tamanho = Convert.ToDouble(fileDestaque.PostedFile.ContentLength) / 1024;
                //Response.Write("tamanho=" + tamanho + "<br/>");


                if ((tamanho > permitido))
                {
                    Msg("Tamanho Máximo permitido é de " + permitido + " kb", false);
                    return;
                }

                produto.Load(hID.Value);
                string nomeArquivo = "foto-destaque-" + produto.sku + extensao;//fileDestaque.FileName.ToString();

                //Response.Write(pathPDFs + nomeArquivo);

                fileDestaque.SaveAs(pathPDFs + nomeArquivo);
                // produto.Logo = nomeArquivo;

                String strSql = "UPDATE Imoveis SET FotoDestaque = '" + nomeArquivo + "' WHERE IDImovel=" + hID.Value;
                FrameWork.DataObject.DataSource.DefaulDataSource.Execute(strSql);


                fileDestaque.Dispose();

                ImageDestaque.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "uploads/fotos/" + nomeArquivo + "?t=" + String.Format("{0:ddMMyyyyHHmm}", DateTime.Now);
                ImageDestaque.Visible = true;

            }

        }
        catch (Exception ex)
        {
            string strErro = ex.ToString();
            Msg(strErro + "!",false);
            return;
        }

		

		pnlUpload.Visible = true;

	}

    public static Bitmap ResizeImage(Image image, int width, int height)
    {
        var destRect = new Rectangle(0, 0, width, height);
        var destImage = new Bitmap(width, height);

        destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

        using (var graphics = Graphics.FromImage(destImage))
        {
            graphics.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;
            graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            graphics.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;

            using (var wrapMode = new System.Drawing.Imaging.ImageAttributes())
            {
                wrapMode.SetWrapMode(System.Drawing.Drawing2D.WrapMode.TileFlipXY);
                graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
            }
        }

        return destImage;
    }


    protected void Msg(String msg, Boolean tipo)
    {
        lblMensagem.CssClass = (tipo ? "box-danger alert alert-success" : "box-danger alert alert-danger");
        lblMensagem.Text = msg;
        lblMensagem.Visible = true;
    }
}