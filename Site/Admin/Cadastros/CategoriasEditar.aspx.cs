﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using FrameWork.DataObject;
using FrameWork.WebControls;
using FrameWork;
using ClassLibrary;
using System.Drawing.Imaging;
using System.Drawing;


public partial class Cadastros_CategoriasEditar : System.Web.UI.Page
{
    ClassLibrary.BlogCategorias objCategorias = new ClassLibrary.BlogCategorias();
    public String strImage = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(this.IsPostBack))
        {
            ////Direitos do Usuário
            //if (!this.UserInfo.Admin && !CheckUserRight(101, "C"))
            //{
            //    Response.Redirect("Default.aspx");
            //}
            //if (!this.UserInfo.Admin && !CheckUserRight(101, "I"))
            //{
            //    this.btnNovaNoticia.Visible = false;
            //}
            //if (!this.UserInfo.Admin && !CheckUserRight(101, "E"))
            //{
            //    this.btnSalvar.Visible = false;
            //}

            //ID DO EMPREENDIMENTO
            //SE NAO HA NADA DENTRO DO CONTROLE HIDDEN PEGA VIA REQUEST
            if (this.Request["id"] != null)
                this.hID.Value = Convert.ToString(this.Request["id"]);

            //CARREGA DADOS
            CarregaDropCategorias(0);
            Carrega();

            txtPosicao.Attributes.Add("type","number");
        }
    }

    public void Carrega()
    {
        //OPCAO DE EDICAO PARA VISUALIZAR OS DADOS
        if (hID.Value != "")
        {
            objCategorias.Load(hID.Value);

            //REGISTRO LOCALIZADO ENTAO APENAS EDITA DADOS DO FORMULARIO
            if (objCategorias.TotalLoad > 0)
            {

                //coloca os dados para edição
                hID.Value = objCategorias.IDCategoria.ToString();
                //txtIDPlano.Text = objCategorias.IDCategoria.ToString();
                txtNome.Text = FrameWork.Util.GetString(objCategorias.Categoria);
                txtPosicao.Text = FrameWork.Util.GetString(objCategorias.Posicao);

                CarregaDropCategorias(objCategorias.PaiID);

                //chkDestaque.Checked = objCategorias.Destaque;
                chkAtivo.Checked = objCategorias.Ativo;

                //txtURL.Text = FrameWork.Util.GetString(objCategorias.URL);
                //txtSEO_Titulo.Text = FrameWork.Util.GetString(objCategorias.SEO_Titulo);
                //txtSEO_Descricao.Text = FrameWork.Util.GetString(objCategorias.SEO_Descricao);

                this.lblUsuario.Text = FrameWork.Util.GetString(objCategorias.Usuario);
                lblDataInclusao.Text = Convert.ToString(objCategorias.DataInclusao);
                lblDataAlteracao.Text = Convert.ToString(objCategorias.DataAlteracao);

                hOperacao.Value = "UPDATE";

                //if (FrameWork.Util.GetString(objCategorias.Icon).Length > 2)
                //{
                //    imgIcon.ImageUrl = pathVirtual + objCategorias.Icon + "?d=" + string.Format("{0:HHmm}", DateTime.Now);
                //    imgIcon.Visible = true;
                //}

                int qtd = FrameWork.Util.GetInt(FrameWork.DataObject.DataSourceObject.DefaulDataSource.Execute("select count(*) as qtd from Blog where Situacao='A' and idcategoria=" + hID.Value));
                btnExcluir.Visible = (qtd == 0);

                if (qtd > 0)
                {
                    lblQtd.Visible = true;
                    lblQtd.Text = "<span class=\"label label-success\"> " + qtd.ToString() + " artigos vinculados</span>";

                }

            }
            else
            {
                //SENAO ACHOU O CODIGO EM QUESTAO É INSERÇÃO
                // btnNovaNoticia_Click(null, null);

            }
        }
        else
        {
            //ZERA TODOS OS CAMPOS DO FORMULARIO
            //btnNovaNoticia_Click(null, null);

        }
    }


    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        //validação de campos
        if (FrameWork.Util.GetString(txtNome.Text) == "")
        {
            Msg("Favor inserir o Nome da Categoria!", false);
            txtNome.Focus();
            return;
        }


        if (FrameWork.Util.GetInt(hID.Value) > 0)
        {
            //Comandos de Atualização 
            objCategorias.Load(hID.Value);
            objCategorias.DataAlteracao = DateTime.Now;

        }
        else
            objCategorias.DataInclusao = DateTime.Now;

        //salva os valores

        objCategorias.Categoria = FrameWork.Util.GetString(txtNome.Text);
        //objCategorias.Descricao = txtDescricao.Text;
        objCategorias.Posicao = FrameWork.Util.GetInt(txtPosicao.Text);

        //objCategorias.Destaque = (chkDestaque.Checked);
        objCategorias.Ativo = (chkAtivo.Checked);
        objCategorias.PaiID = FrameWork.Util.GetInt(Request["ddlCategorias"]);

        Funcoes.registrarJavaScript("$('#ddlCategorias option[value=" + objCategorias.PaiID + "]').attr('selected','selected')", true, this);

        //Response.Write("ddlCategorias=" + Request["ddlCategorias"]);

        //caso nao tenha pagina criada, gera atraves do titulo
        //String pagina = txtURL.Text;
        //if (pagina.Trim().Length == 0)
        //{
        //    pagina = WebUtil.GetPageName(txtNome.Text);
        //    if ((pagina.Length > 70))
        //    {
        //        pagina = pagina.Substring(0, 69);
        //    }
        //    int intQtdPagina = FrameWork.Util.GetInt(FrameWork.DataObject.DataSource.DefaulDataSource.Execute("Select count(*) from Categorias where URL = '" + pagina + "' "));
        //    if (intQtdPagina > 0)
        //    {
        //        pagina += "-" + System.DateTime.Now.Day + System.DateTime.Now.Month + System.DateTime.Now.Year + System.DateTime.Now.Hour + System.DateTime.Now.Minute;
        //        if ((pagina.Length > 70))
        //        {
        //            pagina = WebUtil.GetPageName(pagina).Substring(0, 30) + "-" + System.DateTime.Now.Day + System.DateTime.Now.Month + System.DateTime.Now.Year + System.DateTime.Now.Hour + System.DateTime.Now.Minute;
        //        }
        //    }
        //    txtURL.Text = pagina;
        //}
        //objCategorias.URL = pagina;

        //objCategorias.SEO_Titulo = txtSEO_Titulo.Text;
        //objCategorias.SEO_Descricao = txtSEO_Descricao.Text;


        objCategorias.Usuario = FrameWork.Util.GetString(this.Session["_uiUser"]);


        try
        {
            objCategorias.Save();
            this.hID.Value = objCategorias.IDCategoria.ToString();


            hOperacao.Value = "UPDATE";
            Msg("Salvo com sucesso!", true);

            //try
            //{


            //    if ((fileIcon.HasFile))
            //    {

            //        //inicializar as variáveis
            //        string arq = fileIcon.PostedFile.FileName;


            //        //verificamos a extensão através dos últimos 4 caracteres e verificamos se é permitido
            //        string extensao = arq.Substring(arq.Length - 4).ToLower();
            //        if ((!(extensao == ".gif" || extensao == ".jpg" || extensao == ".png")))
            //        {
            //            Msg("Extensão de arquivo não permitido! Somente é permitido extensão: gif, jpg ou png!", false);
            //            return;
            //        }


            //        //tamanho maximo do upload em kb
            //        double permitido = 2000;
            //        //identificamos o tamanho do arquivo
            //        double tamanho = 0;
            //        tamanho = Convert.ToDouble(fileIcon.PostedFile.ContentLength) / 1024;
            //        if ((tamanho > permitido))
            //        {
            //            Msg("Tamanho Máximo permitido é de " + permitido + " kb", false);
            //            return;
            //        }


            //        string nomeArquivo = Funcoes.RemoveAcentos(FrameWork.Util.GetString(objCategorias.Categoria).Replace(" ", "-").Replace("+", "")) + extensao;
            //        fileIcon.SaveAs(pathFisico + nomeArquivo);

            //        String strSql = "UPDATE Categorias SET Icon = '" + nomeArquivo + "' WHERE IDCategoria=" + hID.Value;
            //        FrameWork.DataObject.DataSource.DefaulDataSource.Execute(strSql);

            //        imgIcon.ImageUrl = pathVirtual + nomeArquivo + "?d=" + string.Format("{0:HHmm}", DateTime.Now);
            //        imgIcon.Visible = true;
            //        fileIcon.Dispose();

            //    }
            //}
            //catch (Exception ex)
            //{
            //    Msg(ex.Message, false);
            //    //return;
            //}


        }
        catch (Exception ex)
        {
            //Response.Write(ex.Message );
            Msg(ex.Message, false);
        }
        //ATUALIZA A TELA 
    }

    public void CarregaDropCategorias(int id)
    {
        String strSQL = "SELECT  IDCategoria, CASE WHEN PAIID>0 THEN ((select '<b>' + Categoria + '</b> ' from BlogCategorias where idcategoria=c.PaiID) + Categoria) ELSE Categoria END AS Categoria  FROM BlogCategorias c WHERE Ativo=1   ";
        if (FrameWork.Util.GetInt(hID.Value) > 0)
            strSQL += " and (IDCategoria != " + hID.Value + ") ";
        strSQL += " ORDER BY PaiID,Categoria  ";

        //Response.Write(strSQL);

        DataTable dt = new DataTable();
        dt = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSQL);

        String sHtml = "  <option value=\"0\">[NENHUM PAI]</option> \r\n ";
        foreach (DataRow item in dt.Rows)
        {


            sHtml += "  <option value=\"" + FrameWork.Util.GetString(item["IDCategoria"]) + "\" " + (id > 0 && id == FrameWork.Util.GetInt(item["IDCategoria"]) ? "selected=\"true\"" : "") + ">" + FrameWork.Util.GetString(item["Categoria"]) + "</option> \r\n ";

        }

        //<optgroup label="NFC EAST">
        //        <option>Dallas Cowboys</option>
        //        <option>New York Giants</option>
        //        <option>Philadelphia Eagles</option>
        //        <option>Washington Redskins</option>
        //      </optgroup>

        ltlCategorias.Text = sHtml;
    }

    protected void btnExcluir_Click(object sender, EventArgs e)
    {
        try
        {
            //Comandos de excluir
            objCategorias.Load(hID.Value);
            objCategorias.Delete();

            Response.Redirect("Categorias");

        }
        catch (Exception ex)
        {
            Msg(ex.Message, false);
        }
    }

    protected void Msg(String msg, Boolean tipo)
    {
        lblMensagem.CssClass = (tipo ? "box-danger alert alert-success" : "box-danger alert alert-danger");
        lblMensagem.Text = msg;
        lblMensagem.Visible = true;
    }



}