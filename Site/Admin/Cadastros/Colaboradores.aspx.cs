﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClassLibrary;
using FrameWork.WebControls;
using FrameWork.DataObject;

public partial class Cadastros_ColaboradoresListar : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Carrega();
    }


    public void Carrega()
    {

        string pathPDFs = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "uploads/colaboradores/";

        String strSQL = "SELECT  ID,Nome,Cargo,Descricao,Ativo,Email,Linkedin,CASE    WHEN len(foto) > 0 THEN ('" + pathPDFs + "' + foto)        ELSE('" + pathPDFs + "foto-perfil.jpg') END AS Foto,Sequencia FROM Colaboradores WHERE 1=1  ";
        if (FrameWork.Util.GetString(TextBox1.Text).ToString().Trim() != "")
            strSQL += " and (Nome LIKE '%" + TextBox1.Text + "%') ";

        strSQL += " ORDER BY ID  ";

        //Response.Write(strSQL);


        DataTable dt = new DataTable();
        dt = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSQL, new ParameterItem("@text", TextBox1.Text));

        //Response.Write(strSQL);

        Repeater1.DataSource = dt;
        Repeater1.DataBind();



    }
}