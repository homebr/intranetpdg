﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClassLibrary;
using FrameWork.WebControls;
using FrameWork.DataObject;

public partial class Cadastros_Categorias : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Carrega();
    }

    public void Carrega()
    {

        String strSQL = "SELECT IDCategoria,Categoria,url,ativo,destaque,posicao,(select count(*) as qtd from blog where Ativo = 1 and IDCategoria=BlogCategorias.IDCategoria) as qtd " +
            ",CASE WHEN PAIID>0 THEN (select c.Categoria FROM BlogCategorias c where c.idcategoria=BlogCategorias.paiid) ELSE '' END as Pai " +
            "FROM BlogCategorias WHERE 1=1  ";
        if (!string.IsNullOrEmpty(FrameWork.Util.GetString(this.TextBox1.Text).ToString().Trim()))
        {
            // FlagFiltro = True
            strSQL += " and Categoria LIKE @text ";
        }
        strSQL += " ORDER BY PaiId,Categoria  ";
        //Response.Write(strSQL);
        //return;

        DataTable dt = new DataTable();
        dt = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSQL, new ParameterItem("@text", TextBox1.Text));



        Repeater1.DataSource = dt;
        Repeater1.DataBind();



    }

}