﻿<%@ Page Title="Dados Cadastrais | Portal do Investidor Hoteleiro | JLL" Language="C#" MasterPageFile="/Admin/Admin.master" AutoEventWireup="true" CodeFile="dados-cadastrais.aspx.cs" Inherits="dados_cadastrais" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <script>
        //$(document).ready(function () {
        //    $('input').iCheck({
        //        checkboxClass: 'icheckbox_flat-yellow',
        //        radioClass: 'iradio_flat-yellow'
        //    });
        //});
    </script>

    <link href="../content/css/dados-cadastrais.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="col-12 dashboard">
        <div class="row">
            <div class="col-12 titulo">
                <h2>Dados cadastrais</h2>
            </div>
        </div>
    </div>


<form class="form" name="dadoscadastrais" id="dadoscadastrais"  runat="server" method="post" enctype="multipart/form-data">
    <div class="dashboard-int">
            <div class="row">
                <div class="col-12 col-md-6 dashboard-box">
                    <div class="titulo-form"><span><i class="icon-user"></i>Informações pessoais</span></div>
                    <div class="form-row">

                        <div class="col-12 col-md-7">
                            <label for="nome">Nome completo / Razão social</label>
                            <asp:TextBox ID="txtNome" CssClass="form-control" runat="server" MaxLength="100" Width="100%" Enabled="false"></asp:TextBox>
                        </div>

                        <div class="col-12  col-md-5">
                            <label for="cpf">CPF / CNPJ</label>
                            <asp:TextBox ID="txtCPF" CssClass="form-control cpfcnpj-mask" runat="server" Enabled="false" MaxLength="20" Width="100%"></asp:TextBox>
                        </div>

                        <div class="col-12">
                            <label for="email">E-mail</label>
                            <asp:TextBox ID="txtEmail" CssClass="form-control" runat="server" MaxLength="150" Width="100%"></asp:TextBox>
                        </div>

                        <div class="col-6">
                            <label for="telefone">Telefone</label>
                            <asp:TextBox ID="txtTelefone" CssClass="form-control phone-mask" runat="server" MaxLength="30" Width="100%"></asp:TextBox>
                        </div>

                        <div class="col-6">
                            <label for="celular">Celular</label>
                            <asp:TextBox ID="txtCelular" CssClass="form-control phone-mask" runat="server" MaxLength="30" Width="100%"></asp:TextBox>
                        </div>

                        <div class="col-12">
                            <label for="endereco">Endereço</label>
                            <asp:TextBox ID="txtEndereco" CssClass="form-control" runat="server" MaxLength="150" Width="100%" Enabled="true"></asp:TextBox>
                        </div>

                        <div class="col-3">
                            <label for="numero">Número</label>
                            <asp:TextBox ID="txtNumero" CssClass="form-control" runat="server" MaxLength="30" Width="100%" Enabled="true"></asp:TextBox>
                        </div>

                        <div class="col-4">
                            <label for="complemento">Complemento</label>
                            <asp:TextBox ID="txtComplemento" CssClass="form-control" runat="server" MaxLength="30" Width="100%" Enabled="true"></asp:TextBox>
                        </div>

                        <div class="col-5">
                            <label for="cep">CEP</label>
                            <asp:TextBox ID="txtCep" CssClass="form-control cep-mask" runat="server" MaxLength="9" Width="100%" Enabled="true"></asp:TextBox>
                        </div>

                        <div class="col-5">
                            <label for="bairro">Bairro</label>
                            <asp:TextBox ID="txtBairro" CssClass="form-control" runat="server" MaxLength="80" Width="100%" Enabled="true"></asp:TextBox>
                        </div>
                        <div class="col-4 col-md-5">
                            <label for="cidade">Cidade</label>
                            <asp:TextBox ID="txtCidade" CssClass="form-control" runat="server" MaxLength="80" Width="100%" Enabled="true"></asp:TextBox>
                        </div>

                        <div class="col-3 col-md-2">
                            <label for="estado">Estado</label>
                            <asp:TextBox ID="txtEstado" CssClass="form-control" runat="server" MaxLength="2" Width="100%" Enabled="true"></asp:TextBox>
                        </div>
                        <div class="col-12">
                            <asp:Label ID="lblMensagem" runat="server" CssClass="alert alert-dados"  />
                        </div>
                        <div class="col-12">
                            
                            <asp:LinkButton ID="btnAlterarDados" runat="server" CssClass="btn-padrao btn-salvar" OnClick="btnAlterarDados_Click" Visible="true">Salvar alterações</asp:LinkButton>
                            <asp:HiddenField ID="hCPF_Cliente" runat="server" />
                        </div>

                    </div>
                </div>


                <!-- // Alterar Senha  -->
                <div class="col-12 col-md-6 dashboard-box">
                    <div class="titulo-form"><span><i class="icon-senha"></i>Alterar senha </span></div>
                    <div class="form-row">
                        <div class="col-12">
                            <label for="cpf">Digite sua senha atual</label>
                            <asp:TextBox ID="txtSenhaAtual" TextMode="Password" CssClass="form-control" runat="server" MaxLength="20" Width="100%"></asp:TextBox>
                        </div>
                        <div class="col-12">
                            <label for="cpf">Digite sua nova senha</label>
                            <asp:TextBox ID="txtNovaSenha" TextMode="Password" CssClass="form-control" runat="server" MaxLength="20" Width="100%" data-indicator="pwindicator"></asp:TextBox>
                            <div class="help-block" style="float:left;">Mínimo de 6 caracteres</div><div id="pwindicator"><div class="bar"></div><div class="label"></div>
                        </div>
                        </div>
                        <div class="col-12">
                            <label for="cpf">Repita sua nova senha</label>
                            <asp:TextBox ID="txtRepetirSenha" TextMode="Password" CssClass="form-control" runat="server" MaxLength="20" Width="100%"></asp:TextBox>
                        </div>
                        <div class="col-12">
                            <asp:Label ID="lblMensagemSenha" runat="server" CssClass="alert alert-alterarsenha"  /><%--<div class="alert alert-alterarsenha"></div>--%>
                        </div>
                        <div class="col-12">
                            <asp:LinkButton ID="btnAlterarSenha" runat="server" CssClass="btn-padrao" OnClientClick="return validaSenha();" OnClick="btnAlterarSenha_Click">Alterar SENHA</asp:LinkButton>
                        </div>
                    </div>

                    <div class="titulo-form"><span><i class="icon-user"></i>Alterar foto </span></div>
                    <div class="form-row">
                        <div class="col-12">
                        <div class="avatar-upload">
                            <div class="avatar-edit">
                                <asp:FileUpload ID="imageUpload" runat="server" class="imageUpload" accept=".png, .jpg, .jpeg" />
                                <label for="ctl00_content_imageUpload"></label>
                            </div>
                            <div class="avatar-preview">
                                <div id="imagePreview" style="background-image: url(../content/img/foto-perfil.jpg);">
                                </div>
                            </div>
                        </div>
                        </div>
                        <div class="col-12">
                            <div class="alert alert-foto"></div>
                        </div>
                        <div class="col-12">
                            <asp:HiddenField ID="hFoto" runat="server" />
                            <asp:LinkButton ID="btnAlterarFoto" runat="server" CssClass="btn-padrao btnAlterarFoto" OnClick="btnAlterarFoto_Click">SALVAR FOTO</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</form>
   <!-- // Alterar Senha  -->

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="Server">

    <script type="text/jscript" src="../content/js/jquery.maskedinput-1.3.1.js"></script>
    <script src="../content/js/jquery.pwstrength.js" type="text/javascript"></script>

    <script>
        var campo = '<%=btnAlterarSenha.ClientID.Replace("btnAlterarSenha","")%>';


        jQuery(document).ready(function () {

            $(".cpfcnpj-mask").mask("999.999.999-99?999").on("focusout", function () {
                var len = this.value.replace(/\D/g, '').length;
                $(this).mask(len > 11 ? "99.999.999/9999-99":"999.999.999-99?999");
            });
            $(".phone-mask").mask("(99) 9999-9999?9").on("focusout", function () {
                var len = this.value.replace(/\D/g, '').length;
                $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
            });

            $(".cep-mask").mask("99999-999");

            if ($("#" + campo + "hFoto").val().length > 3) {
                aux = $("#" + campo + "hFoto").val();
                console.log(aux);
                $("#imagePreview").attr("style","background-image: url(" + aux + ");");
            }

            $('#' + campo + 'txtNovaSenha').pwstrength(); 
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#imagePreview').css('background-image', 'url('+e.target.result +')');
                    $('#imagePreview').hide();
                    $('#imagePreview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
                $(".btnAlterarFoto").show();
            }
        }
        $(".imageUpload").change(function() {
            readURL(this);
        });

    </script>


    <script>
        /*jslint unparam: true */
        /*global window, $ */
        $(function () {


            $(".btn-salvar").click(function () {
                var email = $("#" + campo + "txtEmail");
                var endereco = $("#" + campo + "txtEndereco");


                if (email.val() == '') {
                    email.focus();
                    $(".alert-dados").html('Favor digitar o E-mail!');
                    $(".alert-dados").addClass("alert-danger");
                    return false;
                } else {
                    $(".alert-dados").css('border', '1px solid #818181');
                }
                if (endereco.val() == '') {
                    endereco.focus();
                    $(".alert-dados").html('Favor digitar o Endereço!');
                    $(".alert-dados").addClass("alert-danger");
                    return false;
                } else {
                    $(".alert-dados").css('border', '1px solid #818181');
                }

                $(".alert-dados").html('Aguarde enquanto salvamos suas informações!');
                $(".alert-dados").addClass("alert-warning");
                return true;
            });

        });

        function validaSenha(sender, arg) {
            with (document.forms[0]) {
                if (ctl00_content_txtSenhaAtual.value == "") {
                    $('.alert-alterarsenha').html("Ops, o campo <strong>senha atual</strong> é campo obrigatório!");
                    $('.alert-alterarsenha').addClass("alert-danger");
                    ctl00_content_txtSenhaAtual.focus();
                    return false;
                }

                xnova = ctl00_content_txtNovaSenha.value;
                xredigite = ctl00_content_txtRepetirSenha.value;
                if (xnova == "" || xnova.length < 6) {
                    aux = 1;
                    $('.alert-alterarsenha').html("Ops, o campo <strong>nova senha</strong> deverá ter no mínimo 6 caracteres!");
                    ctl00_content_txtNovaSenha.focus();
                    $('.alert-alterarsenha').addClass("alert-danger");
                    return false;
                }
                else if (xnova != xredigite) {
                    aux = 1;
                    $('.alert-alterarsenha').html("Ops, as senhas que você digitou não são iguais. Verifique sua <strong>nova senha</strong> e tente novamente.");
                    ctl00_content_txtNovaSenha.focus();
                    $('.alert-alterarsenha').addClass("alert-danger");
                    return false;
                }

                $('.alert-alterarsenha').removeClass("alert-danger");
            }
        }
    </script>
</asp:Content>

