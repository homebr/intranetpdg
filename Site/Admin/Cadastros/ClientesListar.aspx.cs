﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClassLibrary;
using FrameWork.WebControls;
using FrameWork.DataObject;


public partial class ClientesListar : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            if (FrameWork.Util.GetString(Session["Clientes_Ativo"]) == "A")
                rdbAtivos_A.Checked = true;
            else if (FrameWork.Util.GetString(Session["Clientes_Ativo"]) == "N")
                rdbAtivos_N.Checked = true;
            else
                rdbAmbos.Checked = true;
        }
        Carrega();
    }

    public void Carrega()
    {
        string pathFisico = System.Configuration.ConfigurationManager.AppSettings["pathFisico"] + "uploads\\logos\\";
        string pathVirtual = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "uploads/logos/";
       

        String strSQL = "SELECT  ID,nome,('" + pathVirtual + "' + logo) as Imagem,Ativo FROM Clientes WHERE 1=1  ";
        if (FrameWork.Util.GetString(TextBox1.Text).ToString().Trim() != "")
        {
            
            strSQL += " and (nome LIKE '%" + TextBox1.Text + "%') ";
            //strSQL += " and (Titulo LIKE @text) ";
            Session["Clientes_TextBox1"] = TextBox1.Text;
        }
        else
            Session["Clientes_TextBox1"] = "";

        if (rdbAtivos_A.Checked)
        {
            strSQL += " and (Ativo = 1) ";
            Session["Clientes_Ativo"] = "A";
        }
        else if (rdbAtivos_N.Checked)
        {
            strSQL += " and (Ativo = 0) ";
            Session["Clientes_Ativo"] = "N";
        }
        else
            Session["Clientes_Ativo"] = "";


        strSQL += " ORDER BY ID DESC ";

        //Response.Write(strSQL);


        DataTable dt = new DataTable();
        dt = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSQL, new ParameterItem("@text", TextBox1.Text));



        Repeater1.DataSource = dt;
        Repeater1.DataBind();



    }
}
