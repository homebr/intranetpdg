﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.SessionState;
using System.Drawing;

public partial class dados_cadastrais : System.Web.UI.Page
{
    public ClassLibrary.Clientes objCliente = new ClassLibrary.Clientes();
    //caminho fisico no server
    string pathFisico = System.Configuration.ConfigurationManager.AppSettings["pathFisico"] + "uploads\\perfil\\";
    string pathVirtual = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "uploads/perfil/";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {

            //if (FrameWork.Util.GetString(Session["_uiCodCliente"]) == "")
            //{
            //    Response.Redirect("/login", true);
            //}


            try
            {
                String strUsuario = FrameWork.Util.GetString(Session["_uiCPF"]);
                //String strSenha = FrameWork.Util.GetString(Session["_uiSenha"]);

                
                hCPF_Cliente.Value = (strUsuario);

                
                objCliente.Load(FrameWork.Util.GetString(Session["_uiCodCliente"]));

                if (objCliente.TotalLoad == 1)
                {

                    txtNome.Text = objCliente.Nome;
                    txtEmail.Text = objCliente.Email;
                    txtCPF.Text = objCliente.CpfCnpj;
                    txtCep.Text = objCliente.Cep;
                    txtEstado.Text = objCliente.Estado;
                    txtCidade.Text = objCliente.Cidade;
                    txtBairro.Text = objCliente.Bairro;
                    txtEndereco.Text = objCliente.Endereco;
                    txtNumero.Text = objCliente.Numero;
                    txtComplemento.Text = objCliente.Complemento;
                    txtTelefone.Text = objCliente.Telefone;
                    txtCelular.Text = objCliente.Celular;

                    if (FrameWork.Util.GetString(objCliente.Foto).Length > 3)
                        hFoto.Value = "uploads/perfil/" + objCliente.Foto + "?t=" + String.Format("{0:ddMMyyyyHHmm}", DateTime.Now);
                }

            }
            catch (Exception ex)
            {

                Msg(ex.Message, false);
                return;
            }


        }
    }


    protected void btnAlterarDados_Click(object sender, EventArgs e)
    {

        try
        {
            String strUsuario = FrameWork.Util.GetString(Session["_uiCPF"]);
            String sEmail = FrameWork.Util.GetString(txtEmail.Text);

            String sCep = FrameWork.Util.GetString(txtCep.Text);
            String sEndereco = FrameWork.Util.GetString(txtEndereco.Text);
            String sNumero = FrameWork.Util.GetString(txtNumero.Text);
            String sComplemento = FrameWork.Util.GetString(txtComplemento.Text);
            String sBairro = FrameWork.Util.GetString(txtBairro.Text);
            String sCidade = FrameWork.Util.GetString(txtCidade.Text);
            String sEstado = FrameWork.Util.GetString(txtEstado.Text);

            String sTelefone = FrameWork.Util.GetString(txtTelefone.Text);
            String sCelular = FrameWork.Util.GetString(txtCelular.Text);

            objCliente.Load(FrameWork.Util.GetString(Session["_uiCodCliente"]));

            //objCliente.Nome = FrameWork.Util.GetString(txtNome.Text);
            objCliente.DataAlteracao = DateTime.Now;
            objCliente.Email = sEmail;
            objCliente.Telefone = txtTelefone.Text;
            objCliente.Celular = txtCelular.Text;

            objCliente.Cep = sCep;
            objCliente.Estado = sEstado;
            objCliente.Cidade = sCidade;
            objCliente.Bairro = sBairro;
            objCliente.Endereco = txtEndereco.Text;
            objCliente.Numero = sNumero;
            objCliente.Complemento = sComplemento;
            objCliente.Save();

            if (!String.IsNullOrEmpty(txtEmail.Text))
            {
                //ENVIO AO CLIENTE
                System.IO.StreamReader objStreamReader;
                String strResultadoEnvio = "";
                string strAssunto = "Portal do Cliente - Atualização Cadastral";
                string FilePath = Server.MapPath("template_email/ModeloAtualizacaoCadastral.htm");
                objStreamReader = System.IO.File.OpenText(FilePath);
                string strArq = objStreamReader.ReadToEnd();
                strArq = strArq.Replace("#NOME#", txtNome.Text);
                strArq = strArq.Replace("#IP#", Request.ServerVariables["REMOTE_ADDR"]);
                strArq = strArq.Replace("#DATA#", String.Format("{0:dd/MM/yyyy hh:mm}", DateTime.Now));
                string pathVirtual = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"];
                strArq = strArq.Replace("src=\"img/", "src=\"" + (pathVirtual + "template_email/") + "img/");
                strArq = strArq.Replace("#PathVirtual#", pathVirtual);

                string strTo = "naoresponda@homebrasil.com";
                string strPara = "oliveira@homebrasil.com"; // strEmailRecupera;
                string strCc = "";

                strResultadoEnvio = FrameWork.Util.GetString(Funcoes.EnvioEmail(strArq, strTo, strAssunto, strPara, strCc, ""));  
                objStreamReader.Dispose();
                objStreamReader.Close();


                Msg("Suas informações foram atualizadas com sucesso!", true);
            }
        }
        catch (Exception ex)
        {

            Msg(ex.Message, false);
            return;
        }
    }

    protected void btnAlterarSenha_Click(object sender, EventArgs e)
    {
        String senha = FrameWork.Util.GetString(txtSenhaAtual.Text);
        String nova = FrameWork.Util.GetString(txtNovaSenha.Text);
        String redigite = FrameWork.Util.GetString(txtRepetirSenha.Text);


        if (senha.Length == 0)
        {
            MsgSenha("Ops, o campo <strong>senha atual</strong> é campo obrigatório!", false);
            return;
        }
        else if (nova.Length < 6)
        {
            MsgSenha("Ops, o campo <strong>nova senha</strong> deverá ter no mínimo 6 caracteres!", false);
            return;
        }
        else if (nova != redigite)
        {
            MsgSenha("Ops, as senhas que você digitou não são iguais. Verifique sua <strong>nova senha</strong> e tente novamente.", false);
            return;
        }

        String strUsuario = FrameWork.Util.GetString(Session["_uiCPF"]);

        objCliente.Load(FrameWork.Util.GetString(Session["_uiCodCliente"]));

        try
        {
            if (objCliente.TotalLoad == 1)
            {


                //atualiza a senha no banco de dados
                if (objCliente.Senha == "" || (FrameWork.Util.GetString(objCliente.Senha).Length > 1 && FrameWork.Security.DeCript(objCliente.Senha) == senha))
                {
                    objCliente.Senha = FrameWork.Security.Cript(nova);
                    objCliente.AlteracaoSenha = DateTime.Now;
                    objCliente.Save();

                    txtSenhaAtual.Text = "";
                    txtNovaSenha.Text = "";
                    txtRepetirSenha.Text = "";

                    MsgSenha("Sua senha foi atualizada com sucesso!", true);
                }
                else
                    MsgSenha("A sua <strong>senha atual</strong> está incorreta. Por favor, tente novamente.", false);


            }
            else
                MsgSenha("Erro ao salvar, verifique os dados e tente novamente!", false);
        }
        catch
        {
            MsgSenha("Erro ao salvar, tente novamente ou abra um chamado reportando o problema!", false);
        }

    }

    protected void btnAlterarFoto_Click(object sender, EventArgs e)
    {


       if ((imageUpload.HasFile))
        {
            //Response.Write("passo 2");

            string arq = imageUpload.PostedFile.FileName;

            //verificamos a extensão através dos últimos 4 caracteres e verificamos se é permitido
            string extensao = arq.Substring(arq.Length - 4).ToLower();
            if ((!(extensao == ".gif" || extensao == ".jpg" || extensao == ".png")))
            {
                Msg("Extensão de arquivo não permitido! Somente é permitido extensão: gif, jpg ou png!", false);
                return;
            }
            string nomeArquivo = "foto-" + (hCPF_Cliente.Value).Replace(".","").Replace("-", "").Replace("/", "") + "-" + String.Format("{0:ddMMyyyyHHmm}", DateTime.Now) + extensao;

            // Save the posted file in our "data" virtual directory.
            imageUpload.SaveAs(pathFisico + nomeArquivo);

            objCliente.Load(FrameWork.Util.GetString(Session["_uiCodCliente"]));
            if (objCliente.TotalLoad == 1)
            {
                objCliente.Foto = nomeArquivo;
                objCliente.Save();
            }

            hFoto.Value = "uploads/perfil/" + nomeArquivo;
            Session["_uiFoto"] = "/uploads/perfil/" + nomeArquivo;
            imageUpload.Dispose();


        }
         

    }

    protected void Msg(String msg, Boolean tipo)
    {
        lblMensagem.CssClass = (tipo ? "box-danger alert alert-success" : "box-danger alert alert-danger");
        lblMensagem.Text = msg;
        lblMensagem.Visible = true;
    }

    protected void MsgSenha(String msg, Boolean tipo)
    {
        lblMensagemSenha.CssClass = (tipo ? "box-danger alert alert-success" : "box-danger alert alert-danger");
        lblMensagemSenha.Text = msg;
        lblMensagemSenha.Visible = true;
    }

}