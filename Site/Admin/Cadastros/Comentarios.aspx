﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="Comentarios.aspx.cs" Inherits="Cadastros_Comentarios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!-- JQUERY UI-->
    <link rel="stylesheet" type="text/css" href="/admin/content/js/jquery-ui-1.10.3.custom/css/custom-theme/jquery-ui-1.10.3.custom.min.css" />

    <!--DataTables [ OPTIONAL ]-->
    <link href="/admin/content/plugins/datatables/media/css/dataTables.bootstrap.css" rel="stylesheet">
    <link href="/admin/content/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css" rel="stylesheet">

    <link href="/admin/content/css/select2.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <!--Page Title-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <div id="page-title">
        <h1 class="page-header text-overflow">Comentários</h1>
    </div>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End page title-->


    <!--Breadcrumb-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <ol class="breadcrumb">
        <li><a href="/">Home</a></li>
        <li class="active">Comentários</li>
        <li>Listar</li>

    </ol>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End breadcrumb-->




    <form id="form1" runat="server">
        <!--Page content-->
        <!--===================================================-->
        <div id="page-content">

            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">FILTROS</h3>
                </div>
                <div class="panel-body">

                    <div class="form-group">
                        <div class="col-md-4">
                            <asp:TextBox ID="TextBox1" runat="server" Width="100%" CssClass="form-control" placehorder="Adicione o setença de sua busca"></asp:TextBox>
                        </div>
                          <div class="col-md-4">
                            <span>Ativo: </span>&nbsp;&nbsp;
                                        <asp:RadioButton ID="rdbAtivos_A" runat="server" GroupName="situacao" AutoPostBack="true" Checked="true" Text=" Liberado"  />&nbsp;&nbsp;
                                                                      <asp:RadioButton ID="rdbAtivos_N" runat="server" GroupName="situacao" Text=" Bloqueado"  AutoPostBack="true" />&nbsp;&nbsp;

                                        <asp:RadioButton ID="rdbAmbos" runat="server" GroupName="situacao" AutoPostBack="true" Text=" Ambos" />
                        </div>
                        <div class="col-md-4">
                            <asp:Button ID="btnBuscar" CssClass="btn btn-success fa-search-plus" Text="BUSCAR" runat="server" />
                        </div>

                    </div>

                </div>
                <div class="panel-footer">
                </div>
            </div>

            <!-- DATA TABLES -->
            <div class="panel">
                <div class="panel-body">
                    <table id="datatable1" class="demo-add-niftycheck table table-hover" data-sort-name="nome" data-search="true" data-show-refresh="false" data-show-toggle="false" data-show-columns="false" data-page-size="20" data-pagination="true" data-show-pagination-switch="true">
                        <thead>
                            <tr>
                            <%--    <th style="width: 80px;"></th>--%>
                                 <th>Data</th>
                                 <th>Feed</th>
                                <th>Titulo</th>
                                <th>Comentario</th>
                                <th>Colaborador</th>
                                <th>Liberado</th>
                            <%--    <th>Linkedin</th>
                                <th>Ativo</th>--%>
                               <%-- <th>Posição</th>--%>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>

                            <asp:Repeater ID="Repeater1" runat="server">
                                <ItemTemplate>
                                    <tr>
                                           
                                        <td><%# DataBinder.Eval(Container, "DataItem.datainclusao")%></td>
                                       
                                        <td><%# DataBinder.Eval(Container, "DataItem.Feed")%></td>
                                         <td><%# DataBinder.Eval(Container, "DataItem.titulofeed")%></td>
                                            
                                        <td><%# DataBinder.Eval(Container, "DataItem.comentario")%></td>
                                        <td><%# DataBinder.Eval(Container, "DataItem.nome")%></td>
                                        <td><%# (Convert.ToBoolean(DataBinder.Eval(Container, "DataItem.liberado"))?"<i class=\"fa fa-check\"></i>":"<i class=\"fa fa-ban\"></i>") %></td>
<%--                                            <td class="center"><a class="btn btn-danger" href='<%# "BannersEditar?id=" + DataBinder.Eval(Container, "DataItem.id")%>'>Ver</a></td>--%>
                                                    <td>     <a class="" title="Conf. Pagamento" href='<%# "?id=" + DataBinder.Eval(Container, "DataItem.ID")  + "&liberado=" + DataBinder.Eval(Container, "DataItem.liberado")%>'><%# (Convert.ToBoolean(DataBinder.Eval(Container, "DataItem.liberado"))?"<i class=\"btn btn-danger\">Desativar</i>":"<i class=\"btn btn-success\">Ativar</i>") %></a></td>
<%--                                        <td><%# (Convert.ToBoolean(DataBinder.Eval(Container, "DataItem.liberado"))?"\ <a class=\"btn btn-danger\" href='<%# \"ColaboradoresEditar?id=\" + DataBinder.Eval(Container, \"DataItem.ID\")%>'>Desativar</a>":"<a class=\"btn btn-success\" href=\"ColaboradoresEditar?id=\" + DataBinder.Eval(Container, \"DataItem.ID\")'>Ativar</a>") %>'</td>--%>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /DATA TABLES -->

          <%--  <p>
                <a class="btn btn-danger" href="ColaboradoresEditar">NOVO</a>
            </p>--%>
        </div>
    </form>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">

    <!--Bootstrap Table [ OPTIONAL ] EDICAO PRINCIPAL-->
    <script src="/admin/content/plugins/bootstrap-table/bootstrap-table.js"></script>
    <script src="/admin/content/plugins/bootstrap-table/bootstrap-table-pt-BR.js"></script>

    <!-- DATE RANGE PICKER -->
    <script src="/admin/content/js/bootstrap-daterangepicker/moment.min.js"></script>
    <script src="/admin/content/js/bootstrap-daterangepicker/daterangepicker.min.js"></script>
    <!-- DATE PICKER -->
    <script type="text/javascript" src="/admin/content/js/datepicker/picker.js"></script>
    <script type="text/javascript" src="/admin/content/js/datepicker/picker.date.js"></script>
    <script type="text/javascript" src="/admin/content/js/datepicker/picker.time.js"></script>

    <script type="text/jscript" src="/admin/content/js/jquery.maskedinput-1.3.1.js"></script>

    <script type="text/javascript">


        $(document).ready(function () {
            $(".mask-data").mask("99/99/9999");

            $(".mask-data").datepicker({
                showOn: "button",
                buttonImage: "/admin/content/img/Calendar_scheduleHS.png",
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd/mm/yy',
                dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado', 'Domingo'],
                dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
                dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
                monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez']
            });

            var stable = $('#datatable1').bootstrapTable();
            $(".fixed-table-loading").text('');
        });
    </script>

</asp:Content>


