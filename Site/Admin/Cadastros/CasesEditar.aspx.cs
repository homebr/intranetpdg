﻿using System;
using System.IO;
using System.Drawing;
using System.Data;
using FrameWork;
using FrameWork.DataObject;
using FrameWork.WebControls;

public partial class Cadastros_CasesEditar : System.Web.UI.Page
{
    ClassLibrary.Cases objCases = new ClassLibrary.Cases();
    public String strImage = "";
    string pathVirtual = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "uploads/cases/";
    String pathFisico = System.Configuration.ConfigurationManager.AppSettings["pathFisico"] + "uploads\\cases\\";


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(this.IsPostBack))
        {

            txtPosicao.Attributes.Add("type", "number");
            txtPosicao.Attributes.Add("min", "1");
            txtPosicao.Attributes.Add("max", "99");


            if (this.Request["id"] != null)
                this.hID.Value = FrameWork.Util.GetString(this.Request["id"]);


            CarregaClientes();
            CarregaSegmentos();

            //CARREGA DADOS
            Carrega();

        }
    }

    public void Carrega()
    {
        //OPCAO DE EDICAO PARA VISUALIZAR OS DADOS
        if (FrameWork.Util.GetInt(hID.Value) > 0)
        {
            objCases.Load(this.hID.Value);

            //REGISTRO LOCALIZADO ENTAO APENAS EDITA DADOS DO FORMULARIO
            if (objCases.TotalLoad == 1)
            {

                txtTitulo.Text = objCases.Titulo;
                txtDescricao.Text = FrameWork.Util.GetString(objCases.Descricao);


                if (ddlTipo.Items.FindByValue(FrameWork.Util.GetString(objCases.IDTipo)) != null)
                    ddlTipo.SelectedValue = FrameWork.Util.GetString(objCases.IDTipo);
                if (ddlClientes.Items.FindByValue(FrameWork.Util.GetString(objCases.IDCliente)) != null)
                    ddlClientes.SelectedValue = FrameWork.Util.GetString(objCases.IDCliente);
                if (ddlSegmento.Items.FindByText(FrameWork.Util.GetString(objCases.Segmento)) != null)
                    ddlSegmento.SelectedValue = FrameWork.Util.GetString(objCases.Segmento);

                txtData.Text = String.Format("{0:yyyy}", objCases.Data);
                txtLocal.Text = FrameWork.Util.GetString(objCases.Local);
                txtEspaco.Text = FrameWork.Util.GetString(objCases.Espaco);
                txtVideo.Text = FrameWork.Util.GetString(objCases.Video);
                if (FrameWork.Util.GetString(objCases.Foto).Length > 1)
                {
                    strImage = pathVirtual + FrameWork.Util.GetString(objCases.Foto);
                    this.Image1.ImageUrl = strImage+"?t=" + String.Format("{0:yyyyMMddHHmm}",DateTime.Now);
                }

                txtSku.Text = FrameWork.Util.GetString(objCases.sku);
                txtPosicao.Text = FrameWork.Util.GetString(objCases.Sequencia);
                chkAtivo.Checked = (Convert.ToBoolean(objCases.Ativo));

                lblUsuario.Text = FrameWork.Util.GetString(objCases.Usuario);
                lblDataInclusao.Text = FrameWork.Util.GetString(objCases.DataInclusao);
                lblDataAlteracao.Text = FrameWork.Util.GetString(objCases.DataAlteracao);


                //relato
                txtRelato_Nome.Text = FrameWork.Util.GetString(objCases.Relato_Nome);
                txtRelato_Cargo.Text = FrameWork.Util.GetString(objCases.Relato_Cargo);
                txtRelato_Descricao.Text = FrameWork.Util.GetString(objCases.Relato_Descricao);
                if (FrameWork.Util.GetString(objCases.Relato_foto).Length > 1)
                {
                    strImage = pathVirtual + FrameWork.Util.GetString(objCases.Relato_foto);
                    this.Relato_Image.ImageUrl = strImage + "?t=" + String.Format("{0:yyyyMMddHHmm}", DateTime.Now);
                }

                hOperacao.Value = "UPDATE";

                btnExcluir.Visible = true;

                CarregaFotos();
            }
            else
            {
                //SENAO ACHOU O CODIGO EM QUESTAO É INSERÇÃO

            }

        }
        else
        {
            //ZERA TODOS OS CAMPOS DO FORMULARIO

        }
    }
    public void CarregaClientes()
    {
        string strSql = "SELECT ID,Nome FROM Clientes where Ativo=1  ";
        ddlClientes.DataSource = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSql);
        ddlClientes.DataTextField = "Nome";
        ddlClientes.DataValueField = "ID";
        ddlClientes.DataBind();
    }
    public void CarregaSegmentos()
    {
        string strSql = "SELECT ID,Segmento FROM SegmentoCases where Ativo=1  ";
        ddlSegmento.DataSource = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSql);
        ddlSegmento.DataTextField = "Segmento";
        ddlSegmento.DataValueField = "Segmento";
        ddlSegmento.DataBind();
    }
    public void CarregaFotos()
    {
        string strSql = "SELECT FotoID,('" + pathVirtual + "' + FileName) as FileNameFull,FileName,Descricao,Posicao,FileName_600x600 ";
        strSql += "FROM Fotos WHERE CaseID=" + hID.Value + " order by Posicao ";
        DataTable dt = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSql);

        foreach (DataRow item in dt.Rows)
        {

            try
            {
                if (FrameWork.Util.GetString(item["FileName_600x600"]).Length == 0)
                {
                    String pathToSave_100 = pathFisico + FrameWork.Util.GetString(item["FileName"]);
                    if (System.IO.File.Exists(pathToSave_100))
                    {
                        using (var vimage = Image.FromFile(pathToSave_100))
                        {
                            //salva em 600x600
                            int swidth = 600;
                            int sheight = 600;

                            if (vimage.Width > vimage.Height)
                            {
                                decimal fator = Convert.ToDecimal(Convert.ToDecimal(swidth) / vimage.Width);
                                sheight = Convert.ToInt16(vimage.Height * fator);
                                if (sheight == 0)
                                    sheight = 600;
                            }
                            else
                            {
                                decimal fator = Convert.ToDecimal(Convert.ToDecimal(sheight) / vimage.Height);
                                swidth = Convert.ToInt16(vimage.Width * fator);
                                if (swidth == 0)
                                    swidth = 600;

                            }

                            System.Drawing.Image image2 = ResizeImage(vimage, swidth, sheight);
                            string nomeArquivo = FrameWork.Util.GetString(item["FileName"]).Replace(".jpg", "-600x600.jpg").Replace(".gif", "-600x600.gif").Replace(".png", "-600x600.png");
                            image2.Save(pathFisico + nomeArquivo, System.Drawing.Imaging.ImageFormat.Jpeg);
                            item["FileName_600x600"] = nomeArquivo;

                            strSql = "update Fotos set FileName_600x600='" + nomeArquivo + "' where CaseID=" + hID.Value + " and FotoID=" + FrameWork.Util.GetString(item["FotoID"]);
                            FrameWork.DataObject.DataSource.DefaulDataSource.Execute(strSql);

                        }
                    }
                }
            }
            catch
            {

            }

        }


        rptFotos.DataSource = dt;
        rptFotos.DataBind();

        //Response.Write(strSql)
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        //validação de campos
        if (FrameWork.Util.GetString(txtTitulo.Text) == "")
        {
            Msg("Favor inserir um Título!", false);
            txtTitulo.Focus();
            return;
        }


        if (FrameWork.Util.GetInt(hID.Value) > 0)
        {
            //Comandos de Atualização do Empreendimento
            objCases.Load(hID.Value);
            objCases.DataAlteracao = DateTime.Now;

        }
        else
        {
            hOperacao.Value = "INSERT";
            objCases.DataInclusao = DateTime.Now;
        }


        //salva os valores
        objCases.IDTipo = Convert.ToInt32(this.ddlTipo.SelectedValue);
        objCases.IDCliente = Convert.ToInt32(this.ddlClientes.SelectedValue);
        objCases.Segmento = (ddlSegmento.SelectedValue);

        objCases.Titulo = this.txtTitulo.Text;
        objCases.Descricao = txtDescricao.Text;
        if (Funcoes.IsNumeric(txtData.Text) && txtData.Text.Length == 4)
            objCases.Data = Convert.ToDateTime("01/01/" + txtData.Text);
        objCases.Local  = (txtLocal.Text);
        objCases.Espaco = (txtEspaco.Text);
        objCases.Video = (txtVideo.Text);
        objCases.Ativo = chkAtivo.Checked;
        objCases.Sequencia = FrameWork.Util.GetInt(txtPosicao.Text);
        objCases.Usuario = (string)this.Session["_uiUser"];

        //caso nao tenha pagina criada, gera atraves do titulo
        String pagina = txtSku.Text;
        if (pagina.Trim().Length == 0)
        {
            pagina = WebUtil.GetPageName(txtTitulo.Text);
            //title.Trim().ToLower().Replace(" ", "-")
            if ((pagina.Length > 70))
            {
                pagina = pagina.Substring(0, 69);
            }
            int intQtdPagina = FrameWork.Util.GetInt(FrameWork.DataObject.DataSource.DefaulDataSource.Execute("Select count(*) from Cases where sku = '" + pagina + "' "));
            if (intQtdPagina > 0)
            {
                pagina += "-" + System.DateTime.Now.Day + System.DateTime.Now.Month + System.DateTime.Now.Year + System.DateTime.Now.Hour + System.DateTime.Now.Minute;
                if ((pagina.Length > 70))
                {
                    pagina = WebUtil.GetPageName(pagina).Substring(0, 30) + "-" + System.DateTime.Now.Day + System.DateTime.Now.Month + System.DateTime.Now.Year + System.DateTime.Now.Hour + System.DateTime.Now.Minute;
                }
            }
            txtSku.Text = pagina;
        }
        objCases.sku = txtSku.Text;

        //relato
        objCases.Relato_Nome = txtRelato_Nome.Text;
        objCases.Relato_Cargo = txtRelato_Cargo.Text;
        objCases.Relato_Descricao = txtRelato_Descricao.Text;



        try
        {
            objCases.Save();
            this.hID.Value = objCases.ID.ToString();

            hOperacao.Value = "UPDATE";
            Msg("Salvo com sucesso!", true);

        }
        catch (Exception ex)
        {
            Msg(ex.Message, false);
        }

        try
        {


            if ((txtArquivo.HasFile))
            {

                //inicializar as variáveis
                string arq = txtArquivo.PostedFile.FileName;
                string fileExtension = txtArquivo.PostedFile.ContentType.ToLower();

                if (!string.IsNullOrEmpty(arq))
                {


                    fileExtension = Path.GetExtension(arq);
                    string str_image = "case-" + hID.Value + fileExtension.ToLower();

                    //verificamos a extensão através dos últimos 4 caracteres e verificamos se é permitido
                    if ((!(fileExtension == ".gif" || fileExtension == ".jpg" || fileExtension == ".png")))
                    {
                        Msg("Extensão de arquivo não permitido! Somente é permitido extensão: gif, jpg ou png!", false);
                        return;
                    }


                    //tamanho maximo do upload em kb
                    double permitido = 2000;
                    //identificamos o tamanho do arquivo
                    double tamanho = 0;
                    tamanho = Convert.ToDouble(txtArquivo.PostedFile.ContentLength) / 1024;
                    if ((tamanho > permitido))
                    {
                        Msg("Tamanho Máximo permitido é de " + permitido + " kb", false);
                        return;
                    }

                    //caminho fisico no server
                    string pathPDFs = System.Configuration.ConfigurationManager.AppSettings["pathFisico"] + "uploads\\Cases\\";

                    txtArquivo.SaveAs(pathPDFs + str_image);

                    String strSql = "UPDATE Cases SET Foto = '" + str_image + "' WHERE ID=" + hID.Value;
                    FrameWork.DataObject.DataSource.DefaulDataSource.Execute(strSql);

                    txtArquivo.Dispose();

                    strImage = pathVirtual + str_image;
                    this.Image1.ImageUrl = strImage + "?t=" + String.Format("{0:yyyyMMddHHmm}", DateTime.Now);
                }

            }
            if ((fileRelato_Upload.HasFile))
            {

                //inicializar as variáveis
                string arq = fileRelato_Upload.PostedFile.FileName;
                string fileExtension = fileRelato_Upload.PostedFile.ContentType.ToLower();

                if (!string.IsNullOrEmpty(arq))
                {


                    fileExtension = Path.GetExtension(arq);
                    string str_image = "relato-" + hID.Value + fileExtension.ToLower();

                    //verificamos a extensão através dos últimos 4 caracteres e verificamos se é permitido
                    if ((!(fileExtension == ".gif" || fileExtension == ".jpg" || fileExtension == ".png")))
                    {
                        Msg("Extensão de arquivo não permitido! Somente é permitido extensão: gif, jpg ou png!", false);
                        return;
                    }


                    //tamanho maximo do upload em kb
                    double permitido = 2000;
                    //identificamos o tamanho do arquivo
                    double tamanho = 0;
                    tamanho = Convert.ToDouble(fileRelato_Upload.PostedFile.ContentLength) / 1024;
                    if ((tamanho > permitido))
                    {
                        Msg("Tamanho Máximo permitido é de " + permitido + " kb", false);
                        return;
                    }

                    //caminho fisico no server
                    string pathPDFs = System.Configuration.ConfigurationManager.AppSettings["pathFisico"] + "uploads\\Cases\\";

                    fileRelato_Upload.SaveAs(pathPDFs + str_image);

                    String strSql = "UPDATE Cases SET Relato_foto = '" + str_image + "' WHERE ID=" + hID.Value;
                    FrameWork.DataObject.DataSource.DefaulDataSource.Execute(strSql);

                    fileRelato_Upload.Dispose();

                    strImage = pathVirtual + str_image;
                    this.Relato_Image.ImageUrl = strImage + "?t=" + String.Format("{0:yyyyMMddHHmm}", DateTime.Now);
                }

            }


            

        }
        catch (Exception ex)
        {
            Msg(ex.Message, false);
            return;
        }


    }

    protected void btnExcluir_Click(object sender, EventArgs e)
    {

        try
        {
            objCases.Load(hID.Value);
            objCases.Delete();
            Response.Redirect("Cases");
        }
        catch (Exception ex)
        {
            Msg(ex.Message, false);
        }
    }

    protected void Msg(String msg, Boolean tipo)
    {
        lblMensagem.CssClass = (tipo ? "box-danger alert alert-success" : "box-danger alert alert-danger");
        lblMensagem.Text = msg;
        lblMensagem.Visible = true;
    }

    public static Bitmap ResizeImage(Image image, int width, int height)
    {
        var destRect = new Rectangle(0, 0, width, height);
        var destImage = new Bitmap(width, height);

        destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

        using (var graphics = Graphics.FromImage(destImage))
        {
            graphics.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;
            graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            graphics.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;

            using (var wrapMode = new System.Drawing.Imaging.ImageAttributes())
            {
                wrapMode.SetWrapMode(System.Drawing.Drawing2D.WrapMode.TileFlipXY);
                graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
            }
        }

        return destImage;
    }
}