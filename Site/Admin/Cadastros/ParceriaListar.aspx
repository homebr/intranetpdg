﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="ParceriaListar.aspx.cs" Inherits="Cadastros_ParceriaListar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!-- JQUERY UI-->
    <link rel="stylesheet" type="text/css" href="/admin/content/js/jquery-ui-1.10.3.custom/css/custom-theme/jquery-ui-1.10.3.custom.min.css" />

    <!--DataTables [ OPTIONAL ]-->
    <link href="/admin/content/plugins/datatables/media/css/dataTables.bootstrap.css" rel="stylesheet">
    <link href="/admin/content/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css" rel="stylesheet">

    <link href="/admin/content/css/select2.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <!--Page Title-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <div id="page-title">
        <h1 class="page-header text-overflow">Parcerias</h1>
    </div>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End page title-->


    <!--Breadcrumb-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <ol class="breadcrumb">
        <li><a href="/">Home</a></li>
        <li class="active">Parcerias</li>
        <li>Listar</li>

    </ol>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End breadcrumb-->




    <form id="form1" runat="server">
        <!--Page content-->
        <!--===================================================-->
        <div id="page-content">

            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">FILTROS</h3>
                </div>
                <div class="panel-body">

                    <div class="form-group">
                        <div class="col-md-3">
                            <asp:TextBox ID="TextBox1" runat="server" Width="100%" CssClass="form-control" placehorder="Adicione o setença de sua busca"></asp:TextBox>
                        </div>
                        <div class="col-md-9">
                            <asp:Button ID="btnBuscar" CssClass="btn btn-success fa-search-plus" Text="BUSCAR" runat="server" />
                        </div>

                    </div>

                </div>
                <div class="panel-footer">
                </div>
            </div>

            <!-- DATA TABLES -->
            <div class="panel">
                <div class="panel-body">
                    <table id="datatable1" class="demo-add-niftycheck table table-hover" data-sort-name="nome" data-search="true" data-show-refresh="false" data-show-toggle="false" data-show-columns="false" data-page-size="20" data-pagination="true" data-show-pagination-switch="true">
                        <thead>
                            <tr>
                                <th style="width: 80px;"></th>
                                <th>Empresa</th>
                                <th>Link</th>
                                <th>Cupom</th>
                                <th>Desconto</th>
                                <th>Ativo</th>
                               <%-- <th>Posição</th>--%>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>

                            <asp:Repeater ID="Repeater1" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td class="tdBlack" style="width: 80px;"><a href='<%# "ParceriaEditar?id=" + DataBinder.Eval(Container, "DataItem.ID")%>'>
                                           
                                        <td class="tdBlack"><a href='<%# "ParceriaEditar?id=" + DataBinder.Eval(Container, "DataItem.ID")%>'><b><%# DataBinder.Eval(Container, "DataItem.Empresa")%></b></a></td>
                                       
                                        <td><%# DataBinder.Eval(Container, "DataItem.Link")%></td>
                                        <td><%# DataBinder.Eval(Container, "DataItem.Cupom")%></td>
                                         <td><%# DataBinder.Eval(Container, "DataItem.Desconto")%></td>
                                         
                                        <td><%# (Convert.ToBoolean(DataBinder.Eval(Container, "DataItem.Ativo"))?"<i class=\"fa fa-check\"></i>":"") %></td>
                                      <%--  <td><%# DataBinder.Eval(Container, "DataItem.Sequencia")%></td>--%>
                                        <td><a class="btn btn-danger" href='<%# "ParceriaEditar?id=" + DataBinder.Eval(Container, "DataItem.ID")%>'>Ver</a></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /DATA TABLES -->

            <p>
                <a class="btn btn-danger" href="ParceriaEditar">NOVO</a>
            </p>
        </div>
    </form>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">

    <!--Bootstrap Table [ OPTIONAL ] EDICAO PRINCIPAL-->
    <script src="/admin/content/plugins/bootstrap-table/bootstrap-table.js"></script>
    <script src="/admin/content/plugins/bootstrap-table/bootstrap-table-pt-BR.js"></script>

    <!-- DATE RANGE PICKER -->
    <script src="/admin/content/js/bootstrap-daterangepicker/moment.min.js"></script>
    <script src="/admin/content/js/bootstrap-daterangepicker/daterangepicker.min.js"></script>
    <!-- DATE PICKER -->
    <script type="text/javascript" src="/admin/content/js/datepicker/picker.js"></script>
    <script type="text/javascript" src="/admin/content/js/datepicker/picker.date.js"></script>
    <script type="text/javascript" src="/admin/content/js/datepicker/picker.time.js"></script>

    <script type="text/jscript" src="/admin/content/js/jquery.maskedinput-1.3.1.js"></script>

    <script type="text/javascript">


        $(document).ready(function () {
            $(".mask-data").mask("99/99/9999");

            $(".mask-data").datepicker({
                showOn: "button",
                buttonImage: "/admin/content/img/Calendar_scheduleHS.png",
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd/mm/yy',
                dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado', 'Domingo'],
                dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
                dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
                monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez']
            });

            var stable = $('#datatable1').bootstrapTable();
            $(".fixed-table-loading").text('');
        });
    </script>

</asp:Content>


