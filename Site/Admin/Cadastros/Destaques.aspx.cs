﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClassLibrary;
using FrameWork.WebControls;
using FrameWork.DataObject;

public partial class Cadastros_Destaques : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!this.IsPostBack)
        {
            //CarregaPaginas();
            //if (this.ddlPagina.Items.FindByValue(FrameWork.Util.GetString(Request["p"])) != null)
            //    this.ddlPagina.SelectedValue = FrameWork.Util.GetString(Request["p"]);

        }

        Carrega();
    }

    //public void CarregaPaginas()
    //{

    //    //String strSql = "SELECT ID,Titulo,'1' as cat FROM Paginas WHERE Situacao='A' and Video=1 union all SELECT 0 AS ID,'[TODOS]' as Titulo,'0' as cat order by cat,Titulo ";
    //    String strSql = "SELECT ID,Titulo,'2' as cat FROM Paginas WHERE Situacao='A' and Destaques=1 union all SELECT 0 AS ID,'[TODOS]' as Titulo,'0' as cat union all SELECT 999 AS ID,'HOME' as Titulo,'1' as cat order by cat,Titulo ";
    //    ddlPagina.DataSource = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSql);
    //    ddlPagina.DataValueField = "ID";
    //    ddlPagina.DataTextField = "Titulo";
    //    ddlPagina.DataBind();


    //}

    public void Carrega()
    {

        String strSQL = "SELECT  * FROM Destaques WHERE 1=1  ";
        if (FrameWork.Util.GetString(TextBox1.Text).ToString().Trim() != "")
            strSQL += " and (Titulo LIKE @text) ";

        //if (FrameWork.Util.GetInt(ddlPagina.SelectedValue) > 0)
        //{
        //    strSQL += " and (IDPagina = " + ddlPagina.SelectedValue + ") ";
        //}

        strSQL += " ORDER BY ID DESC ";

        //Response.Write(strSQL);


        DataTable dt = new DataTable();
        dt = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSQL, new ParameterItem("@text", TextBox1.Text));



        Repeater1.DataSource = dt;
        Repeater1.DataBind();



    }
}