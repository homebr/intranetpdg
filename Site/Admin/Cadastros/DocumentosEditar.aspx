﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="DocumentosEditar.aspx.cs" Inherits="Cadastros_DocumentosEditar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <!--Switchery [ OPTIONAL ]-->
    <link href="/admin/content/plugins/switchery/switchery.min.css" rel="stylesheet">
    <link href="/admin/content/css/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet" type="text/css" />

    <!--Ion Icons [ OPTIONAL ]-->
    <link href="/admin/content/plugins/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="/admin/content/plugins/themify-icons/themify-icons.min.css" rel="stylesheet">
    <script src="/admin/content/js/demo/icons.js"></script>


    <link href="/admin/content/plugins/spectrum/spectrum.css" rel="stylesheet">


    <link rel="stylesheet" type="text/css" href="/admin/content/css/responsive.css">
    <!-- ANIMATE -->
    <link rel="stylesheet" type="text/css" href="/admin/content/css/animatecss/animate.min.css" />
    <!-- COLORBOX -->
    <link rel="stylesheet" type="text/css" href="/admin/content/js/colorbox/colorbox.min.css" />


    <!-- BOOTSTRAP SWITCH -->
    <link rel="stylesheet" type="text/css" href="/admin/content/js/bootstrap-switch/bootstrap-switch.min.css" />
    <link rel="stylesheet" type="text/css" href="/admin/content/css/themes/default.css" id="skin-switcher">

    <!-- JQUERY UI-->
    <link rel="stylesheet" type="text/css" href="/admin/content/js/jquery-ui-1.10.3.custom/css/custom-theme/jquery-ui-1.10.3.custom.min.css" />

    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="/admin/content/plugins/iCheck/all.css">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="/admin/content/plugins/colorpicker/bootstrap-colorpicker.min.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="/admin/content/plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="/admin/content/plugins/select2/select2.min.css">

    <!-- DROPZONE -->
    <link rel="stylesheet" type="text/css" href="/admin/content/js/dropzone/dropzone.min.css" />

    <!--Magic Checkbox [ OPTIONAL ]-->
    <link href="/admin/content/plugins/magic-check/css/magic-check.min.css" rel="stylesheet">
    <script src="https://cdn.ckeditor.com/4.7.3/standard-all/ckeditor.js"></script>

    <style>
        .image-grid img {
            width: 312px;
            height: 200px;
        }

        .foto-descricao-oculto {
            display: none;
        }

        .imgPrincipal img {
            margin: 0 auto 0 auto;
            max-height: 150px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!--Page Title-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <div id="page-title">
        <h1 class="page-header text-overflow">Arquivos</h1>

    </div>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End page title-->

    <!--Breadcrumb-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <ol class="breadcrumb">
        <li><a href="/">Home</a></li>
        <li class="active">Arquivos</li>
        <li>Editar</li>
    </ol>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End breadcrumb-->


    <!-- Main content -->
    <div id="page-content">
        <form id="form1" runat="server">


            <asp:Label ID="lblMensagem" runat="server" CssClass="box-danger alert alert-danger" Style="display: block" Visible="false"></asp:Label>

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="panel-control">
                        <ul class="nav nav-tabs">
<%--                            <li class="active"><a href="#box_tab1" data-toggle="tab"><i class="fa fa-bars"></i><span class="hidden-inline-mobile">&nbsp;Dados</span></a></li>
                            <li><a href="#box_tab2" data-toggle="tab"><i class="fa fa-laptop"></i><span class="hidden-inline-mobile">&nbsp;Relato</span></a></li>--%>
                        </ul>
                    </div>
                    <h3 class="panel-title">Arquivo</h3>
                </div>
                <div class="panel-body">


                    <div class="tab-content">
                        <!-- Aba Case -->
                        <div class="tab-pane fade in active" id="box_tab1">
                            <div class="form-group">
                               
                                <div class="col-sm-6">
                                    <label>Título do Documento:<span class="required">*</span></label>
                                    <asp:TextBox ID="txtTitulo" CssClass="form-control" required="required" runat="server" MaxLength="80" Width="100%" placeholder="título do documento"></asp:TextBox>
                                </div>
                              <div class="col-sm-2 ">
                                    <label>Setor:</label>
                                 
                                     <asp:DropDownList ID="ddlarea" runat="server" CssClass="form-control"></asp:DropDownList>

                                </div>
                                  <div class="col-sm-2 ">
                                    <label>Tipo:</label>
                                 
                                     <asp:DropDownList ID="ddltipo" runat="server" CssClass="form-control">
                                         <asp:ListItem>Selecione</asp:ListItem>
                                         <asp:ListItem>Documento</asp:ListItem>
                                         <asp:ListItem>Manual</asp:ListItem>
                                      </asp:DropDownList>

                                </div>
                                   <div class="col-sm-2 ">
                                    <label>Data Documento:</label>
                                 <asp:TextBox ID="txtData" CssClass="form-control mask-data form-inline" placeholder="dd/mm/yyyy" runat="server" Width="130px"></asp:TextBox>

                                </div>
                            </div>
                            <div style="clear: both;"></div>
                       


                            <div class="col-sm-12">
                                <div class="separator"></div>
                            </div>

                         <%--   <div class="form-group video">
                                <div class="col-sm-12">
                                    <label>Vídeo:</label>
                                </div>
                            </div>--%>
                           
                            <div style="clear: both;"></div>
                            <div class="form-group">
                                <div class="col-sm-2">
                                    <label>Ativar:</label>
                                    <div>
                                        <div class="make-switch switch-small" data-on="success" data-off="danger" data-on-label="<i class='fa fa-check icon-white'></i>" data-off-label="<i class='fa fa-times'></i>">
                                            <asp:CheckBox ID="chkAtivo" runat="server" />
                                        </div>
                                    </div>
                                </div>
                              
                            </div>


                            <div style="clear: both;"></div>
                            <div class="col-sm-12">
                                <div class="separator"></div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-4">
                                    <asp:Repeater ID="Repeater1" runat="server">
                                <ItemTemplate>
                                    <a class="btn btn-sucess hover-link" href='<%# DataBinder.Eval(Container, "DataItem.Link")%>' target="_blank" title="Clique para Abrir">
                                    <i class='<%# "fa " + (FrameWork.Util.GetString(DataBinder.Eval(Container, "DataItem.Arquivo")).Contains(".pdf")?"fa-file-pdf-o":(FrameWork.Util.GetString(DataBinder.Eval(Container, "DataItem.Arquivo")).Contains(".xlx")?"fa-file-excel-o":(FrameWork.Util.GetString(DataBinder.Eval(Container, "DataItem.Arquivo")).Contains(".doc")?"fa-file-word-o":(FrameWork.Util.GetString(DataBinder.Eval(Container, "DataItem.Arquivo")).Contains(".jpg") || FrameWork.Util.GetString(DataBinder.Eval(Container, "DataItem.Arquivo")).Contains(".gif") || FrameWork.Util.GetString(DataBinder.Eval(Container, "DataItem.Arquivo")).Contains(".png")?"fa-file-image-o":"")))) %>' aria-hidden="true"></i>
                                </a>
                                              </ItemTemplate>
                            </asp:Repeater>
                                   <%-- <asp:Image ID="Image1" runat="server" CssClass="img-responsive" />--%>
                                </div>
                                <div class="col-sm-8">
                                    <label>Documento:</label>
                                    <asp:FileUpload ID="txtArquivo" CssClass="form-control" runat="server" Width="100%"></asp:FileUpload>
                                    <small>Upload </small>
                                </div>
                            </div>
                            <div style="clear: both;"></div>
                            <div class="col-sm-12">
                                <div class="separator"></div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-4">
                                    <label for="ContentPlaceHolder1_lblDataInclusao">Data Inclusão:</label>
                                    <asp:Label ID="lblDataInclusao" runat="server" Text=""></asp:Label>
                                </div>
                                <div class="col-sm-4">
                                    <label for="ContentPlaceHolder1_lblDataAlteracao">Data Alteração:</label>
                                    <asp:Label ID="lblDataAlteracao" runat="server" Text=""></asp:Label>
                                </div>
                                <div class="col-sm-4">
                                    <label for="ContentPlaceHolder1_lblUsuario">Usuário:</label>
                                    <asp:Label ID="lblUsuario" runat="server" Text=""></asp:Label>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Literal ID="ltlLOG" runat="server"></asp:Literal>
                            </div>
                        </div>

                        <!-- Aba Relato -->
                        <div class="tab-pane fade in" id="box_tab2">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label for="ContentPlaceHolder1_Nome">Nome:<span class="required">*</span></label>
                                    <asp:TextBox ID="txtRelato_Nome" CssClass="form-control" required="required" runat="server" MaxLength="80" Width="100%" placeholder="Nome do Colaborador"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label for="ContentPlaceHolder1_Cargo">Cargo:</label>
                                    <asp:TextBox ID="txtRelato_Cargo" CssClass="form-control" runat="server" MaxLength="50" Width="100%"></asp:TextBox>
                                </div>

                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label for="ContentPlaceHolder1_Chamada">Descrição:</label>
                                    <asp:TextBox ID="txtRelato_Descricao" CssClass="form-control" runat="server" Height="100px" TextMode="MultiLine" Width="100%" placeholder=""></asp:TextBox>

                                </div>
                            </div>


                            <div class="col-sm-12">
                                <div class="separator"></div>
                            </div>

                            <div style="clear: both;"></div>
                            <div class="col-sm-12">
                                <div class="separator"></div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-4">
                                    <asp:Image ID="Relato_Image" runat="server" CssClass="img-responsive" />
                                </div>
                                <div class="col-sm-8">
                                    <label>Foto:</label>
                                    <asp:FileUpload ID="fileRelato_Upload" CssClass="form-control" runat="server" Width="100%"></asp:FileUpload>
                                    <small>Upload (Gif/Jpg/Png) formato: 400 x 700</small>
                                </div>
                            </div>





                        </div>
                    </div>

                </div>
                <div class="panel-footer text-right" style="background-color: white;">
                    <asp:LinkButton ID="btnSalvar" runat="server" CssClass="btn btn-success col-sm-1" OnClick="btnSalvar_Click"><i class="fa fa-save fa-1x"></i> SALVAR</asp:LinkButton>&nbsp;&nbsp;
                            <asp:Button ID="btnExcluir" runat="server" Text="EXCLUIR" CssClass="btn btn-warning fa-delicious" Visible="false" OnClientClick="return confirm('Confirma exclusão deste registro?');" OnClick="btnExcluir_Click" />&nbsp;&nbsp;

                            <asp:Button ID="btnVoltar" runat="server" Text="VOLTAR" CssClass="btn fa-reply" PostBackUrl="Cases" />

                    <br />
                    <br />
                    <asp:HiddenField ID="hOperacao" runat="server" />
                    <asp:HiddenField ID="hID" runat="server" />
                    <asp:HiddenField ID="hUrlReferrer" runat="server" />
                    <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                </div>
            </div>


        </form>


        <br />
        <p>
       
        </p>
        <br />


  

    </div>


    <div class="modal" id="editImagem">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Editar Imagem</h4>
                </div>
                <form id="modal-edit-foto" name="modal-edit-foto">
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>Descrição:</label>
                                <input id="txtModal_Descricao" name="txtModal_Descricao" class="form-control" maxlength="50" placeholder="Descrição da Foto" />
                                <span class="error-span"></span>
                            </div>
                        </div>

                        <p>
                            &nbsp;
                                    
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-success btn-foto-edit">Salvar</button>
                    </div>
                    <input type="hidden" id="hFotoEdit" />
                </form>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="Server">
    <!-- JQUERY UI-->
    <link rel="stylesheet" type="text/css" href="/admin/content/js/jquery-ui-1.10.3.custom/css/custom-theme/jquery-ui-1.10.3.custom.min.css" />

    <!-- DATE RANGE PICKER -->
    <script src="/admin/content/js/bootstrap-daterangepicker/moment.min.js"></script>
    <script src="/admin/content/js/bootstrap-daterangepicker/daterangepicker.min.js"></script>

    <!-- bootstrap color picker -->
    <script src="/admin/content/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
    <!-- bootstrap time picker -->
    <script src="/admin/content/plugins/timepicker/bootstrap-timepicker.min.js"></script>

    <!-- iCheck 1.0.1 -->
    <script src="/admin/content/plugins/iCheck/icheck.min.js"></script>

    <!-- BOOTSTRAP SWITCH -->
    <script type="text/javascript" src="/admin/content/js/bootstrap-switch/bootstrap-switch.min.js"></script>

    <!-- ISOTOPE -->
    <script type="text/javascript" src="/admin/content/js/isotope/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="/admin/content/js/isotope/imagesloaded.pkgd.min.js"></script>
    <!-- COLORBOX -->
    <script type="text/javascript" src="/admin/content/js/colorbox/jquery.colorbox.min.js"></script>

    <!-- KNOB -->
    <script type="text/javascript" src="/admin/content/js/jQuery-Knob/js/jquery.knob.min.js"></script>
    <script type="text/jscript" src="/admin/content/js/jquery.maskedinput-1.3.1.js"></script>

    <!-- DROPZONE -->
    <script src="/admin/content/js/dropzone/dropzone.min.js"></script>
    <script src="/admin/content/js/jquery-fancybox-pack.js"></script>
    <script src="/admin/content/js/jquery.mixitup.js"></script>

    <script type="text/jscript">

        var campo = '<%=hID.ClientID.ToString().Replace("hID","")%>';

        jQuery(document).ready(function () {

            var adjustMinHeight = function (y) {
                $(y).each(function () {
                    var A = $($($(this).attr("href")));
                    var z = $(this).parent().parent();
                    if (z.height() > A.height()) {
                        A.css("min-height", z.height())
                    }
                })
            };
            $("body").on("click", '.nav.nav-tabs.tabs-left a[data-toggle="tab"], .nav.nav-tabs.tabs-right a[data-toggle="tab"]', function () {
                adjustMinHeight($(this));
            });
            adjustMinHeight('.nav.nav-tabs.tabs-left > li.active > a[data-toggle="tab"], .nav.nav-tabs.tabs-right > li.active > a[data-toggle="tab"]');
            if (location.hash) {
                var w = location.hash.substr(1);
                $('a[href="#' + w + '"]').click();
            }
        });


        $(document).ready(function () {
            $(".mask-data").mask("99/99/9999");

            $(".mask-data").datepicker({
                showOn: "button",
                buttonImage: "/admin/content/img/Calendar_scheduleHS.png",
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd/mm/yy',
                dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado', 'Domingo'],
                dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
                dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
                monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez']
            });

            $("#" + campo + "ddlTipo").change(function () {
                ChangeTipo();
            });

            ChangeTipo();

            function ChangeTipo() {
                if ($("#" + campo + "ddlTipo option:selected").val() == "2") {
                    $('.padrao').hide();
                    $('.video').show();
                }
                else if ($("#" + campo + "ddlTipo option:selected").val() == "1") {
                    $('.padrao').show();
                    $('.video').hide();
                }
            }

            $(".remove-img").click(function () {
                if (confirm("Confirma a exclusão da foto?")) {
                    $.post("FotosRemover.ashx?id=" + $(this).data('value'), function (data) {
                    });
                    e = $(this).parent().parent();

                    //alert(e.html());
                    e.hide();
                }
            });

            $('.image-grid').mixItUp();


            $(".edit-img").click(function () {
                var $this = $(this);

                var id = $(this).attr('data-value');
                $("#modal-edit-foto #hFotoEdit").val(id);

                var stitle = $(this).parent().parent().attr("title");
                $("#modal-edit-foto #txtDescricao").val(stitle);
            });

            //foto edit
            $(".btn-foto-edit").click(function () {
                var result = 0;
                var id = $('#modal-edit-foto #hFotoEdit').val();
                var sDescricao = $('#modal-edit-foto #txtModal_Descricao').val();

                data = {
                    acao: 'edit',
                    id: id,
                    desc: sDescricao,
                };

                $.post("wsFotos.ashx", data, function (result) {

                    console.log("result=" + result);

                    e = $(".image-grid .edit-img[data-value=" + id + "]");
                    z = e.parent().parent();
                    z.attr("title", sDescricao);

                    $('#editImagem').modal('toggle');

                });

                return false;
            });

            //ordernação das imagens
            $(".image-grid").sortable({
                placeholder: "highlight",
                connectWith: ".image-grid .mix",
                start: function (event, ui) {
                    ui.item.toggleClass("highlight");
                },
                stop: function (event, ui) {
                    var slides = [];
                    var items = [];

                    ui.item.toggleClass("highlight");

                    $(".image-grid .mix .remove-img").each(function (index, element) {
                        slides[index] = index + 1;
                        items[index] = parseInt($(element).attr('data-value'));

                        //alert("pos=" + slides[index] + " - item=" + items[index]);
                    });

                    console.log(slides, items);
                    $.post('GaleriaOrdenacao.ashx',
                        { 'item_id': items.join(','), 'positions': slides.join(',') }, function (data) { }, 'json');


                }
            });

            $(".image-grid").disableSelection();


        });





    </script>

</asp:Content>





