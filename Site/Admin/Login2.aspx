﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login2.aspx.cs" Inherits="Login2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>PDG Portal Conselho Admin | Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
    <meta name="description" content="Sistema Administrativo Homebrasil">
    <meta name="author" content="Fernando">


    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="content/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link href="content/css/admin-login.css" rel="stylesheet">

<!-- iCheck -->
    <link rel="stylesheet" href="content/plugins/iCheck/square/blue.css">
	<!-- ANIMATE -->
	<link rel="stylesheet" href="content/css/animatecss/animate.min.css" />

        <!--JAVASCRIPT-->
    <!--=================================================-->

    <!--Pace - Page Load Progress Par [OPTIONAL]-->
    <link href="content/plugins/pace/pace.min.css" rel="stylesheet">
    <script src="content/plugins/pace/pace.min.js"></script>


    <!--jQuery [ REQUIRED ]-->
    <script src="content/js/jquery.min.js"></script>


    <!--BootstrapJS [ RECOMMENDED ]-->
    <script src="content/js/bootstrap.min.js"></script>


    <!--NiftyJS [ RECOMMENDED ]-->
    <script src="content/js/nifty.min.js"></script>






    <!--=================================================-->
    
    <!--Background Image [ DEMONSTRATION ]-->
    <script src="content/js/demo/bg-images.js"></script>

</head>
<body class="hold-transition login-page">
    <!-- PAGE -->
    <div class="logo-flut"></div>
    <div class="box-login col-xs-12 col-sm-12 col-md-6">
    	<div class="login-box">

        <div class="login-logo">
            <b><img src="/admin/content/img/logo.jpg" class="img-responsivo "></b>
        </div>

        <div class="bg-home-portal-left">
        <h1>Bem-vindo<br><b>ao Admin do Portal Conselho</b></h1>
        <p><span>Para acessar o admin digite<br> os dados abaixo</span></p>
        </div>

        <!-- /.login-logo -->

        <form id="form1" action="login" runat="server">

            <div class="login-box-body visible animated fadeInUp">

                <div class="form-group has-feedback">
                    <input type="text" maxlength="30" id="usuario" name="usuario" class="form-control campo_login" placeholder="Usuário" />
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>

                <div class="form-group has-feedback">
                    <input type="password" class="form-control campo_login" maxlength="20" id="senha" name="senha" placeholder="Senha de Acesso">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>

                <div class="row">
                
                <div class="col-xs-12">
                        <asp:Button ID="btnLogin" class="btn btn-primary btn-block btn-flat" runat="server" Text="ENTRAR" OnClick="btnLogin_Click" />
                        <asp:HiddenField ID="hapelido" runat="server" />
                        <asp:HiddenField ID="hUrl" runat="server" />
                    </div>
                    <!-- /.col -->
                
                    <div class="col-xs-12" style="text-align: center;">
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox" id="chkConectado" name="chkConectado">
                                Lembre-me&nbsp;&nbsp;&nbsp;|
  
                            </label>
                            <label>
                            	<a href="#" onclick="swapScreen('login-box-forgot');return false;">Recuperar Senha</a>
                           </label>
                        </div>
                    </div>
                    
                    <!-- /.col -->
                    
                </div>
                 <div class="col-md-12">
                        <asp:Label ID="lblmsg" runat="server" CssClass="box-danger alert alert-danger" Style="display: block" Visible="false" />
                    </div>
            </div>

            <!-- FORGOT PASSWORD -->
            <div class="login-box-forgot">
                <div class="row">
                    <div class="col-md-12">
                        <div class="login-box-plain">
                            <h2 class="bigintro">Lembrar Senha</h2>
                            <div class="divide-40"></div>
                            
                            
                            <div class="form-group has-feedback">
                                <input type="email" class="form-control campo_login" maxlength="20" id="email" name="email" placeholder="Coloque seu E-mail">
                                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                            </div>
                            
                            
                            <div class="form-actions">
                                <asp:Button ID="btnLembrete" class="btn btn-primary btn-block btn-flat" runat="server" Text="ENVIAR PARA MEU E-MAIL" OnClick="btnLembrete_Click" />
                            </div>
                            <label>
                                <a href="#" onclick="swapScreen('login-box-body');return false;">Voltar ao Login</a>
                                <br />
                            </label>
                            
                        </div>
                    </div>
                </div>
                <!-- FORGOT PASSWORD -->
            </div>

        </form>


        <div class="login-box-body">
            <div class="row">
                <div class="col-md-12">
                <asp:Label ID="lblMensagem" runat="server" CssClass="box-danger alert alert-danger" Style="display: block" Visible="false" />
                </div>
            </div>
        </div>

        <!-- /.login-box-body -->




    </div>
	</div>
   <%-- <!-- jQuery 2.1.4 -->
    <script src="/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="/js/bootstrap.min.js"></script> --%>
    <!-- iCheck -->
    <script src="content/plugins/iCheck/icheck.min.js"></script>
    <!-- JQUERY UI-->
    <script src="content/plugins/jQueryUI/jquery-ui.min.js"></script>

    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
        function swapScreen(id) {
            jQuery('.visible').removeClass('visible animated fadeInUp');
            jQuery('.' + id).addClass('visible animated fadeInUp');
        }
                        <% if (sBox == "lembrete")
            { %>
        jQuery('.visible').removeClass('visible animated fadeInUp');
        jQuery('.login-box-forgot').addClass('visible animated fadeInUp');



        <% } %>

        //Checa o cookie se foi marcado para lembrar
        if ($.cookie("_JLL_piAdmin_login") != "undefined") {
            $("#usuario").val($.cookie("_JLL_piAdmin_login"));
            $("#chkConectado").prop('checked', true);
            $(".icheckbox_flat-yellow").addClass("checked");
        }
    </script>
</body>
</html>
