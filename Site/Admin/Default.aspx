﻿<%@ Page Title="" Language="C#" MasterPageFile="/Admin/Admin.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
     <!-- ANIMATE -->
    <link rel="stylesheet" type="text/css" href="/admin/content/css/animatecss/animate.min.css" />
    <!-- TODO -->
    <link rel="stylesheet" type="text/css" href="/admin/content/js/jquery-todo/css/styles.css" />
    <!-- FULL CALENDAR -->
    <link rel="stylesheet" type="text/css" href="/admin/content/js/fullcalendar/fullcalendar.min.css" />
    <!-- GRITTER -->
    <link rel="stylesheet" type="text/css" href="/admin/content/js/gritter/css/jquery.gritter.css" />
    <link href="../AdminLTE-master/dist/css/adminlte.css" rel="stylesheet" />
    <link href="../AdminLTE-master/dist/css/adminlte.min.css" rel="stylesheet" />
  <%--  <!--Specify page [ SAMPLE ]-->
    <script src="../content/js/demo/dashboard.js"></script>--%>



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
      <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="clearfix">
            <h3 class="content-title pull-left">Dashboard</h3>
            <!-- DATE RANGE PICKER -->
            <span class="date-range pull-right">
                <div class="btn-group">
                    <a class="js_update btn btn-default <%=(FrameWork.Util.GetString(Request["p"])=="h"?"active":"") %>" href="?p=h">Hoje</a>
                    <a class="js_update btn btn-default <%=(FrameWork.Util.GetString(Request["p"])=="s"?"active":"") %>" href="?p=s">Semana</a>
                    <a class="js_update btn btn-default hidden-xs <%=(FrameWork.Util.GetString(Request["p"])==""?"active":"") %>" href="">Mês</a>

                    <a id="reportrange" class="btn reportrange">
                        <i class="fa fa-calendar"></i>
                        <span></span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                </div>
            </span>
            <!-- /DATE RANGE PICKER -->
        </div>
        <div class="description">Visão geral, estatísticas e muito mais</div>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row1">
            <div class="col-lg-4">
                <div class="small-box bg-orange">
                    <div class="inner">
                        <h3>
                            <asp:Literal ID="ltlQtdVisitas" runat="server"></asp:Literal></h3>
                        <p>Visitas</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-magic"></i>
                    </div>
                    <a href="Relatorios/Visitas" class="small-box-footer">Veja Mais <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>


        </div>
        <!-- /.row -->

        <!-- DASHBOARD CONTENT -->
        <div class="row1">
            <!-- COLUMN 1 -->


    


            <div class="col-md-12">
                <div class="box">

                    <div class="box-body">
                        <div class="row">
                                <div class="col-md-4">
       
                <div class="box box-warning border">
                    <div class="box-header with-border">
                        <h4><i class="fa fa-boolets"></i>Acessos de hoje ao sistema</h4>

                    </div>
                    <div class="box-body">
                        <ul class="unstyled" style="height: 155px;">
                            <asp:Repeater ID="RepeaterAcessos" runat="server">
                                <ItemTemplate>
                                    <li style="margin: 5px; height: 20px;">
                                        <%--<i class="icon-pessoa"></i>--%>
                                        <div style="float: left; width: 80%">
                                            <div style="float: left;"><%# Eval("loginrede")%></div>
                                            <div style="float: right;"><%# String.Format("{0:HH:mm}", Eval("dataacesso"))%></div>

                                        </div>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                    </div>
                </div>

            </div>








                        </div>
                        <!-- ./box-body -->

                    </div>


                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->

        
            <!-- /.row -->

            <!-- /COLUMN 2 -->
        </div>
        <!-- /DASHBOARD CONTENT -->
    </section>
    <!-- /.content -->





</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
   <!-- BLOCK UI -->
    <script type="text/javascript" src="content/js/jQuery-BlockUI/jquery.blockUI.min.js"></script>

    <!-- EASY PIE CHART -->
    <script src="content/js/jquery-easing/jquery.easing.min.js"></script>
    <script type="text/javascript" src="content/js/easypiechart/jquery.easypiechart.min.js"></script>
    <!-- FLOT CHARTS -->
    <script src="content/js/flot/jquery.flot.min.js"></script>
    <script src="content/js/flot/jquery.flot.time.min.js"></script>
    <script src="content/js/flot/jquery.flot.selection.min.js"></script>
    <script src="content/js/flot/jquery.flot.resize.min.js"></script>
    <script src="content/js/flot/jquery.flot.pie.min.js"></script>
    <script src="content/js/flot/jquery.flot.stack.min.js"></script>
    <script src="content/js/flot/jquery.flot.crosshair.min.js"></script>
    <!-- TODO -->
    <script type="text/javascript" src="content/js/jquery-todo/js/paddystodolist.js"></script>
    <!-- TIMEAGO -->
    <script type="text/javascript" src="content/js/timeago/jquery.timeago.min.js"></script>
    <!-- FULL CALENDAR -->
    <script type="text/javascript" src="content/js/fullcalendar/fullcalendar.min.js"></script>

    <!-- GRITTER -->
    <script type="text/javascript" src="content/js/gritter/js/jquery.gritter.min.js"></script>

    <!-- DATE RANGE PICKER -->
    <script src="../content/js/bootstrap-daterangepicker/moment.min.js"></script>
    <script src="../content/js/bootstrap-daterangepicker/daterangepicker.min.js"></script>

    <!-- /JAVASCRIPTS -->
    <!-- ChartJS 1.0.1 -->
    <script src="/plugins/chartjs/Chart.min.js"></script>
    <script>
        $(function () {
            /* ChartJS
             * -------
             * Here we will create a few charts using ChartJS
             */

            //--------------
            //- AREA CHART -
            //--------------

            // Get context with jQuery - using jQuery's .get() method.
            //var areaChartCanvas = $("#areaChart").get(0).getContext("2d");
            // This will get the first returned node in the jQuery collection.
            //var areaChart = new Chart(areaChartCanvas);

            var areaChartData = {
                labels: [<%=sLabels %>],
                datasets: [
                  {
                      label: "Corretor Online",
                      fillColor: "rgba(60,140,180,0.9)",
                      strokeColor: "rgba(219,94,94,1)",
                      pointColor: "#dd4b39",
                      pointStrokeColor: "rgba(60,141,188,1)",
                      pointHighlightFill: "#fff",
                      pointHighlightStroke: "rgba(60,110,100,1)",
                      data: [<%=sAux[0] %>]
                  },
            {
                label: "Ligamos para você",
                fillColor: "rgba(0,166,90,0.9)",
                strokeColor: "rgba(0,166,90,0.9)",
                pointColor: "#00a65a",
                pointStrokeColor: "rgba(0,166,90,0.9)",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(0,166,90,0.9)",
                data: [<%=sAux[1] %>]
            },
            {
                label: "Informações por E-mail",
                fillColor: "rgba(243,156,18,0.9)",
                strokeColor: "rgba(243,156,18,0.9)",
                pointColor: "#f39c12",
                pointStrokeColor: "rgba(243,156,18,0.9)",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(243,156,18,0.9)",
                data: [<%=sAux[2] %>]
            },
            {
                label: "WhatsApp",
                fillColor: "rgba(0,192,239,0.9)",
                strokeColor: "rgba(0,192,239,0.9)",
                pointColor: "#00c0ef",
                pointStrokeColor: "rgba(0,192,239,0.9)",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(0,192,239,0.9)",
                data: [<%=sAux[3] %>]
            }

                ]
            };

            var areaChartOptions = {
                //Boolean - If we should show the scale at all
                showScale: true,
                //Boolean - Whether grid lines are shown across the chart
                scaleShowGridLines: false,
                //String - Colour of the grid lines
                scaleGridLineColor: "rgba(0,0,0,.05)",
                //Number - Width of the grid lines
                scaleGridLineWidth: 1,
                //Boolean - Whether to show horizontal lines (except X axis)
                scaleShowHorizontalLines: true,
                //Boolean - Whether to show vertical lines (except Y axis)
                scaleShowVerticalLines: true,
                //Boolean - Whether the line is curved between points
                bezierCurve: true,
                //Number - Tension of the bezier curve between points
                bezierCurveTension: 0.3,
                //Boolean - Whether to show a dot for each point
                pointDot: false,
                //Number - Radius of each point dot in pixels
                pointDotRadius: 4,
                //Number - Pixel width of point dot stroke
                pointDotStrokeWidth: 1,
                //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
                pointHitDetectionRadius: 20,
                //Boolean - Whether to show a stroke for datasets
                datasetStroke: true,
                //Number - Pixel width of dataset stroke
                datasetStrokeWidth: 2,
                //Boolean - Whether to fill the dataset with a color
                datasetFill: true,
                //String - A legend template
            <%--          legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",--%>
                legendTemplate: "<ul class=\"corretoronline-legend\"><li><span style=\"background-color:blue\"></span>WhatsApp</li><li><span style=\"background-color:red\"></span>Corretor Online</li></ul>",

                //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                maintainAspectRatio: true,
                //Boolean - whether to make the chart responsive to window resizing
                responsive: true
            };



            //-------------
            //- LINE CHART -
            //--------------
            var lineChartCanvas = $("#lineChart").get(0).getContext("2d");
            var lineChart = new Chart(lineChartCanvas);
            var lineChartOptions = areaChartOptions;
            lineChartOptions.datasetFill = false;
            lineChart.Line(areaChartData, lineChartOptions);
		
		
		
            //-------------
            //- PIE CHART -
            //-------------
            // Get context with jQuery - using jQuery's .get() method.
            var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
            var pieChart = new Chart(pieChartCanvas);
            var PieData = [
              {
                  value: <%=sAuxOrigens[0] %>,
                  color: "#f56954",
                  highlight: "#f56954",
                  label: "Google"
              },
    {
        value: <%=sAuxOrigens[1] %>,
        color: "#3c8dbc",
        highlight: "#3c8dbc",
        label: "Facebook"
    },
    {
        value: <%=sAuxOrigens[2] %>,
        color: "#00c0ef",
        highlight: "#00c0ef",
        label: "YouTube"
    },
    {
        value: <%=sAuxOrigens[3] %>,
        color: "#00a65a",
        highlight: "#00a65a",
        label: "Google Adwords"
    },
    {
        value: <%=sAuxOrigens[4] %>,
        color: "#d2d6de",
        highlight: "#d2d6de",
        label: "Android App Google"
    },
    {
        value: <%=sAuxOrigens[5] %>,
        color: "#4a47ed",
        highlight: "#4a47ed",
        label: "Outlook"
    },
    {
        value: <%=sAuxOrigens[6] %>,
        color: "#8382cc",
        highlight: "#8382cc",
        label: "Bing"
    },
    {
        value: <%=sAuxOrigens[7] %>,
        color: "#9382cc",
        highlight: "#9382cc",
        label: "EmailMarketing"
    }



            ];
            var pieOptions = {
                //Boolean - Whether we should show a stroke on each segment
                segmentShowStroke: true,
                //String - The colour of each segment stroke
                segmentStrokeColor: "#fff",
                //Number - The width of each segment stroke
                segmentStrokeWidth: 1,
                //Number - The percentage of the chart that we cut out of the middle
                percentageInnerCutout: 50, // This is 0 for Pie charts
                //Number - Amount of animation steps
                animationSteps: 100,
                //String - Animation easing effect
                animationEasing: "easeOutBounce",
                //Boolean - Whether we animate the rotation of the Doughnut
                animateRotate: true,
                //Boolean - Whether we animate scaling the Doughnut from the centre
                animateScale: false,
                //Boolean - whether to make the chart responsive to window resizing
                responsive: true,
                // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                maintainAspectRatio: false,
                //String - A legend template
            };
            //Create pie or douhnut chart
            // You can switch between pie and douhnut using the method below.
            pieChart.Doughnut(PieData, pieOptions);
            //-----------------
            //- END PIE CHART -
            //-----------------
            //-------------
            //- BAR CHART -
            //-------------
            /*
          var barChartCanvas = $("#barChart").get(0).getContext("2d");
          var barChart = new Chart(barChartCanvas);
          var barChartData = areaChartData;
          barChartData.datasets[1].fillColor = "#00a65a";
          barChartData.datasets[1].strokeColor = "#00a65a";
          barChartData.datasets[1].pointColor = "#00a65a";
          var barChartOptions = {
              //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
              scaleBeginAtZero: true,
              //Boolean - Whether grid lines are shown across the chart
              scaleShowGridLines: true,
              //String - Colour of the grid lines
              scaleGridLineColor: "rgba(0,0,0,.05)",
              //Number - Width of the grid lines
              scaleGridLineWidth: 1,
              //Boolean - Whether to show horizontal lines (except X axis)
              scaleShowHorizontalLines: true,
              //Boolean - Whether to show vertical lines (except Y axis)
              scaleShowVerticalLines: true,
              //Boolean - If there is a stroke on each bar
              barShowStroke: true,
              //Number - Pixel width of the bar stroke
              barStrokeWidth: 2,
              //Number - Spacing between each of the X value sets
              barValueSpacing: 5,
              //Number - Spacing between data sets within X values
              barDatasetSpacing: 1,

              //Boolean - whether to make the chart responsive
              responsive: true,
              maintainAspectRatio: true
          };

          barChartOptions.datasetFill = false;
          barChart.Bar(barChartData, barChartOptions);
          */

            $('#reportrange').daterangepicker(
                {
                    startDate: moment().subtract('days', 29),
                    endDate: moment(),
                    minDate: '01/01/2016',
                    maxDate: '31/12/2020',
                    dateLimit: { days: 60 },
                    showDropdowns: true,
                    showWeekNumbers: true,
                    timePicker: false,
                    timePickerIncrement: 1,
                    timePicker12Hour: true,
                    ranges: {
                        'Hoje': [moment().subtract('days', 1), moment().subtract('days', 1)],
                        'Últimos 30 dias': [moment().subtract('days', 29), moment()],
                        'Este mês': [moment().startOf('month'), moment().endOf('month')]
                    },
                    opens: 'left',
                    buttonClasses: ['btn btn-default'],
                    applyClass: 'btn-small btn-primary',
                    cancelClass: 'btn-small',
                    format: 'DD/MM/YYYY',
                    separator: ' para ',
                    locale: {
                        applyLabel: 'Confirma',
                        fromLabel: 'De',
                        toLabel: 'Para',
                        customRangeLabel: 'Custom Range',
                        daysOfWeek: ['Do', 'Se', 'Te', 'Qa', 'Qi', 'Se','Sa'],
                        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                        firstDay: 1
                    }
                },
                function(start, end) {
                    console.log("Callback has been called!");
                    $('#reportrange span').html(start.format('D MMMM, YYYY') + ' - ' + end.format('D MMMM, YYYY'));
                }
             );
            //Set the initial state of the picker label
            $('#reportrange span').html('Custom');
 
        });
    </script>


</asp:Content>
