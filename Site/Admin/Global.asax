﻿<%@ Application Language="C#" %>

<%@ Import Namespace="FrameWork" %>
<%@ Import Namespace="FrameWork.DataObject" %>
<%@ Import Namespace="FrameWork.WebControls" %>
<%@ Import Namespace="System.Web.Optimization" %>
<%@ Import Namespace="System.Web.Routing" %>


<script runat="server">
    public static bool lErroActive = false;
    void Application_Start(object sender, EventArgs e) 
    {
        // Code that runs on application startup
        //ClassLibrary.Geral.Start(this);
        Application["ContadorAcessos"] = 0;


        string cSQL = System.Configuration.ConfigurationManager.AppSettings["Conexao"];
        FrameWork.DataObject.DataSource ds = new FrameWork.DataObject.DataSource(cSQL);
        FrameWork.DataObject.DataSourceObject.DefaulDataSource = ds;

       tripguard.RouteConfig.RegisterRoutes(RouteTable.Routes);


        //'================================ PARA START DAS TAREFAS AUTOMÁTICAS =====================================================
        //Dim strpara As String = "alexandre@homebr.com.br"
        //strpara = "alexandre@homebr.com.br"

        //Dim dt As DateTime = Now.Date.AddHours(14).AddMinutes(1)
        //Dim dt1 As DateTime = Now.Date.AddHours(9).AddMinutes(30)
        //Dim dt2 As DateTime = Now.Date.AddHours(20).AddMinutes(1)
        //Dim dt3 As DateTime = Now.Date.AddHours(20).AddMinutes(30)

        //If dt < Now Then
        //    FrameWork.TaskRun.Run("GeraXMLAniversariantes", dt1, AddressOf TesteRun, "GeraXMLAniversariantes")
        //ElseIf dt > Now Then
        //    FrameWork.TaskRun.Run("Termometro2", dt, AddressOf TesteRun, strpara)
        //ElseIf dt2 > Now Then
        //    FrameWork.TaskRun.Run("Termometro3", dt2, AddressOf TesteRun, strpara)
        //    FrameWork.TaskRun.Run("AtualizaPrecoUnidades", dt3, AddressOf TesteRun, "AtualizaPrecoUnidades")
        //    FrameWork.TaskRun.Run("GeraXMLColaboradoresSite", dt3, AddressOf TesteRun, "GeraXMLColaboradoresSite")
        //End If


        //Dim ta As New ClassLibrary.TarefasAutomaticas

        //Dim TIMEOUT As Integer = 10 * 60 * 1000
        //'10 min, tem 60 seg, 1 seg tem 1000 miles. 
        //'logo, 10 min tem 600.000 milesimos
        //Dim d As New Date

        //While ((New System.DateTime.Now - d.) < TIMEOUT)
        //    ta.KillProcessos()
        //End While
    }

    public void Application_BeginRequest(object sender, EventArgs e)
    {
        Geral.TotalRequest += 1;
    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown

    }
        
    void Application_Error(object sender, EventArgs e) 
    { 
        // Code that runs when an unhandled error occurs
        if ((this.Request != null) && this.Request.IsLocal)
        {

            try
            {
                //Controla possivel recursividade!
                if (lErroActive)
                {
                    return;
                }
                lErroActive = true;

                FrameWork.Erro.SendTitle = "[ERRO: Site] - " + Request.ServerVariables["REMOTE_ADDR"] + " - " + Request.UrlReferrer.ToString();
                FrameWork.Erro.SendApplicationError(this);

            }
            catch (System.Threading.ThreadAbortException ex)
            {
                //ex = (System.Threading.ThreadAbortException)new Exception("Erro de Thread em Application_Error()", ex);
                FrameWork.Erro.SendApplicationError(ex);
            }
            catch (Exception ex)
            {
                //ex = new Exception("Erro Interno em Application_Error()", ex);
                FrameWork.Erro.SendApplicationError(ex);
            }

            //lErroActive = False


        }
    }

    void Session_Start(object sender, EventArgs e) 
    {
        // Code that runs when a new session is started
        Application["ContadorAcessos"] = FrameWork.Util.GetInt(Application["ContadorAcessos"]) - 1;
    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }
       
</script>
