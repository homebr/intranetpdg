﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Login : System.Web.UI.Page
{
    String strCampo = "ctl00$ContentPlaceHolder1$";
    public String sBox = "";

    protected void Page_Load(object sender, System.EventArgs e)
    {

        if (!IsPostBack)
        {
            //if (FrameWork.Util.GetString(Session["_uiReferer"]).Length > 0)
            //    hUrl.Value = FrameWork.Util.GetString(Session["_uiReferer"]);
            //else
            //    hUrl.Value = FrameWork.Util.GetString(Request.UrlReferrer);

            //Session.Abandon();

        }
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {

        lblMensagem.Text = "";
        if (string.IsNullOrEmpty(Request.Form["usuario"]))
        {
            Msg("Favor preencher o campo Usuário!", false);
            return;
        }
        if (string.IsNullOrEmpty(Request.Form["senha"]))
        {
            Msg("Favor preencher o campo Senha!", false);
            return;
        }

        if (!string.IsNullOrEmpty(Request.Form["usuario"]))
        {
            string strUsuario = FrameWork.Util.GetString(Request.Form["usuario"]).Trim();
            string strSenha = FrameWork.Util.GetString(Request.Form["senha"]).Trim();
            if (FrameWork.Util.GetString(strUsuario) == "")
            {
                Msg("Favor preencher o campo Email!", false);
                return;
            }
            if (FrameWork.Util.GetString(strSenha) == "")
            {
                Msg("Favor preencher o campo Senha!", false);
                return;
            }

            ClassLibrary.Colaboradores cli = new ClassLibrary.Colaboradores();
            cli.LoadWhere("loginrede=@user AND ativo = 1 and adm=1 ", new FrameWork.DataObject.ParameterItem("@user", strUsuario)); //

           // Response.Write(cli.TotalLoad);
           // Response.End();

            try
            {
                if (cli.TotalLoad == 1)
                {

                    //Checa senha
                    if (strSenha.Length == 0)
                    {

                        Msg("Favor inserir a senha!", false);
                        return;
                    }


                    if (strSenha != FrameWork.Security.DeCript(cli.Senha)) //FrameWork.WebControls.WebSecurity.DeCript
                    {
                        Msg("Login ou Senha inválida!", false);
                        return;
                    }

                    Session["_uiUserID"] = cli.ID;
                    Session["_uiUser"] = cli.LoginRede;
                    Session["_uiNome"] = cli.Nome;
                    Session["_uiEmail"] = cli.Email;
                    //Session["_uiGruip"] = cli.Grupo;
                    Session["_uiRights"] = cli.ID;
                    ////if (!cli.Admin)
                    //  Session["_uiRights"] = FrameWork.Util.GetString(FrameWork.DataObject.DataSource.DefaulDataSource.Execute("SELECT Acessos FROM GrupoUsuarios WHERE IDGrupo=" + cli.Grupo));

                    //cli.UltimoAcesso = DateTime.Now;
                    //cli.SessionID = this.Page.Session.SessionID;
                    // cli.Save(); Session["_uiUserID"] = 1;
                    // Session["_uiUser"] = cli.loginrede;
                    // Session["_uiNome"] = cli.Nome;
                    // Session["_uiEmail"] = cli.Email;
                    // Session["_uiAdmin"] = cli.Admin;
                    // Session["_uiFoto"] = cli.Foto;
                    //if (!cli.Admin)
                    //    Session["_uiRights"] = FrameWork.Util.GetString(FrameWork.DataObject.DataSource.DefaulDataSource.Execute("SELECT Acessos FROM GrupoUsuarios WHERE IDGrupo=" + cli.Grupo));

                    // cli.UltimoAcesso = DateTime.Now;
                    // cli.SessionID = this.Page.Session.SessionID;
                    cli.Save();
                    //Response.Write("1");
                    //Response.End();

                    // Grava o cookie se foi marcado para lembrar
                    if (FrameWork.Util.GetString(Request.Form["chkConectado"]) == "on")
                    {
                        HttpCookie cookie = new HttpCookie("_PDG_intranetAdmin_login", cli.LoginRede);
                        cookie.Expires = DateTime.Now.AddMonths(6);
                        Response.Cookies.Add(cookie);
                    }

                    //Response.Write("2");
                    //Response.End();
                    //redireciona
                    //if (FrameWork.Util.GetString(hUrl.Value).Length > 1)
                    //    Response.Redirect(hUrl.Value, true);
                    //else
                    Response.Redirect("/admin/default", true);


                    ////Log de Acesso
                    //String strSql = "insert into login (DataAcesso,QtdAcesso,IP,Usuario) ";
                    //strSql += "values (getdate(),";
                    //strSql += FrameWork.Util.GetInt(Session["Acesso"]) + ",'";
                    //strSql += FrameWork.Util.GetString(Request.ServerVariables["REMOTE_ADDR"]) + "','";
                    //strSql += FrameWork.Util.GetString(Session["_uiUser"]) + "')";
                    //FrameWork.DataObject.DataSource.DefaulDataSource.Execute(strSql);

                    

                }
                else
                {
                    Msg("Login ou Senha inválida!", false);
                    //Response.Write("3");
                    //Response.End();
                }
            }
            catch
            {
            }
        }
        else
        {
            Session.Abandon();
            Session.RemoveAll();
            Session.Clear();
        }
    }

    protected void btnLembrete_Click(object sender, EventArgs e)
    {
        lblMensagem.Text = "";
        sBox = "lembrete";
        if (string.IsNullOrEmpty(Request.Form["email"]))
        {
            Msg("Favor preencher o campo Email!", false);
            return;
        }


        if (!string.IsNullOrEmpty(Request.Form["email"]))
        {
            string strEmail = FrameWork.Util.GetString(Request.Form["email"]).Trim();
            if (FrameWork.Util.GetString(strEmail) == "")
            {
                Msg("Favor preencher o campo Email!", false);
                return;
            }

            ClassLibrary.Usuarios cli = new ClassLibrary.Usuarios();
            cli.LoadWhere("email=@user AND SituacaoLogin = 'A' and grupo in  (0,5)  ", new FrameWork.DataObject.ParameterItem("@user", strEmail)); //

            try
            {
                if (cli.TotalLoad == 1)
                {

                    string strDestinatarios = "";
                    string strAssunto = "";
                    string FilePath = "";
                    System.IO.StreamReader objStreamReader;
                    string strArq;
                    String strResultadoEnvio = "";

                    strDestinatarios = cli.Email;
                    strAssunto = "Esqueci a Senha";

                    FilePath = Server.MapPath("template/ModeloEsqueciSenha.htm");
                    objStreamReader = System.IO.File.OpenText(FilePath);
                    strArq = objStreamReader.ReadToEnd();

                    strArq = strArq.Replace("#Nome", cli.Nome.Substring(0));
                    strArq = strArq.Replace("#Senha", (cli.Senha));
                    strArq = strArq.Replace("#Email", cli.Email);
                    strArq = strArq.Replace("#IP", Request.ServerVariables["REMOTE_ADDR"]);
                    strArq = strArq.Replace("#DATA", String.Format("{0:d}", DateTime.Now));
                    string pathVirtual = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "template_email/";
                    strArq = strArq.Replace("src=\"img/", "src=\"" + pathVirtual + "/img/");

                    strResultadoEnvio = FrameWork.Util.GetString(Funcoes.EnvioEmail(strArq, "naoresponda@homebrasil.com", strAssunto, strDestinatarios, "oliveira@homebrasil.com", ""));

                    Msg("Foi enviado ao email cadastrado a senha atual!", true);

                    objStreamReader.Dispose();
                    objStreamReader.Close();

                    return;

                }
                else
                {
                    Msg("E-mail inválido!", false);

                }
            }
            catch
            {
            }
        }
        else
        {
            Session.Abandon();
            Session.RemoveAll();
            Session.Clear();
        }
    }

   protected void Msg(String msg, Boolean tipo)
   {
      lblMensagem.CssClass = (tipo ? "box-danger alert alert-success" : "box-danger alert alert-danger");
        lblMensagem.Text = msg;
        lblMensagem.Visible = true;
   }

   // protected void Msg(String msg, Boolean tipo)
    //{
    //    lblmsg.CssClass = (tipo ? "box-danger alert alert-success" : "box-danger alert alert-danger");
    //    lblmsg.Text = msg;
  //      lblmsg.Visible = true;
   // }
}