﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Configuracoes.aspx.cs" Inherits="Ferramentas_Configuracoes" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="head">

    <link href="/content/css/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <!-- JQUERY UI-->
    <link rel="stylesheet" type="text/css" href="/content/js/jquery-ui-1.10.3.custom/css/custom-theme/jquery-ui-1.10.3.custom.min.css" />

    <!-- DATE RANGE PICKER -->
    <link rel="stylesheet" type="text/css" href="/content/js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
    <!-- ANIMATE -->
    <link rel="stylesheet" type="text/css" href="/content/css/animatecss/animate.min.css" />
    <!-- COLORBOX -->
    <link rel="stylesheet" type="text/css" href="/content/js/colorbox/colorbox.min.css" />

    <!-- BOOTSTRAP SWITCH -->
    <link rel="stylesheet" type="text/css" href="/content/js/bootstrap-switch/bootstrap-switch.min.css" />
    <link rel="stylesheet" type="text/css" href="/content/css/themes/default.css" id="skin-switcher">

    <!-- JQUERY UI-->
    <link rel="stylesheet" type="text/css" href="/content/js/jquery-ui-1.10.3.custom/css/custom-theme/jquery-ui-1.10.3.custom.min.css" />



</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <!--Page Title-->
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <div id="page-title">
            <h1 class="page-header text-overflow">Configurações</h1>

        </div>
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <!--End page title-->

    <!--Breadcrumb-->
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li class="active">Configurações</li>
            <li>Editar</li>
        </ol>
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <!--End breadcrumb-->


    <!-- Main content -->
    <div id="page-content">
        <div class="panel">
                <div class="panel-body">
        <div class="row">

            <div class="col-md-12">


                <!-- Empresa -->
                <div class="box box-warning collapsed-box">
                    <form id="form1" method="post">
                        <div class="box-header with-border">
                            <h3 class="box-title">Empresa</h3>
                        <br />
                        <small>Informação em cache no Site, sua atualização pode demorar 24 horas ou ser necessário reiniciar a aplicação.<br />
                        </small>

                            <div class="box-tools pull-right">
                                <button class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label for="ContentPlaceHolder1_Nome">Nome:</label>
                                    <input type="text" id="txtEmpresa_Nome" name="txtEmpresa_Nome" maxlength="150" class="form-control" placeholder="Nome de exibição" value='<%=objEMPRESA.nome %>' />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label for="ContentPlaceHolder1_Nome">Telefone - Capitais e regiões metropolitanas:</label>
                                    <input type="text" id="txtEmpresa_Telefone" name="txtEmpresa_Telefone" maxlength="50" class="form-control" placeholder="Telefone - Capitais e regiões metropolitanas" value="<%=objEMPRESA.telefone %>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label for="ContentPlaceHolder1_Nome">Telefone - Demais localidades</label>
                                    <input type="text" id="txtEmpresa_Telefone2" name="txtEmpresa_Telefone2" maxlength="50" class="form-control" placeholder="Telefone - Demais localidades" value="<%=objEMPRESA.telefone2 %>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label for="ContentPlaceHolder1_Nome">Endereço:</label>
                                    <input type="text" id="txtEmpresa_Endereco" name="txtEmpresa_Telefone" maxlength="300" class="form-control" placeholder="endereço da matriz" value="<%=objEMPRESA.endereco %>" />
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="col-sm-4">
                                <button type="submit" class="btn btn-success bt-save-empresa">SALVAR CONFIGURAÇÃO</button>
                            </div>

                            <div class="col-sm-4">
                                <div class="alert-empresa"></div>
                            </div>
                            <div class="col-sm-4">
                                <small><asp:HyperLink ID="lnkAtualizacao" ForeColor="Red" runat="server">Forçar atualização no Site</asp:HyperLink></small>
                            </div>
                        </div>
                    </form>
                </div>


                <!-- Redes Sociais -->
                <div class="box box-primary collapsed-box">
                    <form id="form2" method="post">
                        <div class="box-header with-border">
                            <h3 class="box-title">Redes Sociais</h3><br />
                            <small>Informação em cache no Site, sua atualização pode demorar 24 horas ou ser necessário reiniciar a aplicação.<br />
                        </small>
                            <div class="box-tools pull-right">
                                <button class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label for="ContentPlaceHolder1_Nome">Facebook:</label>
                                    <input id="txtFacebook" class="form-control" maxlength="150" placeholder="url do facebook" value="<%=objREDES.facebook %>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label for="ContentPlaceHolder1_Nome">Youtube:</label>
                                    <input id="txtYoutube" class="form-control" maxlength="150" placeholder="url do Youtube" value="<%=objREDES.youtube %>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label for="ContentPlaceHolder1_Nome">Instagram:</label>
                                    <input id="txtInstagram" class="form-control" maxlength="150" placeholder="url do Instagram" value="<%=objREDES.instagram %>" />
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="col-sm-4">
                                <button type="submit" class="btn btn-success bt-save-redes">SALVAR CONFIGURAÇÃO</button>
                            </div>
                            <div class="col-sm-4">
                                <div class="alert-redes"></div>
                            </div>
                        </div>
                    </form>
                </div>

                <!-- SMTP E-MAIL -->
                <div class="box box-primary collapsed-box">
                    <form id="form3" method="post">
                        <div class="box-header with-border">
                            <h3 class="box-title">SMTP E-mail</h3>
                            <div class="box-tools pull-right">
                                <button class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label>Servidor:<span class="required">*</span></label>
                                    <input id="txtSMTP_Servidor" class="form-control" required="required" maxlength="150" placeholder="host da conta smtp" value="<%=objSMTP.servidor %>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label>Usuário:<span class="required">*</span></label>
                                    <input id="txtSMTP_Usuario" class="form-control" required="required" maxlength="50" placeholder="login de autenticação" value="<%=objSMTP.usuario  %>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label>Senha:</label>
                                    <input id="txtSMTP_Senha" class="form-control" required="required" maxlength="50" style="width: 350px" placeholder="" value="<%=objSMTP.senha %>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label>Número da porta:</label>
                                    <input id="txtSMTP_Porta" class="form-control" required="required" maxlength="5" style="width: 100px" placeholder="ex: 465 ou 587" value="<%=objSMTP.porta %>" />

                                </div>

                            </div>
                            <div class="form-group trLink">
                                <div class="col-sm-12">
                                    <label>Enable Ssl:</label>
                                    <div>
                                        <div class="make-switch switch-small" data-on="success" data-off="danger" data-on-label="<i class='fa fa-check icon-white'></i>" data-off-label="<i class='fa fa-times'></i>">
                                            <input type="checkbox" id="chkSMTP_Ativo" <%=(objSMTP.ssl=="on"?"checked":"") %> />
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="form-group trScript">
                                <div class="col-sm-12">
                                    <label>E-mail do Remetente:</label>
                                    <input id="txtSMTP_Remetente" class="form-control" required="required" maxlength="250" placeholder="" value="<%=objSMTP.remetente %>" />
                                </div>
                            </div>


                        </div>
                        <div class="box-footer">
                            <div class="col-sm-4">
                                <button type="submit" class="btn btn-success bt-save-smtp">SALVAR CONFIGURAÇÃO</button>
                            </div>
                            <div class="col-sm-4">
                                <div class="alert-smtp"></div>
                            </div>

                        </div>
                    </form>
                </div>

                <!-- E-MAIL DESTINATÁRIOS -->
                <div class="box box-primary collapsed-box">
                    <form id="form4" method="post">
                        <div class="box-header with-border">
                            <h4 class="box-title">E-mail Destinatários</h4>
                            <div class="box-tools pull-right">
                                <button class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>

                        <div class="box-body">


                            <h4>Fale Conosco</h4>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label>E-mail do destinatário (Para):<span class="required">*</span></label>
                                    <input id="txtFaleConosco_Para" class="form-control" maxlength="250" placeholder="e-mail do destinatários para, coloque mais de um separado por virgula" value="<%=objDEST_faleconosco.para %>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label>E-mail da cópia (Cc):</label>
                                    <input id="txtFaleConosco_Cc" class="form-control" maxlength="250" placeholder="e-mail do destinatários com cópia, coloque mais de um separado por virgula" value="<%=objDEST_faleconosco.cc %>" />
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="separator"></div>
                            </div>



                        </div>
                        <div class="box-footer">
                            <div class="col-sm-4">
                                <button type="submit" class="btn btn-success bt-save-destinatarios">SALVAR CONFIGURAÇÃO</button>
                            </div>
                            <div class="col-sm-4">
                                <div class="alert-destinatarios"></div>
                            </div>
                        </div>
                    </form>
                </div>


                <!-- Scripts Google Analytics, Bing -->
                <div class="box box-primary collapsed-box">
                    <form id="form6" method="post">
                        <div class="box-header with-border">
                            <h3 class="box-title">Google Analytics</h3>
                            <br />
                            <small>Informação em cache no Site, sua atualização pode demorar 24 horas ou ser necessário reiniciar a aplicação.<br />
                        </small>
                            <div class="box-tools pull-right">
                                <button class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group trScript">
                                <div class="col-sm-12">
                                    <label for="ContentPlaceHolder1_Script">Código analytics:</label>
                                    <input id="txtScript" type="text" class="form-control" value="<%=objGOOGLE.script %>" />

                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="col-sm-4">
                                <button type="submit" class="btn btn-success bt-save-google">SALVAR CONFIGURAÇÃO</button>
                            </div>
                            <div class="col-sm-4">
                                <div class="alert-google"></div>
                            </div>
                        </div>
                    </form>
                </div>


            </div>

            <div class="footer-tools">
                <span class="go-top">
                    <i class="fa fa-chevron-up"></i>Top
                </span>
            </div>

        </div>
                    </div>
            </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="footer">

    <!-- BLOCK UI -->
    <script type="text/javascript" src="/content/js/jQuery-BlockUI/jquery.blockUI.min.js"></script>
    <!-- BOOTSTRAP SWITCH -->
    <script type="text/javascript" src="/content/js/bootstrap-switch/bootstrap-switch.min.js"></script>
    <!-- ISOTOPE -->
    <script type="text/javascript" src="/content/js/isotope/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="/content/js/isotope/imagesloaded.pkgd.min.js"></script>
    <!-- COLORBOX -->
    <script type="text/javascript" src="/content/js/colorbox/jquery.colorbox.min.js"></script>


    <script type="text/jscript" src="/content/js/jquery.maskedinput-1.3.1.js"></script>

    <!-- Script Json -->
    <script type="text/jscript" src="/content/js/script-configuracoes.js"></script>


    <script type="text/jscript">

        var campo = "ContentPlaceHolder1_";

        $(function () {

            return false;
        });


    </script>

</asp:Content>



