﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Reflection;
using FrameWork.WebControls;
using FrameWork.DataObject;
using ClassLibrary;
using System.Text.RegularExpressions;

public partial class Ferramentas_GruposEditar : System.Web.UI.Page
{


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            //if (FrameWork.Util.GetString(Session["_uiGrup"]) != "AD" & !ClassLibrary.Funcoes.VeCheckUserRight(7, "O"))
            //{
            //    ClassLibrary.Funcoes.SkinMessage("Sem Acesso", Page);
            //    return;
            //}

            //if (FrameWork.Util.GetString(Session["_uiGrup"]) == "AD")
            //{
            //    trEmpresa.Visible = true;
            //    CarregaEmpresas();
            //}
            CarregaGrupos();

        }

        Literal1.Text = "";
    }

    public void CarregaGrupos()
    {
        ClassLibrary.GrupoUsuarios grupos = new ClassLibrary.GrupoUsuarios();
        hID.Value = FrameWork.Util.GetString(Request["id"]);

        if (FrameWork.Util.GetInt(hID.Value) > 0)
        {

            grupos.Load(hID.Value);

            //Response.Write(grupos.TotalLoad);

            if (grupos.TotalLoad == 1)
            {
                txtGrupo.Text = grupos.Grupo;


                if ((ddlSituacao.Items.FindByValue(grupos.Situacao) != null))
                {
                    ddlSituacao.SelectedValue = grupos.Situacao;
                }


                lblDataInclusao.Text = string.Format("{0:dd/MM/yyyy}", grupos.DataInclusao);
                lblDataAlteracao.Text = string.Format("{0:dd/MM/yyyy}", grupos.DataAlteracao);
                lblUsuario.Text = string.Format("{0:dd/MM/yyyy}", grupos.Usuario);

                grupos.LoadDireitos(ltl);

            }

        }
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {

        if (FrameWork.Util.GetString(txtGrupo.Text) == "")
        {
            txtGrupo.Focus();
            Msg("Favor preencher o Login Acesso!",false);
            return;
        }



        ClassLibrary.GrupoUsuarios objUsuarios = new ClassLibrary.GrupoUsuarios();
        if (FrameWork.Util.GetInt(hID.Value) > 0)
        {
            objUsuarios.Load(hID.Value);
            hOperacao.Value = "UPDATE";
            objUsuarios.DataAlteracao = DateTime.Now;
        }
        else
        {
            hOperacao.Value = "INSERT";
            objUsuarios.Grupo = txtGrupo.Text;
            objUsuarios.DataInclusao = DateTime.Now;
        }
     
        objUsuarios.Situacao = ddlSituacao.SelectedValue;
        objUsuarios.Usuario = FrameWork.Util.GetString(Session["_uiUser"]);

        String strDireitos = "";

        //direitos
        //ctl00$ContentPlaceHolder1$ClassRight_9_E
        //ctl00$ContentPlaceHolder1$ClassRight:9:E}{ctl00$ContentPlaceHolder1$ClassRight:5:E}{ctl00$ContentPlaceHolder1$ClassRight:11:E}{ctl00$ContentPlaceHolder1$ClassRight:7:E}{ctl00$ContentPlaceHolder1$ClassRight:10:E}{ctl00$ContentPlaceHolder1$ClassRight:16:E}{ctl00$ContentPlaceHolder1$ClassRight:17:E
        foreach (string key in Request.Form)
        {
            if (key.StartsWith("ctl00$ContentPlaceHolder1$ClassRight_"))
            {
                String[] arrAux = key.Split('_');
                if (strDireitos.Contains("{" + arrAux[1] + ":"))
                {
                    int pos = strDireitos.IndexOf("{" + arrAux[1] + ":");
                    if (pos > -1)
                    {
                        String str = strDireitos.Insert(pos+("{" + arrAux[1] + ":").Length, arrAux[2] + ",");

                        //String strAux2 = strDireitos.Substring(0,pos) + arrAux[1] + "," + strDireitos.Substring(pos+1);
                        strDireitos = str;
                    }
                }
                else
                    strDireitos += "{" + arrAux[1] + ":" + arrAux[2] + "}";
            }
        }
        //Response.Write(strDireitos);
        objUsuarios.Acessos = strDireitos;
       objUsuarios.Save();

        hID.Value = FrameWork.Util.GetString(objUsuarios.IDGrupo);
        Msg("Salvo com sucesso!", true);

       Response.Redirect("GruposListar");
    }

    protected void Msg(String msg, Boolean tipo)
    {
        lblMensagem.CssClass = (tipo ? "box-danger alert alert-success" : "box-danger alert alert-danger");
        lblMensagem.Text = msg;
        lblMensagem.Visible = true;
    }
}
