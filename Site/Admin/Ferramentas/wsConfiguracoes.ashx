﻿<%@ WebHandler Language="C#" Class="wsConfiguracoes" %>

using System;
using System.Web;
using FrameWork.WebControls;
using FrameWork.DataObject;
using ClassLibrary;
using System.Web.SessionState;
using System.Web.Script.Serialization;

public class wsConfiguracoes : IHttpHandler, IReadOnlySessionState {

    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "application/json"; //"text/plain";

        string json = "";

        string modulo = FrameWork.Util.GetString(context.Request["modulo"]);

        ClassLibrary.Configuracoes objConfig = new ClassLibrary.Configuracoes();
        JavaScriptSerializer js = new JavaScriptSerializer();


        if (modulo == "empresa")
        {
            var f = new
            {
                nome = FrameWork.Util.GetString(context.Request["nome"]),
                telefone = FrameWork.Util.GetString(context.Request["telefone"]),
                endereco = FrameWork.Util.GetString(context.Request["endereco"]),
            };

            objConfig.LoadWhere("Nome='Empresa'");
            if (objConfig.TotalLoad==1)
                objConfig.DataAlteracao = DateTime.Now;
            else
                objConfig.DataInclusao = DateTime.Now;

            objConfig.Usuario = FrameWork.Util.GetString(context.Session["_uiUser"]);
            objConfig.Nome = "Empresa";
            objConfig.Valor = js.Serialize(f);
            objConfig.Save();

            json = js.Serialize(f);

            json = "{ \"response\": \"ok\",\"message\": \"Salvo com sucesso!\" }";
        }
        else if (modulo == "redes")
        {
            var f = new
            {
                facebook = FrameWork.Util.GetString(context.Request["facebook"]),
                youtube = FrameWork.Util.GetString(context.Request["youtube"]),
                instagram = FrameWork.Util.GetString(context.Request["instagram"]),
            };

            objConfig.LoadWhere("Nome='Redes'");
            if (objConfig.TotalLoad==1)
                objConfig.DataAlteracao = DateTime.Now;
            else
                objConfig.DataInclusao = DateTime.Now;

            objConfig.Usuario = FrameWork.Util.GetString(context.Session["_uiUser"]);
            objConfig.Nome = "Redes";
            objConfig.Valor = js.Serialize(f);
            objConfig.Save();

            json = js.Serialize(f);

            json = "{ \"response\": \"ok\",\"message\": \"Salvo com sucesso!\" }";
        }
        else if (modulo == "smtp")
        {
            var f = new
            {
                servidor = FrameWork.Util.GetString(context.Request["servidor"]),
                usuario = FrameWork.Util.GetString(context.Request["usuario"]),
                senha = FrameWork.Util.GetString(context.Request["senha"]),
                porta = FrameWork.Util.GetString(context.Request["porta"]),
                ssl = FrameWork.Util.GetString(context.Request["ssl"]),
                remetente = FrameWork.Util.GetString(context.Request["remetente"]),
            };
          
            
            objConfig.LoadWhere("Nome='SMTP'");
            if (objConfig.TotalLoad == 1)
                objConfig.DataAlteracao = DateTime.Now;
            else
                objConfig.DataInclusao = DateTime.Now;

            objConfig.Usuario = FrameWork.Util.GetString(context.Session["_uiUser"]);
            objConfig.Nome = "SMTP";
            objConfig.Valor = js.Serialize(f);
            objConfig.Save();

            json = js.Serialize(f);

            json = "{ \"response\": \"ok\",\"message\": \"Salvo com sucesso!\" }";
        }
        else if (modulo == "destinatarios")
        {
            var f = new
            {
                para = FrameWork.Util.GetString(context.Request["faleconosco_para"]),
                cc = FrameWork.Util.GetString(context.Request["faleconosco_cc"]),
            };
            objConfig.LoadWhere("Nome='Destinatarios_FaleConosco'");
            if (objConfig.TotalLoad == 1)
                objConfig.DataAlteracao = DateTime.Now;
            else
                objConfig.DataInclusao = DateTime.Now;
            objConfig.Usuario = FrameWork.Util.GetString(context.Session["_uiUser"]);
            objConfig.Nome = "Destinatarios_FaleConosco";
            objConfig.Valor = js.Serialize(f);
            objConfig.Save();
            
            var f2 = new
            {
                para = FrameWork.Util.GetString(context.Request["ligamos_para"]),
                cc = FrameWork.Util.GetString(context.Request["ligamos_cc"]),
            };
            objConfig.LoadWhere("Nome='Destinatarios_Ligamos'");
            if (objConfig.TotalLoad == 1)
                objConfig.DataAlteracao = DateTime.Now;
            else
                objConfig.DataInclusao = DateTime.Now;
            objConfig.Usuario = FrameWork.Util.GetString(context.Session["_uiUser"]);
            objConfig.Nome = "Destinatarios_Ligamos";
            objConfig.Valor = js.Serialize(f2);
            objConfig.Save();
            
            var f3 = new
            {
                para = FrameWork.Util.GetString(context.Request["vizinho_para"]),
                cc = FrameWork.Util.GetString(context.Request["vizinho_cc"]),
            };
            objConfig.LoadWhere("Nome='Destinatarios_SouVizinhoObra'");
            if (objConfig.TotalLoad == 1)
                objConfig.DataAlteracao = DateTime.Now;
            else
                objConfig.DataInclusao = DateTime.Now;
            objConfig.Usuario = FrameWork.Util.GetString(context.Session["_uiUser"]);
            objConfig.Nome = "Destinatarios_SouVizinhoObra";
            objConfig.Valor = js.Serialize(f3);
            objConfig.Save();
            
            var f4 = new
            {
                para = FrameWork.Util.GetString(context.Request["whatsapp_para"]),
                cc = FrameWork.Util.GetString(context.Request["whatsapp_cc"]),
            };
            objConfig.LoadWhere("Nome='Destinatarios_WhatsApp'");
            if (objConfig.TotalLoad == 1)
                objConfig.DataAlteracao = DateTime.Now;
            else
                objConfig.DataInclusao = DateTime.Now;
            objConfig.Usuario = FrameWork.Util.GetString(context.Session["_uiUser"]);
            objConfig.Nome = "Destinatarios_WhatsApp";
            objConfig.Valor = js.Serialize(f4);
            objConfig.Save();

            var f8 = new
            {
                para = FrameWork.Util.GetString(context.Request["informacoesporemail_para"]),
                cc = FrameWork.Util.GetString(context.Request["informacoesporemail_cc"]),
            };
            objConfig.LoadWhere("Nome='Destinatarios_InformacoesporEmail'");
            if (objConfig.TotalLoad == 1)
                objConfig.DataAlteracao = DateTime.Now;
            else
                objConfig.DataInclusao = DateTime.Now;
            objConfig.Usuario = FrameWork.Util.GetString(context.Session["_uiUser"]);
            objConfig.Nome = "Destinatarios_InformacoesporEmail";
            objConfig.Valor = js.Serialize(f8);
            objConfig.Save();
            
            var f5 = new
            {
                para = FrameWork.Util.GetString(context.Request["trabalhe_para"]),
                cc = FrameWork.Util.GetString(context.Request["trabalhe_cc"]),
            };
            objConfig.LoadWhere("Nome='Destinatarios_TrabalheConosco'");
            if (objConfig.TotalLoad == 1)
                objConfig.DataAlteracao = DateTime.Now;
            else
                objConfig.DataInclusao = DateTime.Now;
            objConfig.Usuario = FrameWork.Util.GetString(context.Session["_uiUser"]);
            objConfig.Nome = "Destinatarios_TrabalheConosco";
            objConfig.Valor = js.Serialize(f5);
            objConfig.Save();
            
            var f6 = new
            {
                para = FrameWork.Util.GetString(context.Request["corretor_para"]),
                cc = FrameWork.Util.GetString(context.Request["corretor_cc"]),
            };
            objConfig.LoadWhere("Nome='Destinatarios_Corretor'");
            if (objConfig.TotalLoad == 1)
                objConfig.DataAlteracao = DateTime.Now;
            else
                objConfig.DataInclusao = DateTime.Now;
            objConfig.Usuario = FrameWork.Util.GetString(context.Session["_uiUser"]);
            objConfig.Nome = "Destinatarios_Corretor";
            objConfig.Valor = js.Serialize(f6);
            objConfig.Save();
            
            var f7 = new
            {
                para = FrameWork.Util.GetString(context.Request["terreno_para"]),
                cc = FrameWork.Util.GetString(context.Request["terreno_cc"]),
            };
            objConfig.LoadWhere("Nome='Destinatarios_OferecerTerreno'");
            if (objConfig.TotalLoad == 1)
                objConfig.DataAlteracao = DateTime.Now;
            else
                objConfig.DataInclusao = DateTime.Now;
            objConfig.Usuario = FrameWork.Util.GetString(context.Session["_uiUser"]);
            objConfig.Nome = "Destinatarios_OferecerTerreno";
            objConfig.Valor = js.Serialize(f7);
            objConfig.Save();

            //json = js.Serialize(f);

            json = "{ \"response\": \"ok\",\"message\": \"Salvo com sucesso!\" }";
        }
        else if (modulo == "chat")
        {
            var f = new
            {
                link = FrameWork.Util.GetString(context.Request["chat"]),
            };

            objConfig.LoadWhere("Nome='Chat'");
            if (objConfig.TotalLoad == 1)
                objConfig.DataAlteracao = DateTime.Now;
            else
                objConfig.DataInclusao = DateTime.Now;

            objConfig.Usuario = FrameWork.Util.GetString(context.Session["_uiUser"]);
            objConfig.Nome = "Chat";
            objConfig.Valor = js.Serialize(f);
            objConfig.Save();

            json = "{ \"response\": \"ok\",\"message\": \"Salvo com sucesso!\" }";
        }
        else if (modulo == "google")
        {
            var f = new
            {
                script = FrameWork.Util.GetString(context.Request["google"]),
            };

            objConfig.LoadWhere("Nome='Google'");
            if (objConfig.TotalLoad == 1)
                objConfig.DataAlteracao = DateTime.Now;
            else
                objConfig.DataInclusao = DateTime.Now;

            objConfig.Usuario = FrameWork.Util.GetString(context.Session["_uiUser"]);
            objConfig.Nome = "Google";
            objConfig.Valor = js.Serialize(f);
            objConfig.Save();

            json = "{ \"response\": \"ok\",\"message\": \"Salvo com sucesso!\" }";
        }


        context.Response.Write(json);

    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}