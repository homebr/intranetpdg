﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;

public partial class Ferramentas_Configuracoes : System.Web.UI.Page
{

    //public String[] arrEmpresa = {"","","" };
    //public String[] arrRedes = { "", "", "" };
    //public String[] arrSMTP = { "", "", "", "", "", "" };
    //public String[] arrDestinatarios = { "", "", "", "", "", "", "", "", "", "", "", "", "", "" };
    //public String[] arrChat = { "" };
    //public String[] arrGoogle = { "" };

    public ClassLibrary.Configuracoes.EMPRESA objEMPRESA = new ClassLibrary.Configuracoes.EMPRESA();
    public ClassLibrary.Configuracoes.REDES objREDES = new ClassLibrary.Configuracoes.REDES();
    public ClassLibrary.Configuracoes.SMTP objSMTP = new ClassLibrary.Configuracoes.SMTP();
    public ClassLibrary.Configuracoes.GOOGLE objGOOGLE = new ClassLibrary.Configuracoes.GOOGLE();

    public ClassLibrary.Configuracoes.DESTINATARIOS.faleconosco objDEST_faleconosco = new ClassLibrary.Configuracoes.DESTINATARIOS.faleconosco();


    protected void Page_Load(object sender, EventArgs e)
    {
        String strSql = "SELECT * FROM Configuracoes ";
        DataTable dt = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSql);

        for (int n=0;n<dt.Rows.Count;n++)
        {
            if(FrameWork.Util.GetString(dt.Rows[n]["Nome"]) == "Empresa")
            {
                String sJson = FrameWork.Util.GetString(dt.Rows[n]["Valor"]);

                JavaScriptSerializer js = new JavaScriptSerializer();

                dynamic result = js.DeserializeObject(sJson);
                objEMPRESA.nome = FrameWork.Util.GetString(result["nome"]);
                objEMPRESA.telefone = FrameWork.Util.GetString(result["telefone"]);
                //objEMPRESA.telefone2 = FrameWork.Util.GetString(result["telefone2"]);
                objEMPRESA.endereco = FrameWork.Util.GetString(result["endereco"]);
            }
            else if (FrameWork.Util.GetString(dt.Rows[n]["Nome"]) == "Redes")
            {
                String sJson = FrameWork.Util.GetString(dt.Rows[n]["Valor"]);

                JavaScriptSerializer js = new JavaScriptSerializer();

                dynamic result = js.DeserializeObject(sJson);
                objREDES.facebook = FrameWork.Util.GetString(result["facebook"]);
                objREDES.youtube = FrameWork.Util.GetString(result["youtube"]);
                objREDES.instagram = FrameWork.Util.GetString(result["instagram"]);
            }
            else if (FrameWork.Util.GetString(dt.Rows[n]["Nome"]) == "SMTP")
            {
                String sJson = FrameWork.Util.GetString(dt.Rows[n]["Valor"]);

                JavaScriptSerializer js = new JavaScriptSerializer();

                dynamic result = js.DeserializeObject(sJson);
                objSMTP.servidor = FrameWork.Util.GetString(result["servidor"]);
                objSMTP.usuario = FrameWork.Util.GetString(result["usuario"]);
                objSMTP.senha = FrameWork.Util.GetString(result["senha"]);
                objSMTP.porta = FrameWork.Util.GetString(result["porta"]);
                objSMTP.ssl = FrameWork.Util.GetString(result["ssl"]);
                objSMTP.remetente = FrameWork.Util.GetString(result["remetente"]);
            }
            else if (FrameWork.Util.GetString(dt.Rows[n]["Nome"]) == "Destinatarios_FaleConosco")
            {
                String sJson = FrameWork.Util.GetString(dt.Rows[n]["Valor"]);

                JavaScriptSerializer js = new JavaScriptSerializer();

                dynamic result = js.DeserializeObject(sJson);
                objDEST_faleconosco.para = FrameWork.Util.GetString(result["para"]);
                objDEST_faleconosco.cc = FrameWork.Util.GetString(result["cc"]);
            }
            else if (FrameWork.Util.GetString(dt.Rows[n]["Nome"]) == "Google")
            {
                String sJson = FrameWork.Util.GetString(dt.Rows[n]["Valor"]);

                JavaScriptSerializer js = new JavaScriptSerializer();

                dynamic result = js.DeserializeObject(sJson);
                objGOOGLE.script = FrameWork.Util.GetString(result["script"]);
            }
        }

        string dirPath = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"];
        lnkAtualizacao.NavigateUrl = dirPath + "maintenance.ashx";
        lnkAtualizacao.Target = "_blank";
    }
}