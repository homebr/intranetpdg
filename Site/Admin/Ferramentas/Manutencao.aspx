﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Manutencao.aspx.cs" Inherits="Ferramentas_Manutencao" ValidateRequest="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        h1,h3{
            color:red;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
   

     <p>
        <asp:Button ID="btnImportarClientes" runat="server" Text="Importar Clientes" OnClick="btnImportarClientes_Click" />
    </p>
     <p>
        <asp:Button ID="btnImportarEmpreendimentos" runat="server" Text="Importar Empreendimentos" OnClick="btnImportarEmpreendimentos_Click" />
    </p>
     <p>
        <asp:Button ID="btnImportarUnidades" runat="server" Text="Importar Unidades" OnClick="btnImportarUnidades_Click" />
    </p>
        <asp:GridView ID="GridView1" runat="server"></asp:GridView>
    </form>
</body>
</html>
