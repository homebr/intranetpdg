﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Reflection;
using FrameWork.WebControls;
using FrameWork.DataObject;
using ClassLibrary;
using System.Text.RegularExpressions;
using System.IO;

public partial class Ferramentas_UsuariosEditar : System.Web.UI.Page
{

    String strSQL = "";
    public String strImage = "";


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            //if (FrameWork.Util.GetString(Session["_uiGrup"]) != "AD" & !ClassLibrary.Funcoes.VeCheckUserRight(7, "O"))
            //{
            //    ClassLibrary.Funcoes.SkinMessage("Sem Acesso", Page);
            //    return;
            //}

            //if (FrameWork.Util.GetString(Session["_uiGrup"]) == "AD")
            //{
            //    trEmpresa.Visible = true;
            //    CarregaEmpresas();
            //}

            CarregaGrupos();
            CarregaUsuario();

        }

        Literal1.Text = "";
    }

    public void CarregaGrupos()
    {

        String strSql = "SELECT IDGrupo,Grupo as Grupo,'1' as cat FROM GrupoUsuarios  union all SELECT 0 AS IDGrupo,'[SELECIONE O GRUPO]' as Grupo,'0' as cat order by cat,Grupo ";
        ddlGrupo.DataSource = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSql);
        ddlGrupo.DataValueField = "IDGrupo";
        ddlGrupo.DataTextField = "Grupo";
        ddlGrupo.DataBind();

    }




    public void CarregaUsuario()
    {
        ClassLibrary.Usuarios usuario = new ClassLibrary.Usuarios();
        hLogin.Value = FrameWork.Util.GetString(Request["id"]);

        if (FrameWork.Util.GetString(hLogin.Value).Length > 1)
        {

            
                usuario.Load(hLogin.Value);



            if (usuario.TotalLoad == 1)
            {
                txtLoginAcesso.Text = usuario.LoginAcesso;
                txtNome.Text = usuario.Nome;
                txtSenha.Text = "";
                try
                {
                    hSenha.Value = usuario.Senha; // FrameWork.Security.DeCript(usuario.Senha);
                }
                catch (Exception ex)
                {
                    lblMensagem.Text = ex.Message;
                }

                txtEmail.Text = usuario.Email;
                chkAdministrador.Checked = Convert.ToBoolean(usuario.Admin);

                if ((ddlGrupo.Items.FindByValue(usuario.Grupo) != null))
                {
                    ddlGrupo.SelectedValue = usuario.Grupo;
                }



                chkAtivo.Checked = (usuario.SituacaoLogin == "A");


                string pathPDFs = "/uploads/fotos/";
                strImage = pathPDFs + FrameWork.Util.GetString(usuario.Foto);
                this.Image1.ImageUrl = strImage;


                lblDataInclusao.Text = string.Format("{0:dd/MM/yyyy}", usuario.DataInclusao);
                lblDataAlteracao.Text = string.Format("{0:dd/MM/yyyy}", usuario.DataAlteracao);
                lblUsuario.Text = string.Format("{0:dd/MM/yyyy}", usuario.Usuario);

            }

        }
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {

        if (FrameWork.Util.GetString(txtLoginAcesso.Text) == "")
        {
            txtLoginAcesso.Focus();
            Msg("Favor preencher o Login Acesso!",false);
            return;
        }
        if (FrameWork.Util.GetString(txtNome.Text) == "")
        {
            txtNome.Focus();
            Msg("Favor preencher o nome!",false);
            return;
        }
        if (FrameWork.Util.GetString(txtEmail.Text).Length > 3)
        {
            string Email = txtEmail.Text;
            Regex rg = new Regex("^[A-Za-z0-9](([_\\.\\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\\.\\-]?[a-zA-Z0-9]+)*)\\.([A-Za-z]{2,})$");
            if (!rg.IsMatch(Email))
            {
                Msg("Email inválido, por favor informe um email válido!",false);
                txtEmail.Focus();
                return;
            }
        }


        ClassLibrary.Usuarios objUsuarios = new ClassLibrary.Usuarios();
        if (FrameWork.Util.GetString(hLogin.Value).Length > 1)
            objUsuarios.LoadWhere("LoginAcesso = '" + hLogin.Value + "'");


        if (FrameWork.Util.GetString(hLogin.Value).Length > 1)
        {

            objUsuarios.Load(hLogin.Value);
            hOperacao.Value = "UPDATE";
            objUsuarios.DataAlteracao = DateTime.Now;



        }
        else
        {
            hOperacao.Value = "INSERT";

            if (FrameWork.Util.GetString(txtSenha.Text).Trim().Length < 4)
            {
                txtSenha.Focus();
                Msg("Favor inserir uma senha de 4 a 8 digitos!",false);
                return;
            }

            strSQL = "SELECT isnull(count(*),0) FROM Usuarios where loginacesso = '" + this.txtLoginAcesso.Text + "'";
            string strAux = FrameWork.Util.GetString(txtLoginAcesso.Text).Trim();
            int qtd = FrameWork.Util.GetInt(FrameWork.DataObject.DataSource.DefaulDataSource.Execute(strSQL, new ParameterItem("@Login", strAux)));

            if (qtd > 0)
            {
                //Response.Write(Sql)
                Msg("Login já existe!", false);
                return;
            }

            objUsuarios.LoginAcesso = txtLoginAcesso.Text;
            //usuario.DataInclusao = Now
        }

        if (FrameWork.Util.GetString(txtSenha.Text).Trim().Length > 3)
        {
            objUsuarios.Senha = (FrameWork.Util.GetString(txtSenha.Text).Trim()); //FrameWork.Security.Cript
        }

        objUsuarios.Admin = (chkAdministrador.Checked);

        if ((!chkAdministrador.Checked) && FrameWork.Util.GetInt(ddlGrupo.SelectedValue) == 0)
        {
            Msg("Obrigatório a seleção de um grupo!", false);
            return;
        }

        string Grupo = FrameWork.Util.GetString(ddlGrupo.SelectedValue);
        objUsuarios.Grupo = Grupo;


        objUsuarios.Nome = (txtNome.Text);
        objUsuarios.Email = FrameWork.Util.GetString(txtEmail.Text).ToLower();
        //objUsuarios.SituacaoLogin = ddlStatus.SelectedValue;
        objUsuarios.SituacaoLogin = (chkAtivo.Checked ? "A" : "S");

        objUsuarios.Usuario = FrameWork.Util.GetString(Session["_uiUser"]);
        objUsuarios.Save();

        hLogin.Value = objUsuarios.LoginAcesso;


        //ATUALIZA A TELA 
        //this.Image1.ImageUrl = FrameWork.Util.GetString(objDestaquesImagem"]);
        try
        {


            if ((fileFoto.HasFile))
            {

                //inicializar as variáveis
                string arq = fileFoto.PostedFile.FileName;
                string fileExtension = fileFoto.PostedFile.ContentType.ToLower();

                if (!string.IsNullOrEmpty(arq))
                {


                    fileExtension = Path.GetExtension(arq);
                    string str_image = "foto-" + hLogin.Value + fileExtension.ToLower();

                    //verificamos a extensão através dos últimos 4 caracteres e verificamos se é permitido
                    if ((!(fileExtension == ".gif" || fileExtension == ".jpg" || fileExtension == ".png")))
                    {
                        Msg("Extensão de arquivo não permitido! Somente é permitido extensão: gif, jpg ou png!", false);
                        return;
                    }


                    //tamanho maximo do upload em kb
                    double permitido = 2000;
                    //identificamos o tamanho do arquivo
                    double tamanho = 0;
                    tamanho = Convert.ToDouble(fileFoto.PostedFile.ContentLength) / 1024;
                    if ((tamanho > permitido))
                    {
                        Msg("Tamanho Máximo permitido é de " + permitido + " kb", false);
                        return;
                    }

                    //caminho fisico no server
                    string pathPDFs = Server.MapPath("..\\uploads\\fotos\\");

                    fileFoto.SaveAs(pathPDFs + str_image);

                    String strSql = "UPDATE Usuarios SET Foto = '" + str_image + "' WHERE LoginAcesso = '" + hLogin.Value + "'";
                    FrameWork.DataObject.DataSource.DefaulDataSource.Execute(strSql);


                    fileFoto.Dispose();

                    pathPDFs = "/uploads/fotos/";
                    String strImage = pathPDFs + FrameWork.Util.GetString(objUsuarios.Foto);
                    this.Image1.ImageUrl = strImage;
                }

            }


        }
        catch (Exception ex)
        {
            Response.Write("erro=" + ex.ToString());
            string strErro = ex.ToString();
            Literal1.Text = "<script>alert('" + strErro + "!');</script>";
            return;
        }

        Msg("Salvo com sucesso!", true);

        Response.Redirect("UsuariosListar");
    }

    protected void Msg(String msg, Boolean tipo)
    {
        lblMensagem.CssClass = (tipo ? "box-danger alert alert-success" : "box-danger alert alert-danger");
        lblMensagem.Text = msg;
        lblMensagem.Visible = true;
    }
}