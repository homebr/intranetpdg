﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="UsuariosListar.aspx.cs" Inherits="Ferramentas_UsuariosListar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!-- JQUERY UI-->
    <link rel="stylesheet" type="text/css" href="/content/js/jquery-ui-1.10.3.custom/css/custom-theme/jquery-ui-1.10.3.custom.min.css" />
    <!-- DATE RANGE PICKER -->
    <link rel="stylesheet" type="text/css" href="/content/js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
    <!-- DATA TABLES -->
    <link rel="stylesheet" type="text/css" href="/content/js/datatables/media/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" type="text/css" href="/content/js/datatables/media/assets/css/datatables.min.css" />
    <link rel="stylesheet" type="text/css" href="/content/js/datatables/extras/TableTools/media/css/TableTools.min.css" />

    <script type="text/javascript">
        $(document).ready(function () {
            oTable = $('#example').dataTable({
                "bPaginate": true,
                "bJQueryUI": true,
                "sPaginationType": "full_numbers",
                "iDisplayLength": "50"
            });
        });


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <!--Page Title-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <div id="page-title">
        <h1 class="page-header text-overflow">Usuários</h1>
    </div>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End page title-->


    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i>&nbsp;Home</a></li>
        <li>Usuários</li>
        <li class="active">Listagem</li>
    </ol>

    <form id="form1" runat="server">

        <div id="page-content">

            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">FILTROS</h3>
                </div>
                <div class="panel-body">





                            <asp:TextBox ID="TextBox1" runat="server" Width="296px"></asp:TextBox>



                            <asp:Button ID="btnBuscar" CssClass="btn" Text="Buscar" runat="server" />




                        </div>
            </div>

            <!-- DATA TABLES -->
           <div class="panel">

                <div class="panel-body">

                            <table id="datatable1" cellpadding="0" cellspacing="0" border="0" class="datatable table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th class="col-md-1"></th>
                                        <th>LoginAcesso</th>
                                        <th>Nome</th>
                                        <th>E-mail</th>
                                        <th>Situação</th>
                                        <th>Último Acesso</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="Repeater1" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td class="col-md-1"><%# DataBinder.Eval(Container, "DataItem.fotofull")%></td>
                                                <td class="left"><a href='<%# "UsuariosEditar?id=" + DataBinder.Eval(Container, "DataItem.LoginAcesso")%>'><%# DataBinder.Eval(Container, "DataItem.LoginAcesso")%></a></td>
                                                <td><%# DataBinder.Eval(Container, "DataItem.Nome")%></a></td>
                                                <td><%# DataBinder.Eval(Container, "DataItem.Email")%></a></td>
                                                <td><%# DataBinder.Eval(Container, "DataItem.SituacaoLogin")%></a></td>
                                                <td><%# DataBinder.Eval(Container, "DataItem.UltimoAcesso")%></a></td>
                                                <td class="center"><a class="btn btn-danger" href='<%# "UsuariosEditar?id=" + DataBinder.Eval(Container, "DataItem.LoginAcesso")%>'>Ver</a></td>
                                            </tr>

                                        </ItemTemplate>
                                    </asp:Repeater>

                                </tbody>
                            </table>
                </div>

                <div class="panel-footer">
                    <a class="btn btn-danger" href="UsuariosEditar">NOVO</a>
                </div>


            </div>

                    
        </div>
      
    </form>

    <!--/PAGE -->


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <!--DataTables [ OPTIONAL ]-->
    <script src="/content/plugins/datatables/media/js/jquery.dataTables.js"></script>
    <script src="/content/plugins/datatables/media/js/dataTables.bootstrap.js"></script>
    <script src="/content/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js"></script>


    <!--DataTables Sample [ SAMPLE ]-->
    <script src="/content/js/demo/tables-datatables.js"></script>





    <script type="text/javascript">
        $(document).ready(function () {
            oTable = $('#example').dataTable({
                "bPaginate": true,
                "bJQueryUI": true,
                "sPaginationType": "full_numbers",
                "iDisplayLength": "50"
            });
        });
        //ordernação da ficha tecnica
        $(function () {
            $("#datatable1 tbody").sortable({
                connectWith: "#datatable1",
                axis: "y",
                start: function (event, ui) {
                    ui.item.toggleClass("highlight");
                },
                stop: function (event, ui) {
                    var slides = [];
                    var items = [];

                    ui.item.toggleClass("highlight");

                    $("#datatable1 tr .item-value").each(function (index, element) {
                        slides[index] = index + 1;
                        items[index] = parseInt($(element).val());
                    });

                    // console.log(slides, items);
                    $.post('wsOrdenacao.ashx?modulo=cms',
                        { 'item_id': items.join(','), 'positions': slides.join(',') }, function (data) { }, 'json');
                }
            });

            $("#datatable1").disableSelection();


        });


    </script>

</asp:Content>

