﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="UsuariosEditar.aspx.cs" Inherits="Ferramentas_UsuariosEditar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="/content/css/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet" type="text/css" />

    <!-- JQUERY UI-->
    <link rel="stylesheet" type="text/css" href="/content/js/jquery-ui-1.10.3.custom/css/custom-theme/jquery-ui-1.10.3.custom.min.css" />

    <!-- DATE RANGE PICKER -->
    <link rel="stylesheet" type="text/css" href="/content/js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
    <!-- ANIMATE -->
    <link rel="stylesheet" type="text/css" href="/content/css/animatecss/animate.min.css" />
    <!-- COLORBOX -->
    <link rel="stylesheet" type="text/css" href="/content/js/colorbox/colorbox.min.css" />

    <!-- BOOTSTRAP SWITCH -->
    <link rel="stylesheet" type="text/css" href="/content/js/bootstrap-switch/bootstrap-switch.min.css" />
    <link rel="stylesheet" type="text/css" href="/content/css/themes/default.css" id="skin-switcher">

    <!-- JQUERY UI-->
    <link rel="stylesheet" type="text/css" href="/content/js/jquery-ui-1.10.3.custom/css/custom-theme/jquery-ui-1.10.3.custom.min.css" />


    <script type="text/javascript">
        function consistir(sender, arg) {
            with (document.forms[0]) {
                if (txtSenha.value == "") {
                    alert("Senha Atual é campo obrigatório!");
                    (txtSenha.focus());
                    arg.IsValid = false;
                }

                aux = 0;
                xnova = txtNova.value;
                xredigite = txtRedigite.value;
                if (aux == 0) {
                    if (xnova == "") {
                        aux = 1;
                        alert("Preencha o campo Nova Senha");
                        txtNova.focus();
                        arg.IsValid = false;
                    }
                }
                if (aux == 0) {
                    if (xnova != xredigite) {
                        aux = 1;
                        alert("Campo Nova Senha e Redigite precisam ser iguais!");
                        txtNova.focus();
                        arg.IsValid = false;
                    }
                }
            }
        }
        function ImprimeUsuario(usuario) { //v2.0
            //theURL = 'http://200.204.154.59/Relatorios.aspx?rel='+Relatorio+'&Titulo='+Titulo+'&Parametros='+Parametros;
            theURL = '../GeraImpressao.aspx?hUsu=' + usuario;
            winName = "Imob";
            Tipo = '';
            if (Tipo == 'Vertical') {
                features = "scrollbars=yes,width=1000,height=650,top=5,left=10";
            } else {
                features = "scrollbars=yes,width=790,height=650,top=5,left=10";
            }
            window.open(theURL, winName, features);
        }

        function VerificarGrauDaSenha(senha, user) {
            var points = 0;
            var descr = new Array();
            descr[0] = "Muito fraco";
            descr[1] = "Fraco";
            descr[2] = "Razoável";
            descr[3] = "Médio";
            descr[4] = "Forte";
            descr[5] = "Muito forte";

            // grau se o temanho é maior que 4 caracteres, aumenta 1 ponto        
            if (senha.length > 4) points++;

            // grau se a senha contém caracteres maiúsculos e minúsculos, aumenta 1 pto        
            if ((senha.match(/[a-z]/)) && (senha.match(/[A-Z]/))) points++;

            // grau se a senha tem algum número, aumenta 1 ponto        
            if (senha.match(/\d+/)) points++;

            // grau se a senha tem algum caractere especial, aumenta 1 ponto        
            if (senha.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)) points++;

            // grau se o tamanho da senha for maior que 12, aumenta 1 ponto        
            if (senha.length > 12) points++;

            if (senha == user || senha == "102030" || senha == "12345" || senha == "123456") points = 0;

            if (points > 0 && (senha.indexOf(user) > -1 || senha.indexOf("102030") > -1 || senha.indexOf("12345") > -1)) points--;


            // muda descrição e a classe do div conforme o grau da senha        
            document.getElementById("descricaoGrau").innerHTML = descr[points];
            document.getElementById("grauSenha").className = "grau" + points;


            //    if (points<=0)
            //        document.getElementById("DataControl_ButtonSave").style.display = "none";   
            //    else
            //        document.getElementById("DataControl_ButtonSave").style.display = "block";
            //    
            //    

        }

        //-->
    </script>
    <style type="text/css">
        #dificuldade {
            float: left;
        }

        #descricaoGrau {
            font-size: 12px;
        }

        #grauSenha {
            height: 10px;
        }

        #bordaSenha {
            width: 144px;
            height: 10px;
            border: 1px solid black;
        }

        .grau0 {
            width: 144px;
            background: #ff0000;
        }

        .grau1 {
            width: 40px;
            background: #ff5f5f;
        }

        .grau2 {
            width: 60px;
            background: #cccccc;
        }

        .grau3 {
            width: 80px;
            background: #56e500;
        }

        .grau4 {
            background: #4dcd00;
            width: 100px;
        }

        .grau5 {
            background: #399800;
            width: 144px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <!--Page Title-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <div id="page-title">
        <h1 class="page-header text-overflow">&nbsp;Usuários</h1>

    </div>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End page title-->

    <!--Breadcrumb-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <ol class="breadcrumb">
        <li><a href="/">Home</a></li>
        <li class="active">Usuários</li>
        <li>Editar</li>
    </ol>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End breadcrumb-->


    <form id="form2" runat="server">


        <asp:Label ID="lblMensagem" runat="server" CssClass="box-danger alert alert-danger" Style="display: block" Visible="false"></asp:Label>

        <div id="page-content">

            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <asp:Label ID="lblNomeImovel" runat="server" Text=""></asp:Label></h3>
                </div>
                <div class="panel-body">


                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>LoginAcesso: *</label>
                            <asp:TextBox ID="txtLoginAcesso" CssClass="form-control" runat="server" Width="100%" MaxLength="20"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>Senha: *</label>
                            <asp:TextBox ID="txtSenha" CssClass="form-control" TextMode="Password" onkeyup="VerificarGrauDaSenha(this.value,document.getElementById('_ctl0_CenterContent_txtSenha').value);" runat="server" Width="100%" MaxLength="20"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>Nome: *</label>
                            <asp:TextBox ID="txtNome" CssClass="form-control" runat="server" Width="100%" MaxLength="50"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>E-mail:</label>

                                <asp:TextBox ID="txtEmail" CssClass="form-control" runat="server" Width="100%" MaxLength="250"></asp:TextBox></td>

                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2">
                                <label for="ContentPlaceHolder1_chkAtivo">Ativo:</label>
                                <div>
                                    <div class="make-switch switch-small" data-on="success" data-off="danger" data-on-label="<i class='fa fa-check icon-white'></i>" data-off-label="<i class='fa fa-times'></i>">
                                        <asp:CheckBox ID="chkAtivo" runat="server" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <label for="ContentPlaceHolder1_chkAtivo">É Administrador:</label>
                                <div>
                                    <div class="make-switch switch-small class-administrador" data-on="success" data-off="danger" data-on-label="<i class='fa fa-check icon-white'></i>" data-off-label="<i class='fa fa-times'></i>">
                                        <asp:CheckBox ID="chkAdministrador" runat="server" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-8 no-admin">
                                <label>Grupo:</label>
                                <asp:DropDownList ID="ddlGrupo" CssClass="form-control" runat="server" Width="100%">
                                </asp:DropDownList>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="separator"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="ContentPlaceHolder1_Chamada">Foto:</label>
                                <asp:FileUpload ID="fileFoto" CssClass="form-control" runat="server" Width="100%"></asp:FileUpload>
                                <small>Upload (Gif/Jpg/Png) </small>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="separator"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4">
                                <label for="ContentPlaceHolder1_lblDataInclusao">Data Inclusão:</label>
                                <asp:Label ID="lblDataInclusao" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="col-sm-4">
                                <label for="ContentPlaceHolder1_lblDataAlteracao">Data Alteração:</label>
                                <asp:Label ID="lblDataAlteracao" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="col-sm-4">
                                <label for="ContentPlaceHolder1_lblUsuario">Usuário:</label>
                                <asp:Label ID="lblUsuario" runat="server" Text=""></asp:Label>
                            </div>
                        </div>


                    </div>


                </div>
                <div class="panel-footer text-right">
                    <asp:Button ID="btnSalvar" runat="server" Text="SALVAR" CssClass="btn btn-success fa fa-check-circle" OnClick="btnSalvar_Click" />
                </div>
            </div>





            <asp:HiddenField ID="hOperacao" runat="server" />
            <asp:HiddenField ID="hID" runat="server" />
            <asp:HiddenField ID="hUrlReferrer" runat="server" />
            <asp:HiddenField ID="hLogin" runat="server" />
            <asp:HiddenField ID="hSenha" runat="server" />

            <asp:Literal ID="Literal1" runat="server"></asp:Literal>

  




    <asp:Panel ID="pnlFotos" Visible="true" runat="server">
        <p>

            <!-- GALERIA -->
            <div id="filter-items" class="row">

                <div class="col-md-3 category_1 item">
                    <div class="filter-content">
                        <asp:Image ID="Image1" runat="server" CssClass="img-responsive" />
                        <div class="hover-content">
                            <a class="btn btn-warning hover-link colorbox-button" href='<%=(strImage) %>' title=''>
                                <i class="fa fa-search-plus fa-1x"></i>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /GALERIA -->
        </p>
    </asp:Panel>

      </div>
    </form>

    <div class="footer-tools">
        <span class="go-top">
            <i class="fa fa-chevron-up"></i>Top
        </span>
    </div>



</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="Server">

    <!-- JQUERY UI-->
    <link rel="stylesheet" type="text/css" href="/js/jquery-ui-1.10.3.custom/css/custom-theme/jquery-ui-1.10.3.custom.min.css" />

    <%--    <!-- SLIMSCROLL -->
    <script type="text/javascript" src="/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.min.js"></script>
    <script type="text/javascript" src="/js/jQuery-slimScroll-1.3.0/slimScrollHorizontal.min.js"></script>
    <!-- BLOCK UI -->
    <script type="text/javascript" src="/js/jQuery-BlockUI/jquery.blockUI.min.js"></script>
    <!-- BOOTSTRAP SWITCH -->
    <script type="text/javascript" src="/js/bootstrap-switch/bootstrap-switch.min.js"></script>--%>
    <!-- ISOTOPE -->
    <script type="text/javascript" src="/js/isotope/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="/js/isotope/imagesloaded.pkgd.min.js"></script>
    <!-- COLORBOX -->
    <script type="text/javascript" src="/js/colorbox/jquery.colorbox.min.js"></script>

    <script>

        $(document).ready(function () {
            var campo = '<%=hID.ClientID.ToString().Replace("hID","")%>';

            if ($("#" + campo + "chkAdministrador").attr("checked") == "checked")
                $(".no-admin").hide();



            $(".class-administrador").click(function () {
                if ($(this).children().hasClass("switch-on"))
                    $(".no-admin").hide();
                else
                    $(".no-admin").show();
            });

        });

    </script>


</asp:Content>

