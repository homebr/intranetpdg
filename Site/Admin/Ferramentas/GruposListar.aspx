﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="GruposListar.aspx.cs" Inherits="Ferramentas_GruposListar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!-- JQUERY UI-->
    <link rel="stylesheet" type="text/css" href="/content/js/jquery-ui-1.10.3.custom/css/custom-theme/jquery-ui-1.10.3.custom.min.css" />
    <!-- DATE RANGE PICKER -->
    <link rel="stylesheet" type="text/css" href="/content/js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
    <!-- DATA TABLES -->
    <link rel="stylesheet" type="text/css" href="/content/js/datatables/media/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" type="text/css" href="/content/js/datatables/media/assets/css/datatables.min.css" />
    <link rel="stylesheet" type="text/css" href="/content/js/datatables/extras/TableTools/media/css/TableTools.min.css" />

    <script type="text/javascript">
        $(document).ready(function () {
            oTable = $('#example').dataTable({
                "bPaginate": true,
                "bJQueryUI": true,
                "sPaginationType": "full_numbers",
                "iDisplayLength": "50"
            });
        });


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
        <!--Page Title-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <div id="page-title">
        <h1 class="page-header text-overflow">Grupos</h1>
    </div>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End page title-->

        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i>&nbsp;Home</a></li>
            <li>Grupos</li>
            <li class="active">Listagem</li>
        </ol>



    <form id="form1" runat="server">

        <div id="page-content">

            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">FILTROS</h3>
                </div>
                <div class="panel-body">





                            <asp:TextBox ID="TextBox1" runat="server" Width="296px"></asp:TextBox>



                            <asp:Button ID="btnBuscar" CssClass="btn" Text="Buscar" runat="server" />



                </div>
            </div>

            <!-- DATA TABLES -->
            <div class="panel">
                <div class="panel-body">
                    <table id="datatable1" cellpadding="0" cellspacing="0" border="0" class="datatable table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Grupo</th>
                                        <th>Situação</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="Repeater1" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td class="left"><a href='<%# "GruposEditar?id=" + DataBinder.Eval(Container, "DataItem.IDGrupo")%>'><%# DataBinder.Eval(Container, "DataItem.Grupo")%></a></td>
                                                <td><%# DataBinder.Eval(Container, "DataItem.Situacao")%></a></td>
                                                <td class="center"><a class="btn btn-danger" href='<%# "GruposEditar?id=" + DataBinder.Eval(Container, "DataItem.IDGrupo")%>'>Ver</a></td>
                                            </tr>

                                        </ItemTemplate>
                                    </asp:Repeater>




                                </tbody>
                            </table>
                </div>
                <div class="panel-footer">
                    <a class="btn btn-danger" href="GruposEditar">NOVO</a>
                </div>
            </div>



        </div>
        <!-- /DATA TABLES -->

    </form>


    <!--/PAGE -->


</asp:Content>


