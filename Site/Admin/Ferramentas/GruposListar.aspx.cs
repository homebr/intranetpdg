﻿using System;
using ClassLibrary;
using FrameWork.WebControls;
using System.Web;
using System.Web.UI;
using FrameWork.DataObject;
using System.Data;

public partial class Ferramentas_GruposListar : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CarregaGrupos();
        
    }
    public void CarregaGrupos()
    {
        string strSQL = "SELECT * FROM  GrupoUsuarios WHERE 1=1 ";
        if (FrameWork.Util.GetString(TextBox1.Text ).Length> 2) {
            strSQL += " Grupo like @text ";
        }
        strSQL += " ORDER BY Grupo ";
        //Response.Write(strSQL);

        try
        {
            DataTable dt = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSQL, new ParameterItem("@text", TextBox1.Text));
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message.ToString() + " - " + strSQL);
            return;
        }

    }

}