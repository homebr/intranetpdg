﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="GruposEditar.aspx.cs" Inherits="Ferramentas_GruposEditar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
        <link href="/content/css/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet" type="text/css" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

            <div id="page-title">
            <h1 class="page-header text-overflow">Grupos</h1>

        </div>



        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i>&nbsp;Home</a></li>
            <li>Grupos</li>
            <li class="active">Edição</li>
        </ol>


    <form id="form1" runat="server">

            <asp:Label ID="lblMensagem" runat="server" ForeColor="Red" Width="408px"></asp:Label>

            <div id="page-content">

                <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Página</h3>
                </div>
                <div class="panel-body">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label for="ContentPlaceHolder1_Nome">Grupo:<span class="required">*</span></label>
                                    <asp:TextBox ID="txtGrupo" CssClass="form-control" required="required" runat="server" MaxLength="20" Width="20%" placeholder="Sigla do grupo"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label for="ContentPlaceHolder1_Nome">Situação:<span class="required">*</span></label>
                                    <asp:DropDownList ID="ddlSituacao" CssClass="form-control" runat="server" Width="100px">
                                        <asp:ListItem Value="A">Ativo</asp:ListItem>
                                        <asp:ListItem Value="S">Suspenso</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div id="ltl" runat="server"></div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="separator"></div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <label for="ContentPlaceHolder1_lblDataInclusao">Data Inclusão:</label>
                                    <asp:Label ID="lblDataInclusao" runat="server" Text=""></asp:Label>
                                </div>
                                <div class="col-sm-4">
                                    <label for="ContentPlaceHolder1_lblDataAlteracao">Data Alteração:</label>
                                    <asp:Label ID="lblDataAlteracao" runat="server" Text=""></asp:Label>
                                </div>
                                <div class="col-sm-4">
                                    <label for="ContentPlaceHolder1_lblUsuario">Usuário:</label>
                                    <asp:Label ID="lblUsuario" runat="server" Text=""></asp:Label>
                                </div>
                            </div>

                </div>
                 <div class="panel-footer text-right">
                            <asp:Button ID="btnSalvar" runat="server" Text="SALVAR" CssClass="btn btn-success fa fa-check-circle" OnClick="btnSalvar_Click" />
                            <asp:HiddenField ID="hOperacao" runat="server" />
                            <asp:HiddenField ID="hID" runat="server" />
                            <asp:HiddenField ID="hUrlReferrer" runat="server" />

                            <asp:Literal ID="Literal1" runat="server"></asp:Literal>

                        </div>
                    


                </div>


                <div class="footer-tools">
                    <span class="go-top">
                        <i class="fa fa-chevron-up"></i>Top
                    </span>
                </div>




            </div>
      </form>



</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="Server">



</asp:Content>

