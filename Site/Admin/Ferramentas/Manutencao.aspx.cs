﻿using System;
using System.Runtime.Serialization;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ServiceModel.Web;
using System.Net;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.Data.SqlClient;
using System.IO;
using Newtonsoft.Json;

public partial class Ferramentas_Manutencao : System.Web.UI.Page
{


    protected void btnImportarClientes_Click(object sender, EventArgs e)
    {
        String connectionString = "server=186.202.148.146; user id=hia; password=h6g5f4d3s2; database=hia; Min Pool Size=5;Max Pool Size=1200; Connect Timeout=5";
        SqlConnection connection = new SqlConnection(connectionString);
        connection.Open();
        String queryString = "SELECT * FROM [dbo].[TB_CLIENTES] where isnull(ds_cpfcnpj,'') != ''";
        SqlDataAdapter adapter = new SqlDataAdapter(queryString, connection);
        DataTable data = new DataTable();
        adapter.Fill(data);


        //String sLog = "";
        foreach (DataRow item in data.Rows)
        {
            ClassLibrary.Clientes objCliente = new ClassLibrary.Clientes();
            objCliente.Load(FrameWork.Util.GetInt(item["idt"]));
            if (objCliente.TotalLoad == 0)
            {
                objCliente.ID = FrameWork.Util.GetInt(item["idt"]);
            }
            
            objCliente.Nome = FrameWork.Util.GetString(item["ds_nome"]);
            objCliente.CpfCnpj = FrameWork.Util.GetString(item["ds_cpfcnpj"]);
            objCliente.Pessoa = FrameWork.Util.GetString(item["ds_pessoa"]);
            objCliente.Endereco  = FrameWork.Util.GetString(item["ds_endereco"]);
            objCliente.Cidade = FrameWork.Util.GetString(item["ds_cidade"]);
            objCliente.Estado = FrameWork.Util.GetString(item["ds_estado"]);
            objCliente.Cep = FrameWork.Util.GetString(item["ds_cep"]);
            objCliente.Telefone = FrameWork.Util.GetString(item["ds_telefone"]);
            objCliente.Email = FrameWork.Util.GetString(item["ds_email"]);

            objCliente.Login = FrameWork.Util.GetString(item["ds_login"]);
            objCliente.Observacao = FrameWork.Util.GetString(item["ds_obs"]);

            if (Funcoes.IsDate(FrameWork.Util.GetString(item["DH_INCLUSAO"])))
                objCliente.DataInclusao = Convert.ToDateTime(item["DH_INCLUSAO"]);
            if (Funcoes.IsDate(FrameWork.Util.GetString(item["DH_ALTERACAO"])))
                objCliente.DataAlteracao = Convert.ToDateTime(item["DH_ALTERACAO"]);

            objCliente.Correio = FrameWork.Util.GetInt(item["TG_CORREIO"]);
            objCliente.Usuario = FrameWork.Util.GetString(item["ds_Usuario"]);
            if (Funcoes.IsDate(FrameWork.Util.GetString(item["DH_TROCARSENHA"])))
                objCliente.AlteracaoSenha = Convert.ToDateTime(item["DH_TROCARSENHA"]);


           objCliente.Save();

        }

        //GridView1.DataSource = data;
        //GridView1.DataBind();

        data.Dispose();
        adapter.Dispose();
        connection.Close();
    }


    protected void btnImportarEmpreendimentos_Click(object sender, EventArgs e)
    {
        String connectionString = "server=186.202.148.146; user id=hia; password=h6g5f4d3s2; database=hia; Min Pool Size=5;Max Pool Size=1200; Connect Timeout=5";
        SqlConnection connection = new SqlConnection(connectionString);
        connection.Open();
        String queryString = "SELECT * FROM [dbo].[TB_EMPREENDIMENTOS] where TG_INATIVO=0 ";
        SqlDataAdapter adapter = new SqlDataAdapter(queryString, connection);
        DataTable data = new DataTable();
        adapter.Fill(data);


        //String sLog = "";
        foreach (DataRow item in data.Rows)
        {
            ClassLibrary.Empreendimentos objEMP = new ClassLibrary.Empreendimentos();
            objEMP.Load(FrameWork.Util.GetInt(item["idt"]));
            if (objEMP.TotalLoad == 0)
            {
                objEMP.ID = FrameWork.Util.GetInt(item["idt"]);
            }

            objEMP.Nome = FrameWork.Util.GetString(item["ds_nome"]);
            objEMP.Sigla = FrameWork.Util.GetString(item["ds_sigla"]);
            objEMP.Logo = FrameWork.Util.GetString(item["ds_caminho"]);
            //objEMP.Endereco = FrameWork.Util.GetString(item["ds_endereco"]);
            //objEMP.Cidade = FrameWork.Util.GetString(item["ds_cidade"]);
            //objEMP.Estado = FrameWork.Util.GetString(item["ds_estado"]);
            //objEMP.Cep = FrameWork.Util.GetString(item["ds_cep"]);
            objEMP.UrlExterna = FrameWork.Util.GetString(item["ds_url"]);
            objEMP.Mensagem = FrameWork.Util.GetString(item["ds_mensagem"]);
            objEMP.FK_OWNER = FrameWork.Util.GetInt(item["FK_OWNER"]);
            objEMP.Ativo = true;
            if (Funcoes.IsDate(FrameWork.Util.GetString(item["DH_INCLUSAO"])))
                objEMP.DataInclusao = Convert.ToDateTime(item["DH_INCLUSAO"]);
            if (Funcoes.IsDate(FrameWork.Util.GetString(item["DH_ALTERACAO"])))
                objEMP.DataAlteracao = Convert.ToDateTime(item["DH_ALTERACAO"]);

            objEMP.DataImportacao = DateTime.Now;
            //objEMP.Usuario = FrameWork.Util.GetString(item["ds_Usuario"]);
            //if (Funcoes.IsDate(FrameWork.Util.GetString(item["DH_TROCARSENHA"])))
            //    objEMP.AlteracaoSenha = Convert.ToDateTime(item["DH_TROCARSENHA"]);


            objEMP.Save();

        }

        //GridView1.DataSource = data;
        //GridView1.DataBind();

        data.Dispose();
        adapter.Dispose();
        connection.Close();
    }


    protected void btnImportarUnidades_Click(object sender, EventArgs e)
    {
        String connectionString = "server=186.202.148.146; user id=hia; password=h6g5f4d3s2; database=hia; Min Pool Size=5;Max Pool Size=1200; Connect Timeout=5";
        SqlConnection connection = new SqlConnection(connectionString);
        connection.Open();
        String queryString = "SELECT * FROM [dbo].[TB_CLIENTESUNIDADE] where TG_INATIVO=0 ";
        SqlDataAdapter adapter = new SqlDataAdapter(queryString, connection);
        DataTable data = new DataTable();
        adapter.Fill(data);


        //String sLog = "";
        foreach (DataRow item in data.Rows)
        {
            ClassLibrary.ClientesUnidades objEMP = new ClassLibrary.ClientesUnidades();
            objEMP.LoadWhere("ID=" + FrameWork.Util.GetInt(item["idt"]));
            if (objEMP.TotalLoad == 0)
            {
                objEMP.ID = FrameWork.Util.GetInt(item["idt"]);
            }

            objEMP.IDCliente = FrameWork.Util.GetInt(item["idt_cliente"]);
            objEMP.IDEmpreendimento = FrameWork.Util.GetInt(item["idt_empreendimento"]);
            objEMP.Unidade = FrameWork.Util.GetString(item["ds_unidade"]);
            objEMP.Coordenador = FrameWork.Util.GetInt(item["tg_coordenador"]);
            objEMP.FK_OWNER = FrameWork.Util.GetInt(item["FK_OWNER"]);
            objEMP.Ativo = true;
            if (Funcoes.IsDate(FrameWork.Util.GetString(item["DH_INCLUSAO"])))
                objEMP.DataInclusao = Convert.ToDateTime(item["DH_INCLUSAO"]);
            if (Funcoes.IsDate(FrameWork.Util.GetString(item["DH_ALTERACAO"])))
                objEMP.DataAlteracao = Convert.ToDateTime(item["DH_ALTERACAO"]);

            objEMP.DataImportacao = DateTime.Now;


            objEMP.Save();

        }

        GridView1.DataSource = data;
        GridView1.DataBind();

        data.Dispose();
        adapter.Dispose();
        connection.Close();
    }



    public byte[] GetBytesFromUrl(string url)
    {
        byte[] b = null;
        System.Net.HttpWebRequest myReq = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url);
        System.Net.WebResponse myResp = myReq.GetResponse();

        Stream stream = myResp.GetResponseStream();
        //int i;
        using (BinaryReader br = new BinaryReader(stream))
        {
            //i = (int)(stream.Length);
            b = br.ReadBytes(500000);
            br.Close();
        }
        myResp.Close();
        return b;
    }

    public void WriteBytesToFile(string fileName, byte[] content)
    {
        FileStream fs = new FileStream(fileName, FileMode.Create);
        BinaryWriter w = new BinaryWriter(fs);
        try
        {
            w.Write(content);
        }
        finally
        {
            fs.Close();
            w.Close();
        }

    }

    public static System.Drawing.Bitmap ResizeImage(System.Drawing.Image image, int width, int height)
    {
        var destRect = new System.Drawing.Rectangle(0, 0, width, height);
        var destImage = new System.Drawing.Bitmap(width, height);

        destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

        using (var graphics = System.Drawing.Graphics.FromImage(destImage))
        {
            graphics.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;
            graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            graphics.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;

            using (var wrapMode = new System.Drawing.Imaging.ImageAttributes())
            {
                wrapMode.SetWrapMode(System.Drawing.Drawing2D.WrapMode.TileFlipXY);
                graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, System.Drawing.GraphicsUnit.Pixel, wrapMode);
            }
        }

        return destImage;
    }

}

