﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="TrocaSenha.aspx.cs" Inherits="Ferramentas_TrocaSenha" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="/content/css/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        function consistir(sender, arg) {
            with (document.forms[0]) {
                if (txtSenha.value == "") {
                    alert("Senha Atual é campo obrigatório!");
                    (txtSenha.focus());
                    arg.IsValid = false;
                }

                aux = 0;
                xnova = txtNova.value;
                xredigite = txtRedigite.value;
                if (aux == 0) {
                    if (xnova == "") {
                        aux = 1;
                        alert("Preencha o campo Nova Senha");
                        txtNova.focus();
                        arg.IsValid = false;
                    }
                }
                if (aux == 0) {
                    if (xnova != xredigite) {
                        aux = 1;
                        alert("Campo Nova Senha e Redigite precisam ser iguais!");
                        txtNova.focus();
                        arg.IsValid = false;
                    }
                }
            }
        }

        //-->
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <!--Page Title-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <div id="page-title">
        <h1 class="page-header text-overflow">Troca de Senha</h1>

    </div>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End page title-->



    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i>&nbsp;Home</a></li>
        <li>Troca de Senha</li>
        <li class="active"></li>
    </ol>



    <form id="form1" runat="server">


        <asp:Label ID="lblMensagem" runat="server" CssClass="box-danger alert alert-danger" Style="display: block" Visible="false"></asp:Label>

        <div id="page-content">

            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title"></h3>
                </div>
                <div class="panel-body">

                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="ContentPlaceHolder1_Chamada">Senha Atual:</label>
                            <asp:TextBox ID="txtSenha" CssClass="form-control" MaxLength="20" required="required" runat="server" TextMode="Password"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="ContentPlaceHolder1_Chamada">Nova Senha:</label>
                            <asp:TextBox ID="txtNova" CssClass="form-control" MaxLength="20" required="required" runat="server" TextMode="Password"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="ContentPlaceHolder1_Chamada">Redigite:</label>
                            <asp:TextBox ID="txtRedigite" CssClass="form-control" MaxLength="20" required="required" runat="server" TextMode="Password"></asp:TextBox>
                        </div>
                    </div>

                </div>

                <div class="panel-footer text-right">
                    <asp:Button ID="btnSalvar" runat="server" Text="SALVAR" OnClientClick="return consistir();" CssClass="btn btn-success fa fa-check-circle" />


                    <br />
                    <br />
                    <asp:HiddenField ID="hOperacao" runat="server" />
                    <asp:HiddenField ID="hID" runat="server" />
                    <asp:HiddenField ID="hUrlReferrer" runat="server" />
                    <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                </div>



            </div>
        </div>
    </form>





</asp:Content>


