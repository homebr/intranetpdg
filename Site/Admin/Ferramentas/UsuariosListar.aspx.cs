﻿using System;
using ClassLibrary;
using FrameWork.WebControls;
using System.Web;
using System.Web.UI;
using FrameWork.DataObject;
using System.Data;

public partial class Ferramentas_UsuariosListar : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
            CarregaUsuarios();
        
    }
    public void CarregaUsuarios()
    {
        string strSQL = "SELECT  *,CASE WHEN isnull(foto,'') ='' THEN '' ELSE ('<img src=\"/uploads/fotos/' + foto + '\" class=\"thumbnail\" style=\"width:80px;\" />') END  as fotofull  FROM  Usuarios WHERE   1=1 ";
        if (FrameWork.Util.GetString(TextBox1.Text ).Length> 2) {
            strSQL += " LoginAcesso like @text or Nome like @text ";
        }
        strSQL += " ORDER BY LoginAcesso ";
        //Response.Write(strSQL);

        try
        {
            DataTable dt = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSQL, new ParameterItem("@text", TextBox1.Text));
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message.ToString() + " - " + strSQL);
            return;
        }

    }

}