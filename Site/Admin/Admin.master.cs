﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
       // Response.Redirect("/admin/login");
        if (FrameWork.Util.GetString(Session["_uiUser"]) == "")
        {
            Response.Redirect("/admin/login");
        }
        else
        {
            lblNomeUsuario.Text = FrameWork.Util.GetString(Session["_uiNome"]);
            Session["_uiReferer"] = Request.Url.ToString();
            if (FrameWork.Util.GetString(Session["_uiFoto"]).Length > 1)
            {
                imgFotoUser.ImageUrl = "/uploads/fotos/" + FrameWork.Util.GetString(Session["_uiFoto"]);
                //imgFotoUser.Visible = true;
            }

        }
        Session["_uiMenu"] = "";
        CarregaMenu();
    }

    protected void CarregaMenu()
    {
        //comente para não recarregar sempre
        //Session["_uiMenu"] = "";

        if (FrameWork.Util.GetString(Session["_uiMenu"]).Length == 0)
        {

            string strSQL1 = null;
            string strSQL2 = null;
            string strAux = null;
            int pos = 0;
            strAux = "";

            string cIDs = FrameWork.Util.GetString(Session["_uiRights"]);

            //Response.Write("cIDs=" + cIDs + "<br/>");
            //Response.Write("_uiAdmin=" + Session["_uiAdmin"] + "<br/>");
            if (Convert.ToBoolean(Session["_uiAdmin"]) || (cIDs.Length > 0))
            {

                try
                {


                    //",2,3,1,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,20,19" 
                    if (!string.IsNullOrEmpty(FrameWork.Util.GetString(cIDs)))
                    {
                        if (cIDs.Contains(":"))
                        {
                            string[] arrAux = cIDs.Split(':');
                            for (int k = 0; k <= arrAux.Length - 1; k++)
                            {
                                if (arrAux[k].Contains("{"))
                                {
                                    pos = arrAux[k].IndexOf("{");
                                    if (pos > -1)
                                    {
                                        strAux += arrAux[k].Substring(pos + 1) + ",";
                                    }
                                }
                            }
                            cIDs = strAux.Substring(0, strAux.Length - 1);
                        }
                        else if (!string.IsNullOrEmpty(FrameWork.Util.GetString(cIDs)) & cIDs.Contains(":"))
                        {
                            cIDs = cIDs.Replace("}{", ",");
                            cIDs = cIDs.Substring(1, cIDs.Length - 2);
                        }
                        else
                        {
                            cIDs = "0";
                        }
                    }


                    strSQL1 = "SELECT f.ID,f.Formulario, f.TipoFormulario, f.Icone, f.Sequencia,f.Link  " + "FROM Formularios f " + "WHERE (f.formulario in (select distinct formulario from formularios where tipoformulario='S' AND Link <> '' AND formulario <> '' ";
                    //if (!Convert.ToBoolean(Session["_uiAdmin"]))
                    //    strSQL1 += " and id in (" + cIDs + ")";

                    strSQL1 += ") OR (Link <> '' ";

                    //if (!Convert.ToBoolean(Session["_uiAdmin"]))
                    //    strSQL1 += " and id in (" + cIDs + ")";

                    strSQL1 += ")) AND f.TipoFormulario = 'T' AND f.Status = 'A' " + "ORDER BY f.Formulario ";

                    strSQL2 = "SELECT f.ID,f.Formulario, f.SubFormulario, f.Icone, f.TipoFormulario, f.Sequencia,f.Link  FROM Formularios f WHERE 1=1  ";

                    //if (!Convert.ToBoolean(Session["_uiAdmin"]))
                    //    strSQL2 += " and id in (" + cIDs + ")";

                    strSQL2 += " AND f.TipoFormulario = 'S' AND f.Status = 'A' AND f.Formulario <> '' ORDER BY f.Formulario ";

// Response.Write(strSQL1);
                  // Response.Write("<br/><br/>" + strSQL2);


                    //Response.End()
                    DataTable dt = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSQL1);

                    //Dim adpt As New System.Data.SqlClient.SqlDataAdapter(strSQL2, conn)
                    //conn.Open()
                    //adpt.Fill(tbSub)
                    DataTable tbSub = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSQL2);



                    //string strResultsHolder = null;

                    string vaux1 = null;
                    //string vCodigo = null;
                    //string vaux3 = null;



                    int x = 0;
                    int i = 0;
                    string Formulario = null;


                    String Link1 = "<li><a href=\"/Admin/LINK\"><span>ITEM</span><i class=\"arrow\"></i></a></li> \r\n ";
                    String Link2 = "<li><a href=\"/Admin/LINK\">ITEM</a></li> \r\n ";
                    String Submenus = "<li>  \r\n " +
                        "<a href=\"#\" > \r\n " +
                        "    <i class=\"fa fa-circle-o\"></i><span class=\"menu-title\">ITEM</span> \r\n " +
                        "    <i class=\"arrow\"></i> \r\n " +
                        "</a> \r\n " +
                        "<ul class=\"collapse\"> \r\n " +
                        "SUBITENS \r\n " +
                        "</ul> \r\n ";


                    for (int z = 0; z < dt.Rows.Count; z++)
                    {
                        Formulario = FrameWork.Util.GetString(dt.Rows[z]["Formulario"]).Trim();
                        String sLink = FrameWork.Util.GetString(dt.Rows[z]["Link"]);
                        x = x + 1;


                        if ((!string.IsNullOrEmpty(FrameWork.Util.GetString(dt.Rows[z]["Link"]))))
                        {
                            vaux1 += Link1.Replace("ITEM", Formulario).Replace("LINK", sLink).Replace("fa-circle-o", FrameWork.Util.GetString(dt.Rows[z]["icone"]));

                        }
                        else
                        {
                            vaux1 += Submenus.Replace("ITEM", Formulario).Replace("LINK", sLink).Replace("fa-circle-o", FrameWork.Util.GetString(dt.Rows[z]["icone"]));

                        }

                        String vaux2 = "";
                        if (sLink == "")
                        {

                            for (i = 0; i < tbSub.Rows.Count; i++)
                            {
                                String sFormulario = FrameWork.Util.GetString(tbSub.Rows[i]["Formulario"]).Trim();

                                if (Formulario == sFormulario)
                                {

                                    vaux2 += Link1.Replace("ITEM", FrameWork.Util.GetString(tbSub.Rows[i]["SubFormulario"])).Replace("LINK", FrameWork.Util.GetString(tbSub.Rows[i]["Link"]));

                                }
                            }
                            vaux1 = vaux1.Replace("SUBITENS", vaux2);
                        }

                        //break;
                    }


                    ltlMenu.Text = vaux1;

                    Session["_uiMenu"] = vaux1;


                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message + "<br/>");
                }

            }

        }
        else
        {
            ltlMenu.Text = FrameWork.Util.GetString(Session["_uiMenu"]);
        }


    }
}
