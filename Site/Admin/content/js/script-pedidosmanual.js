$(function ($) {

    //atualiza o combo do status
    var idstatus = $("#" + campo + "hStatus").val();
    if (Number(idstatus) > 0) {
        $("#situacao_id option[value=" + idstatus + "]").attr("selected", "selected");
    }

    //atualiza o combo do bandeira
    //var bandeira = $("#" + campo + "hCobranca_Bandeira").val();
    //if (bandeira.length > 0) {
    //    $("#deposito_banco option[value=" + bandeira + "]").attr("selected", "selected");
    //}


    SomaValores();


});

function SomaValores() {
    valortotalitens = 0;
    $(".lista-itens .item").each(function (i) {
        //valorunitario = $(this).find("#hvalortotalproduto").val().replace(".", "").replace(",", ".");
        //qtd = $(this).find("#hquantidade").val();
        //valorfinalitem = parseFloat(parseFloat(valorunitario) * parseFloat(qtd));
        valorfinalitem = $(this).find(".hvalortotalproduto").val().replace(".", "").replace(",", ".");
        valorText = numberToReal(parseFloat(valorfinalitem, 10));
        valortotalitens = parseFloat(valortotalitens) + parseFloat(valorfinalitem);
        //console.log("valorText=" + valorText);
        tvalor = $(this).find(".valortotal").html(valorText);
    });


    subtotal = valortotalitens;
    $("#hvalorsubtotal").val(subtotal);

    auxfrete = $("#ContentPlaceHolder1_txtFrete").val().replace(".", "").replace(",", ".");
    valorfrete = 0;
    if (auxfrete != "")
        valorfrete = parseFloat(auxfrete);

    valorsemdesconto = subtotal + valorfrete;

    //console.log("valChanged=" + d + " - " + id);
    //valor = $("#" + id).data("valor-unitario").replace(".", "").replace(",", ".");
    //valorfinal = parseFloat(parseFloat(valor) * parseFloat(d));
    //valorText = numberToReal(parseFloat(valorfinal, 10));
    //console.log("valorText=" + valorText);
    $("#hvalorsemdesconto").val(valorsemdesconto);
    $("#valor-total-semdesconto").val(valorsemdesconto);

    auxdesconto = $("#ContentPlaceHolder1_txtDesconto").val().replace(".", "").replace(",", ".");

    desconto = 0;
    if (auxdesconto != "")
        desconto = parseFloat(auxdesconto);
       
    valortotal = valorsemdesconto - desconto;

    $(".subtotal .valor-subtotal").html(numberToReal(subtotal));
    $(".box-total .subtotal .valor-desconto").html(numberToReal(desconto));
    $(".box-total .total .valor-total").html(numberToReal(valortotal));
    $("#hvalortotal").val(parseFloat(valortotal).toFixed(2));

    //pagamento por deposito com desconto
    valortotalavista = (valortotal - ((valortotal / 100) * 10));
    $(".box-total .valores-descontos .avista .texto").html(numberToReal(valortotalavista));
    $("#valor-total-boleto").val(valortotalavista);


    valortotalparcelado = (valortotal / 3);
    $(".box-total .valores-descontos .parcelas .texto").html(numberToReal(valortotalparcelado));
    $("#valor-total-parcelado").val(valortotalparcelado);
}

function deleteItem(obj, idpedido, iditem) {
    if (confirm('Confirma exclus�o do item!')) {
        data = {
            id: idpedido,
            iditem: iditem,
            modulo: "excluiritem"
        };
        $.post("ws-venda.ashx", data,
            function (msg) {
                if (msg['response'] == "ok") {
                    e = $(obj).parent().parent();
                    e.remove();
                    SomaValores();
                }
            }
        );



    }
}

function numberToReal(numero) {
    numero = numero.toFixed(2).split('.');
    numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
    return numero.join(',');
}

function replaceAll(str, mapObj) {
    var re = new RegExp(Object.keys(mapObj).join("|"), "gi");

    return str.replace(re, function (matched) {
        return mapObj[matched.toLowerCase()];
    });
}


function boxCarregando() {
    $(".bg-load").removeClass('hide');
    $(".bg-load").fadeIn(2000);
}
function closeBoxCarregando() {
    $(".bg-load").fadeOut(2000);
}
