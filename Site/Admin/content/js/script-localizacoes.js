﻿jQuery(document).ready(function () {

    //var campo = "" + campo + "";


    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
    });


    //cidades
    $(".cidades-adicionar").click(function () {
        if ($("#" + campo + "txtCidade").val() == "") {
            alert("Preencher o Nome da Cidade!");
            return false;
        }

        var result = 0;
        var regiao = $("input:radio[name='cidades_regiao']:checked").val();
        console.log("cidades_regiao: " + regiao);
        console.log("cidades_padrao: " + $('#cidades_padrao').val());
        console.log("ddlEstados: " + $("#" + campo + "ddlEstados").val() + " - " + $("#" + campo + "ddlEstados").find("option:selected").text());

        //var selected = $("input[type='radio'][name='s_2_1_6_0']:checked");
        var id = $('#hIDCidade').val();

        var padrao = "";
        if ($('#cidades_padrao').parent().hasClass("checked"))
            padrao = "true";

        $.post("wsLocalizacoes.ashx?modulo=cidade&acao=inserir&id=" + id + "&idestado=" + $("#" + campo + "ddlEstados").val() + "&cidade=" + $("#" + campo + "txtCidade").val() + "&regiao=" + regiao + "&padrao=" + padrao, function (data) {
            result = data;

            if (Number(id) > 0) {
                $("#datatableCidades").find("tr").each(function (index) {
                    $(this).find("td").each(function (index) {
                        //console.log(index + ": " + $(this).html());
                        if (index == 4 && $(this).html().indexOf('data-value="' + result + '"') > 0)
                        {
                            var z = $(this).parent();

                            $(z).find("td").each(function (index) {
                                if (index==0)
                                    $(this).html($("#" + campo + "ddlEstados").find("option:selected").text());
                                else if (index == 1)
                                    $(this).html($("#" + campo + "txtCidade").val());
                                else if (index == 2)
                                    $(this).html(regiao);
                                else if (index == 3 && padrao == "true")
                                    $(this).html("<i class=\"fa fa-check\"></i>");
                            });
                        }
                    });
                });
            }
            else {

                var sHtml = "<tr><td>" + $("#" + campo + "ddlEstados").find("option:selected").text() + "</td><td>" + $("#" + campo + "txtCidade").val() + "</td><td>" + regiao + "</td><td>";
                if (padrao == "true")
                    sHtml += "<i class=\"fa fa-check\"></i>";

                sHtml += "</td><td class='center'>";
                //sHtml += "<a class=\"ficha-remove-img btn btn-danger hover-link\" data-value=\"" + result + "\">";
                sHtml += "<a href='javascript:void(0);' onclick='removerCidades(" + result + ",this);' class=\"btn btn-danger hover-link\" id=\"trLugares" + result + "\" data-value=\"" + result + "\">";
                sHtml += "<i class=\"fa fa-trash-o fa-1x\"></i></a>";
                sHtml += "</td></tr>";

                e = $('#datatableCidades tbody');
                e.append(sHtml);
                //$('#box_tab3').html($('#box_tab3').html() + "<p>&nbsp;</p>");

            }

            $('#hIDCidade').val("");
            $("#" + campo + "txtCidade").val("");
            $('#cidades_regiao').val("");

            var padrao = $("#cidades_padrao");
            padrao.val("");
            var div = padrao.parent();
            div.removeClass("checked");
            div.attr("aria-checked", "false");

        });

        return false;
    });

    $(".cidades-editar").click(function () {

        var id = $(this).data('value');
        var z = $(this).parent().parent();

        //console.log("z: " + z.html());
        var arr = new Array();;

        $(z).find("td").each(function (index) {
            //console.log(index + ": " + $(this).html());
            arr[index] = $(this).html();
        });

        $('#hIDCidade').val(id);
        //$("#" + campo + "ddlEstados").val(arr[0]);
        //$("#" + campo + "ddlEstados option[text='" + arr[0] + "']").attr('selected', 'selected');
        $("#" + campo + "ddlEstados option").filter(function () {
            return $(this).text() == arr[0];
        }).attr('selected', true);

        $("#" + campo + "txtCidade").val(arr[1]);
        // $('#cidades_padrao').val(arr[3]);

        $(".iradio_minimal-blue").each(function (index) {
            //console.log(index + ": " + $(this).html());
            $(this).removeClass("checked");
            $(this).attr("aria-checked", "false");
            //$(this + " input").attr("checked", "");
        });

        if (arr[2] == "Capital")
            ch = $('input:radio[id=cidades_regiao_capital]');
        else if (arr[2] == "Interior")
            ch = $('input:radio[id=cidades_regiao_interior]');
        else if (arr[2] == "Litoral")
            ch = $('input:radio[id=cidades_regiao_litoral]');
        else
            ch = $('input:radio[id=cidades_regiao_desativado]');


        

        ch.attr('checked', 'checked');
        var chdiv = ch.parent();
        chdiv.addClass("checked");
        chdiv.attr("aria-checked", "true");

        //console.log(arr[3] + " - " +arr[3].indexOf("fa-check"));
        var padrao = $("#cidades_padrao");
        //console.log(padrao);
        if (arr[3].indexOf("fa-check") > 0) {
            padrao.attr("checked", "checked");
            var chdiv = padrao.parent();
            chdiv.addClass("checked");
            chdiv.attr("aria-checked", "true");
        }
        else {
            padrao.removeAttr("checked");
            var chdiv = padrao.parent();
            chdiv.removeClass("checked");
            chdiv.attr("aria-checked", "false");
        }
            


        return false;
    });

    $(".cidades-remover").click(function () {
        if (confirm('Confirma exclusão!')) {
            $.post("wsLocalizacoes.ashx?modulo=cidade&acao=remover&id=" + $(this).data('value'), function (data) {
            });

            z = $(this).parent().parent();
            z.hide();

            $('#hIDCidade').val("");
            $("#" + campo + "txtCidade").val("");
            $('#cidades_regiao').val("");

            var padrao = $("#cidades_padrao");
            padrao.val("");
            var div = padrao.parent();
            div.removeClass("checked");
            div.attr("aria-checked", "false");


            return false;
        }

    });

    //bairros
    $(".bairros-adicionar").click(function () {
        if ($("#" + campo + "txtBairro").val() == "") {
            alert("Preencher o Nome do Bairro!");
            return false;
        }

        var result = 0;
        //console.log("cidades_regiao: " + regiao);
        //console.log("cidades_padrao: " + $('#cidades_padrao').val());
        //console.log("ddlEstados: " + $("#" + campo + "ddlEstados").val() + " - " + $("#" + campo + "ddlEstados").find("option:selected").text());

        //var selected = $("input[type='radio'][name='s_2_1_6_0']:checked");
        var id = $('#hIDBairro').val();

        $.post("wsLocalizacoes.ashx?modulo=bairro&acao=inserir&id=" + id + "&idcidade=" + $("#ddlBairros_Cidades").find("option:selected").val() + "&bairro=" + $("#" + campo + "txtBairro").val(), function (data) {
            result = data;

            if (Number(id) > 0) {
                $("#datatableBairros").find("tr").each(function (index) {
                    $(this).find("td").each(function (index) {
                        //console.log(index + ": " + $(this).html());
                        if (index == 4 && $(this).html().indexOf('data-value="' + result + '"') > 0) {
                            var z = $(this).parent();

                            $(z).find("td").each(function (index) {
                                if (index == 0)
                                    $(this).html($('#ddlBairros_Cidades').find("option:selected").text());
                                else if (index == 1)
                                    $(this).html($("#" + campo + "txtBairro").val());

                            });
                        }
                    });
                });
            }
            else {

                var sHtml = "<tr><td>" + $('#ddlBairros_Cidades').find("option:selected").text() + "</td><td>" + $("#" + campo + "txtBairro").val() + "</td>";
                sHtml += "<td class='center'>";
                sHtml += "<a href='javascript:void(0);' onclick='removerBairros(" + result + ",this);' class=\"btn btn-danger hover-link\" id=\"trLugares" + result + "\" data-value=\"" + result + "\">";
                sHtml += "<i class=\"fa fa-trash-o fa-1x\"></i></a>";
                sHtml += "</td></tr>";

                e = $('#datatableBairros tbody');
                e.append(sHtml);
                //$('#box_tab3').html($('#box_tab3').html() + "<p>&nbsp;</p>");

            }

            $('#hIDBairro').val("");
            $("#" + campo + "txtBairro").val("");

        });

        return false;
    });

    $(".bairros-editar").click(function () {

        var id = $(this).data('value');
        var z = $(this).parent().parent();

        //console.log("z: " + z.html());
        var arr = new Array();;

        $(z).find("td").each(function (index) {
            //console.log(index + ": " + $(this).html());
            arr[index] = $(this).html();
        });

        $('#hIDBairro').val(id);

        var varOption = arr[0].split(" - ");
        // optgroup[label='" + varOption[0] + "']
        //var optgroup = $('#ddlBairros_Cidades optgroup[label="Amazonas"]');
        //console.log(optgroup.html());

        //$("#ddlBairros_Cidades #optgroupAmazonas option").filter(function () {
        //    return $(this).text() == varOption[1];
        //}).attr('selected', true);

        var aux = $("#ddlBairros_Cidades #optgroupAmazonas");
        //console.log(aux.html());
        //$(aux).prop('selected', true);
        //$("#ddlBairros_Cidades #optgroupAmazonas option").filter(function () {
        //    return $(this).text() == varOption[1];
        //}).attr('selected', true);

        $("#select2-ddlBairros_Cidades-container").attr("title", varOption[1]);
        $("#select2-ddlBairros_Cidades-container").html(varOption[1]);

        $("#" + campo + "txtBairro").val(arr[1]);

        //$("label[for='comedyclubs']")

        return false;
    });

    $(".bairros-remover").click(function () {
        if (confirm('Confirma exclusão!')) {
            $.post("wsLocalizacoes.ashx?modulo=bairro&acao=remover&id=" + $(this).data('value'), function (data) {
            });

            z = $(this).parent().parent();
            z.hide();

            $('#hIDBairro').val("");
            $("#" + campo + "txtBairro").val("");

            return false;
        }

    });
});


function removerCidades(id, obj) {

    $.post("wsLocalizacoes.ashx?modulo=cidade&acao=remover&id=" + id, function (data) {
    });
    //alert("removerFicha=" + obj.id);
    z = $("#" + obj.id).parent().parent();
    //alert("z=" + z.html());
    z.hide();

    $('#hIDCidade').val("");
    $("#" + campo + "txtCidade").val("");
    $('#cidades_regiao').val("");

    var padrao = $("#cidades_padrao");
    padrao.val("");
    var div = padrao.parent();
    div.removeClass("checked");
    div.attr("aria-checked", "false");

    return false;

}

function removerBairros(id, obj) {

    $.post("wsLocalizacoes.ashx?modulo=bairro&acao=remover&id=" + id, function (data) {
    });
    //alert("removerFicha=" + obj.id);
    z = $("#" + obj.id).parent().parent();
    //alert("z=" + z.html());
    z.hide();

    $('#hIDBairro').val("");
    $("#" + campo + "txtBairro").val("");


    return false;

}


