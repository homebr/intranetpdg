﻿jQuery(document).ready(function () {

    var date = new Date();
    date.setDate(date.getMonth() - 1);
    var iAno = date.getFullYear();
    var iAnoAnterior = date.getFullYear()-1;

    //DADOS 
    var monthsShort = ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"];
    var colors = ['#FF0000', '#C0C0C0', '#1c1c1c', '#6A5ACD', '#FF8C00', '#B0C4DE', '#B0E0E6', '#00b5f7', '#EEE8AA', '#EEDD82', '#CDB7B5', '#DA70D6', '#FFA500', '#ffb805'];

    //-------------------- TAB 1 - GRAFICO DONUT  --------------

    var iMeses = 12;
    var iLinha = 0;

    objOcc = $("#tabela_ocupacao tbody");
    var aux_data = "[";
    for (x = 0; x < iMeses; x++) {

        value_1 = $(objOcc).find("tr:eq(0) td:eq(" + (x + 1) + ")").text().replace("%", "");
        value_2 = $(objOcc).find("tr:eq(1) td:eq(" + (x + 1) + ")").text().replace("%", "");
        value_3 = $(objOcc).find("tr:eq(2) td:eq(" + (x + 1) + ")").text().replace("%", "");

        if (x > 0)
            aux_data += ", ";
        aux_data += "{\"mes\": \"" + monthsShort[x] + "\"";
        if (value_1.length > 0)
            aux_data += ", \"r1\": " + value_1;
        if (value_2.length > 0)
            aux_data += ", \"r2\": " + value_2;
        if (value_3.length > 0)
            aux_data += ", \"r3\": " + value_3;
        aux_data += "}";
    }
    aux_data += "]";

    var data_bar_valor = JSON.parse(aux_data);

    var letras_bar = ['r1', 'r2', 'r3'];
    var labels_bar = ['Occ ' + iAno + ' (Real)', 'Occ ' + iAno + ' (Orçado)', 'Occ ' + iAnoAnterior];
    var i_ymax_valor = 100;

    $("#grafico-ocupacao").html(""); //destroy
    grafico_ocupacao = Morris.Line({
        element: 'grafico-ocupacao',
        data: data_bar_valor,
        xkey: 'mes',
        ykeys: letras_bar,
        ymax: i_ymax_valor,
        labels: labels_bar,
        postUnits: "%",
        gridEnabled: true,
        gridLineColor: 'rgba(0,0,0,.1)',
        gridTextColor: '#8f9ea6',
        gridTextSize: '10px',
        lineColors: colors,
        xLabelColors: colors,
        xLabelMargin: 2,
        lineWidth: 2,
        parseTime: false,
        resize: true,
        hideHover: true
    });

    //yLabelFormat: function (y) {
    //    var parts = y.toString().split(".");
    //    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    //    return parts.join(",");
    //},

    //grafico_ocupacao.prototype.drawGridLine = function (path) {
    //    return this.raphael.path(path).attr('stroke', this.options.gridLineColor).attr('stroke-width', this.options.gridStrokeWidth).attr('stroke-dasharray', this.options.gridDashed);
    //};

    value_1 = $(objOcc).find("tr:eq(0) td:eq(12)").text().replace("%", "");
    value_2 = $(objOcc).find("tr:eq(1) td:eq(12)").text().replace("%", "");
    value_3 = $(objOcc).find("tr:eq(2) td:eq(12)").text().replace("%", "");

    for (x = 0; x < 3; x++) {
        value_1 = $(objOcc).find("tr:eq(" + x + ") td:eq(13)").text();
        var legendItem = $('<span class="item"></span>').prepend('<br><span>' + value_1 + '</span>');
        legendItem.find('span')
            .css('backgroundColor', colors[x]);
        $('#grafico-ocupacao-legenda').append(legendItem);
    }
    

    //-------------------- TAB 2 - GRAFICO ADR  --------------
    iLinha = 0;

    objOcc = $("#tabela_adr tbody");
    aux_data = "[";
    for (x = 0; x < iMeses; x++) {

        value_1 = $(objOcc).find("tr:eq(0) td:eq(" + (x + 1) + ")").text().replace("%", "");
        value_2 = $(objOcc).find("tr:eq(1) td:eq(" + (x + 1) + ")").text().replace("%", "");
        value_3 = $(objOcc).find("tr:eq(2) td:eq(" + (x + 1) + ")").text().replace("%", "");

        if (x > 0)
            aux_data += ", ";
        aux_data += "{\"mes\": \"" + monthsShort[x] + "\"";
        if (value_1.length > 0)
            aux_data += ", \"r1\": " + value_1;
        if (value_2.length > 0)
            aux_data += ", \"r2\": " + value_2;
        if (value_3.length > 0)
            aux_data += ", \"r3\": " + value_3;
        aux_data += "}";
    }
    aux_data += "]";

    data_bar_valor = JSON.parse(aux_data);

    letras_bar = ['r1', 'r2', 'r3'];
    labels_bar = ['ADR ' + iAno + ' (Real)', 'ADR ' + iAno + ' (Orçado)', 'ADR ' + iAnoAnterior];
    i_ymax_valor = 300;

    $("#grafico-adr").html(""); //destroy
    grafico_adr = Morris.Line({
        element: 'grafico-adr',
        data: data_bar_valor,
        xkey: 'mes',
        ykeys: letras_bar,
        ymax: i_ymax_valor,
        labels: labels_bar,
        gridEnabled: true,
        preUnits: "$",
        gridLineColor: 'rgba(0,0,0,.1)',
        gridTextColor: '#8f9ea6',
        gridTextSize: '10px',
        lineColors: colors,
        xLabelColors: colors,
        xLabelMargin: 2,
        lineWidth: 2,
        parseTime: false,
        resize: true,
        hideHover: true
    });

    value_1 = $(objOcc).find("tr:eq(0) td:eq(12)").text().replace("%", "");
    value_2 = $(objOcc).find("tr:eq(1) td:eq(12)").text().replace("%", "");
    value_3 = $(objOcc).find("tr:eq(2) td:eq(12)").text().replace("%", "");

    for (x = 0; x < 3; x++) {
        value_1 = $(objOcc).find("tr:eq(" + x + ") td:eq(13)").text();
        legendItem = $('<span class="item"></span>').prepend('<br><span>$' + value_1 + '</span>');
        legendItem.find('span')
            .css('backgroundColor', colors[x]);
        $('#grafico-adr-legenda').append(legendItem);
    }


    //-------------------- TAB 3 - GRAFICO REVPAR  -------------- 
    iLinha = 0;

    objOcc = $("#tabela_RevPAR tbody");
    aux_data = "[";
    for (x = 0; x < iMeses; x++) {

        value_1 = $(objOcc).find("tr:eq(0) td:eq(" + (x + 1) + ")").text().replace("%", "");
        value_2 = $(objOcc).find("tr:eq(1) td:eq(" + (x + 1) + ")").text().replace("%", "");
        value_3 = $(objOcc).find("tr:eq(2) td:eq(" + (x + 1) + ")").text().replace("%", "");

        if (x > 0)
            aux_data += ", ";
        aux_data += "{\"mes\": \"" + monthsShort[x] + "\"";
        if (value_1.length > 0)
            aux_data += ", \"r1\": " + value_1;
        if (value_2.length > 0)
            aux_data += ", \"r2\": " + value_2;
        if (value_3.length > 0)
            aux_data += ", \"r3\": " + value_3;
        aux_data += "}";
    }
    aux_data += "]";

    data_bar_valor = JSON.parse(aux_data);

    letras_bar = ['r1', 'r2', 'r3'];
    labels_bar = ['RevPAR ' + iAno + ' (Real)', 'RevPAR ' + iAno + ' (Orçado)', 'RevPAR ' + iAnoAnterior];
    i_ymax_valor = 200;

    $("#grafico-RevPAR").html(""); //destroy
    grafico_RevPAR = Morris.Line({
        element: 'grafico-RevPAR',
        data: data_bar_valor,
        xkey: 'mes',
        ykeys: letras_bar,
        ymax: i_ymax_valor,
        ymin: 20,
        labels: labels_bar,
        preUnits: '$',
        gridEnabled: true,
        gridLineColor: 'rgba(0,0,0,.1)',
        gridTextColor: '#8f9ea6',
        gridTextSize: '10px',
        lineColors: colors,
        xLabelColors: colors,
        xLabelMargin: 2,
        lineWidth: 2,
        parseTime: false,
        resize: true,
        hideHover: true
    });

    value_1 = $(objOcc).find("tr:eq(0) td:eq(12)").text().replace("%", "");
    value_2 = $(objOcc).find("tr:eq(1) td:eq(12)").text().replace("%", "");
    value_3 = $(objOcc).find("tr:eq(2) td:eq(12)").text().replace("%", "");

    for (x = 0; x < 3; x++) {
        value_1 = $(objOcc).find("tr:eq(" + x + ") td:eq(13)").text();
        legendItem = $('<span class="item"></span>').prepend('<br><span>$' + value_1 + '</span>');
        legendItem.find('span')
            .css('backgroundColor', colors[x]);
        $('#grafico-RevPAR-legenda').append(legendItem);
    }

    $(".morris-line path[stroke=#c0c0c0]").attr("stroke-dasharray", "4");
    $(".morris-line path[stroke=#1c1c1c]").attr("stroke-dasharray", "4");

});
