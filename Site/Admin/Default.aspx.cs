﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{

    public string[] sAux = new string[8];
    public string[] sAuxOrigens = new string[8];
    public String sLabels = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (FrameWork.Util.GetString(Session["_uiUser"]) == "")
            {
                Response.Redirect("/admin/login");
            }

            //trata o periodo
            String strSqlWhere = "";
              if (FrameWork.Util.GetString(Request["p"]) == "h")
                  strSqlWhere += "where Convert(char(8),DataInclusao,112) = '" + String.Format("{0:yyyyMMdd}", DateTime.Now) + "' ";
              else if (FrameWork.Util.GetString(Request["p"]) == "s")
                  strSqlWhere += "where Convert(char(8),DataInclusao,112) between '" + String.Format("{0:yyyyMMdd}", DateTime.Now.AddDays(-7)) + "' and '" + String.Format("{0:yyyyMMdd}", DateTime.Now) + "' ";
              else
                  strSqlWhere += "where Convert(char(6),DataInclusao,112) = '" + String.Format("{0:yyyyMM}", DateTime.Now) + "' ";

            /*
            String strSql = "SELECT COUNT(*) AS QTD FROM FALECONOSCO " + strSqlWhere;
                   lblQtdFaleConosco.Text = String.Format("{0:00}", FrameWork.DataObject.DataSource.DefaulDataSource.Execute(strSql));
                     strSql = "SELECT COUNT(*) AS QTD FROM NewsLetter " + strSqlWhere;
                     ltlQtdNewsletter.Text = String.Format("{0:00}", FrameWork.DataObject.DataSource.DefaulDataSource.Execute(strSql));

                     //PIZZA CHART - ORIGENS
                     strSql = " SELECT  ";
                     strSql += "( SELECT isnull(count(VisitanteID), 0) as qtd FROM Visitantes WHERE Referer like '%google.com%' " + strSqlWhere.Replace("DataInclusao", "Data").Replace("where", "and") + ")  as Google ";
                     strSql += ",( SELECT isnull(count(VisitanteID), 0) as qtd FROM Visitantes WHERE Referer like '%facebook.com%' " + strSqlWhere.Replace("DataInclusao", "Data").Replace("where", "and") + ")  as Facebook ";
                     strSql += ",( SELECT isnull(count(VisitanteID), 0) as qtd FROM Visitantes WHERE Referer like '%youtube.com%' " + strSqlWhere.Replace("DataInclusao", "Data").Replace("where", "and") + ")  as Youtube ";
                     strSql += ",( SELECT isnull(count(VisitanteID), 0) as qtd FROM Visitantes WHERE Referer like '%googleadservices.com%' " + strSqlWhere.Replace("DataInclusao", "Data").Replace("where", "and") + ")  as Ads ";
                     strSql += ",( SELECT isnull(count(VisitanteID), 0) as qtd FROM Visitantes WHERE Referer like '%android-app%' " + strSqlWhere.Replace("DataInclusao", "Data").Replace("where", "and") + ")  as AndroidApp ";
                     strSql += ",(SELECT isnull(count(VisitanteID), 0) as qtd FROM Visitantes WHERE Referer like '%outlook%' " + strSqlWhere.Replace("DataInclusao", "Data").Replace("where", "and") + ")  ";
                     strSql += ",(SELECT isnull(count(VisitanteID), 0) as qtd FROM Visitantes WHERE Referer like '%bing%' " + strSqlWhere.Replace("DataInclusao", "Data").Replace("where", "and") + ")  as Bing ";
                     strSql += ",(SELECT isnull(count(VisitanteID), 0) as qtd FROM Visitantes WHERE (Origem like 'mkt%' or Origem='Email_Marketing' or Referer like '%o=mkt%') " + strSqlWhere.Replace("DataInclusao", "Data").Replace("where", "and") + ")  as MKT ";

                     DataTable dtPieChart = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSql);

                     if (dtPieChart.Rows.Count > 0)
                     {
                         sAuxOrigens[0] = FrameWork.Util.GetString(dtPieChart.Rows[0][0]);
                         sAuxOrigens[1] = FrameWork.Util.GetString(dtPieChart.Rows[0][1]);
                         sAuxOrigens[2] = FrameWork.Util.GetString(dtPieChart.Rows[0][2]);
                         sAuxOrigens[3] = FrameWork.Util.GetString(dtPieChart.Rows[0][3]);
                         sAuxOrigens[4] = FrameWork.Util.GetString(dtPieChart.Rows[0][4]);
                         sAuxOrigens[5] = FrameWork.Util.GetString(dtPieChart.Rows[0][5]);
                         sAuxOrigens[6] = FrameWork.Util.GetString(dtPieChart.Rows[0][6]);
                         sAuxOrigens[7] = FrameWork.Util.GetString(dtPieChart.Rows[0][7]);

                     }
                     */

            //QTD DE VISITAS
           String strSql = "SELECT COUNT(*) AS QTD FROM Log_Acessos " + strSqlWhere.Replace("DataInclusao", "DataAcesso");
                      ltlQtdVisitas.Text = String.Format("{0:00}", FrameWork.DataObject.DataSource.DefaulDataSource.Execute(strSql));


            //QTD DE LOGINS
            strSql = "SELECT top 50 loginrede,dataacesso FROM Log_Acessos   WHERE CONVERT(char(8),dataacesso,112)=CONVERT(char(8),getdate(),112)  ORDER BY dataacesso DESC  ";
            RepeaterAcessos.DataSource = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSql);
            RepeaterAcessos.DataBind();


            /*
                        if (Convert.ToBoolean(Session["_uiAdmin"]) || Funcoes.VeCheckUserRight(20, "E"))
                        {
                            //QTD DE VENDAS
                            strSql = "SELECT COUNT(*) AS QTD FROM VENDAS " + strSqlWhere.Replace("DataInclusao", "Data") + " and Status < 3 and idformapagamento>0 ";
                            ltlQtdVendas.Text = String.Format("{0:00}", FrameWork.DataObject.DataSource.DefaulDataSource.Execute(strSql));
                            //Response.Write(strSql);
                            //Response.Write(Convert.ToBoolean(Session["_uiAdmin"]));
                            //Response.Write(Funcoes.VeCheckUserRight(20, "E"));

                            

                            pnlVendas.Visible = true;
                        }
                        */


        }
    }
    
}