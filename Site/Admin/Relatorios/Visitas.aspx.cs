﻿using System;
using ClassLibrary;
using FrameWork.WebControls;
using System.Web;
using System.Web.UI;
using FrameWork.DataObject;
using System.Data;
using System.Web.UI.WebControls;


public partial class Visitas : System.Web.UI.Page
{
    public String lineChartData = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
           
            txtDataInicial.Text = FrameWork.Util.GetString(Session["Visitas_DataInicial"]);
            txtDataFinal.Text = FrameWork.Util.GetString(Session["Visitas_DataFinal"]);

            if (FrameWork.Util.GetString(txtDataInicial.Text).Length == 0)
                txtDataInicial.Text = "01/" + string.Format("{0:MM/yyyy}", DateTime.Now);

            if (FrameWork.Util.GetString(txtDataFinal.Text).Length == 0)
                txtDataFinal.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);

        }

        Carrega();
    }





    public void Carrega()
    {

        string strSQLWhere = " WHERE 1=1 ";

     
        if (Funcoes.IsDate(txtDataInicial.Text))
        {
            //FlagFiltro = True
            strSQLWhere += " and Convert(char(8),dataacesso,112) >= '" + String.Format("{0:yyyyMMdd}", Convert.ToDateTime(txtDataInicial.Text)) + "'  ";
            Session["Visitas_DataInicial"] = txtDataInicial.Text;
        }
        if (Funcoes.IsDate(txtDataFinal.Text))
        {
            //FlagFiltro = True
            strSQLWhere += " and Convert(char(8),dataacesso,112) <= '" + String.Format("{0:yyyyMMdd}", Convert.ToDateTime(txtDataFinal.Text)) + "'  ";
            Session["Visitas_DataFinal"] = txtDataFinal.Text;
        }


        string strSQL = "";

        strSQL = @"SELECT convert(char(8),dataacesso,112) as dia,convert(char(10),dataacesso,20) as dia2,(substring(convert(char(8),dataacesso,112),7,2)+'/'+substring(convert(char(8),dataacesso,112),5,2)+'/'+substring(convert(char(8),dataacesso,112),1,4)) as legenda,count(*) as qtd 
            FROM    Log_Acessos ";
        strSQL += strSQLWhere;
        strSQL += " group by convert(char(8), dataacesso, 112),convert(char(10),dataacesso,20) order by dia ";

        try
        {

            DataTable dtLineChart = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSQL);
            for (int n = 0; n < dtLineChart.Rows.Count; n++)
            {
                if (lineChartData.Length > 1)
                    lineChartData += ", ";
                lineChartData += "{y: '" + FrameWork.Util.GetString(dtLineChart.Rows[n]["dia2"]) + "', item1: " + FrameWork.Util.GetString(dtLineChart.Rows[n]["qtd"]) + "}";
            }


            //DataTable dt = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSQL);
            //Repeater1.DataSource = dt;
            //Repeater1.DataBind();


        }
        catch (Exception ex)
        {
            Response.Write(ex.Message.ToString() + " - " + strSQL);
            return;
        }




       
    }

    public int sTotalVisitasRegionais = 0;



}