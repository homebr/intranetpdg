﻿<%@ Page Title="" Language="C#" MasterPageFile="/Admin/Admin.master" AutoEventWireup="true" CodeFile="Visitas.aspx.cs" Inherits="Visitas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!-- JQUERY UI-->
    <link rel="stylesheet" type="text/css" href="/js/jquery-ui-1.10.3.custom/css/custom-theme/jquery-ui-1.10.3.custom.min.css" />
    <!-- DATE RANGE PICKER -->
    <link rel="stylesheet" type="text/css" href="/js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
    <!-- DATA TABLES -->
    <link rel="stylesheet" type="text/css" href="/js/datatables/media/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" type="text/css" href="/js/datatables/media/assets/css/datatables.min.css" />
    <link rel="stylesheet" type="text/css" href="/js/datatables/extras/TableTools/media/css/TableTools.min.css" />

    <!-- DATE PICKER -->
    <link rel="stylesheet" type="text/css" href="/js/datepicker/themes/default.min.css" />
    <link rel="stylesheet" type="text/css" href="/js/datepicker/themes/default.date.min.css" />
    <link rel="stylesheet" type="text/css" href="/js/datepicker/themes/default.time.min.css" />

    <!-- JQUERY UI-->
    <link rel="stylesheet" type="text/css" href="/js/jquery-ui-1.10.3.custom/css/custom-theme/jquery-ui-1.10.3.custom.min.css" />

    <!-- ANIMATE -->
    <link rel="stylesheet" type="text/css" href="/css/animatecss/animate.min.css" />
    <!-- TODO -->
    <link rel="stylesheet" type="text/css" href="/js/jquery-todo/css/styles.css" />
    <!-- FULL CALENDAR -->
    <link rel="stylesheet" type="text/css" href="/js/fullcalendar/fullcalendar.min.css" />
    <!-- GRITTER -->
    <link rel="stylesheet" type="text/css" href="/js/gritter/css/jquery.gritter.css" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <section class="content-header">
        <h1>Visitas
           
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i>Home</a></li>
            <li>Relatório de visitas</li>
        </ol>
    </section>


    <form id="form1" runat="server">
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <!-- BOX -->
                    <div class="box border blue">
                        <div class="box-body">
                            <div class="form-group">
                                <div class="col-md-2">
                                    <label>Data Inclusão de:</label>
                                    <asp:TextBox ID="txtDataInicial" CssClass="form-control datepicker" placehorder="dd/mm/yyyy" runat="server" Width="100px"></asp:TextBox>
                                </div>
                                <div class="col-md-2">
                                    <label>até:</label>
                                    <asp:TextBox ID="txtDataFinal" CssClass="form-control datepicker" placehorder="dd/mm/yyyy" runat="server" Width="100px"></asp:TextBox>
                                </div>
                           
                                <div class="col-md-5">
                                    <br />
                                    <asp:Button ID="btnBuscar" CssClass="btn btn-success fa-search-plus" Text="BUSCAR" runat="server" />
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>


            <div class="row">

                <div class="col-lg-12">

                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Por DIA</h3>
                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body chart-responsive">
                            <div class="chart" id="visitas-diarias" style="height: 200px;"></div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->


                </div>

            </div>


        

        </section>
    </form>


    <!--/PAGE -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="Server">


    <!-- DATE RANGE PICKER -->
    <script src="/js/bootstrap-daterangepicker/moment.min.js"></script>
    <script src="/js/bootstrap-daterangepicker/daterangepicker.min.js"></script>
    <!-- DATE PICKER -->
    <script type="text/javascript" src="/js/datepicker/picker.js"></script>
    <script type="text/javascript" src="/js/datepicker/picker.date.js"></script>
    <script type="text/javascript" src="/js/datepicker/picker.time.js"></script>

    <script type="text/jscript" src="/js/jquery.maskedinput-1.3.1.js"></script>


    <!-- BLOCK UI -->
    <script type="text/javascript" src="/js/jQuery-BlockUI/jquery.blockUI.min.js"></script>

    <!-- EASY PIE CHART -->
    <script src="/js/jquery-easing/jquery.easing.min.js"></script>
    <script type="text/javascript" src="/js/easypiechart/jquery.easypiechart.min.js"></script>
    <!-- FLOT CHARTS -->
    <script src="/js/flot/jquery.flot.min.js"></script>
    <script src="/js/flot/jquery.flot.time.min.js"></script>
    <script src="/js/flot/jquery.flot.selection.min.js"></script>
    <script src="/js/flot/jquery.flot.resize.min.js"></script>
    <script src="/js/flot/jquery.flot.pie.min.js"></script>
    <script src="/js/flot/jquery.flot.stack.min.js"></script>
    <script src="/js/flot/jquery.flot.crosshair.min.js"></script>
    <!-- TODO -->
    <script type="text/javascript" src="/js/jquery-todo/js/paddystodolist.js"></script>
    <!-- TIMEAGO -->
    <script type="text/javascript" src="/js/timeago/jquery.timeago.min.js"></script>
    <!-- FULL CALENDAR -->
    <script type="text/javascript" src="/js/fullcalendar/fullcalendar.min.js"></script>

    <!-- GRITTER -->
    <script type="text/javascript" src="/js/gritter/js/jquery.gritter.min.js"></script>

    <!-- Morris.js charts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="/plugins/morris/morris.min.js"></script>



    <script type="text/javascript">

<%--        var campo = '<%=Repeater1.ID.ToString().Replace("Repeater1","")%>';--%>


        jQuery(function ($) {
            $("#" + campo + "txtDataInicial").mask("99/99/9999");
            $("#" + campo + "txtDataFinal").mask("99/99/9999");

            $(".datepicker").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd/mm/yy',
                dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado', 'Domingo'],
                dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
                dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
                monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez']
            });

        });
    </script>

    <script>
        $(function () {
            "use strict";

            // LINE CHART - visitas diarias
            var line = new Morris.Line({
                element: 'visitas-diarias',
                resize: true,
                data: [
              <%=lineChartData%>
                    //{y: '2011 Q1', item1: 2666},
                ],
                xkey: 'y',
                ykeys: ['item1'],
                xLabelFormat: function (y) { return y.getDate() + '/' + (y.getMonth() + 1) + '/' + y.getFullYear(); },
                labels: ['Qtd'],
                xLabels: 'day',
                lineColors: ['#3c8dbc'],
                hideHover: 'auto',
                lineWidth: 2
            });


        });
    </script>

</asp:Content>

