﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="PastaEditar.aspx.cs" Inherits="PastaEditar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <!--Switchery [ OPTIONAL ]-->
    <link href="/admin/content/plugins/switchery/switchery.min.css" rel="stylesheet">
    <link href="/admin/content/css/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet" type="text/css" />

    <!--Ion Icons [ OPTIONAL ]-->
    <link href="/admin/content/plugins/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="/admin/content/plugins/themify-icons/themify-icons.min.css" rel="stylesheet">
    <script src="/admin/content/js/demo/icons.js"></script>


    <link href="/admin/content/plugins/spectrum/spectrum.css" rel="stylesheet">



    <!-- ANIMATE -->
    <link rel="stylesheet" type="text/css" href="/admin/content/css/animatecss/animate.min.css" />
    <!-- COLORBOX -->
    <link rel="stylesheet" type="text/css" href="/admin/content/js/colorbox/colorbox.min.css" />


    <!-- BOOTSTRAP SWITCH -->
    <link rel="stylesheet" type="text/css" href="/admin/content/js/bootstrap-switch/bootstrap-switch.min.css" />
    <link rel="stylesheet" type="text/css" href="/admin/content/css/themes/default.css" id="skin-switcher">

    <!-- JQUERY UI-->
    <link rel="stylesheet" type="text/css" href="/admin/content/js/jquery-ui-1.10.3.custom/css/custom-theme/jquery-ui-1.10.3.custom.min.css" />

        <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="/admin/content/plugins/iCheck/all.css">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="/admin/content/plugins/colorpicker/bootstrap-colorpicker.min.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="/admin/content/plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="/admin/content/plugins/select2/select2.min.css">


    <!--Magic Checkbox [ OPTIONAL ]-->
    <link href="/admin/content/plugins/magic-check/css/magic-check.min.css" rel="stylesheet">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
     <!--Page Title-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <div id="page-title">
        <h1 class="page-header text-overflow">Segmento Cases</h1>

    </div>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End page title-->

    <!--Breadcrumb-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <ol class="breadcrumb">
        <li><a href="/">Home</a></li>
        <li class="active">Segmento Cases</li>
        <li>Editar</li>
    </ol>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End breadcrumb-->



   <div id="page-content">
        <form id="form1" runat="server">

            <asp:Label ID="lblMensagem" runat="server" CssClass="box-danger alert alert-danger" Style="display: block" Visible="false"></asp:Label>

            
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Dados</h3>
                </div>
                <div class="panel-body">

                            <div class="form-group">
                                <div class="col-sm-6">
                                    <label for="Nome">Pasta:<span class="required">*</span></label>
                                    <asp:TextBox ID="txtPasta" CssClass="form-control" required="required" runat="server" MaxLength="50" Width="100%" placeholder="Pasta"></asp:TextBox>
                                </div>
                                  <div class="col-sm-4">
                                    <label for="Nome">Setor:<span class="required">*</span></label>
                                      <asp:DropDownList ID="ddlSetor" CssClass="form-control" required="required" runat="server">
                                          <asp:ListItem>Selecione</asp:ListItem>
                                          <asp:ListItem>Conselho de Administração</asp:ListItem>
                                          <asp:ListItem>Conselho Fiscal</asp:ListItem>
                                      </asp:DropDownList>
                                </div>
                                 <div class="col-sm-2">
                                    <label for="Nome">Sequencia:<span class="required">*</span></label>
                                    <asp:TextBox ID="txtSequencia" CssClass="form-control" required="required" runat="server" MaxLength="10" Width="100%" placeholder="Sequencia"></asp:TextBox>
                                </div>
                            </div>
                          
                     
                       
                          
                            <div style="clear: both;"></div>
                    <div class="col-sm-12">
                                <div class="separator"></div>
                            </div>
                           
                            <div class="col-sm-12">
                                <div class="separator"></div>
                            </div> 
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label>Ativar em Produção:</label>
                                        <div>
                                            <div class="make-switch switch-small" data-on="success" data-off="danger" data-on-label="<i class='fa fa-check icon-white'></i>" data-off-label="<i class='fa fa-times'></i>">
                                                <asp:CheckBox ID="chkAtivo" runat="server" />
                                            </div>
                                        </div>
                                </div>

                            </div>
                            <div class="col-sm-12">
                                <div class="separator"></div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <label for="lblDataInclusao">Data Inclusão:</label>
                                    <asp:Label ID="lblDataInclusao" runat="server" Text=""></asp:Label>
                                </div>
                                <div class="col-sm-4">
                                    <label for="lblDataAlteracao">Data Alteração:</label>
                                    <asp:Label ID="lblDataAlteracao" runat="server" Text=""></asp:Label>
                                </div>
                                <div class="col-sm-4">
                                    <label for="lblUsuario">Usuário:</label>
                                    <asp:Label ID="lblUsuario" runat="server" Text=""></asp:Label>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Literal ID="ltlLOG" runat="server"></asp:Literal>
                            </div>

                        </div>
                     <div class="panel-footer text-right">
                    <asp:LinkButton ID="btnSalvar" runat="server" CssClass="btn btn-success col-sm-1" OnClick="btnSalvar_Click" ><i class="fa fa-save fa-1x"></i> SALVAR</asp:LinkButton>&nbsp;&nbsp;

                   <asp:LinkButton ID="btnExcluir" runat="server" CssClass="btn btn-warning col-sm-1" OnClick="btnExcluir_Click" Visible="false" ><i class="fa fa-trash fa-1x"></i> EXCLUIR</asp:LinkButton>&nbsp;&nbsp;



                            <asp:Button ID="btnVoltar" runat="server" Text="VOLTAR" CssClass="btn fa-reply" PostBackUrl="BannersListar.aspx" />

                            <br />
                            <br />
                            <asp:HiddenField ID="hOperacao" runat="server" />
                            <asp:HiddenField ID="hID" runat="server" />
                            <asp:HiddenField ID="hUrlReferrer" runat="server" />
                            <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                        </div>
                 </div>


        </form>

    </div>

                <asp:Panel ID="pnlFotos" Visible="true" runat="server">
                    <p>

                        <!-- GALERIA -->
                        <div id="filter-items" class="row">

                            <div class="col-md-3 category_1 item">
                                <div class="filter-content">
                                    <asp:Image ID="Image1" runat="server" CssClass="img-responsive" />
                                </div>
                            </div>
                            <div class="col-md-2 category_1 item">
                                <div class="filter-content">
                                    <asp:Image ID="Image2" runat="server" CssClass="img-responsive" />
                                </div>
                            </div>
                            <div class="col-md-1 category_1 item">
                                <div class="filter-content">
                                    <asp:Image ID="Image3" runat="server" CssClass="img-responsive" />
                                </div>
                            </div>

                        </div>
                        <!-- /GALERIA -->
                    </p>
                </asp:Panel>




</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="Server">
  <!-- JQUERY UI-->
    <link rel="stylesheet" type="text/css" href="/admin/content/js/jquery-ui-1.10.3.custom/css/custom-theme/jquery-ui-1.10.3.custom.min.css" />

    <!-- DATE RANGE PICKER -->
    <script src="/admin/content/js/bootstrap-daterangepicker/moment.min.js"></script>
    <script src="/admin/content/js/bootstrap-daterangepicker/daterangepicker.min.js"></script>


    <!-- bootstrap color picker -->
    <script src="/admin/content/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
    <!-- bootstrap time picker -->
    <script src="/admin/content/plugins/timepicker/bootstrap-timepicker.min.js"></script>

            <!-- iCheck 1.0.1 -->
    <script src="/admin/content/plugins/iCheck/icheck.min.js"></script>


    <!-- BOOTSTRAP SWITCH -->
    <script type="text/javascript" src="/admin/content/js/bootstrap-switch/bootstrap-switch.min.js"></script>

    <!-- ISOTOPE -->
    <script type="text/javascript" src="/admin/content/js/isotope/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="/admin/content/js/isotope/imagesloaded.pkgd.min.js"></script>
    <!-- COLORBOX -->
    <script type="text/javascript" src="/admin/content/js/colorbox/jquery.colorbox.min.js"></script>

    <!-- KNOB -->
    <script type="text/javascript" src="/admin/content/js/jQuery-Knob/js/jquery.knob.min.js"></script>


    <script type="text/jscript" src="/admin/content/js/jquery.maskedinput-1.3.1.js"></script>

    <script type="text/jscript">

        var campo = '<%=hID.ClientID.ToString().Replace("hID","")%>';


        $(document).ready(function () {
            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });

            obj = $("#" + campo + "chkIndiferente").parent();
            obj2 = $(obj).find(".bootstrap-switch-handle-off");
            $(obj2).removeClass('bootstrap-switch-default').addClass('bootstrap-switch-danger');

        });



        $(document).ready(function () {
            $("#" + campo + "txtDataInicial").mask("99/99/9999");
            $("#" + campo + "txtDataFinal").mask("99/99/9999");

            //$(".datepicker").datepicker({
            //    changeMonth: true,
            //    changeYear: true,
            //    dateFormat: 'dd/mm/yy',
            //    dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado', 'Domingo'],
            //    dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
            //    dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
            //    monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            //    monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez']
            //});




            function ChangeItens() {
                if ($("#" + campo + "ddlTipoLink option:selected").val() == "N") {
                    $('.trLink').hide();
                    $('.trScript').hide();
                   // $('.trPixel').hide();
                }
                else if ($("#" + campo + "ddlTipoLink option:selected").val() == "E") {
                    $('.trLink').show();
                    $('.trScript').hide();
                    //$('.trPixel').show();
                }
                else if ($("#" + campo + "ddlTipoLink option:selected").val() == "I") {
                    $('.trLink').show();
                    $('.trScript').hide();
                    //$('.trPixel').show();
                }
                else if ($("#" + campo + "ddlTipoLink option:selected").val() == "H") {
                    $('.trLink').hide();
                    $('.trScript').show();
                    //$('.trPixel').show();
                }
                else if ($("#" + campo + "ddlTipoLink option:selected").val() == "S") {
                    $('.trLink').hide();
                    $('.trScript').show();
                    //$('.trPixel').show();
                }
                else if ($("#" + campo + "ddlTipoLink option:selected").val() == "A") {
                    $('.trLink').show();
                    $('.trScript').hide();
                    //$('.trPixel').show();
                }
            }

            $("#" + campo + "ddlTipoLink").change(function () {
                ChangeItens();
            });

            ChangeItens();


            function ChangePeriodo() {
                var n = $("#" + campo + "chkIndiferente:checked").length;
                if (n === 1) {
                    $('#trDataInicial').hide();
                    $('#trDataFinal').hide();
                } else {
                    $('#trDataInicial').show();
                    $('#trDataFinal').show();
                }
            }

            ChangePeriodo();

            $(".bootstrap-switch-id-ctl00_ContentPlaceHolder1_chkIndiferente, .bootstrap-switch-id-ctl00_ContentPlaceHolder1_chkIndiferente label").click(function () {
                ChangePeriodo();
            });

        });
    </script>

</asp:Content>

