﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using FrameWork.DataObject;
using FrameWork.WebControls;
using FrameWork;
using ClassLibrary;
using System.Drawing.Imaging;
using System.Drawing;


public partial class PastaEditar : System.Web.UI.Page
{

    ClassLibrary.Pastas objSegmentoCases = new ClassLibrary.Pastas();
    public String strImage = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(this.IsPostBack))
        {

            //txtPosicao.Attributes.Add("type","number");
            //txtPosicao.Attributes.Add("min", "1");
            //txtPosicao.Attributes.Add("max", "99");

            ////Direitos do Usuário
            //if (!this.UserInfo.Admin && !CheckUserRight(101, "C"))
            //{
            //    Response.Redirect("Default.aspx");
            //}
            //if (!this.UserInfo.Admin && !CheckUserRight(101, "I"))
            //{
            //    this.btnNovaNoticia.Visible = false;
            //}
            //if (!this.UserInfo.Admin && !CheckUserRight(101, "E"))
            //{
            //    this.btnSalvar.Visible = false;
            //}

            //ID DO EMPREENDIMENTO
            //SE NAO HA NADA DENTRO DO CONTROLE HIDDEN PEGA VIA REQUEST
            if (this.Request["id"] != null)
                this.hID.Value = FrameWork.Util.GetString(this.Request["id"]);

            //CARREGA DADOS
            Carrega();

        }
    }


    public void Carrega()
    {
        //OPCAO DE EDICAO PARA VISUALIZAR OS DADOS
        if (hID.Value != "")
        {
            string strSql = "SELECT * FROM Pastas " +
                            "WHERE ID = " + this.hID.Value;

            DataTable dt = new DataTable();
            dt = DataSource.DefaulDataSource.Select(strSql);

            //REGISTRO LOCALIZADO ENTAO APENAS EDITA DADOS DO FORMULARIO
            if (dt.Rows.Count > 0)
            {

                //coloca os dados para edição
                hID.Value = dt.Rows[0]["ID"].ToString();


                this.txtPasta.Text = FrameWork.Util.GetString(dt.Rows[0]["Pasta"]);
                this.txtSequencia.Text = FrameWork.Util.GetString(dt.Rows[0]["Sequencia"]);


                //chkIndiferente.Checked = Convert.ToBoolean(dt.Rows[0]["Indeterminado"]);
                chkAtivo.Checked = (Convert.ToBoolean(dt.Rows[0]["Ativo"]));

                this.ddlSetor.SelectedValue = FrameWork.Util.GetString(dt.Rows[0]["Setor"]);

                //this.txtPagina.Text = FrameWork.Util.GetString(dt.Rows[0]["pagina"]);
                lblUsuario.Text  = FrameWork.Util.GetString(dt.Rows[0]["Usuario"]);
                lblDataInclusao.Text = FrameWork.Util.GetString(dt.Rows[0]["DataInclusao"]);
                lblDataAlteracao.Text = FrameWork.Util.GetString(dt.Rows[0]["DataAlteracao"]);

                hOperacao.Value = "UPDATE";

                btnExcluir.Visible = true;
                // abaFotos.Visible = true;
                //CarregaFotos();

            }

        }

    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        //validação de campos
        if (FrameWork.Util.GetString(txtPasta.Text) == "")
        {
            Funcoes.msgbox("Favor inserir um Pasta!", this);
            txtPasta.Focus();
            return;
        }


        if (hOperacao.Value == "UPDATE")
        {
            //Comandos de Atualização do Empreendimento
            objSegmentoCases.LoadWhere("ID=@ID", new ParameterItem("@ID", hID.Value));
            objSegmentoCases.DataAlteracao = DateTime.Now;

        }
        else
            objSegmentoCases.DataInclusao = DateTime.Now;

        objSegmentoCases.Pasta = this.txtPasta.Text;
        objSegmentoCases.Setor = this.ddlSetor.SelectedValue;
        objSegmentoCases.Sequencia = FrameWork.Util.GetInt(txtSequencia.Text);



        //objBanners.Indeterminado = (chkIndiferente.Checked);
        objSegmentoCases.Ativo = (chkAtivo.Checked);




        //txtPagina.Text = pagina;
        //objBanners.pagina = pagina;

        objSegmentoCases.DataAlteracao = DateTime.Now;
        objSegmentoCases.Usuario = (string)this.Session["_uiUser"];


        try
        {
            objSegmentoCases.Save();
            this.hID.Value = objSegmentoCases.ID.ToString();

            hOperacao.Value = "UPDATE";
            Msg("Salvo com sucesso!", true);
            //this.abaFotos.Visible = true;
            Response.Redirect("PastaListar");
        }
        catch (Exception ex)
        {
            //this.pnlEmp.Visible = false;
            //if (ex.ErrorCode.ToString() == "-2146232060")
            //    this.DisplayMessage = " ID da Notícia já existente! ";
            //else
            //    this.DisplayMessage = ex.ErrorCode.ToString() + " | " + ex.InnerException.ToString();

            Msg(ex.Message, false);
        }

        //ATUALIZA A TELA 
        //this.Image1.ImageUrl = FrameWork.Util.GetString(dt.Rows[0]["Imagem"]);
        //IMAGENS 
        try
        {

          


        }
        catch (Exception ex)
        {
            Msg(ex.Message, false);
        }


    }

    protected void btnExcluir_Click(object sender, EventArgs e)
    {


        try
        {
            //Comandos de excluir do Empreendimento
            objSegmentoCases.LoadWhere("ID=@ID", new ParameterItem("@ID", hID.Value));
            objSegmentoCases.Delete();

            Response.Redirect("PastaListar");

        }
        catch (Exception ex)
        {
            //this.pnlEmp.Visible = false;
            //if (ex.ErrorCode.ToString() == "-2146232060")
            //    this.DisplayMessage = " ID da Notícia já existente! ";
            //else
            //    this.DisplayMessage = ex.ErrorCode.ToString() + " | " + ex.InnerException.ToString();

            Funcoes.msgbox(ex.Message, this);
        }





    }
    protected void Msg(String msg, Boolean tipo)
    {
        lblMensagem.CssClass = (tipo ? "box-danger alert alert-success" : "box-danger alert alert-danger");
        lblMensagem.Text = msg;
        lblMensagem.Visible = true;
    }


}