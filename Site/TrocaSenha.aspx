﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeFile="TrocaSenha.aspx.cs" Inherits="TrocaSenha" %>

<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Bem-vindo ao Portal do Conselho</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />


    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/bootstrap-theme.min.css">
    <link href="/skins/all.css?v=1.0.2" rel="stylesheet">
    <link href="/skins/flat/yellow.css" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet">

    <script src="/js/jquery-1.12.4.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/skins/icheck.js?v=1.0.2"></script>
  
    <script>
        $(document).ready(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_flat-yellow',
                radioClass: 'iradio_flat-yellow'
            });
        });
    </script>
    <style>
        .myBtn {
            display: Block;
            position: fixed;
            bottom: 50px;
            right: 20px;
            z-index: 999999;
            border: none;
            outline: none;
            background-color: #74a838;
            color: black;
            cursor: pointer;
            padding: 15px;
            border-radius: 10px;
        }
    </style>
    <link href="/css/style.css" rel="stylesheet" />

    <script type="text/javascript">
        function consistir(sender, arg) {
            with (document.forms[0]) {
               // if (txtSenha.value == "") {
               //     alert("Senha Atual é campo obrigatório!");
               //     (txtSenha.focus());
                //    arg.IsValid = false;
               // }

                aux = 0;
                xnova = txtNova.value;
                xredigite = txtRedigite.value;
                if (aux == 0) {
                    if (xnova == "") {
                        aux = 1;
                        alert("Preencha o campo Nova Senha");
                        txtNova.focus();
                        arg.IsValid = false;
                    }
                }
                if (aux == 0) {
                    if (xnova != xredigite) {
                        aux = 1;
                        alert("Campo Nova Senha e Redigite precisam ser iguais!");
                        txtNova.focus();
                        arg.IsValid = false;
                    }
                }
            }
        }

        //-->
    </script>
</head>
<body>


    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">
                <img src="/img/logo.JPG" width="180" alt="PDG Portal Conselho" /></a>
        </div>






        <div class="navbar-collapse navbar-right collapse">
            <ul class="nav navbar-left top-nav">
                <%--   <p class="obs right">Observações do upload</p>

                <p class="obs right">
                    O tamanho máximo de arquivo para uploads é de 5000 KB.<br>
                    Somente arquivos (PDF, JPG) são permitidos.
                           
                </p>--%>
            </ul>
            <ul class="nav navbar-right top-nav">
                <% 
                    if (FrameWork.Util.GetInt(Session["qtdContratos"]) > 0)
                    { %>
                <li class="dropdown">
                    <a title="Meus Contratos" data-toggle="tooltip" data-placement="bottom" href="/?c=1" class="dropdown-toggle"><i class="fa fa-building-o" aria-hidden="true"></i></a>
                </li>
                <% } %>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-bell"></i><b class="caret"></b></a>
                    <ul class="dropdown-menu message-dropdown">
                        <% if (FrameWork.Util.GetString(Session["_uiAlertas"]).Length > 1)
                            {
                                Response.Write(FrameWork.Util.GetString(Session["_uiAlertas"]));
                            }
                            else
                            {    %>

                        <li class="message-footer"><a href="#">Nenhum alerta!</a></li>
                        <% } %>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-user" aria-hidden="true"></i>
                        <b class="caret"></b></a>

                    <ul class="dropdown-menu perfil">
                        <h6>Bem-vindo</h6>
                        <h3>
                            <asp:Literal ID="lblNomeUsuario" runat="server" Text=""></asp:Literal></h3>
                        <li><a href="/trocasenha" class="message-footer">Trocar Senha</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a title="Sair" data-toggle="tooltip" data-placement="bottom" href="/login" class="dropdown-toggle" id="exit"><i class="fa fa-power-off" aria-hidden="true"></i></a>
                </li>
            </ul>
        </div>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
        </div>
        <!-- /.navbar-collapse -->
    </nav>


    <!--Page Title-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <div id="page-title">
        <h1 class="page-header text-overflow">Troca de Senha</h1>

    </div>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End page title-->



    <ol class="breadcrumb">
        <li><a href="/docs"><i class="fa fa-dashboard"></i>&nbsp;Home</a></li>
        <li>Troca de Senha</li>
        <li class="active"></li>
    </ol>



    <form id="form1" runat="server">


        <asp:Label ID="lblMensagem" runat="server" CssClass="box-danger alert alert-danger" Style="display: block" Visible="false"></asp:Label>

        <div id="page-content">

            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title"></h3>
                </div>
                <div class="panel-body">

                 <%--   <div class="form-group">
                        <div class="col-sm-12">
                            <label for="ContentPlaceHolder1_Chamada">Senha Atual:</label>
                            <asp:TextBox ID="txtSenha" CssClass="form-control" MaxLength="20" required="required" runat="server" TextMode="Password"></asp:TextBox>
                        </div>
                    </div>--%>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="ContentPlaceHolder1_Chamada">Nova Senha:</label>
                            <asp:TextBox ID="txtNova" CssClass="form-control" MaxLength="20" required="required" runat="server" TextMode="Password"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="ContentPlaceHolder1_Chamada">Redigite:</label>
                            <asp:TextBox ID="txtRedigite" CssClass="form-control" MaxLength="20" required="required" runat="server" TextMode="Password"></asp:TextBox>
                        </div>
                    </div>

                </div>

                <div class="panel-footer text-right">
                    <asp:Button ID="btnSalvar" runat="server" Text="SALVAR" OnClientClick="return consistir();" CssClass="btn btn-success fa fa-check-circle" OnClick="btnSalvar_Click1" />


                    <br />
                    <br />
                    <asp:HiddenField ID="hOperacao" runat="server" />
                    <asp:HiddenField ID="hID" runat="server" />
                    <asp:HiddenField ID="hUrlReferrer" runat="server" />
                    <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                </div>



            </div>
        </div>
    </form>







