﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _noticiasView : System.Web.UI.Page
{
    public ClassLibrary.Noticias objCliente = new ClassLibrary.Noticias();
    //caminho fisico no server
    string pathFisico = System.Configuration.ConfigurationManager.AppSettings["pathFisico"] + "uploads\\imgnoticias\\";
    string pathVirtual = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "uploads/imgnoticias/";

    public string[] sAux = new string[8];
    public string[] sAuxOrigens = new string[8];
    public String sLabels = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {

            if (this.Request["id"] != null)
                this.hID.Value = Convert.ToString(this.Request["id"]);

            //CARREGA DADOS
            // CarregaCategorias();
            //CarregaNoticia();


            {
                //String strUsuario = FrameWork.Util.GetString(Session["_uiCPF"]);
                //String strSenha = FrameWork.Util.GetString(Session["_uiSenha"]);

                //  hID.Value = (FrameWork.Util.GetString(FrameWork.Util.GetString(Session["_uiUserID"])));
                //hCPF_Cliente.Value = (strUsuario);
                //

                objCliente.Load(FrameWork.Util.GetString(hID.Value));

                // objCliente.Load(FrameWork.Util.GetString(Session["_uiUserID"]));

                if (objCliente.TotalLoad == 1)
                {
                    txttitulo.Text = objCliente.Titulo;
                     txtdata.Text = String.Format("{0:dd/MM/yyyy}", objCliente.Data);
                    ltldescricao.Text = objCliente.Conteudo;
                    // txtEmail.Text = objCliente.Email;
                    //txtcargo.Text = objCliente.Cargo;
                    //txtCPF.Text = objCliente.Cpf;

                    //txtRamal.Text = objCliente.Ramal;
                    //txtaniversario.Text = objCliente.DataAniversario;

                    if (FrameWork.Util.GetString(objCliente.Imagem).Length > 3)
                    // hFoto.Value = "uploads/colaboradores/" + objCliente.Foto + "?t=" + String.Format("{0:ddMMyyyyHHmm}", DateTime.Now);
                    Img.ImageUrl = pathVirtual + objCliente.Imagem  ;
                }




            }



        }
    }
}