﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Documentos : System.Web.UI.Page
{

    public string[] sAux = new string[8];
    public string[] sAuxOrigens = new string[8];
    public String sLabels = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
         
            CarregaDocumentos();
           

        }
    }
    public void CarregaDocumentos()
    { 
        string pathVirtual = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "uploads/documentos/";
    String strUsuario = FrameWork.Util.GetString(Session["_uiUser"]);

    string strSql = "SELECT *,('" + pathVirtual + "' + Arquivo) as Link, a.setor as setorempresa  FROM Documentos d    left join area a on a.id = d.area where 1=1 and d.tipo = 'Documento' and  d.ativo =1 ";

    strSql += " ORDER BY d.descricao desc";
        // Response.Write(strSql);
        // Response.End();
        DataTable dt = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSql);
    rptDocs.DataSource = dt;
        rptDocs.DataBind();

        if (dt.Rows.Count > 0)
        {


            //pnlComprovantes.Visible = false;
            // ltlTextoEndereco.Text = "Aguardando validação manualmente na PDG!";

        }


    }

}