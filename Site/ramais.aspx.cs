﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Ramais : System.Web.UI.Page
{

    public string[] sAux = new string[8];
    public string[] sAuxOrigens = new string[8];
    public String sLabels = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
         
            CarregaRamais();
           

        }
    }
    public void CarregaRamais()
    {

        //string pathVirtual = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "uploads/documentos/";
        string pathPDFs = System.Configuration.ConfigurationManager.AppSettings["pathVirtual"] + "uploads/colaboradores/";

        String strUsuario = FrameWork.Util.GetString(Session["_uiUser"]);

        string strSql = "select *, Convert(char(2),dataAniversario,101)  as data,CASE    WHEN len(foto) > 0 THEN ('" + pathPDFs + "' + foto)        ELSE('" + pathPDFs + "foto-perfil.jpg') END AS linkfoto from  colaboradores where ativo =1  ";
      
        strSql += " ORDER BY nome desc";
        // Response.Write(strSql);
        // Response.End();
        DataTable dt = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSql);
        rptRamais.DataSource = dt;
        rptRamais.DataBind();

        if (dt.Rows.Count > 0)
        {


            //pnlComprovantes.Visible = false;
            // ltlTextoEndereco.Text = "Aguardando validação manualmente na PDG!";

        }





    }


}