﻿<%@ Page Title="" Language="C#" MasterPageFile="/MasterPage.master" AutoEventWireup="true" CodeFile="perfil.aspx.cs" Inherits="_Perfil" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%--   <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <script>
        $(document).ready(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_flat-yellow',
                radioClass: 'iradio_flat-yellow'
            });
        });
    </script>--%>


    <!--Specify page [ SAMPLE ]-->
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    <link href="content/css/style.css" rel="stylesheet" />
    <link href="content/css/dados-cadastrais.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="Server">

    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">



            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>Meu Perfil
            <small></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="default"><i class="fa fa-dashboard"></i>Home</a></li>
                        <li><a href="perfil">Meu Perfil</a></li>
                        <%--<li class="active">Data tables</li>--%>
                    </ol>
                </section>

                <!-- Main content -->
                <%--  <section class="content">--%>

                <div class="box">
                    <div class="box-header">


                        <form class="form" name="dadoscadastrais" id="dadoscadastrais" runat="server" method="post" enctype="multipart/form-data">
                            <%-- <div class="dashboard-int">--%>
                            <%--<div class="row">--%>
                            <div class="col-12 col-md-7 dashboard-box">
                                <div class="titulo-form"><span><i class="icon-user"></i>Informações pessoais</span></div>
                                <div class="form-row">

                                    <div class="col-12">
                                        <label for="nome">Nome completo</label>
                                        <asp:TextBox ID="txtNome" CssClass="form-control" runat="server" MaxLength="100" Width="100%" Enabled="false"></asp:TextBox>
                                    </div>

                                    <%--  <div class="col-12  col-md-5">
                                    <label for="cpf">CPF</label>
                                    <asp:TextBox ID="txtCPF" CssClass="form-control cpfcnpj-mask" runat="server" Enabled="false" MaxLength="20" Width="100%"></asp:TextBox>
                                </div>--%>

                                    <div class="col-12">
                                        <label for="email">E-mail</label>
                                        <asp:TextBox ID="txtEmail" CssClass="form-control" runat="server" MaxLength="150" Width="100%" Enabled="false"></asp:TextBox>
                                    </div>
                                  <%--    <div class="col-12">
                                        <label for="email">Cargo</label>
                                        <asp:TextBox ID="txtcargo" CssClass="form-control" runat="server" MaxLength="150" Width="100%"></asp:TextBox>
                                    </div>--%>
                                    <div class="col-12 col-md-7">
                                        <label for="cargo">Cargo</label>
                                        <asp:TextBox ID="txtcargo1" CssClass="form-control" runat="server"  Width="100%" Enabled="false"></asp:TextBox>
                                    </div>

                                    <div class="col-12 col-md-5">
                                        <label for="cargo">Área</label>
                                        <asp:TextBox ID="txtarea" CssClass="form-control" runat="server"  Width="100%" Enabled="false"></asp:TextBox>
                                    </div>
                                     <div class="col-12 col-md-6">
                                        <label for="telefone">Regional</label>
                                        <asp:TextBox ID="txtRegional" CssClass="form-control " runat="server" MaxLength="30" Width="100%" Enabled="false"></asp:TextBox>
                                    </div>
                                    <div class="col-12 col-md-3">
                                        <label for="telefone">Aniversário</label>
                                        <asp:TextBox ID="txtaniversario" CssClass="form-control data" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask runat="server" MaxLength="30" Width="100%"></asp:TextBox>
                                    </div>

                                    <div class="col-12 col-md-3">
                                        <label for="telefone">Ramal</label>
                                        <asp:TextBox ID="txtRamal" CssClass="form-control " runat="server" MaxLength="30" Width="100%"></asp:TextBox>
                                    </div>
                                         <div class="col-12">
                                        <label for="email">Descrição Pessoal</label>
                                        <asp:TextBox ID="txtDescricao" CssClass="form-control" runat="server" TextMode="MultiLine" Width="100%" Rows="7"></asp:TextBox>
                                    </div>



                                    <div class="col-12">
                                        <asp:Label ID="lblMensagem" runat="server" CssClass="alert alert-dados" />
                                    </div>
                                    <div class="col-12">

                                        <asp:LinkButton ID="btnAlterarDados" runat="server" CssClass="btn-padrao btn-salvar" OnClick="btnAlterarDados_Click" Visible="true">Salvar alterações</asp:LinkButton>
                                        <asp:HiddenField ID="hCPF_Cliente" runat="server" />
                                    </div>

                                </div>
                            </div>


                            <!-- // Alterar Senha  -->
                            <div class="col-12 col-md-5 dashboard-box">
                                <div class="titulo-form"><span><i class="icon-senha"></i>Alterar senha </span></div>
                                <div class="form-row">
                                    <div class="col-12">
                                        <label for="cpf">Digite sua senha atual</label>
                                        <asp:TextBox ID="txtSenhaAtual" TextMode="Password" CssClass="form-control" runat="server" MaxLength="20" Width="100%"></asp:TextBox>
                                    </div>
                                    <div class="col-12">
                                        <label for="cpf">Digite sua nova senha</label>
                                        <asp:TextBox ID="txtNovaSenha" TextMode="Password" CssClass="form-control" runat="server" MaxLength="20" Width="100%" data-indicator="pwindicator"></asp:TextBox>
                                        <div class="help-block" style="float: left;">Mínimo de 6 caracteres</div>
                                        <div id="pwindicator">
                                            <div class="bar"></div>
                                            <div class="label"></div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <label for="cpf">Repita sua nova senha</label>
                                        <asp:TextBox ID="txtRepetirSenha" TextMode="Password" CssClass="form-control" runat="server" MaxLength="20" Width="100%"></asp:TextBox>
                                    </div>
                                    <div class="col-12">
                                        <asp:Label ID="lblMensagemSenha" runat="server" CssClass="alert alert-alterarsenha" /><%--<div class="alert alert-alterarsenha"></div>--%>
                                    </div>
                                    <div class="col-12">
                                        <asp:LinkButton ID="btnAlterarSenha" runat="server" CssClass="btn-padrao" OnClientClick="return validaSenha();" OnClick="btnAlterarSenha_Click">Alterar SENHA</asp:LinkButton>
                                    </div>
                                </div>

                                <div class="titulo-form"><span><i class="icon-user"></i>Alterar foto </span></div>
                                <%--              <label for="cpf">Digite sua senha atual</label>--%>
                                <div class="form-row">
                                    <div class="col-12">
                                        <div class="avatar-upload">
                                            <div class="avatar-edit">
                                                <asp:FileUpload ID="imageUpload" runat="server" class="imageUpload" accept=".png, .jpg, .jpeg" />
                                                <label for="ctl00_content_imageUpload"></label>
                                            </div>
                                            <div class="avatar-preview">
                                                <div id="imagePreview" runat="server" style="background-image: url(/content/img/foto-perfil.jpg);">
                                                </div>
                                            </div>
                                        </div>
                                        <label for="cpf">Somente é permitido arquivos com extensão: gif, jpg ou png!</label>
                                    </div>
                                    <div class="col-12">
                                        <div class="alert alert-foto"></div>
                                    </div>
                                    <div class="col-12">
                                        <asp:HiddenField ID="hFoto" runat="server" />
                                        <asp:HiddenField ID="hID" runat="server" />
                                        <asp:LinkButton ID="btnAlterarFoto" runat="server" CssClass="btn-padrao btnAlterarFoto" OnClick="btnAlterarFoto_Click">SALVAR FOTO</asp:LinkButton>
                                    </div>
                                </div>
                            </div>

                            <%-- </div>--%>
                            <%-- </div>--%>
                        </form>
                        <!-- // Alterar Senha  -->
                        <%--      </section>--%>
                    </div>
                </div>
                <!-- /.content -->
                <%-- </div>
            <!-- /.content-wrapper -->

                --%>
                <div class="control-sidebar-bg"></div>
            </div>
            <!-- ./wrapper -->
        </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- InputMask -->
    <script src="plugins/input-mask/jquery.inputmask.js"></script>
    <script src="plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <!-- DataTables -->
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <script src="content/js/jquery.maskedinput-1.3.1.js"></script>
    <!-- page script -->
    <script>


        jQuery(document).ready(function () {

            $(".cpfcnpj-mask").mask("999.999.999-99?999").on("focusout", function () {
                var len = this.value.replace(/\D/g, '').length;
                $(this).mask(len > 11 ? "99.999.999/9999-99" : "999.999.999-99?999");
            });
            //$(".phone-mask").mask("(99) 9999-9999?9").on("focusout", function () {
            //    var len = this.value.replace(/\D/g, '').length;
            //    $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
            //});

            $(".data").mask("99/99/9999");

            if ($("#" + campo + "hFoto").val().length > 3) {
                aux = $("#" + campo + "hFoto").val();
                console.log(aux);
                $("#imagePreview").attr("style", "background-image: url(" + aux + ");");
            }

            $('#' + campo + 'txtNovaSenha').pwstrength();
        });
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });
    </script>




    <%-- <script type="text/jscript" src="/content/js/jquery.maskedinput-1.3.1.js"></script>
    <script src="content/js/jquery.pwstrength.js" type="text/javascript"></script>--%>

    <script>
<%--        var campo = '<%=btnAlterarSenha.ClientID.Replace("btnAlterarSenha","")%>';--%>


        jQuery(document).ready(function () {




            //if ($("#" + campo + "hFoto").val().length > 3) {
            //    aux = $("#" + campo + "hFoto").val();
            //    console.log(aux);
            //    $("#imagePreview").attr("style", "background-image: url(" + aux + ");");
            //}

            //$('#' + campo + 'txtNovaSenha').pwstrength();
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
                    $('#imagePreview').hide();
                    $('#imagePreview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
                $(".btnAlterarFoto").show();
            }
        }
        $(".imageUpload").change(function () {
            readURL(this);
        });

    </script>


    <script>
        /*jslint unparam: true */
        /*global window, $ */
        $(function () {


            $(".btn-salvar").click(function () {
                var email = $("#" + campo + "txtEmail");
                var endereco = $("#" + campo + "txtEndereco");


                if (email.val() == '') {
                    email.focus();
                    $(".alert-dados").html('Favor digitar o E-mail!');
                    $(".alert-dados").addClass("alert-danger");
                    return false;
                } else {
                    $(".alert-dados").css('border', '1px solid #818181');
                }
                if (endereco.val() == '') {
                    endereco.focus();
                    $(".alert-dados").html('Favor digitar o Endereço!');
                    $(".alert-dados").addClass("alert-danger");
                    return false;
                } else {
                    $(".alert-dados").css('border', '1px solid #818181');
                }

                $(".alert-dados").html('Aguarde enquanto salvamos suas informações!');
                $(".alert-dados").addClass("alert-warning");
                return true;
            });

        });

        function validaSenha(sender, arg) {
            with (document.forms[0]) {
                if (ctl00_content_txtSenhaAtual.value == "") {
                    $('.alert-alterarsenha').html("Ops, o campo <strong>senha atual</strong> é campo obrigatório!");
                    $('.alert-alterarsenha').addClass("alert-danger");
                    ctl00_content_txtSenhaAtual.focus();
                    return false;
                }

                xnova = ctl00_content_txtNovaSenha.value;
                xredigite = ctl00_content_txtRepetirSenha.value;
                if (xnova == "" || xnova.length < 6) {
                    aux = 1;
                    $('.alert-alterarsenha').html("Ops, o campo <strong>nova senha</strong> deverá ter no mínimo 6 caracteres!");
                    ctl00_content_txtNovaSenha.focus();
                    $('.alert-alterarsenha').addClass("alert-danger");
                    return false;
                }
                else if (xnova != xredigite) {
                    aux = 1;
                    $('.alert-alterarsenha').html("Ops, as senhas que você digitou não são iguais. Verifique sua <strong>nova senha</strong> e tente novamente.");
                    ctl00_content_txtNovaSenha.focus();
                    $('.alert-alterarsenha').addClass("alert-danger");
                    return false;
                }

                $('.alert-alterarsenha').removeClass("alert-danger");
            }
        }
    </script>
</asp:Content>
