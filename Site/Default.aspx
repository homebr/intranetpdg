﻿<%@ Page Title="" Language="C#" MasterPageFile="/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">



    <!--Specify page [ SAMPLE ]-->
    <!-- Font Awesome -->
    <link rel="stylesheet" href="AdminLTE-master/plugins/fontawesome-free/css/all.min.css">

    <!-- Theme style -->
    <link rel="stylesheet" href="AdminLTE-master/dist/css/adminlte.css">

    <style>
        body {
            width: 610;
            font-family: Arial;
            color: #212121;
            margin: 0 auto;
        }

        .demo-table {
            width: 100%;
            border-spacing: initial;
            margin: 20px 0px;
            word-break: break-word;
            table-layout: auto;
            line-height: 1.8em;
            color: #333;
        }

            .demo-table td {
                background-color: #ffffff;
                padding: 20px 5px 5px;
            }

                .demo-table td div.feed_title {
                    text-decoration: none;
                    color: #003fff;
                    font-weight: bold;
                }

            .demo-table ul {
                margin: 0;
                padding: 5px;
                margin-right: 15px;
            }

            .demo-table li {
                cursor: pointer;
                list-style-type: none;
                display: inline-block;
                color: #F0F0F0;
                text-shadow: 0 0 1px #666666;
                font-size: 20px;
            }

        .emoji-rating-box {
            border: #E4E4E4 1px solid;
            margin: 8px 0px;
        }

        .emoji-rating-count {
            border-top: #E4E4E4 1px solid;
            font-size: 0.8em;
            padding: 2px 5px;
            background: #f9f9f9;
            color: #828181;
        }

        .emoji-section {
            position: relative;
            margin-top: 15px;
        }

        .emoji-icon-container {
            display: none;
            position: absolute;
            bottom: 5px;
            left: 20px;
        }

            .emoji-icon-container.show {
                display: block;
            }

            .emoji-icon-container li {
                float: left;
                margin-right: 10px;
                margin-top: 10px;
                list-style-type: none;
            }

        .emoji-icon {
            width: 48px;
            margin: 10px 2px 0px 0px;
        }

        .like-link {
            padding: 5px;
            font-size: 0.95em;
            color: #949494;
            cursor: pointer;
        }

            .like-link img {
                vertical-align: top;
            }

        .emoji-data {
            width: 16px;
            vertical-align: text-top;
        }
    </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="Server">
    <!--CONTENT CONTAINER-->





    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Dashboard
            <small>PDG</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="default"><i class="fa fa-dashboard"></i>Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->

            <div id="fb-root"></div>





            <!-- /.row (main row) -->







            <!-- /.row -->
            <!-- Main row -->
            <div class="row">
                <!-- Left col -->
                <section class="col-lg-8 connectedSortable">
                    <!-- Custom tabs (Charts with tabs)-->


                    <!-- Main content -->
                    <section class="content">

                        <!-- row -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box box-solid">

                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" aling="center">
                                            <ol class="carousel-indicators">
                                                <asp:Repeater ID="rptBanners1" runat="server">
                                                    <ItemTemplate>
                                                <li data-target="#carousel-example-generic" data-slide-to="<%# DataBinder.Eval(Container, "DataItem.Posicao")%>" class="<%# DataBinder.Eval(Container, "DataItem.class")%>"></li>
<%--                                                <li data-target="#carousel-example-generic" data-slide-to="4" class=""></li>
                                                <li data-target="#carousel-example-generic" data-slide-to="5" class=""></li>
                                                <li data-target="#carousel-example-generic" data-slide-to="3" class=""></li>--%>
                                                </ItemTemplate>
                                                    </asp:Repeater>
                                                        <%--<li data-target="#carousel-example-generic" data-slide-to="4" class=""></li>--%>
                                            </ol>
                                            <div class="carousel-inner">
                                                <asp:Repeater ID="rptBanners" runat="server">
                                                    <ItemTemplate>
                                                        <%--   <div class="item active" align="center">
                                                    <a href="https://pt.surveymonkey.com/r/PDGCULTURAEMARCA" target="_blank"><img src="http://intranet.pdg.com.br/uploads/banners/banner4.png" alt="Clique para abrir o Chat"></a>
                                                    <div class="carousel-caption">
                                                    </div>
                                                </div>--%>
                                                        <div class="item <%# DataBinder.Eval(Container, "DataItem.class")%>" align="center">
                                                            <a href="<%# DataBinder.Eval(Container, "DataItem.Link")%>" target="_blank">
                                                            <img src="<%# DataBinder.Eval(Container, "DataItem.LinkImagem")%>" alt="<%# DataBinder.Eval(Container, "DataItem.Titulo")%>">
                                                            <div class="carousel-caption">
                                                            </div>
                                                        </div>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                        <%--<div class="item" align="center">
                                                    <a href="http://chat.pdg.com.br:3000/home" target="_blank"><img src="http://intranet.pdg.com.br/uploads/banners/chat.jpg" alt="Clique para abrir o Chat"></a>
                                                    <div class="carousel-caption">
                                                    </div>
                                                </div>
                                               <div class="item" align="center">
                                                    <a href="https://docs.google.com/forms/d/e/1FAIpQLSfNVsj3ItFO4E315Tf4vo0FYapD6rPUhtvYldRxFaoi7amLLA/viewform" target="_blank"><img src="http://intranet.pdg.com.br/uploads/banners/cestas.png" alt="Clique para abrir!!!"></a>
                                                    <div class="carousel-caption">
                                                    </div>
                                                </div>
                                              <div class="item" align="center" height="500px">
                                                    <img src="http://intranet.pdg.com.br/uploads/banners/maes.jpg" alt="Second slide">
                                                    <div class="carousel-caption">
                                                    </div>
                                                </div>--%>
                                                        <%-- <div class="item" align="center" height="500px">
                                                    <img src="http://intranet.pdg.com.br/uploads/banners/meta.png" alt="Second slide">
                                                    <div class="carousel-caption">
                                                    </div>
                                              
                                                </div>--%>
                                                        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                                            <span class="fa fa-angle-left"></span>
                                                        </a>
                                                        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                                                            <span class="fa fa-angle-right"></span>
                                                        </a>
                                                        </div>
                                        </div>
                                        <!-- /.box-body -->
                                                        </div>
                                    <!-- /.box -->
                                                        </div>

                                <div class="col-md-12">
                                    <ul class="timeline" id="boxTimeline">
                                        <!-- timeline time label -->
                                        <li class="time-label">
                                            <span class="bg-red">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </span>
                                        </li>




                                    </ul>
                                </div>
                                                        </section>
                    <!-- /.content -->

                                                        <!-- MODELO FEED -->
                                                        <div id="modelo-feed" style="display: none;">
                                                            <li>
                                                                <i class="fa fa-envelope bg-blue"></i>
                                                                <div class="timeline-item">
                                                                    <span class="time"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;<span class="bg-red">&nbsp;&nbsp;&nbsp;#data#&nbsp;&nbsp;&nbsp;
                                                                    </span></span>
                                                                    <h3 class="timeline-header"><a href="#">#TITULO#</a> #TIPO#</h3>
                                                                    <div class="timeline-body" align="center">
                                                                        #CHAMADA#
                                                    
                                                  <%--<a href="#link#" target="_blank" >--%>#link#<img src="#IMGLINK#" class="margin" width="95%" /></a>

                                    #VIDEO#
                                                        
                                                                    </div>

                                                                    <div class="timeline-footer">
                                                                        <a class="btn btn-primary right" style='<%#Convert.ToString(Eval("video")).Length > 0 ?"display:none;": "" %>' href="#LINK1#" target="_self">Saiba mais</a><br />

                                                                        <!-- CURTIDAS COM EMOJI -->
                                                                        <div class="emoji-section">
                                                                            <a class="like-link"
                                                                                onmouseover="showEmojiPanel(this)"
                                                                                onmouseout="hideEmojiPanel(this)">
                                                                                <i class="#curtida#"></i>Curtir
                                                                            </a>
                                                                            <ul class="emoji-icon-container">
                                                                                <li>
                                                                                    <img src="../../icons/like.png" class="emoji-icon"
                                                                                        data-emoji-rating="like"
                                                                                        data-tipo="1" />
                                                                                </li>
                                                                                <li>
                                                                                    <img src="../../icons/love.png" class="emoji-icon"
                                                                                        data-emoji-rating="love"
                                                                                        data-tipo="2" />
                                                                                </li>
                                                                                <li>
                                                                                    <img src="../../icons/smile.png" class="emoji-icon"
                                                                                        data-emoji-rating="smile"
                                                                                        data-tipo="3" />
                                                                                </li>
                                                                                <li>
                                                                                    <img src="../../icons/wow.png" class="emoji-icon"
                                                                                        data-emoji-rating="wow"
                                                                                        data-tipo="4" />
                                                                                </li>
                                                                                <li>
                                                                                    <img src="../../icons/sad.png" class="emoji-icon"
                                                                                        data-emoji-rating="sad"
                                                                                        data-tipo="5" />
                                                                                </li>
                                                                                <li>
                                                                                    <img src="../../icons/angry.png" class="emoji-icon"
                                                                                        data-emoji-rating="angry"
                                                                                        data-tipo="6" />
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                        <div id="emoji-rating-count-1" class="emoji-rating-count">
                                                                            <span>#qtd2#</span>
                                                                            <button type="button" data-id="#idfeed1#" data-idtipo="#idtipo1#" class="btn btn-tool btn-abre-curtidas" title="Clique para ver o resumo das curtidas">
                                                                                Curtidas
                                                                            </button>
                                                                            <%--  <img src="../../icons/like.png" class="emoji-data" />--%>
                                                                        </div>


                                                                        <!-- FIM CURTIDAS COM EMOJI -->


                                                                    </div>

                                                                    <!-- DIRECT CHAT -->
                                                                    <div class="card direct-chat direct-chat-primary collapsed-card">
                                                                        <div class="card-header">
                                                                            <h3 class="card-title">
                                                                                <%-- #BTN#--%>

                                                                                <%--                                                        <button type="button"  data-id="#idfeed#" data-idtipo="#idtipo#" data-idcolaborador="#idcolaborador#" class="search-results-gallery-icon flex flex-column items-center justify-center color-inherit w-100 pa2 br2 br--top no-underline hover-bg-blue4 hover-white"  ><i class="far fa-thumbs-up" style="font-size: 22px;"></i>Curtir</button>--%>

                                                                                <%--                                                            <button type="button" data-id1="#idfeed1#" data-idtipo1="#idtipo1#" data-idcolaborador1="#idcolaborador1#"  class="btn btn-tool btn-removercurtida" style="color: #000000;">
                                                                <i class="far fa-thumbs-up" style="font-size: 22px;"></i>Você e outras pessoas</button> 

                                                            <button type="button" data-id1="#idfeed1#" data-idtipo1="#idtipo1#" data-idcolaborador1="#idcolaborador1#"  class="btn btn-tool btn-criarcurtida">
                                                                <i class="far fa-thumbs-up" style="font-size: 22px;"></i>Curtir</button>--%> 
                                                            </h3>

                                                                            <div class="card-tools">
                                                                                <span title="" class="badge badge-primary">#qtd#</span>
                                                                                <button type="button" data-id="#idfeed#" data-idtipo="#idtipo#" data-idcolaborador="#idcolaborador#" class="btn btn-tool btn-abre-comentarios" data-card-widget="collapse">
                                                                                    Comentários
                                                     
                                                                                </button>

                                                                            </div>
                                                                        </div>

                                                                        <div class="card-footer">
                                                                            <form action="#" method="post">
                                                                                <div class="input-group">
                                                                                    <input type="text" id="txtComentario" name="message" placeholder="Escreva um comentário ..." class="form-control">
                                                                                    <span class="input-group-append">
                                                                                        <button type="button" class="btn btn-primary btn-send-criarcomentario">Comentar</button>
                                                                                    </span>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                        <!-- /.card-header -->
                                                                        <div class="card-body">
                                                                            <!-- Conversations are loaded here -->
                                                                            <div class="direct-chat-messages">
                                                                                <div class="grid-contas">
                                                                                    <!--/.carrega os comentarios-->
                                                                                </div>
                                                                            </div>
                                                                            <!--/.direct-chat-messages-->

                                                                            <!-- /.direct-chat-pane -->
                                                                        </div>
                                                                        <!-- /.card-body -->

                                                                        <!-- /.card-footer-->
                                                                    </div>
                                                                    <!--/.direct-chat -->
                                                                </div>

                                                            </li>
                                                        </div>
                                                        <!-- FIM MODELO -->

                                                        </section>

                <div class="modal fade" id="modal-curtidas" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Resumo das Curtidas</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="grid-curtidas">
                                    <!--/.carrega resumo das curtidas-->
                                </div>
                            </div>

                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>



                                                        <section class="col-lg-4 connectedSortable">


                                                            <%--ANIVERSARIANTES--%>
                                                            <div class="col-md-12">
                                                                <!-- USERS LIST -->

                                                                <div class="box box-danger widget-user-2">
                                                                    <div class="widget-user-header bg-yellow" style="background: #737e84 !important;">
                                                                        <div class="box-tools pull-right">
                                                                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                                                        </div>
                                                                        <div class="widget-user-image">
                                                                            <img class="img-circle" src="dist/img/logo1.jpg" alt="User Avatar">
                                                                        </div>
                                                                        <!-- /.widget-user-image -->
                                                                        <h3 class="widget-user-username">Aniversariantes</h3>
                                                                        <h5 class="widget-user-desc">do mês</h5>
                                                                    </div>

                                                                    <div class="box-body">
                                                                        <ul class="users-list clearfix">
                                                                            <asp:Repeater ID="rptAniv" runat="server">
                                                                                <ItemTemplate>
                                                                                    <li>
                                                                                        <img src="<%# DataBinder.Eval(Container, "DataItem.Link")%>" alt="User Image">
                                                                                        <a class="users-list-name" href="#"><%# DataBinder.Eval(Container, "DataItem.Nome")%></a>
                                                                                        <span class="users-list-date"><%#  String.Format("{0:dd/MM}",DataBinder.Eval(Container, "DataItem.DataAniversario"))%></span>

                                                                                        <%--                                <i class="fa fa-calendar" aria-hidden="true"></i><%# DataBinder.Eval(Container, "DataItem.Titulo")%></span><b><%#  String.Format("{0:dd/MM/yyyy}",DataBinder.Eval(Container, "DataItem.Data"))%></b></a>--%>
                                                                                    </li>

                                                                                </ItemTemplate>
                                                                            </asp:Repeater>

                                                                        </ul>
                                                                        <!-- /.users-list -->
                                                                    </div>
                                                                    <!-- /.box-body -->
                                                                    <%--    <div class="box-footer text-center">
                      <a href="javascript::" class="uppercase">Veja mais</a>
                    </div><!-- /.box-footer -->--%>
                                                                </div>
                                                                <!--/.box -->
                                                            </div>

                                                            <!-- links uteis -->
                                                            <div class="col-md-12">

                                                                <div class="box box-widget widget-user-2 collapsed-box">
                                                                    <!-- Add the bg color to the header using any of the bg-* classes -->
                                                                    <div class="widget-user-header bg-yellow">
                                                                        <div class="box-tools pull-right">
                                                                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                                                                        </div>
                                                                        <div class="widget-user-image">
                                                                            <img class="img-circle" src="dist/img/logo1.jpg" alt="User Avatar">
                                                                        </div>
                                                                        <!-- /.widget-user-image -->
                                                                        <h3 class="widget-user-username">Links</h3>
                                                                        <h5 class="widget-user-desc">Úteis</h5>


                                                                        <!-- /.box-tools -->
                                                                    </div>
                                                                    <!-- /.box-header -->
                                                                    <div class="box-body">
                                                                        <ul class="products-list product-list-in-box">
                                                                            <asp:Repeater ID="rptLinks" runat="server">
                                                                                <ItemTemplate>
                                                                                    <li class="item">
                                                                                        <div class="product-img1">
                                                                                            <a href="<%# DataBinder.Eval(Container, "DataItem.Link")%>" target="_blank" style="width: 100px;"><i class="">
                                                                                                <img src="<%# DataBinder.Eval(Container, "DataItem.linklogo")%>" width="100px" alt="" /></i></a>

                                                                                        </div>
                                                                                        <div class="product-info">
                                                                                            <a href="<%# DataBinder.Eval(Container, "DataItem.Link")%>" target="_blank" class="product-title"><%# DataBinder.Eval(Container, "DataItem.Nome")%><span class="label  pull-right">
                                                                                                <button class="btn btn-block btn-primary">Acessar</button></span></a>
                                                                                            <a href="<%# DataBinder.Eval(Container, "DataItem.Link")%>" target="_blank">
                                                                                                <span class="product-description"><%# DataBinder.Eval(Container, "DataItem.Descricao")%></a>
                                                                                        </div>
                                                                                    </li>
                                                                                    <!-- /.item -->
                                                                                </ItemTemplate>
                                                                            </asp:Repeater>
                                                                        </ul>
                                                                    </div>
                                                                    <!-- /.box-body -->
                                                                </div>
                                                                <!-- /.box -->
                                                            </div>
                                                            <!-- /.col -->

                                                            <%--CENTRAL DE DOCUMENTOS--%>

                                                            <div class="col-md-12">
                                                                <div class="box box-widget widget-user-2 collapsed-box">
                                                                    <div class="widget-user-header bg-aqua">
                                                                        <div class="box-tools pull-right">
                                                                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                                                                        </div>
                                                                        <div class="widget-user-image">
                                                                            <img class="img-circle" src="dist/img/logo1.jpg" alt="User Avatar">
                                                                        </div>
                                                                        <!-- /.widget-user-image -->
                                                                        <h3 class="widget-user-username">Central</h3>
                                                                        <h5 class="widget-user-desc">de Documentos</h5>

                                                                        <!-- /.box-tools -->
                                                                    </div>


                                                                    <div class="box-body">
                                                                        <ul class="products-list product-list-in-box">
                                                                            <asp:Repeater ID="rptDocs" runat="server">
                                                                                <ItemTemplate>
                                                                                    <li class="item">
                                                                                        <div class="product-img">
                                                                                            <i class="fa fa-file-<%# DataBinder.Eval(Container, "DataItem.setor")%>-o arquivo"></i>
                                                                                        </div>
                                                                                        <div class="product-info">
                                                                                            <a href="<%# DataBinder.Eval(Container, "DataItem.Link")%>" class="product-title"><%# DataBinder.Eval(Container, "DataItem.Descricao")%><span class="label  pull-right">
                                                                                                <button class="btn btn-block btn-primary">Download</button></span></a>
                                                                                            <span class="product-description"></span>
                                                                                        </div>
                                                                                    </li>
                                                                                    <!-- /.item -->
                                                                                </ItemTemplate>
                                                                            </asp:Repeater>

                                                                        </ul>
                                                                    </div>
                                                                    <!-- /.box-body -->
                                                                </div>
                                                                <!-- /.box -->
                                                            </div>
                                                            <!-- /.col -->


                                                            <%--CENTRAL DE Manual--%>

                                                            <div class="col-md-12">
                                                                <div class="box box-widget widget-user-2 collapsed-box">
                                                                    <div class="widget-user-header bg-yellow">
                                                                        <div class="box-tools pull-right">
                                                                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                                                                        </div>
                                                                        <div class="widget-user-image">
                                                                            <img class="img-circle" src="dist/img/logo1.jpg" alt="User Avatar">
                                                                        </div>
                                                                        <!-- /.widget-user-image -->
                                                                        <h3 class="widget-user-username">Central</h3>
                                                                        <h5 class="widget-user-desc">de Manuais</h5>

                                                                        <!-- /.box-tools -->
                                                                    </div>


                                                                    <div class="box-body">
                                                                        <ul class="products-list product-list-in-box">
                                                                            <asp:Repeater ID="rptManual" runat="server">
                                                                                <ItemTemplate>
                                                                                    <li class="item">
                                                                                        <div class="product-img">
                                                                                            <i class="fa fa-file-<%# DataBinder.Eval(Container, "DataItem.setor")%>-o arquivo"></i>
                                                                                        </div>
                                                                                        <div class="product-info">
                                                                                            <a href="<%# DataBinder.Eval(Container, "DataItem.Link")%>" class="product-title"><%# DataBinder.Eval(Container, "DataItem.Descricao")%><span class="label  pull-right">
                                                                                                <button class="btn btn-block btn-primary">Download</button></span></a>
                                                                                            <span class="product-description"></span>
                                                                                        </div>
                                                                                    </li>
                                                                                    <!-- /.item -->
                                                                                </ItemTemplate>
                                                                            </asp:Repeater>
                                                                            <%--       <li class="item">
                      <div class="product-img">
                        <i class="fa  fa-file-excel-o arquivo"></i>
                      </div>
                      <div class="product-info">
                        <a href="javascript::;" class="product-title">Tabela de vendas <span class="label  pull-right"><button class="btn btn-block btn-primary">Download</button></span></a>
                        <span class="product-description">
                          Descrição
                        </span>
                      </div>
                    </li><!-- /.item -->--%>
                                                                        </ul>
                                                                    </div>
                                                                    <!-- /.box-body -->
                                                                </div>
                                                                <!-- /.box -->
                                                            </div>
                                                            <!-- /.col -->


                                                            <!-- links uteis -->
                                                            <div class="col-md-12">

                                                                <div class="box box-widget widget-user-2 collapsed-box">
                                                                    <!-- Add the bg color to the header using any of the bg-* classes -->
                                                                    <div class="widget-user-header bg-aqua">
                                                                        <div class="box-tools pull-right">
                                                                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                                                                        </div>
                                                                        <div class="widget-user-image">
                                                                            <img class="img-circle" src="dist/img/logo1.jpg" alt="User Avatar">
                                                                        </div>
                                                                        <!-- /.widget-user-image -->
                                                                        <h3 class="widget-user-username">Parceiros</h3>
                                                                        <h5 class="widget-user-desc">%descontos</h5>


                                                                        <!-- /.box-tools -->
                                                                    </div>
                                                                    <!-- /.box-header -->
                                                                    <div class="box-body">
                                                                        <ul class="products-list product-list-in-box">
                                                                            <asp:Repeater ID="rptParceria" runat="server">
                                                                                <ItemTemplate>
                                                                                    <li class="item">
                                                                                        <div class="product-img1">
                                                                                            <a href="<%# DataBinder.Eval(Container, "DataItem.Link")%>" target="_blank" alt="" style="width: 100px;"><i class="">
                                                                                                <img src="<%# DataBinder.Eval(Container, "DataItem.linklogo")%>" width="100px" title="<%# DataBinder.Eval(Container, "DataItem.Obs")%>" /></i></a>

                                                                                        </div>
                                                                                        <div class="product-info">
                                                                                            <a href="<%# DataBinder.Eval(Container, "DataItem.Link")%>" target="_blank" class="product-title"><%# DataBinder.Eval(Container, "DataItem.Empresa")%><span class="label  pull-right">
                                                                                                <button class="btn btn-block btn-primary">Acessar</button></span></a>
                                                                                            <a href="<%# DataBinder.Eval(Container, "DataItem.Link")%>" target="_blank">
                                                                                                <span class="product-description"><%# DataBinder.Eval(Container, "DataItem.Desconto")%></a>
                                                                                        </div>

                                                                                    </li>
                                                                                    <!-- /.item -->
                                                                                </ItemTemplate>
                                                                            </asp:Repeater>
                                                                        </ul>
                                                                    </div>
                                                                    <!-- /.box-body -->
                                                                </div>
                                                                <!-- /.box -->
                                                            </div>
                                                            <!-- /.col -->


                                                            <%--SOBRE PDG--%>
                                                            <div class="col-lg-12">
                                                                <div class="box box-widget widget-user-2">
                                                                    <!-- Add the bg color to the header using any of the bg-* classes -->
                                                                    <div class="widget-user-header bg">
                                                                        <div class="widget-user-image">
                                                                            <img class="img-circle" src="dist/img/logo1.jpg" alt="User Avatar">
                                                                        </div>
                                                                        <!-- /.widget-user-image -->
                                                                        <h3 class="widget-user-username">PDG</h3>
                                                                        <h5 class="widget-user-desc">Um pouco sobre nós</h5>
                                                                    </div>
                                                                    <div class="box-footer no-padding">
                                                                        <ul class="nav nav-stacked">
                                                                            <%-- <li><a href="#">Sobre nós</a></li>
                                    <li><a href="#">Quem somos </a></li>
                                    <li><a href="#">Missão </a></li>
                                    <li><a href="#">Visão </a></li>--%>
                                                                            <%-- <li><a href="#">Valores </a></li>
                                    <li><a href="#">Etica </a></li>
                                    <li><a href="#">Onde estamos </a></li>
                                    <li><a href="#">Nossa Marca </a></li>--%>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <!-- /.widget-user -->
                                                            </div>
                                                            </div>
    </div>









</asp:Content>
                                                            <asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
                                                                <!-- jQuery -->
                                                                <script src="content/js/jquery-3.3.1.min.js"></script>
                                                                <script>
                                                                    function showEmojiPanel(obj) {
                                                                        $(".emoji-icon-container").hide();
                                                                        $(obj).next(".emoji-icon-container").show();
                                                                    }
                                                                    function hideEmojiPanel(obj) {
                                                                        setTimeout(function () {
                                                                            $(obj).next(".emoji-icon-container").hide();
                                                                        }, 3000);
                                                                    }

                                                                    function addUpdateRating(obj, id) {
                                                                        $(obj).closest(".emoji-icon-container").hide();
                                                                        $.ajax({
                                                                            url: "addUpdateRating.php",
                                                                            data: 'id=' + id + '&rating=' + $(obj).data("emoji-rating"),
                                                                            type: "POST",
                                                                            success: function (data) {
                                                                                $("#emoji-rating-count-" + id).html(data);
                                                                            }
                                                                        });
                                                                    }
                                                                </script>
                                                                <script src="AdminLTE-master/plugins/jquery/jquery.min.js"></script>
                                                                <script src="js/default.js"></script>
                                                                <!-- jQuery 2.1.4 -->
                                                                <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
                                                                <!-- jQuery UI 1.11.4 -->
                                                                <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
                                                                <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
                                                                <%--    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>--%>
                                                                <!-- Bootstrap 3.3.5 -->
                                                                <script src="bootstrap/js/bootstrap.min.js"></script>
                                                                <!-- Morris.js charts 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="plugins/morris/morris.min.js"></script>-->
                                                                <!-- Sparkline -->
                                                                <script src="plugins/sparkline/jquery.sparkline.min.js"></script>
                                                                <!-- jvectormap -->
                                                                <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
                                                                <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
                                                                <!-- jQuery Knob Chart -->
                                                                <script src="plugins/knob/jquery.knob.js"></script>
                                                                <!-- daterangepicker -->
                                                                <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
                                                                <script src="plugins/daterangepicker/daterangepicker.js"></script>
                                                                <!-- datepicker -->
                                                                <script src="plugins/datepicker/bootstrap-datepicker.js"></script>
                                                                <!-- Bootstrap WYSIHTML5 -->
                                                                <script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
                                                                <!-- Slimscroll -->
                                                                <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
                                                                <!-- FastClick -->
                                                                <script src="plugins/fastclick/fastclick.min.js"></script>
                                                                <!-- AdminLTE App -->
                                                                <script src="dist/js/app.min.js"></script>
                                                                <%--    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="dist/js/pages/dashboard.js"></script>--%>
                                                                <!-- AdminLTE for demo purposes -->
                                                                <script src="dist/js/demo.js"></script>
                                                                <%--  <script async defer crossorigin="anonymous" src="https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v10.0" nonce="O2yvKFyi"></script>--%>
                                                                <!-- AdminLTE App -->
                                                                <script src="AdminLTE-master/dist/js/adminlte.js"></script>

                                                            </asp:Content>
