﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeFile="UsuariosListar.aspx.cs" ViewStateMode="Disabled" Inherits="Ferramentas_UsuariosListar" %>


<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Bem-vindo ao Portal do Conselho</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link href="skins/all.css?v=1.0.2" rel="stylesheet">
    <link href="skins/flat/yellow.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/login.css" rel="stylesheet">
    <script src="js/jquery-1.12.4.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="skins/icheck.js?v=1.0.2"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            oTable = $('#example').dataTable({
                "bPaginate": true,
                "bJQueryUI": true,
                "sPaginationType": "full_numbers",
                "iDisplayLength": "50"
            });
        });


    </script>
    </head>

<body>
  <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">
                <img src="/img/logo.JPG" width="180" alt="PDG Portal Conselho" /></a>
        </div>






        <div class="navbar-collapse navbar-right collapse">
            <ul class="nav navbar-left top-nav">
             <%--   <p class="obs right">Observações do upload</p>

                <p class="obs right">
                    O tamanho máximo de arquivo para uploads é de 5000 KB.<br>
                    Somente arquivos (PDF, JPG) são permitidos.
                           
                </p>--%>
            </ul>
            <ul class="nav navbar-right top-nav">
                <% 
                    if (FrameWork.Util.GetInt(Session["qtdContratos"]) > 0)
                    { %>
                <li class="dropdown">
                    <a title="Meus Contratos" data-toggle="tooltip" data-placement="bottom" href="/?c=1" class="dropdown-toggle"><i class="fa fa-building-o" aria-hidden="true"></i></a>
                </li>
                <% } %>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-bell"></i><b class="caret"></b></a>
                    <ul class="dropdown-menu message-dropdown">
                        <% if (FrameWork.Util.GetString(Session["_uiAlertas"]).Length > 1)
                            {
                                Response.Write(FrameWork.Util.GetString(Session["_uiAlertas"]));
                            }
                            else
                            {    %>

                        <li class="message-footer"><a href="#">Nenhum alerta!</a></li>
                        <% } %>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-user" aria-hidden="true"></i>
                        <b class="caret"></b></a>

                    <ul class="dropdown-menu perfil">
                        <h6>Bem-vindo</h6>
                        <h3>
                            <asp:Literal ID="lblNomeUsuario" runat="server" Text=""></asp:Literal></h3>
                    </ul>
                </li>
                <li class="dropdown">
                    <a title="Sair" data-toggle="tooltip" data-placement="bottom" href="/login" class="dropdown-toggle" id="exit"><i class="fa fa-power-off" aria-hidden="true"></i></a>
                </li>
            </ul>
        </div>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
        </div>
        <!-- /.navbar-collapse -->
    </nav>

    <div class="container">
        <h2 class="titulo-geral">
            <asp:Literal ID="ltlTitulo" runat="server"></asp:Literal><span></span></h2>
        <p>
            <asp:Literal ID="ltlConteudo" runat="server"></asp:Literal>
        </p>

        <form class="form" name="jasoucliente" id="jasoucliente" runat="server" method="post">
          <%--  <div class="menu-faq">
               <asp:Button ID="btnAdm" CssClass="col-xs-2 col-xs-15 item-faq i-assitenciatecnica ativo " runat="server" OnClick="btnAdm_Click"></asp:Button><span class="arrow-down "></span>

                <asp:Button ID="btnCF" CssClass="col-xs-2 col-xs-15 item-faq i-financiamento" runat="server" OnClick="btnCF_Click"></asp:Button><span class="arrow-down "></span>
                <asp:Button ID="btnCA" CssClass="col-xs-2 col-xs-15 item-faq i-contratos" class="arrow-down " runat="server" OnClick="btnCA_Click"></asp:Button><span class="arrow-down "></span>
            </div>--%>

            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">FILTROS</h3>
                </div>
                <div class="panel-body">





                            <asp:TextBox ID="TextBox1" runat="server" Width="296px"></asp:TextBox>



                            <asp:Button ID="btnBuscar" CssClass="btn" Text="Buscar" runat="server" />




                        </div>
            </div>

            <!-- DATA TABLES -->
           <div class="panel">

                <div class="panel-body">

                            <table id="datatable1" cellpadding="0" cellspacing="0" border="0" class="datatable table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th class="col-md-1"></th>
                                        <th>LoginAcesso</th>
                                        <th>Nome</th>
                                        <th>E-mail</th>
                                        <th>Situação</th>
                                        <th>Último Acesso</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="Repeater1" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td class="col-md-1"><%# DataBinder.Eval(Container, "DataItem.fotofull")%></td>
                                                <td class="left"><a href='<%# "UsuariosEditar?id=" + DataBinder.Eval(Container, "DataItem.LoginAcesso")%>'><%# DataBinder.Eval(Container, "DataItem.LoginAcesso")%></a></td>
                                                <td><%# DataBinder.Eval(Container, "DataItem.Nome")%></a></td>
                                                <td><%# DataBinder.Eval(Container, "DataItem.Email")%></a></td>
                                                <td><%# DataBinder.Eval(Container, "DataItem.SituacaoLogin")%></a></td>
                                                <td><%# DataBinder.Eval(Container, "DataItem.UltimoAcesso")%></a></td>
                                                <td class="center"><a class="btn btn-danger" href='<%# "UsuariosEditar?id=" + DataBinder.Eval(Container, "DataItem.LoginAcesso")%>'>Ver</a></td>
                                            </tr>

                                        </ItemTemplate>
                                    </asp:Repeater>

                                </tbody>
                            </table>
                </div>

                <div class="panel-footer">
                    <a class="btn btn-danger" href="UsuariosEditar">NOVO</a>
                </div>


            </div>

                 </form>      
        </div>
      
 

    <!--/PAGE -->


    <!--DataTables [ OPTIONAL ]-->
    <script src="/admin/content/plugins/datatables/media/js/jquery.dataTables.js"></script>
    <script src="/admin/content/plugins/datatables/media/js/dataTables.bootstrap.js"></script>
    <script src="/admin/content/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js"></script>


    <!--DataTables Sample [ SAMPLE ]-->
    <script src="/admin/content/js/demo/tables-datatables.js"></script>





    <script type="text/javascript">
        $(document).ready(function () {
            oTable = $('#example').dataTable({
                "bPaginate": true,
                "bJQueryUI": true,
                "sPaginationType": "full_numbers",
                "iDisplayLength": "50"
            });
        });
        //ordernação da ficha tecnica
        $(function () {
            $("#datatable1 tbody").sortable({
                connectWith: "#datatable1",
                axis: "y",
                start: function (event, ui) {
                    ui.item.toggleClass("highlight");
                },
                stop: function (event, ui) {
                    var slides = [];
                    var items = [];

                    ui.item.toggleClass("highlight");

                    $("#datatable1 tr .item-value").each(function (index, element) {
                        slides[index] = index + 1;
                        items[index] = parseInt($(element).val());
                    });

                    // console.log(slides, items);
                    $.post('wsOrdenacao.ashx?modulo=cms',
                        { 'item_id': items.join(','), 'positions': slides.join(',') }, function (data) { }, 'json');
                }
            });

            $("#datatable1").disableSelection();


        });


    </script>
    <script src="/js/jQuery-Cookie/jquery.cookie.min.js"></script>

    <script>
        function SomenteNumero(e) {
            var tecla = (window.event) ? event.keyCode : e.which;
            if ((tecla > 47 && tecla < 58)) return true;
            else {
                if (tecla == 8 || tecla == 0) return true;
                else return false;
            }
        }

        $(document).ready(function () {

            $('input[type=text][name=cpf]').tooltip({
                placement: "right",
                trigger: "focus"
            });
            $('.dropup').hover(function () {
                $('.dropdown-toggle', this).trigger('click');
            });

            $('.forgot-pass').click(function (event) {
                $('.login-box-body').removeClass('visible animated fadeInUp');
                $('.login-box-forgot').addClass('visible animated fadeInUp');
            });

            $('.pass-reset').click(function (event) {
                $('.login-box-body').addClass('visible animated fadeInUp');
                $('.login-box-forgot').removeClass('visible animated fadeInUp');
            });




            //Checa o cookie se foi marcado para lembrar
            if ($.cookie("_PDG_pc_login") != "undefined") {
                $("#cpf").val($.cookie("_PDG_pc_login"));
                $("#chkConectado").prop('checked', true);
                $(".icheckbox_flat-yellow").addClass("checked");
            }


        });

    </script>
            <!-- Google Analytics-->
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
                m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-83123671-1', 'auto');
        ga('send', 'pageview');
        </script>

</body>
</html>



