﻿<%@ Page Title="" Language="C#" MasterPageFile="/MasterPage.master" AutoEventWireup="true" CodeFile="treinamentoView.aspx.cs" Inherits="_treinamentoView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>PDG Intranet - Treinamentos</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="Server">

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Treinamento
            <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="default"><i class="fa fa-dashboard"></i>Home</a></li>
                <li><a href="noticias">Treinamento</a></li>
                <%--<li class="active">General Elements</li>--%>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <form id="form1" runat="server">
                    <div class="col-md-12">

                        <!-- general form elements disabled -->
                        <div class="box box-warning">
                            <div class="box-header with-border">
                                <%-- <h3 class="box-title">PDG em Casa</h3>--%>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <form role="form">
                                    <!-- text input -->
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label>Título:</label>
                                            <asp:TextBox ID="txttitulo" runat="server" class="form-control" placeholder=""></asp:TextBox>

                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Data:</label>
                                            <asp:TextBox ID="txtdata" runat="server" class="form-control" placeholder=""></asp:TextBox>

                                        </div>
                                    </div>


                                     <div class="col-md-12">
                                    <!-- textarea -->
                                    <div class="form-group">
                                        <label>Descrição:</label>
                                        <asp:Literal ID="ltldescricao" runat="server"></asp:Literal>
                                        <asp:HiddenField ID="hvideo" runat="server" />
                                        <%-- <asp:TextBox ID="txtdescricao" runat="server" Rows="5" placeholder="" class="form-control" TextMode="MultiLine"></asp:TextBox>--%>
                                    </div>
                                                   <asp:Panel ID="panelVideo" runat="server" Visible="false">
                                                          <div >
                                            <iframe id="framevideo" width="50%" height="350" src="" runat="server" frameborder="0" allowfullscreen></iframe>
                                        </div>
                                  </asp:Panel>
                                    <div class="form-group">
                                        <asp:Image ID="Img" runat="server" ImageUrl="" />
                                    </div>

                                         </div>
                                    <asp:HiddenField ID="hID" runat="server" />

                                </form>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>

                </form>
                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">



    <!-- jQuery 2.1.4 -->
    <script src="../../plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>


</asp:Content>
