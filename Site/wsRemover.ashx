﻿<%@ WebHandler Language="C#" Class="wsRemover" %>

using System;
using System.Web;

public class wsRemover : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/json";
        System.Globalization.CultureInfo culture =  new System.Globalization.CultureInfo("pt-BR");

        int id = FrameWork.Util.GetInt(context.Request["id"]);
        string modulo = FrameWork.Util.GetString(context.Request["modulo"]);

        string strSql = "";

        if (modulo == "comprovante")
        {
            strSql = "delete Comprovantes  where ID=" + id + " ";

            //strSql = "delete Comprovantes where ID=" + id + " ";
        }
        else if (modulo == "notificacao_email")
        {
                string cpf = FrameWork.Util.GetString(context.Request["cpf"]);
                string status = FrameWork.Util.GetString(context.Request["status"]);
                strSql = "update Clientes set Notificacao_Email=" + (status=="true"?1:0) + " where CPF='" + cpf + "' ";
        }
        else if (modulo == "notificacao_sms")
        {
                string cpf = FrameWork.Util.GetString(context.Request["cpf"]);
                string status = FrameWork.Util.GetString(context.Request["status"]);
                strSql = "update Clientes set Notificacao_SMS=" + (status=="true"?1:0) + " where CPF='" + cpf + "' ";
        }

        
        //else if (modulo == "investidor")
        //    strSql = "delete InvestidoresImoveis where ID=" + id + " ";
        //else if (modulo == "imobiliarias")
        //    strSql = "delete ImobiliariasImoveis where ID=" + id + " ";
        //else
        //    strSql = "delete Documentos where ID=" + id + " ";

        FrameWork.DataObject.DataSource.DefaulDataSource.Execute(strSql);
        String json = "{ \"response\": \"ok\",\"message\": \"Notificação alterada!\" }";
        context.Response.Write(json);
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}