﻿<%@ WebHandler Language="C#" Class="maintenance" %>

using System;
using System.Web;

public class maintenance : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";
            CarregaVariaveis();
        context.Response.Write("Atualizado");
    }
 
     protected void CarregaVariaveis()
    {


        //Application["config_Nome"] = 

        String strSql = "SELECT * FROM Configuracoes ";
        System.Data.DataTable dt = FrameWork.DataObject.DataSource.DefaulDataSource.Select(strSql);
        System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();


        try
        {


            for (int n = 0; n < dt.Rows.Count; n++)
            {
                if (FrameWork.Util.GetString(dt.Rows[n]["Nome"]) == "Empresa")
                {
                    String sJson = FrameWork.Util.GetString(dt.Rows[n]["Valor"]);
                    dynamic result = js.DeserializeObject(sJson);
                    Config.nome = FrameWork.Util.GetString(result["nome"]);
                    Config.telefone = FrameWork.Util.GetString(result["telefone"]);
                    Config.telefone2 = FrameWork.Util.GetString(result["telefone2"]);
                    Config.endereco = FrameWork.Util.GetString(result["endereco"]);
                }

            }

        }
        catch
        {

        }


    }


    public bool IsReusable {
        get {
            return false;
        }
    }

}