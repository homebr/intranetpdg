﻿function veTipo(obj, id) {
    nome = $(obj).attr("id");
    if ($(obj).is(":checked") && id == 2) {
        $(".dvMes").hide();

    }
    else {
        $(".dvMes").show();
    }
}





jQuery(document).ready(function () {

    if ($("#" + campo + "rdbTipo_Orcado").is(":checked")) {
        $(".dvMes").hide();
    }

    $(".btn-editar-texto").click(function () {
        $('#modal-texto').modal('show');
    });
    $("#btn-salva-texto").click(function (event) {
        event.preventDefault();
        shtml = $('#campo_texto_ocupacao').val();
        //console.log(shtml);
        arr = shtml.split(" ");
        if (arr.length < 2)
            arr = shtml.split("\n");
        //console.log("arr=" + arr.length);
        iTotalUnidades = Number($("#" + campo + "hQtdUnidades").val());
        decTotalUnidades = 0;
        decTotal = 0;
        $("input.campoOcupacao").each(function (index) {
            valor = 0;
            if (index < arr.length)
                valor = Number(arr[index]);
            $(this).val(valor);
            //console.log(index + ": " + $(this).text());
            if (valor > 0 && iTotalUnidades > 0) {
                decOCC = (valor / iTotalUnidades) * 100;
                $(this).parent().parent().find(".occ").text(Math.round(decOCC) + "%");
                decTotal += valor;
            }
            decTotalUnidades += iTotalUnidades;
        });
        if (decTotal > 0) {
            $(".table tfoot tr td:eq(1)").html(decTotalUnidades);
            $(".table tfoot tr td:eq(2)").html(decTotal);
            aux = Math.round((decTotal / decTotalUnidades) * 100);
            $(".table tfoot tr td:eq(3)").html(aux + "%");
        }
        $('#modal-texto').modal('hide');
    });


    //copi e cola - preenche campos da ocupacao diária
    $(".campoOcupacao").attr("type", "number");
    iTotalUnidades = $("#" + campo + "hQtdUnidades").val();
    $(".campoOcupacao").blur(function () {
        valor = Number($(this).val());
        if (valor > 0 && iTotalUnidades > 0) {
            decOCC = (valor / iTotalUnidades) * 100;
            $(this).parent().parent().find(".occ").text(Math.round(decOCC) + "%");
        }
    });


    //DADOS 
    var monthsShort = ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"];
    var colors = ['#FF0000', '#C0C0C0', '#1c1c1c', '#6A5ACD', '#FF8C00', '#B0C4DE', '#B0E0E6', '#00b5f7', '#EEE8AA', '#EEDD82', '#CDB7B5', '#DA70D6', '#FFA500', '#ffb805'];

    //-------------------- TAB 1 - GRAFICO DONUT  --------------

    var iMeses = 12;
    var iLinha = 0;


    var aux_data = "[";
    x = 1;
    $("#tabela_diaria tbody tr").each(function (index) {

        value_1 = Number(Math.round(parseFloat($(this).find("td:eq(4)").text().replace("%", "").replace(",", "."))));
        if (x > 1)
            aux_data += ", ";
        aux_data += "{\"dia\": \"" + x + "\"";
        if (value_1 > 0)
            aux_data += ", \"r1\": " + value_1;
        aux_data += "}";

        x++;
    });
    aux_data += "]";

    //console.log(aux_data);
    //return;
    var data_bar_valor = JSON.parse(aux_data);

    var letras_bar = ['r1'];
    var labels_bar = [''];
    var i_ymax_valor = 100;

    $("#grafico-diario").html(""); //destroy
    grafico_diario = Morris.Line({
        element: 'grafico-diario',
        data: data_bar_valor,
        xkey: 'dia',
        ykeys: letras_bar,
        ymax: i_ymax_valor,
        labels: labels_bar,
        gridEnabled: true,
        gridLineColor: 'rgba(0,0,0,.1)',
        gridTextColor: '#8f9ea6',
        gridTextSize: '11px',
        lineColors: colors,
        xLabelColors: colors,
        xLabelMargin: 8,
        lineWidth: 2,
        postUnits: "%",
        parseTime: false,
        resize: true,
        hideHover: true
    });


    //tabela semanal
    total1 = 0;
    total2 = 0;
    semana1 = 0;
    semana2 = 0;
    fim1 = 0;
    fim2 = 0;

    $("#tabela_semana tbody tr").each(function (index) {

        dia = $(this).find("td:eq(0)").text().trim();
        value_1 = Number(Math.round(parseFloat($(this).find("td:eq(1)").text())));
        value_2 = Number(Math.round(parseFloat($(this).find("td:eq(2)").text())));
        value_3 = Number(Math.round(parseFloat($(this).find("td:eq(3)").text().replace("%", "").replace(",", "."))));

        total1 += value_1;
        total2 += value_2;
        if (index == 0 || index == 6) {
            fim1 += value_1;
            fim2 += value_2;
        }
        else {
            semana1 += value_1;
            semana2 += value_2;
        }

        x++;
    });
    aux_data += "]";

    $("#tabela_semana tfoot tr td:eq(1)").text(total1);
    $("#tabela_semana tfoot tr td:eq(2)").text(total2);
    total3 = ((total2 / total1) * 100);
    $("#tabela_semana tfoot tr td:eq(3)").text(Math.round(total3) + "%");


    //move a primeira linha
    //$('#myDiv2').append($('#myDiv1>p'));
    $('#tabela_semana tbody .dom').parent().appendTo($('#tabela_semana tbody .sab').parent().parent());

    //colunas subtotais
    total3 = ((semana2 / semana1) * 100);
    $("<tr class='subtotal'><td class='center'>Dias Semana</td><td class='center'>" + semana1 + "</td><td class='center'>" + semana2 + "</td><td class='center'>" + Math.round(total3) + "%" + "</td></tr>").insertAfter($('#tabela_semana tbody .sex').parent());
    total3 = ((fim2 / fim1) * 100);
    $("<tr class='subtotal'><td class='center'>Total Final Semana</td><td class='center'>" + fim1 + "</td><td class='center'>" + fim2 + "</td><td class='center'>" + Math.round(total3) + "%" + "</td></tr>").insertAfter($('#tabela_semana tbody .dom').parent());
    //$(this ).after( "<p>Test</p>" );





    //tabela ANO ACUMULADO
    total1 = 0;
    total2 = 0;
    semana1 = 0;
    semana2 = 0;
    fim1 = 0;
    fim2 = 0;

    $("#tabela_acumulado tbody tr").each(function (index) {
        //dia = $(this).find("td:eq(0)").text().trim();
        value_1 = Number(Math.round(parseFloat($(this).find("td:eq(0)").text())));
        value_2 = Number(Math.round(parseFloat($(this).find("td:eq(1)").text())));
        value_3 = Number(Math.round(parseFloat($(this).find("td:eq(2)").text().replace("%", "").replace(",", "."))));

        total1 += value_1;
        total2 += value_2;
        if (index == 0 || index == 6) {
            fim1 += value_1;
            fim2 += value_2;
        }
        else {
            semana1 += value_1;
            semana2 += value_2;
        }

        x++;
    });

    if (Number(total1) > 0) {
        $("#tabela_acumulado tfoot tr td:eq(0)").text(total1);
        $("#tabela_acumulado tfoot tr td:eq(1)").text(total2);
        total3 = ((total2 / total1) * 100);
        $("#tabela_acumulado tfoot tr td:eq(2)").text(Math.round(total3) + "%");

        //move a primeira linha
        $('#tabela_acumulado tbody .dom').parent().appendTo($('#tabela_acumulado tbody .sab').parent().parent());


        //colunas subtotais
        total3 = ((semana2 / semana1) * 100);
        $("<tr class='subtotal'><td class='center'>" + semana1 + "</td><td class='center'>" + semana2 + "</td><td class='center'>" + Math.round(total3) + "%" + "</td></tr>").insertAfter($('#tabela_acumulado tbody .sex').parent());
        total3 = ((fim2 / fim1) * 100);
        $("<tr class='subtotal'><td class='center'>" + fim1 + "</td><td class='center'>" + fim2 + "</td><td class='center'>" + Math.round(total3) + "%" + "</td></tr>").insertAfter($('#tabela_acumulado tbody .dom').parent());
    }

    //tabela MES DO ANO ANTERIOR
    total1 = 0;
    total2 = 0;
    semana1 = 0;
    semana2 = 0;
    fim1 = 0;
    fim2 = 0;

    $("#tabela_mesanterior tbody tr").each(function (index) {
        //dia = $(this).find("td:eq(0)").text().trim();
        value_1 = Number(Math.round(parseFloat($(this).find("td:eq(0)").text())));
        value_2 = Number(Math.round(parseFloat($(this).find("td:eq(1)").text())));
        value_3 = Number(Math.round(parseFloat($(this).find("td:eq(2)").text().replace("%", "").replace(",", "."))));

        total1 += value_1;
        total2 += value_2;
        if (index == 0 || index == 6) {
            fim1 += value_1;
            fim2 += value_2;
        }
        else {
            semana1 += value_1;
            semana2 += value_2;
        }

        x++;
    });
    if (Number(total1) > 0) {
        $("#tabela_mesanterior tfoot tr td:eq(0)").text(total1);
        $("#tabela_mesanterior tfoot tr td:eq(1)").text(total2);
        total3 = ((total2 / total1) * 100);
        $("#tabela_mesanterior tfoot tr td:eq(2)").text(Math.round(total3) + "%");

        //move a primeira linha
        $('#tabela_mesanterior tbody .dom').parent().appendTo($('#tabela_mesanterior tbody .sab').parent().parent());


        //colunas subtotais
        total3 = ((semana2 / semana1) * 100);
        $("<tr class='subtotal'><td class='center'>" + semana1 + "</td><td class='center'>" + semana2 + "</td><td class='center'>" + Math.round(total3) + "%" + "</td></tr>").insertAfter($('#tabela_mesanterior tbody .sex').parent());
        total3 = ((fim2 / fim1) * 100);
        $("<tr class='subtotal'><td class='center'>" + fim1 + "</td><td class='center'>" + fim2 + "</td><td class='center'>" + Math.round(total3) + "%" + "</td></tr>").insertAfter($('#tabela_mesanterior tbody .dom').parent());
    }

    //tabela ANO ANTERIOR
    total1 = 0;
    total2 = 0;
    semana1 = 0;
    semana2 = 0;
    fim1 = 0;
    fim2 = 0;

    $("#tabela_anoanterior tbody tr").each(function (index) {
        //dia = $(this).find("td:eq(0)").text().trim();
        value_1 = Number(Math.round(parseFloat($(this).find("td:eq(0)").text())));
        value_2 = Number(Math.round(parseFloat($(this).find("td:eq(1)").text())));
        value_3 = Number(Math.round(parseFloat($(this).find("td:eq(2)").text().replace("%", "").replace(",", "."))));

        total1 += value_1;
        total2 += value_2;
        if (index == 0 || index == 6) {
            fim1 += value_1;
            fim2 += value_2;
        }
        else {
            semana1 += value_1;
            semana2 += value_2;
        }

        x++;
    });

    if (Number(total1) > 0) {


        $("#tabela_anoanterior tfoot tr td:eq(0)").text(total1);
        $("#tabela_anoanterior tfoot tr td:eq(1)").text(total2);
        total3 = ((total2 / total1) * 100);
        $("#tabela_anoanterior tfoot tr td:eq(2)").text(Math.round(total3) + "%");

        //move a primeira linha
        $('#tabela_anoanterior tbody .dom').parent().appendTo($('#tabela_anoanterior tbody .sab').parent().parent());


        //colunas subtotais
        total3 = ((semana2 / semana1) * 100);
        $("<tr class='subtotal'><td class='center'>" + semana1 + "</td><td class='center'>" + semana2 + "</td><td class='center'>" + Math.round(total3) + "%" + "</td></tr>").insertAfter($('#tabela_anoanterior tbody .sex').parent());
        total3 = ((fim2 / fim1) * 100);
        $("<tr class='subtotal'><td class='center'>" + fim1 + "</td><td class='center'>" + fim2 + "</td><td class='center'>" + Math.round(total3) + "%" + "</td></tr>").insertAfter($('#tabela_anoanterior tbody .dom').parent());

    }



    aux_data = "[";
    $("#tabela_semana tbody tr").each(function (index) {
        if (index != 5 && index < 8) {
            dia = $(this).find("td:eq(0)").text().trim();
            value_1 = Number(Math.round(parseFloat($(this).find("td:eq(3)").text().replace("%", "").replace(",", "."))));
            value_2 = Number(Math.round(parseFloat($("#tabela_acumulado tbody").find("tr:eq(" + index + ") td:eq(2)").text().replace("%", "").replace(",", "."))));
            //console.log("value_2: " + value_2 + " index:" + index + " - " + $("#tabela_acumulado tbody").find("tr:eq(" + index + ") td:eq(2)").text());
            value_3 = Number(Math.round(parseFloat($("#tabela_mesanterior tbody").find("tr:eq(" + index + ") td:eq(2)").text().replace("%", "").replace(",", "."))));
            value_4 = Number(Math.round(parseFloat($("#tabela_anoanterior tbody").find("tr:eq(" + index + ") td:eq(2)").text().replace("%", "").replace(",", "."))));
            //dados do gráfico
            if (index > 0)
                aux_data += ", ";
            aux_data += "{\"dia\": \"" + dia + "\"";
            if (value_1 > 0)
                aux_data += ", \"r1\": " + value_1;
            if (value_2 > 0)
                aux_data += ", \"r2\": " + value_2;
            if (value_3 > 0)
                aux_data += ", \"r3\": " + value_3;
            if (value_4 > 0)
                aux_data += ", \"r4\": " + value_4;
            aux_data += "}";
        }
        x++;
    });
    aux_data += "]";



    //console.log(aux_data);
    data_bar_valor = JSON.parse(aux_data);

    letras_bar = ['r1', 'r2', 'r3', 'r4'];
    labels_bar = [sNomeMes + " de " + iAno, 'Acumulado ' + iAno, sNomeMes + " de " + (iAno - 1), 'Acumulado ' + (iAno-1)];
    i_ymax_valor = 100;
    i_ymax = 0;
    $.each(data_bar_valor, function (i, val) {
        if (typeof val.r1 != "undefined" && Number(val.r1) > i_ymax)
            i_ymax = Number(val.r1);
        if (typeof val.r2 != "undefined" && Number(val.r2) > i_ymax)
            i_ymax = Number(val.r2);
        if (typeof val.r3 != "undefined" && Number(val.r3) > i_ymax)
            i_ymax = Number(val.r3);
        if (typeof val.r4 != "undefined" && Number(val.r4) > i_ymax)
            i_ymax = Number(val.r4);

    });
    i_ymax_valor = (Math.round((Number(i_ymax) + 20) / 10) * 10);

    colors2 = ['#FF0000', '#000000', '#FF0001', '#c0c0c0'];


    $("#grafico-semana").html(""); //destroy
    grafico_semana = Morris.Line({
        element: 'grafico-semana',
        data: data_bar_valor,
        xkey: 'dia',
        ykeys: letras_bar,
        ymax: i_ymax_valor,
        labels: labels_bar,
        gridEnabled: true,
        gridLineColor: 'rgba(0,0,0,.1)',
        gridTextColor: '#8f9ea6',
        gridTextSize: '11px',
        lineColors: colors2,
        xLabelColors: colors2,
        xLabelMargin: 8,
        lineWidth: 2,
        postUnits: "%",
        parseTime: false,
        resize: true,
        hideHover: true
    });


    setTimeout(function () {
        $('.morris-line svg path[stroke="#c0c0c0"]').attr("stroke-dasharray", "4");
        $('.morris-line svg path[stroke="#ff0001"]').attr("stroke-dasharray", "4");
    }, 1000);

});
