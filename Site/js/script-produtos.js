﻿jQuery(document).ready(function () {

    //var campo = "" + campo + "";
    $("input.dinheiro").maskMoney({ showSymbol: true, symbol: "", decimal: ",", thousands: "." });
    $('input.peso').mask('##0,000', { reverse: true });

    $('input.numeros').keyup(function () {
        $(this).val(this.value.replace(/\D/g, ''));
    });


    //verifica as variacoes ativas
    var arrVariacoes = [];
    $.each($(".box-alternativas-variacoes input[type=checkbox]"), function (key, value) {
        //console.log(key + ": " + value.id + " - " + $("#" + value.id).attr("checked"));
        var i = value.id.replace("chkItem", "");
        if (Number(i) == 1 && $("#" + value.id).attr("checked") == "checked")
            $("#chkItem" + 2).attr("disabled","true");
        else if (Number(i) == 2 && $("#" + value.id).attr("checked") == "checked")
            $("#chkItem" + 1).attr("disabled", "true");
        else if (Number(i) > 2 && $("#" + value.id).attr("checked") == "checked")
        {
            //jQuery.inArray(, arrVariacoes);
            arrVariacoes.push(value.id.replace("chkItem", ""));
        }
    });

    //carrega itens
    var id = $("#" + campo + "hID").val();

    //incluir as variacoes ativas
    //linha-titulo
    $.each(arrVariacoes, function (i, val) {
        var texto = $("label[for=chkItem" + val + "]").find("span").html();
        var soptions = "";
        $.post("/wsEditar.ashx?m=carregavariacaoitens&id=" + val, function (json) {
            if (json.response == "ok") {
                $.each(json.itens, function (x) {
                    soptions += "<option value='" + json.itens[x].item + "'>" + json.itens[x].item + "</option>";
                });
                $(".linha-titulo").append("<div class='col-sm-2'><label class='control-label' for='Item_Variacao_Item" + val + "'>" + texto + ":</label></div>");
                $(".linha-dados").append("<div class='col-sm-2'><select id='Item_Variacao_Item" + val + "_1' name='Item_Variacao_Item" + val + "[]' class='form-control'>" + soptions + "</select></div>");
            }
        });
    });

    if (Number(id) > 0) {
        $.post("/wsEditar.ashx?m=carregaitens&&id=" + id, function (json) {
            if (json.response == "ok") {

                var flagCor = ($("#chkItem1").attr("checked") == "checked" ? true : false);
                if (flagCor)
                    $(".div-cor").show();
                else
                    $(".div-cor").hide();

                var flagCores = ($("#chkItem2").attr("checked") == "checked" ? true : false);
                if (flagCores)
                    $(".div-cores").show();
                else
                    $(".div-cores").hide();


                $.each(json.dados, function (key) {
                    //console.log("key=" + key);

                    if (key == 0) {
                        id = 1;
                        $('#item_id_1').val(json.dados[key].iditem);
                        $('#Item_chkAtivo_1').val(json.dados[key].ativo);
                        //$('#Item_cor_1').val(json.dados[key].cor);
                        $('#Item_Quantidade_1').val(json.dados[key].quantidade);
                        $('#Item_Disponibilidade_1').find("option[value='" + json.dados[key].disponibilidade + "']").attr("selected", "selected");
                        $('#Item_SituacaoSemEstoque_1').find("option[value='" + json.dados[key].semestoque + "']").attr("selected", "selected");
                        $('#Item_Preco_Custo_1').val(json.dados[key].valor_custo);
                        $('#Item_Preco_Venda_1').val(json.dados[key].valor_venda);
                        $('#Item_Preco_Promocional_1').val(json.dados[key].valor_promocional);
                        $('#Item_Peso_1').val(json.dados[key].peso);
                        $('#Item_Altura_1').val(json.dados[key].altura);
                        $('#Item_Largura_1').val(json.dados[key].largura);
                        $('#Item_Profundidade_1').val(json.dados[key].profundidade);
                        $('#Item_SKU_1').val(json.dados[key].sku);

                        $("#Item_chkAtivo_1").val(json.dados[key].ativo);
                        $("#Item_chkAtivo_1").prop('checked', (json.dados[key].ativo == "True"));
                        new Switchery(document.getElementById("Item_chkAtivo_1"));

                        //variacoes
                        $.each(json.dados[key].variacoes, function (x) {
                            if (flagCor && json.dados[key].variacoes[x].idvariacao == 1) {
                                $("#Item_cor_" + id).val(json.dados[key].variacoes[x].valor);
                            } else if (flagCores && json.dados[key].variacoes[x].idvariacao==2) {
                                $("#Item_cor1_" + id).val(json.dados[key].variacoes[x].valor);
                                $("#Item_cor2_" + id).val(json.dados[key].variacoes[x].valor2);
                            }
                            else if (json.dados[key].variacoes[x].idvariacao > 2 && json.dados[key].variacoes[x].valor != "") {
                                $("#Item_Variacao_Item" + json.dados[key].variacoes[x].idvariacao + "_" + id + " option[value=" + json.dados[key].variacoes[x].valor + "]").attr('selected', 'selected');
                            }
                        });

                        if (flagCor) {
                            $("#Item_cor_" + id).parent().find(".sp-replacer").remove();
                            $("#Item_cor_" + id).spectrum({
                                allowEmpty: true,
                                change: function (color) {
                                    $("#Item_cor_" + id).val(color.toHexString());
                                }
                            });
                        }
                        if (flagCores) {
                            $("#Item_cor1_" + id).parent().find(".sp-replacer").remove();
                            $("#Item_cor1_" + id).spectrum({
                                color: $("#item_cor1_" + id).val(),
                                change: function (color) {
                                    $("#Item_cor1_" + id).val(color.toHexString());
                                }
                            });
                            $("#Item_cor2_" + id).spectrum({
                                color: $("#item_cor2_" + id).val(),
                                change: function (color) {
                                    $("#Item_cor2_" + id).val(color.toHexString());
                                }
                            });
                        }



                        
                    }
                    else {

                        if ($('.boxes-itens .box-variacao').length == 0)
                            $('.boxes-itens-demo .box-variacao').last().clone().appendTo(".boxes-itens");
                        else {
                            var bg = $('.boxes-itens .box-variacao').last().hasClass("bg2");
                            $('.boxes-itens .box-variacao').last().clone().appendTo(".boxes-itens");
                        }
                        var modelo = $('.boxes-itens .box-variacao').last();
                        var idantigo = modelo.attr('id').replace("item_", "");
                        var id = Number(idantigo) + 1;
                        modelo.attr('id', "item_" + id);
                        modelo.html(modelo.html().replace("Item_chkAtivo_" + idantigo, "Item_chkAtivo_" + id));
                        modelo.html(modelo.html().replace("Item_cor_" + idantigo, "Item_cor_" + id));
                        modelo.html(modelo.html().replace("Item_cor1_" + idantigo, "Item_cor1_" + id));
                        modelo.html(modelo.html().replace("Item_cor2_" + idantigo, "Item_cor2_" + id));
                        modelo.html(modelo.html().replace("Item_Quantidade_" + idantigo, "Item_Quantidade_" + id));
                        modelo.html(modelo.html().replace("Item_Disponibilidade_" + idantigo, "Item_Disponibilidade_" + id));
                        modelo.html(modelo.html().replace("Item_SituacaoSemEstoque_" + idantigo, "Item_SituacaoSemEstoque_" + id));
                        modelo.html(modelo.html().replace("Item_Preco_Custo_" + idantigo, "Item_Preco_Custo_" + id));
                        modelo.html(modelo.html().replace("Item_Preco_Venda_" + idantigo, "Item_Preco_Venda_" + id));
                        modelo.html(modelo.html().replace("Item_Preco_Promocional_" + idantigo, "Item_Preco_Promocional_" + id));
                        modelo.html(modelo.html().replace("Item_Peso_" + idantigo, "Item_Peso_" + id));
                        modelo.html(modelo.html().replace("Item_Altura_" + idantigo, "Item_Altura_" + id));
                        modelo.html(modelo.html().replace("Item_Largura_" + idantigo, "Item_Largura_" + id));
                        modelo.html(modelo.html().replace("Item_Profundidade_" + idantigo, "Item_Profundidade_" + id));
                        modelo.html(modelo.html().replace("Item_SKU_" + idantigo, "Item_SKU_" + id));
                        modelo.html(modelo.html().replace("item_id_" + idantigo, "item_id_" + id));
                        $.each(arrVariacoes, function (i, val) {
                            modelo.html(modelo.html().replace("Item_Variacao_Item" + val + "_" + idantigo, "Item_Variacao_Item" + val + "_" + id));
                        });

                        //$("#Item_chkAtivo_" + id).val(json.dados[key].ativo);

                        //$("#Item_cor_" + id).val(json.dados[key].cor);
                        $("#Item_Quantidade_" + id).val(json.dados[key].quantidade);
                        $('#Item_Disponibilidade_1').find("option[value='" + json.dados[key].disponibilidade + "']").attr("selected", "selected");
                        $('#Item_SituacaoSemEstoque_1').find("option[value='" + json.dados[key].semestoque + "']").attr("selected", "selected");
                        $("#Item_Preco_Custo_" + id).val(json.dados[key].valor_custo);
                        $("#Item_Preco_Venda_" + id).val(json.dados[key].valor_venda);
                        $("#Item_Preco_Promocional_" + id).val(json.dados[key].valor_promocional);
                        $("#Item_Peso_" + id).val(json.dados[key].peso);
                        $("#Item_Altura_" + id).val(json.dados[key].altura);
                        $("#Item_Largura_" + id).val(json.dados[key].largura);
                        $("#Item_Profundidade_" + id).val(json.dados[key].profundidade);
                        $("#Item_SKU_" + id).val(json.dados[key].sku);
                        $("#item_id_" + id).val(json.dados[key].iditem);

                        //variacoes
                        $.each(json.dados[key].variacoes, function (x) {
                            if (flagCor && json.dados[key].variacoes[x].idvariacao == 1) {
                                $("#Item_cor_" + id).val(json.dados[key].variacoes[x].valor);
                            }
                            else if (flagCores && json.dados[key].variacoes[x].idvariacao == 2) {
                                $("#Item_cor1_" + id).val(json.dados[key].variacoes[x].valor);
                                $("#Item_cor2_" + id).val(json.dados[key].variacoes[x].valor2);
                            }
                            else if (json.dados[key].variacoes[x].idvariacao > 2 && json.dados[key].variacoes[x].valor != "") {
                                $("#Item_Variacao_Item" + json.dados[key].variacoes[x].idvariacao + "_" + id + " option[value=" + json.dados[key].variacoes[x].valor + "]").attr('selected', 'selected');
                            }
                        });
                        var obj = $("#item_id_" + id).parent();
                        var btn = $(obj).find(".btn-delete-item");
                        $(btn).show();


                        if (!bg) //adiciona cor do fundo diferente intercalado
                            modelo.addClass("bg2");
                        else if (modelo.hasClass("bg2"))
                            modelo.removeClass("bg2");

                        //$("#Item_chkAtivo_" + id).trigger('click');
                        //$("input[name=recipients\\[\\]]").attr('checked', true);

                        //trata checkbox
                        $("#Item_chkAtivo_" + id).removeAttr("data-switchery");
                        $("#Item_chkAtivo_" + id).val(json.dados[key].ativo);
                        $("#Item_chkAtivo_" + id).show();
                        $("#Item_chkAtivo_" + id).parent().find("span").remove();

                        if (json.dados[key].ativo == "True") {
                            //$("#Item_chkAtivo_" + id).prop('checked', 'checked');
                            $("#Item_chkAtivo_" + id).attr('checked', 'checked');
                        }


                        new Switchery(document.getElementById("Item_chkAtivo_" + id));

                        if (flagCor) {
                            $("#Item_cor_" + id).parent().find(".sp-replacer").remove();
                            $("#Item_cor_" + id).spectrum({
                                allowEmpty: true,
                                change: function (color) {
                                    $("#Item_cor_" + id).val(color.toHexString());
                                }
                            });
                        }
                        if (flagCores) {
                            $("#Item_cor1_" + id).parent().find(".sp-replacer").remove();
                            $("#Item_cor1_" + id).spectrum({
                                color: $("#Item_cor1_" + id).val(),
                                change: function (color) {
                                    $("#Item_cor1_" + id).val(color.toHexString());
                                }
                            });
                            $("#Item_cor2_" + id).spectrum({
                                color: $("#Item_cor2_" + id).val(),
                                change: function (color) {
                                    $("#Item_cor2_" + id).val(color.toHexString());
                                }
                            });
                        }
                    }


                });
            }
        });
    }
    else {
        new Switchery(document.getElementById("Item_chkAtivo_1"));

        $("#Item_cor_1").spectrum({
            allowEmpty: true,
            change: function (color) {
                $("#Item_cor_1").val(color.toHexString());
            }
        });
        $("#Item_cor1_1").spectrum({
            allowEmpty: true,
            change: function (color) {
                $("#Item_cor1_1").val(color.toHexString());
            }
        });
        $("#Item_cor2_1").spectrum({
            allowEmpty: true,
            change: function (color) {
                $("#Item_cor2_1").val(color.toHexString());
            }
        });

    }

    $('.bt-add-item').click(function (event) {
        event.preventDefault();

        if ($('.boxes-itens .box-variacao').length == 0)
            $('.boxes-itens-demo .box-variacao').last().clone().appendTo(".boxes-itens");
        else {
            var bg = $('.boxes-itens .box-variacao').last().hasClass("bg2");
            $('.boxes-itens .box-variacao').last().clone().appendTo(".boxes-itens");
        }
        var modelo = $('.boxes-itens .box-variacao').last();
        var idantigo = modelo.attr('id').replace("item_", "");
        var id = Number(idantigo) + 1;
        modelo.attr('id', "item_" + id);
        modelo.html(modelo.html().replace("Item_chkAtivo_" + idantigo, "Item_chkAtivo_" + id));
        modelo.html(modelo.html().replace("Item_Quantidade_" + idantigo, "Item_Quantidade_" + id));
        modelo.html(modelo.html().replace("Item_Disponibilidade_" + idantigo, "Item_Disponibilidade_" + id));
        modelo.html(modelo.html().replace("Item_SituacaoSemEstoque_" + idantigo, "Item_SituacaoSemEstoque_" + id));
        modelo.html(modelo.html().replace("Item_Preco_Custo_" + idantigo, "Item_Preco_Custo_" + id));
        modelo.html(modelo.html().replace("Item_Preco_Venda_" + idantigo, "Item_Preco_Venda_" + id));
        modelo.html(modelo.html().replace("Item_Preco_Promocional_" + idantigo, "Item_Preco_Promocional_" + id));
        modelo.html(modelo.html().replace("Item_Peso_" + idantigo, "Item_Peso_" + id));
        modelo.html(modelo.html().replace("Item_Altura_" + idantigo, "Item_Altura_" + id));
        modelo.html(modelo.html().replace("Item_Largura_" + idantigo, "Item_Largura_" + id));
        modelo.html(modelo.html().replace("Item_Profundidade_" + idantigo, "Item_Profundidade_" + id));
        modelo.html(modelo.html().replace("item_id_" + idantigo, "item_id_" + id));
        $("#Item_Quantidade_" + id).val();
        $("#Item_Disponibilidade_" + id).removeAttr("selected");
        $("#Item_SituacaoSemEstoque_" + id).removeAttr("selected");
        $("#Item_Preco_Custo_" + id).val();
        $("#Item_Preco_Venda_" + id).val();
        $("#Item_Preco_Promocional_" + id).val();
        $("#Item_Peso_" + id).val();
        $("#Item_Altura_" + id).val();
        $("#Item_Largura_" + id).val();
        $("#Item_Profundidade_" + id).val();
        $("#item_id_" + id).val();
        var obj = $("#item_id_" + id).parent();
        var btn = $(obj).find(".btn-delete-item");
        $(btn).show();


        if (!bg) //adiciona cor do fundo diferente intercalado
            modelo.addClass("bg2");
        else if (modelo.hasClass("bg2"))
            modelo.removeClass("bg2");

        //$("#passageiro-cpf-" + id).mask("999.999.999-99");
        //$("#passageiro-nascimento-" + id).mask("99/99/9999");
        //$("#passageiro-cpf-" + id).focus();

        var div = $(btn).parent();


        //trata checkbox
        $("#Item_chkAtivo_" + id).removeAttr("data-switchery");
        $("#Item_chkAtivo_" + id).val('');
        $("#Item_chkAtivo_" + id).show();
        $("#Item_chkAtivo_" + id).parent().find("span").remove();

        $("#Item_chkAtivo_" + id).prop('checked', true);
        
        new Switchery(document.getElementById("Item_chkAtivo_" + id));

        modelo.append("<script>$('.make-switch').click(function () { var obj = $(this).find(\"input\");obj.val((obj.val() == \"True\" ? \"False\" : \"True\"));});");
        //$('.make-switch').click(function () {
        //    var obj = $(this).find("input");
        //    obj.val((obj.val() == "True" ? "False" : "True"));
        //});
       // var script = "<script> $('#searchlist').btsListFilter('#passageiro-cpf-" + id + "', { loadingClass: 'loading', sourceData: function (text, callback) { return $.getJSON('wsSearch.ashx?m=comprador&t=' + text, function (json) { $('#passageiro-nome-" + id + "').val(json[0].title); $('#passageiro-nascimento-" + id + "').val(json[0].nascimento);  $('#passageiro-email-" + id + "').val(json[0].email); $('#passageiro-sexo-" + id + "').val(json[0].sexo); $('#searchlist').html(''); });}});  </script> ";

        //div.append(script);
        //modelo.appendTo(".boxes-itens");
        //modelo-passageiros-1
    });


    $(".box-alternativas-variacoes .magic-checkbox").on("click", function () { 
        //event.preventDefault();
        var nome = this.id;
        var val = this.id.replace("chkItem", "");
       // console.log(nome);
        //var id = $("#" + campo + "hID").val();
        var flagCor = false;
        var flagCores = false;

        if (Number(val) > 2 && $(this).attr("checked") == "checked")
        {
            $(".linha-titulo label[for=Item_Variacao_Item" + val + "]").parent().remove();
            $(".linha-dados select[name='Item_Variacao_Item" + val + "[]']").parent().remove();
            $(this).removeAttr("checked");
        }
        else if (Number(val) > 2)
        {
            var texto = $("label[for=chkItem" + val + "]").find("span").html();
            var soptions = "";
            $.post("/wsEditar.ashx?m=carregavariacaoitens&id=" + val, function (json) {
                if (json.response == "ok") {
                    $.each(json.itens, function (x) {
                        soptions += "<option value='" + json.itens[x].item + "'>" + json.itens[x].item + "</option>";
                    });
                    $(".linha-titulo").append("<div class='col-sm-2'><label class='control-label' for='Item_Variacao_Item" + val + "'>" + texto + ":</label></div>");
                    $(".linha-dados").append("<div class='col-sm-2'><select id='Item_Variacao_Item" + val + "_1' name='Item_Variacao_Item" + val + "[]' class='form-control'>" + soptions + "</select></div>");
                }
            });
            $(this).attr("checked", "checked");
        }
        else if (Number(val) == 1 && $(this).attr("checked") == "checked")
        {
            $(".linha-titulo .div-cor").hide();
            $(".linha-dados .div-cor").hide();
            $(this).removeAttr("checked");
            $("#chkItem2").removeAttr("disabled");
        }
        else if (Number(val) == 1) {
            $(".linha-titulo .div-cor").show();
            $(".linha-dados .div-cor").show();
            $(this).attr("checked", "checked");
            flagCor = true;
            $("#chkItem2").attr("disabled", "true");
        }
        else if (Number(val) == 2 && $(this).attr("checked") == "checked") {
            $(".linha-titulo .div-cores").hide();
            $(".linha-dados .div-cores").hide();
            $(this).removeAttr("checked");
            $("#chkItem1").removeAttr("disabled");
        }
        else if (Number(val) == 2) {
            $(".linha-titulo .div-cores").show();
            $(".linha-dados .div-cores").show();
            $(this).attr("checked", "checked");
            flagCores = true;
            $("#chkItem1").attr("disabled", "disabled");
        }

        $('.boxes-itens .box-variacao').each(function () {

            var id = $(this).attr("id").replace("item_", "");
            //var chk = $("#Item_chkAtivo_" + id);
            //var quantidade = $("#Item_Quantidade_" + id);
            //var custo = $("#Item_Preco_Custo_" + id);
            //var venda = $("#Item_Preco_Venda_" + id);
            //var promocional = $("#Item_Preco_Promocional_" + id);
            //var peso = $("#Item_Peso_" + id);
            //var altura = $("#Item_Altura_" + id);
            //var largura = $("#Item_Largura_" + id);
            //var profundidade = $("#Item_Profundidade_" + id);

            if (flagCor) {
                $("#Item_cor_" + id).parent().find(".sp-replacer").remove();
                $("#Item_cor_" + id).spectrum({
                    allowEmpty: true,
                    change: function (color) {
                        $("#Item_cor_" + id).val(color.toHexString());
                    }
                });
            }
            if (flagCores) {
                $("#Item_cor1_" + id).parent().find(".sp-replacer").remove();
                $("#Item_cor1_" + id).spectrum({
                    color: $("#Item_cor1_" + id).val(),
                    change: function (color) {
                        $("#Item_cor1_" + id).val(color.toHexString());
                    }
                });
                $("#Item_cor2_" + id).spectrum({
                    color: $("#Item_cor2_" + id).val(),
                    change: function (color) {
                        $("#Item_cor2_" + id).val(color.toHexString());
                    }
                });
            }

        });

    });


    $(function ($) {
        var options = {
            minimum: 1,
            maximize: 10,
            onChange: valChanged,
            onMinimum: function (e) {
                console.log('reached minimum: ' + e)
            },
            onMaximize: function (e) {
                console.log('reached maximize' + e)
            }
        };
        $(".handle-counter").each(function (i) {
            max = $(this).find("input").data("max");
            //if (max>0)
                $('#handleCounter' + i).handleCounter({ maximize: max, onChange: valChanged });
            //else
            //    $('#handleCounter' + i).handleCounter();
        });
    });
    function valChanged(d, id) {
       // SomaValores();
    }

    
    $(".salvar").click(function (event) {
        //event.preventDefault();

        var nome = $("#" + campo + "txtNome");

        if (nome.val() == '') {
            nome.css('border', '1px solid red');
            nome.focus();
            $(".alert").html('Favor preencher o campo Nome do produto!');
            $(".alert").addClass("alert-danger");
            $(".alert").show();
            flag = false;
            return false;
        } else {
            nome.css('border', '1px solid #818181');
        }


        $('.boxes-itens .box-variacao').each(function () {

            var id = $(this).attr("id").replace("item_", "");
            var chk = $("#Item_chkAtivo_" + id);
            var quantidade = $("#Item_Quantidade_" + id);
            var disponibilidade = $("#Item_Disponibilidade_" + id);
            var semestoque = $("#Item_SituacaoSemEstoque_" + id);
            var custo = $("#Item_Preco_Custo_" + id);
            var venda = $("#Item_Preco_Venda_" + id);
            var promocional = $("#Item_Preco_Promocional_" + id);
            var peso = $("#Item_Peso_" + id);
            var altura = $("#Item_Altura_" + id);
            var largura = $("#Item_Largura_" + id);
            var profundidade = $("#Item_Profundidade_" + id);

            if (quantidade.val() == '') {
                quantidade.css('border', '1px solid red');
                quantidade.focus();
                $(".alert").html('Favor preencher o campo Quantidade!');
                $(".alert").addClass("alert-danger");
                event.preventDefault();
                return false;
            } else {
                quantidade.css('border', '1px solid #818181');
            }
            if (venda.val() == '') {
                venda.css('border', '1px solid red');
                venda.focus();
                $(".alert").html('Favor preencher o campo Preço de Venda!');
                $(".alert").addClass("alert-danger");
                event.preventDefault();
                return false;
            } else {
                venda.css('border', '1px solid #818181');
            }
            

            //sCpf += "[" + cpf.val() + "]";
            //sNome += "[" + nome.val() + "]";
            //sEmail += "[" + email.val() + "]";
            //sNascimento += "[" + nascimento.val() + "]";
            //sSexo += "[" + sexo.val() + "]";


            qtd++;


        });

    });




    $('.make-switch').click(function () {
        var obj = $(this).find("input");
        obj.val((obj.val()=="True"?"False":"True"));
    });

    $(".btn-gerar-sku").click(function () {
        var titulo = removerAcentos($("#" + campo + "txtNome").val().toLowerCase());
        var re = new RegExp(titulo, 'g');
        titulo = titulo.replace(new RegExp('  ', 'g'), ' ');
        titulo = titulo.replace(new RegExp(' ', 'g'), '-');
        $("#" + campo + "txtSKU").val(titulo);
    });
    $(".btn-gerar-sku-item").click(function () {
        var titulo = removerAcentos($("#" + campo + "txtNome").val().toLowerCase());

        var obj = $(this).parent().parent().find("input");

        var iditem = $(obj).attr("id").replace("Item_SKU_", "");

        $.each($(".box-alternativas-variacoes input[type=checkbox]"), function (key, value) {
            var i = value.id.replace("chkItem", "");
            if (Number(i) == 1 && $("#" + value.id).attr("checked") == "checked")
                titulo += "-" + $("#Item_cor_" + iditem).val();
            else if (Number(i) == 2 && $("#" + value.id).attr("checked") == "checked") {
                titulo += "-" + $("#Item_cor1_" + iditem).val();
                titulo += "-" + $("#Item_cor2_" + iditem).val();
            }
            else if (Number(i) > 2 && $("#" + value.id).attr("checked") == "checked") {
                titulo += "-" + $("#Item_Variacao_Item" + i + "_" + iditem).val();
            }
        });


        var re = new RegExp(titulo, 'g');
        titulo = titulo.replace(new RegExp('  ', 'g'), ' ');
        titulo = titulo.replace(new RegExp(' ', 'g'), '-');

        $(obj).val(titulo);
    });
});




function DeleteItem(obj) {
    event.preventDefault();

    if (confirm('Deseja EXCLUIR este item?')) {
        var obj2 = $(obj).parent().parent();
        obj2.remove();

        //setTimeout(function () {
        //    $(".bt-add-item").focus();
        //}, 500);

    }
}

function EditarItem(obj) {
    event.preventDefault();

    //clearInterval(intervalAlertAgree);
    //$(".step-two").slideUp("slow");
    //$(".step-one").slideDown("slow", function () {
    //    $("html, body").animate({ scrollTop: $('.list-traveler').offset().top - 140 }, 1000);
    //});
}

function removerAcentos(newStringComAcento) {
    var string = newStringComAcento;
    var mapaAcentosHex = {
        a: /[\xE0-\xE6]/g,
        e: /[\xE8-\xEB]/g,
        i: /[\xEC-\xEF]/g,
        o: /[\xF2-\xF6]/g,
        u: /[\xF9-\xFC]/g,
        c: /\xE7/g,
        n: /\xF1/g
    };

    for (var letra in mapaAcentosHex) {
        var expressaoRegular = mapaAcentosHex[letra];
        string = string.replace(expressaoRegular, letra);
    }

    return string;
}
//use obj dinamico $(".test").live('click', function(){


