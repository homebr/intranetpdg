﻿var arrMonth = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];

$(document).ready(function () {
    //$.getJSON('/content/js/bancos.json', function (data) {
    //    var items = [];
    //    var options = '<option value="0">Escolha um Banco</option>';
    //    $.each(data, function (key, val) {
    //        options += '<option value="' + val.code + '">' + val.code + ' - ' + val.name + '</option>';
    //    });
    //    $("#ddlBancos").html(options);

    //    $(".select-bancos").select2();

    //});
    $(document).ready(function () {
        $('.modal').on('hidden.bs.modal', function () {
            console.log('fechar modal')
            $(this).find('input:text').val('');
        });
    });
    $(".btncontasalva").click(function () {
        $(this).addClass("ativo");
        $(".btnnovaconta").removeClass("ativo");

        CarregaContas();

        $("#novaconta").hide();
        $("#contasalva").show();
    });

    $(".btnnovaconta").click(function () {
        $(this).addClass("ativo");
        $(".btncontasalva").removeClass("ativo");

        $("#novaconta").show();
        $("#contasalva").hide();
    });



    $(".btn-aceito-criarconta").bind("click", function () {
        $(".alert-criarconta").show();
        $(".alert-criarconta").html('Enviando ... ');
        $(".alert-criarconta").addClass("alert-success");

        var banco = $('#ddlBancos');
        var tipo = $('#ddlTipo');
        var agencia = $('#txtCB_Agencia');
        var conta = $('#txtCB_Conta');
        var apelido = $('#txtCB_Apelido');

        if ($("#ddlBancos option:selected").val() == '0') {
            banco.css('border', '1px solid red');
            banco.focus();
            $(".alert-criarconta").html('Favor selecionar o Banco!');
            $(".alert-criarconta").addClass("alert-danger");
            return false;
        } else {
            banco.css('border', '1px solid #818181');
        }
        if (agencia.val() == '') {
            agencia.css('border', '1px solid red');
            agencia.focus();
            $(".alert-criarconta").html('Favor preencher a Agência!');
            $(".alert-criarconta").addClass("alert-danger");
            return false;
        } else {
            agencia.css('border', '1px solid #818181');
        }
        if (conta.val() == '') {
            conta.css('border', '1px solid red');
            conta.focus();
            $(".alert-criarconta").html('Favor preencher a Conta!');
            $(".alert-criarconta").addClass("alert-danger");
            return false;
        } else {
            conta.css('border', '1px solid #818181');
        }

        data = {
            banco: $("#ddlBancos option:selected").text(),
            numerobanco: $("#ddlBancos option:selected").val(),
            tipo: $("#ddlTipo option:selected").val(),
            agencia: agencia.val(),
            conta: conta.val(),
            apelido: apelido.val(),
            modulo: "criarconta"
        };


        $("#modal-mensagem").modal("hide");


        objAlerta = "alert-criarconta";
        $.post('ws-form.ashx', data,
            function (msg) {
                $("." + objAlerta).html(msg['message']);
                if (msg['response'] == "ok") {
                    $("." + objAlerta).removeClass("alert-danger");
                    $("." + objAlerta).addClass("alert-success");

                    //salva log mensagem
                    mensagem = $("#modal-mensagem .modal-body").html();
                    $.post('ws-form.ashx', { modulo: "LogMensagens", titulo: "Alerta de criação de conta", mensagem: mensagem});

                    var banco = $('#ddlBancos');
                    var tipo = $('#ddlTipo');
                    var agencia = $('#txtCB_Agencia');
                    var conta = $('#txtCB_Conta');
                    var apelido = $('#txtCB_Apelido');

                    if ($(".alert-criarconta").hasClass("alert-success")) {
                        comentario.val('');
                        conta.val('');
                        apelido.val('');
                    }

                    setTimeout(function () {
                        $(".btncontasalva").click();
                    }, 3000);
                }
                else {
                    $("." + objAlerta).removeClass("alert-success");
                    $("." + objAlerta).addClass("alert-danger");
                }
            }
        );

        return false;
    });
    

    $(".btn-send-vincular").bind("click", function () {
        $(".alert-vincularcontas").show();
        //$(".alert-vincularcontas").html('Enviando ... ');
        //$(".alert-vincularcontas").addClass("alert-success");

        if (typeof $('input[name="chkConta"]:checked').attr("id") == 'undefined') {
            $(".alert-vincularcontas").html('Favor selecionar uma Conta Bancária!');
            $(".alert-vincularcontas").addClass("alert-danger");
            return false;
        }

        id = $('input[name="chkConta"]:checked').attr("id").replace("chkConta_","");

        sHoteis = "";
        sNomeHoteis = "";
        $('.grid-hoteis input[type=checkbox]').each(function () {
            if (this.checked) {
                if (sHoteis.length > 0) {
                    sHoteis += ",";
                    sNomeHoteis += ",";
                }
                sHoteis += $(this).attr("id").replace("chkHotel_", "");
                sNomeHoteis += $(this).parent().parent().parent().find("h5").text();
            }
        });

        if (sHoteis.length == 0) {
            $(".alert-vincularcontas").html('Favor selecionar um ou mais Hotéis!');
            $(".alert-vincularcontas").addClass("alert-danger");
            return false;
        }

        objC = $('input[name="chkConta"]:checked').parent().parent().parent();
        sconta = $(objC).find("div.col-4 span").html();
        sconta += " - " + $(objC).find("div.col-2:eq(0) li").html() + " - " + $(objC).find("div.col-2:eq(1) li").html() + " - " + $(objC).find("div.col-2:eq(2) li").html();
        $("#modal-mensagem-vincular .doc-hoteis").html(replaceAll(sNomeHoteis, ",", "<br/>"));
        $("#modal-mensagem-vincular .doc-conta").html(sconta);

        var dHoje = new Date();
        dia = dHoje.getDate();
        if (dia > 20)
            dHoje = new Date(dHoje.setMonth(dHoje.getMonth() + 2));
        else
            dHoje = new Date(dHoje.setMonth(dHoje.getMonth() + 1));

        sdata = arrMonth[dHoje.getMonth()] + " de " + dHoje.getFullYear();
        $("#modal-mensagem-vincular .doc-data").html(sdata);
        $("#modal-mensagem-vincular").modal("show");

        return false;
    });

    $(".btn-aceito-vincular").bind("click", function () {
        $(".alert-vincularcontas").show();
        $(".alert-vincularcontas").html('Enviando ... ');
        $(".alert-vincularcontas").addClass("alert-success");

        if (typeof $('input[name="chkConta"]:checked').attr("id") == 'undefined') {
            $(".alert-vincularcontas").html('Favor selecionar uma Conta Bancária!');
            $(".alert-vincularcontas").addClass("alert-danger");
            return false;
        }

        id = ($('input[name="chkConta"]:checked').attr("id").replace("chkConta_", ""));

        sHoteis = "";
        $('.grid-hoteis input[type=checkbox]').each(function () {
            if (this.checked) {
                if (sHoteis.length > 0)
                    sHoteis += ",";
                sHoteis += $(this).attr("id").replace("chkHotel_", "");

            }
        });

        if (sHoteis.length == 0) {
            $(".alert-vincularcontas").html('Favor selecionar um ou mais Hotéis!');
            $(".alert-vincularcontas").addClass("alert-danger");
            return false;
        }

        data = {
            banco: id,
            hoteis: sHoteis,
            modulo: "vincular"
        };

        objAlerta = "alert-vincularcontas";

        $("#modal-mensagem-vincular").modal("hide");

        $.post('ws-form.ashx', data,
            function (msg) {
                $("." + objAlerta).html(msg['message']);
                if (msg['response'] == "ok") {
                    $("." + objAlerta).removeClass("alert-danger");
                    $("." + objAlerta).addClass("alert-success");


                    //salva log mensagem
                    mensagem = $("#modal-mensagem-vincular .modal-body").html();
                    $.post('ws-form.ashx', { modulo: "LogMensagens", titulo: "Alerta de vinculação de conta", mensagem: mensagem });

                    setTimeout(function () {
                        CarregaContas();
                        CarregaHoteis();
                        $("." + objAlerta).html('');
                        $("." + objAlerta).removeClass("alert-success");
                    }, 3000);
                }
                else {
                    $("." + objAlerta).removeClass("alert-success");
                    $("." + objAlerta).addClass("alert-danger");
                }
            }
        );

        return false;
    });

    $(".btn-aceito-vincular-a").bind("click", function () {
        $(".alert-vincularcontas").show();
        $(".alert-vincularcontas").html('Enviando ... ');
        $(".alert-vincularcontas").addClass("alert-success");

        if (typeof $('input[name="chkConta"]:checked').attr("id") == 'undefined') {
            $(".alert-vincularcontas").html('Favor selecionar uma Conta Bancária!');
            $(".alert-vincularcontas").addClass("alert-danger");
            return false;
        }

        idconta = $('input[name="chkConta"]:checked').attr("id").replace("chkConta_", "");
        id = $('#hIDContaVinculada').val();


        data = {
            id: id,
            idconta: idconta,
            modulo: "alterarconta"
        };

        objAlerta = "alert-vincularcontas";

        $("#modal-mensagem-vincular-a").modal("hide");

        $.post('ws-form.ashx', data,
            function (msg) {
                $("." + objAlerta).html(msg['message']);
                if (msg['response'] == "ok") {
                    $("." + objAlerta).removeClass("alert-danger");
                    $("." + objAlerta).addClass("alert-success");


                    //salva log mensagem
                    mensagem = $("#modal-mensagem-vincular-a .modal-body").html();
                    $.post('ws-form.ashx', { modulo: "LogMensagens", titulo: "Alerta de alteração de conta", mensagem: mensagem });

                    CarregaHoteis();

                    setTimeout(function () {
                        $(".alert-vincularcontas").html('');
                        $(".alert-vincularcontas").removeClass("alert-success");
                    }, 3000);
                }
                else {
                    $("." + objAlerta).removeClass("alert-success");
                    $("." + objAlerta).addClass("alert-danger");
                }
            }
        );

        return false;
    });

    CarregaFeed();

  
});


function CarregaFeed() {


    data = {
        modulo: "listafeed"
    };


    $.post('/ws-form.ashx', data,
        function (result) {

            items = JSON.parse(result);
            sHtml = "";
            sHtmlFim = "";
            sHtmlModelo = $("#modelo-feed").html();
            i = 0;
            items.forEach(function (item) {

                sHtml = sHtmlModelo;


                if (item["video"].length > 1) {
                    sAux = "<div><iframe width=\"80%\" height=\"350\" src=\"" + item["video"].replace("watch?v=", "embed/") + "\" frameborder=\"0\" allowfullscreen></iframe></div>";
                    sHtml = sHtml.replace("#VIDEO#", sAux);
                }
                else
                    sHtml = sHtml.replace("#VIDEO#", "");


                if (item["qtd2"] > 0) {
                 
                    sAux = "fa fa-thumbs-up";
                    sHtml = sHtml.replace("#curtida#", sAux);
                 
                }
                else {

                    sAux = "far fa-thumbs-up";
                    sHtml = sHtml.replace("#curtida#", sAux);
                }

                if (item["link"].length > 1) {
                    sAux = "<a href=\"" + item["link"] + "\" target=\"_blank\" >";
                    //sAux = "<div><iframe width=\"80%\" height=\"350\" src=\"" + item["video"].replace("watch?v=", "embed/") + "\" frameborder=\"0\" allowfullscreen></iframe></div>";
                    sHtml = sHtml.replace("#link#", sAux);
                }
                else
                {

                }

                sHtml = sHtml.replace("#idfeed#", item["id"]);
                sHtml = sHtml.replace("#idfeed1#", item["id"]);
                sHtml = sHtml.replace("#qtd#", item["qtd"]);
                sHtml = sHtml.replace("#qtd2#", item["qtd2"]);
                sHtml = sHtml.replace("#idtipo#", item["idtipo"]);
                sHtml = sHtml.replace("#idtipo1#", item["idtipo"]);
                sHtml = sHtml.replace("#CHAMADA#", item["chamada"]);
                //sHtml = sHtml.replace("#conteudo#", item["conteudo"]);
                sHtml = sHtml.replace("#data#", item["data"]);
                sHtml = sHtml.replace("#idcolaborador#", item["idcolaborador"]);
                sHtml = sHtml.replace("#TITULO#", item["titulo"]);
                sHtml = sHtml.replace("#TIPO#", item["tipo"]);
                sHtml = sHtml.replace("#IMGLINK#", item["imglink"]);
                sHtml = sHtml.replace("#link#", item["link"]);


                sHtml = sHtml.replace("#LINK1#", item["link1"]);
                i++;
                $("#boxTimeline").append(sHtml);
            });


            // colocar as funoes aqui

            $(".btn-abre-curtidas").click(function (e) {
                //e.preventDefault();
                id = $(this).data("id");
                idtipo = $(this).data("idtipo");
                //idcolaborador = $(this).data("idcolaborador");


                data = {
                    modulo: "listartipocurtidas",
                    id: id,
                    idtipo: idtipo,
                    //idcolaborador: idcolaborador
                };

                //var btnGridcurtidas = $(this).parent().parent().parent().find(".grid-curtidas");
                $.post('/ws-form.ashx', data,
                    function (result) {

                        items = JSON.parse(result);
                        itens1 = items["listasoma"];
                        itens2 = items["listanomes"];
                        sHtml = "";
                   
                        itens1.forEach(function (item) {
                         '"'
                            sHtml += "    <span> " + item["Soma"] + " </span > \r\n  ";
                            sHtml += "  <img src=\"../../" + item["Icone"] + "\" class=\"emoji-data\" />\r\n  ";
                        

                        });
                        sHtml += "<br> <hr> \r\n ";
                        itens2.forEach(function (item) {
                            '"'
                            sHtml += "<br>  \r\n ";
                            sHtml += "  <img src=\"../../" + item["Icone"] + "\" class=\"emoji-data\" />\r\n  ";
                            sHtml += "    <span> " + item["nome"] + " </span > \r\n  ";
                         


                        });

                        $("#modal-curtidas .modal-body").html(sHtml);
                        $("#modal-curtidas").modal("show");
                        //btnGridcurtidas.html(sHtml);

                    }
                );


            });

            $(".btn-abre-comentarios").click(function (e) {
                //e.preventDefault();
                id = $(this).data("id");
                idtipo = $(this).data("idtipo");
                idcolaborador = $(this).data("idcolaborador");


                data = {
                    modulo: "listarcomentarios",
                    id: id,
                    idtipo: idtipo,
                    idcolaborador: idcolaborador
                };

                var btnGridContas = $(this).parent().parent().parent().find(".grid-contas");
                $.post('/ws-form.ashx', data,
                    function (result) {

                        items = JSON.parse(result);
                        sHtml = "";
                        items.forEach(function (item) {
                            sHtml += "  <div class=\"direct-chat-msg\" > \r\n  ";
                            sHtml += "  <div class=\"direct-chat-infos clearfix\" > \r\n  ";
                            sHtml += "    <span class=\"direct-chat-name float-left\"> " + item["Nome"] + " </span > \r\n  ";
                            sHtml += "    <span class=\"direct-chat-timestamp float-right\">" + item["Data"] + "</span > \r\n  ";
                            sHtml += "  </div > \r\n ";
                            sHtml += "  <img class=\"direct-chat-img\" src=\"uploads/colaboradores/" + item["Foto"] + " \" alt=\"message user image\" > \r\n  ";
                            sHtml += "  <div class=\"direct-chat-text\"> " + item["Comentario"] + " </div > \r\n  ";
                            sHtml += "  </div > \r\n  ";
                            sHtml += "  <div class=\"col-1 editarcontas\" > \r\n ";

                        });


                        btnGridContas.html(sHtml);

                    }
                );


            });

            $(".btn-send-criarcomentario").bind("click", function () {
                $(".alert-criarcomentario").show();


                var btnDiv = $(this).parent().parent().parent().parent().parent();

                id = btnDiv.find(".btn-abre-comentarios").data("id");
                idtipo = btnDiv.find(".btn-abre-comentarios").data("idtipo");
                idcolaborador = btnDiv.find(".btn-abre-comentarios").data("idcolaborador");
                qtdcomentarios = Number(btnDiv.find(".card-tools span").text());

                //var tipo = btnDiv.find('#ddlTipo');
                var comentario = btnDiv.find('#txtComentario');
                if (comentario.val() == '') {
                    comentario.css('border', '1px solid red');
                    comentario.focus();
                    //$(".alert-criarcomentario").html('Favor preencher o Comentário!');
                    //$(".alert-criarcomentario").addClass("alert-danger");
                    return false;
                } else {
                    // conta.css('border', '1px solid #818181');
                }

                data = {
                    comentario: comentario.val(),
                    idfeed: id,
                    idtipofeed: idtipo,
                    idcolaborardor: idcolaborador,
                    modulo: "criarcomentario"
                };


                //$("#modal-mensagem").modal("hide");


                //objAlerta = "alert-criarcomentario";
                $.post('ws-form.ashx', data,
                    function (msg) {
                        //$("." + objAlerta).html(msg['message']);
                        if (msg['response'] == "ok") {
                           // $("." + objAlerta).removeClass("alert-danger");
                           // $("." + objAlerta).addClass("alert-success");

                            //salva log mensagem
                            //mensagem = $("#modal-mensagem .modal-body").html();
                            //$.post('ws-form.ashx', { modulo: "LogMensagens", titulo: "Alerta de criação de conta", mensagem: mensagem });

                            btnDiv.find(".btn-abre-comentarios").click();
                            qtdcomentarios++;
                            btnDiv.find(".card-tools span").text(qtdcomentarios);
                            //if ($(".alert-criarcomentario").hasClass("alert-success")) {
                                comentario.val('');
                            //}
                            
                            //$(".card direct-chat direct-chat-primary").addClass("collapsed-card");
                        }
                        else {
                            $("." + objAlerta).removeClass("alert-success");
                            $("." + objAlerta).addClass("alert-danger");
                        }
                    }
                );

                return false;
            });

            $(".emoji-section ul li img").bind("click", function (e) {
                e.preventDefault();
                // $(".alert-criarcomentario").show();


                var btnDiv = $(this).parent().parent().parent().parent().parent();

                id = btnDiv.find(".btn-abre-comentarios").data("id");
                idtipo = btnDiv.find(".btn-abre-comentarios").data("idtipo");
                idcolaborador = btnDiv.find(".btn-abre-comentarios").data("idcolaborador");
                tipo = $(this).data("tipo");
                emoji = $(this).data("emoji-rating");
                qtd = $(this).data("emoji-rating-count");
                //qtdcurtidas = Number($(this).data("emoji-rating-count"));
                qtdcurtidas = Number(btnDiv.find(".emoji-rating-count span").text());
             
                data = {
                    //comentario: comentario.val(),
                    idfeed: id,
                    idtipofeed: idtipo,
                    idcolaborardor: idcolaborador,
                    tipo: tipo,
                    modulo: "criarcurtida"
                };


                //$("#modal-mensagem").modal("hide");


                //objAlerta = "alert-criarcomentario";
                $.post('ws-form.ashx', data,
                    function (msg) {
                        //$("." + objAlerta).html(msg['message']);
                        if (msg['response'] == "ok") {
                            // $("." + objAlerta).removeClass("alert-danger");
                            // $("." + objAlerta).addClass("alert-success");
                            //CarregaFeed();
                            //salva log mensagem
                            //mensagem = $("#modal-mensagem .modal-body").html();
                            //$.post('ws-form.ashx', { modulo: "LogMensagens", titulo: "Alerta de criação de conta", mensagem: mensagem });

                            //btnDiv.find(".btn-abre-comentarios").click();

                            //if ($(".alert-criarcomentario").hasClass("alert-success")) {
                            //comentario.val('');
                            //}
                            //$(".btn btn-tool .btn-criarcurtida").removeClass("btn-criarcurtida");
                            //$(".btn btn-tool").addClass("btn-removercurtida");

                            btnDiv.find(".fa-thumbs-up").removeClass("far");
                            btnDiv.find(".fa-thumbs-up").addClass("fa");
                            qtdcurtidas++;

                            btnDiv.find(".emoji-rating-count span").text(qtdcurtidas);

                            //qtdcomentarios++;
                            //btnDiv.find(".card-tools span").text(qtdcomentarios);
                        }
                        else {
                            //$("." + objAlerta).removeClass("alert-success");
                            //$("." + objAlerta).addClass("alert-danger");
                        }
                    }
                );

                return false;
            });


            //$(".btn-criarcurtida").bind("click", function () {
            //   // $(".alert-criarcomentario").show();


            //    var btnDiv = $(this).parent().parent().parent().parent().parent();

            //    id = btnDiv.find(".btn-abre-comentarios").data("id");
            //    idtipo = btnDiv.find(".btn-abre-comentarios").data("idtipo");
            //    idcolaborador = btnDiv.find(".btn-abre-comentarios").data("idcolaborador");


            //    data = {
            //        //comentario: comentario.val(),
            //        idfeed: id,
            //        idtipofeed: idtipo,
            //        idcolaborardor: idcolaborador,
            //        modulo: "criarcurtida"
            //    };


            //    //$("#modal-mensagem").modal("hide");


            //    //objAlerta = "alert-criarcomentario";
            //    $.post('ws-form.ashx', data,
            //        function (msg) {
            //            //$("." + objAlerta).html(msg['message']);
            //            if (msg['response'] == "ok") {
            //                // $("." + objAlerta).removeClass("alert-danger");
            //                // $("." + objAlerta).addClass("alert-success");
            //                //CarregaFeed();
            //                //salva log mensagem
            //                //mensagem = $("#modal-mensagem .modal-body").html();
            //                //$.post('ws-form.ashx', { modulo: "LogMensagens", titulo: "Alerta de criação de conta", mensagem: mensagem });

            //                //btnDiv.find(".btn-abre-comentarios").click();

            //                //if ($(".alert-criarcomentario").hasClass("alert-success")) {
            //                //comentario.val('');
            //                //}
            //                $(".btn btn-tool btn-criarcurtida").removeClass("btn-criarcurtida");
            //                $(".btn btn-tool").addClass("btn-removercurtida");
            //            }
            //            else {
            //                //$("." + objAlerta).removeClass("alert-success");
            //                //$("." + objAlerta).addClass("alert-danger");
            //            }
            //        }
            //    );
                
            //    return false;
            //});


            //$(".btn-removercurtida").bind("click", function () {
            //   // $(".alert-criarcomentario").show();


            //    var btnDiv = $(this).parent().parent().parent().parent().parent();

            //    id = btnDiv.find(".btn-abre-comentarios").data("id");
            //    idtipo = btnDiv.find(".btn-abre-comentarios").data("idtipo");
            //    idcolaborador = btnDiv.find(".btn-abre-comentarios").data("idcolaborador");


            //    data = {
            //        //comentario: comentario.val(),
            //        idfeed: id,
            //        idtipofeed: idtipo,
            //        idcolaborardor: idcolaborador,
            //        modulo: "removercurtida"
            //    };


            //    //$("#modal-mensagem").modal("hide");


            //    //objAlerta = "alert-criarcomentario";
            //    $.post('ws-form.ashx', data,
            //        function (msg) {
            //            //$("." + objAlerta).html(msg['message']);
            //            if (msg['response'] == "ok") {
            //                // $("." + objAlerta).removeClass("alert-danger");
            //                // $("." + objAlerta).addClass("alert-success");
            //               // CarregaFeed();
            //                //salva log mensagem
            //                //mensagem = $("#modal-mensagem .modal-body").html();
            //                //$.post('ws-form.ashx', { modulo: "LogMensagens", titulo: "Alerta de criação de conta", mensagem: mensagem });

            //               // btnDiv.find(".btn-abre-comentarios").click();

            //                //if ($(".alert-criarcomentario").hasClass("alert-success")) {
            //                //comentario.val('');
            //                //}

            //                //$(".card direct-chat direct-chat-primary").addClass("collapsed-card");
            //            }
            //            else {
            //               // $("." + objAlerta).removeClass("alert-success");
            //               // $("." + objAlerta).addClass("alert-danger");
            //            }
            //        }
            //    );

            //    return false;
            //});


        }
    );
}


function CarregaComentario() {


    data = {
        modulo: "listacontas"
    };


    $.post('/ws-form.ashx', data,
        function (result) {

            items = JSON.parse(result);
            sHtml = "";
            items.forEach(function (item) {
                //sHtml += "<div class=\"row\"> \r\n ";
                //sHtml += "  <div class=\"col-1\"> \r\n ";
                //sHtml += "      <div class=\"radio\"><input type=\"radio\" id=\"chkConta_" + item["ID"] + "\" name=\"chkConta\" class=\"magic-radio\" ><label for=\"chkConta_" + item["ID"] + "\"></label></div> \r\n ";
                //sHtml += "  </div > \r\n ";
                //sHtml += "  <div class=\"col-4 col-md-3\" > \r\n ";
                //sHtml += "  <li> <span>" + item["Comentario"] + " </b></span></li > \r\n ";
                //sHtml += "  </div > \r\n ";

                sHtml += "  <div class=\"direct-chat-msg\" > \r\n  ";
                sHtml += "  <div class=\"direct-chat-infos clearfix\" > \r\n  ";
                sHtml += "    <span class=\"direct-chat-name float-left\"> " + item["Nome"] + " </span > \r\n  ";
                sHtml += "    <span class=\"direct-chat-timestamp float-right\">" + item["Data"] + "</span > \r\n  ";
                sHtml += "  </div > \r\n ";
                sHtml += "  <img class=\"direct-chat-img\" src=\"uploads/colaboradores/" + item["Foto"] + " \" alt=\"message user image\" > \r\n  ";
                sHtml += "  <div class=\"direct-chat-text\"> " + item["Comentario"] + " </div > \r\n  ";
                 sHtml += "  </div > \r\n  ";

                //sHtml += "  <div class=\"col-2\" > \r\n ";
                //sHtml += "  <li>" + (item["Tipo"] == 2 ? "Poupança" : "Corrente") + "</li> \r\n ";
                //sHtml += "  </div > \r\n ";
                //sHtml += "  <div class=\"col-2\" > \r\n ";
                //sHtml += "  <li>" + item["Agencia"] + "</li > \r\n ";
                //sHtml += "  </div > \r\n ";
                //sHtml += "  <div class=\"col-2 col-md-3\" > \r\n ";
                //sHtml += "  <li>" + item["Conta"] + "</li > \r\n ";
                //sHtml += "  </div > \r\n ";
                sHtml += "  <div class=\"col-1 editarcontas\" > \r\n ";
                //if (Number(item["Qtd"]) == 0)
                //    sHtml += "  <a href=\"javascript:void(0);\" onclick=\"removerConta(this," + item["ID"] + ");\"><i class=\"far fa-trash-alt\" ></i ></a> \r\n ";
                //sHtml += "  </div > \r\n ";
                //sHtml += "  </div > \r\n ";
            });



            $(".grid-contas").html(sHtml);



        }
    );
}


function removerConta(obj, id) {
    if (confirm('Remover está Conta?')) {

        data = {
            id: id,
            modulo: "removerconta"
        };

        $.post('/ws-form.ashx', data,
            function (msg) {
                $(".alert-gridcontas").html(msg['message']);
                if (msg['response'] == "ok") {
                    $(".alert-gridcontas").removeClass("alert-danger");
                    $(".alert-gridcontas").addClass("alert-success");
                    $(obj).parent().parent().remove();
                }
                else {
                    $(".alert-gridcontas").removeClass("alert-success");
                    $(".alert-gridcontas").addClass("alert-danger");
                }
            }
        );

    }
}

function escapeRegExp(string) {
    return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
}
function replaceAll(str, term, replacement) {
    return str.replace(new RegExp(escapeRegExp(term), 'g'), replacement);
}