﻿

jQuery(document).ready(function () {
    var idloja = $('#ContentPlaceHolder1_hID').val();

    $(".bt-save-empresa").bind("click", function () {

        $(".alert-empresa").show();
        $(".alert-empresa").html('Enviando ... ');
        $(".alert-empresa").addClass("alert-success");

        var nome = $('#txtEmpresa_Nome');
        var telefone = $('#txtEmpresa_Telefone');
        //var endereco = $('#txtEmpresa_Endereco');

        if (nome.val() == '') {
            nome.css('border', '1px solid red');
            $(".alert-empresa").html('Favor preencher o campo Nome!');
            $(".alert-empresa").addClass("alert-danger");
            return false;
        } else {
            nome.css('border', '1px solid transparent');
        }
        if (telefone.val() == '') {
            telefone.css('border', '1px solid red');
            $(".alert-empresa").html('Favor preencher o campo Telefone!');
            $(".alert-empresa").addClass("alert-danger");
            return false;
        } else {
            telefone.css('border', '1px solid transparent');
        }
        //if (endereco.val() == '') {
        //    endereco.css('border', '1px solid red');
        //    $(".alert-empresa").html('Favor preencher o campo Endereço!');
        //    $(".alert-empresa").addClass("alert-danger");
        //    return false;
        //} else {
        //    endereco.css('border', '1px solid transparent');
        //}


        saveInfo({
            nome: nome.val(),
            telefone: telefone.val(),
            endereco: endereco.val(),
            idloja: idloja,
            modulo: 'empresa'
        }, "alert-empresa");

        return false;
    });

    $(".bt-save-redes").bind("click", function () {
        //alert(".bt-save-redes");
        $(".alert-redes").show();
        $(".alert-redes").html('Enviando ... ');
        $(".alert-redes").addClass("alert-success");

        var facebook = $('#txtFacebook');
        var youtube = $('#txtYoutube');
        var instagram = $('#txtInstagram');

        saveInfo({
            facebook: facebook.val(),
            youtube: youtube.val(),
            instagram: instagram.val(),
            idloja: idloja,
            modulo: 'redes'
        }, "alert-redes");

        return false;
    });

    $(".bt-save-smtp").bind("click", function () {

        $(".alert-smtp").show();
        $(".alert-smtp").html('Enviando ... ');
        $(".alert-smtp").addClass("alert-success");

        var servidor = $('#txtSMTP_Servidor');
        var usuario = $('#txtSMTP_Usuario');
        var senha = $('#txtSMTP_Senha');
        var porta = $('#txtSMTP_Porta');
        var ssl = $('#chkSMTP_Ativo');
        var remetente = $('#txtSMTP_Remetente');
        
        saveInfo({
            servidor: servidor.val(),
            usuario: usuario.val(),
            senha: senha.val(),
            porta: porta.val(),
            ssl: ssl.val(),
            remetente: remetente.val(),
            idloja: idloja,
            modulo: 'smtp'
        }, "alert-smtp");

        return false;
    });

    $(".bt-save-destinatarios").bind("click", function () {


        $(".alert-destinatarios").show();
        $(".alert-destinatarios").html('Enviando ... ');
        $(".alert-destinatarios").addClass("alert-success");


        var news_para = $('#txtNewsLetter_Para');
        var news_cc = $('#txtNewsLetter_Cc');


        saveInfo({
            news_para: news_para.val(),
            news_cc: news_cc.val(),
            idloja: idloja,
            modulo: 'destinatarios'
        }, "alert-destinatarios");

        return false;
    });

    $(".bt-save-chat").bind("click", function () {
        $(".alert-chat").show();
        $(".alert-chat").html('Enviando ... ');
        $(".alert-chat").addClass("alert-success");

        var chat = $('#txtChat');

        saveInfo({
            chat: chat.val(),
            modulo: 'chat'
        }, "alert-chat");

        return false;
    });

    $(".bt-save-google").bind("click", function () {
        $(".alert-google").show();
        $(".alert-google").html('Enviando ... ');
        $(".alert-google").addClass("alert-success");

        var google = $('#txtScript');

        saveInfo({
            google: google.val(),
            idloja: idloja,
            modulo: 'google'
        }, "alert-google");

        return false;
    });

    $(".bt-send-exemplo").bind("click", function () {
        $(".alert-cadastro").show();
        $(".alert-cadastro").html('Enviando ... ');
        $(".alert-cadastro").addClass("alert-success");

        var nome = $('#cadastro_nome');
        var email = $('#cadastro_email');
        var telefone = $('#cadastro_telefone');
        var empresa = $('#cadastro_empresa');
        var endereco = $('#cadastro_endereco');
        var numero = $('#cadastro_numero');
        var bairro = $('#cadastro_bairro');
        var cep = $('#cadastro_cep');
        var cidade = $('#cadastro_cidade');
        var estado = $('#cadastro_estado');

        //alert("nome=" + nome);

        if (nome.val() == '') {
            nome.css('border', '1px solid red');
            $(".alert-cadastro").html('Favor preencher o campo Nome!');
            $(".alert-cadastro").addClass("alert-danger");
            return false;
        } else {
            nome.css('border', '1px solid transparent');
        }
        if (email.val() == '') {
            email.css('border', '1px solid red');
            $(".alert-cadastro").html('Favor preencher o campo E-mail!');
            $(".alert-cadastro").addClass("alert-danger");
            return false;
        } else {
            email.css('border', '1px solid transparent');
        }
        if (!validateEmail(email.val())) {
            email.css('border', '1px solid red');
            $(".alert-cadastro").html('Favor preencher o campo E-mail corretamente!');
            $(".alert-cadastro").addClass("alert-danger");
            return false;
        } else {
            email.css('border', '1px solid transparent');
        }

        if (telefone.val() == '') {
            telefone.css('border', '1px solid red');
            $(".alert-cadastro").html('Favor preencher o campo Telefone!');
            $(".alert-cadastro").addClass("alert-danger");
            return false;
        } else {
            telefone.css('border', '1px solid transparent');
        }

        //alert("nome=" + nome);
        saveInfoCadastro({
            nome: nome.val(),
            email: email.val(),
            telefone: telefone.val(),
            empresa: empresa.val(),
            endereco: endereco.val(),
            numero: numero.val(),
            bairro: bairro.val(),
            cep: cep.val(),
            cidade: cidade.val(),
            estado: estado.val()
        });

        return false;
    });


    function saveInfo(data, objAlerta, call_when_saved) {
        this.data = data;
        $.post('wsConfiguracoes.ashx', data,
            function (msg) {
                //console.log(msg);
                $("." + objAlerta).html(msg['message']);
                if (msg['response'] == "ok") {
                    $("." + objAlerta).removeClass("alert-danger");
                    $("." + objAlerta).addClass("alert-success");
                }
                else {
                    $("." + objAlerta).removeClass("alert-success");
                    $("." + objAlerta).addClass("alert-danger");
                }
            }
        );
    }



    function validateEmail(sEmail) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(sEmail)) {
            return true;
        }
        return false;
    }

    //$("#telefone").mask("(99) 9999-9999?9",
    //  {
    //      completed: function () {
    //          $("#telefone").mask("(99) 99999-9999");
    //      }
    //  });

});


function replaceAll(str, de, para) {
    var pos = str.indexOf(de);
    while (pos > -1) {
        str = str.replace(de, para);
        pos = str.indexOf(de);
    }
    return (str);
}