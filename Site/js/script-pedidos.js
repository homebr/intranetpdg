﻿// Create user jQuery function 'getDate'
(function ($) {
    $.fn.getDate = function (format) {

        var gDate = new Date();
        var mDate = {
            'S': gDate.getSeconds(),
            'M': gDate.getMinutes(),
            'H': gDate.getHours(),
            'd': gDate.getDate(),
            'm': gDate.getMonth() + 1,
            'y': gDate.getFullYear(),
        }

        // Apply format and add leading zeroes
        return format.replace(/([SMHdmy])/g, function (key) { return (mDate[key] < 10 ? '0' : '') + mDate[key]; });

        return getDate(str);
    };
})(jQuery);

jQuery(document).ready(function () {

    var idloja = $('#ContentPlaceHolder1_hIDLoja').val();


    $(".btn-observacoes-salvar").bind("click", function () {
        $(".alert-observacoes").show();
        $(".alert-observacoes").html('Enviando ... ');
        $(".alert-observacoes").addClass("alert-success");

        //var titulo = $('#titulo');
        var observacao = $('#text-observacao');
        var idpedido = $("#" + campo + "hID");
        var observacao_por_email = ($("#observacao_por_email").attr("checked") == "checked"?"true":"false");

        if (observacao.val() == '') {
            observacao.css('background', '#FFF');
            observacao.css('color', '#666');
            $(".alert-observacoes").html('Favor preencher a Observação!');
            $(".alert-observacoes").addClass("alert-danger");
            return false;
        } else {
            observacao.css('background', '#31dfba');
            observacao.css('color', '#FFF');
        }
        data = {
            idpedido: idpedido.val(),
            observacao: observacao.val(),
            observacao_por_email: observacao_por_email
        }


        $.post('wsPedidos.ashx?acao=formobservacoes', data,
        function (msg) {
            $(".alert-observacoes").html(msg['message']);
            if (msg['response'] == "ok") {
               
                //$('#historico_id').val(msg['id']);
                $(".alert-observacoes").html('Salvo com sucesso!');
                $(".alert-observacoes").removeClass("alert-danger");
                $(".alert-observacoes").addClass("alert-success");

                var html = $(".modelo-panel-observacoes").clone().appendTo(".pedido-venda-observacoes");
                var novo = $(".pedido-venda-observacoes .panel").last();
                novo.show();
                novo.find(".panel-body").html($('#text-observacao').val());
                var texto = $().getDate("d/m/y H:M") + " - Enviado por: ";
                if  (observacao_por_email=="true")
                    texto += "<span class=\"glyphicon glyphicon-envelope\" style=\"margin-left: 10px;\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"\" data-original-title=\"Mensagem enviada ao cliente\"></span>";
                novo.find(".panel-title").html(texto);
                

                $('#text-observacao').val('');
            }
            else {
                $(".alert-observacoes").removeClass("alert-success");
                $(".alert-observacoes").addClass("alert-danger");
            }
        });

        return false;
    });

    $(".btn-rastreamento-salvar").bind("click", function (event) {
        event.preventDefault();
        $(".alert-rastreamento").show();
        $(".alert-rastreamento").html('Enviando ... ');
        $(".alert-rastreamento").addClass("alert-success");

        var codigo = $('#' + campo + 'txtEnvioRastreamento');
        var idpedido = $("#" + campo + "hID");

        if (codigo.val() == '') {
            codigo.css('background', '#FFF');
            codigo.css('color', '#666');
            $(".alert-rastreamento").html('Favor preencher o código de Rastreamento!');
            $(".alert-rastreamento").addClass("alert-danger");
            return false;
        } else {
            codigo.css('background', '#31dfba');
            codigo.css('color', '#FFF');
        }
        data = {
            idpedido: idpedido.val(),
            rastreamento: codigo.val()
        }


        $.post('wsPedidos.ashx?acao=formrastreamento', data,
        function (msg) {
            $(".alert-rastreamento").html(msg['message']);
            if (msg['response'] == "ok") {
                $(".alert-rastreamento").html('Salvo com sucesso!');
                $(".alert-rastreamento").removeClass("alert-danger");
                $(".alert-rastreamento").addClass("alert-success");
            }
            else {
                $(".alert-rastreamento").removeClass("alert-success");
                $(".alert-rastreamento").addClass("alert-danger");
            }
        });

        return false;
    });

    $('#acompanharPedido').on('click', function (event) {
        event.preventDefault();
        div = $('#mostrarAcompanhamento');

        var codigo = $('#' + campo + 'txtEnvioRastreamento');
        var idpedido = $("#" + campo + "hID");
        data = {
            idpedido: idpedido.val(),
            rastreamento: codigo.val()
        }

        $.post('wsPedidos.ashx?acao=buscarastreamento', data,
        function (msg) {
            
            if (msg['response'] == "ok") {
                div.find('.sucesso').html(msg['conteudo']);
                $(".alert-rastreamento").removeClass("alert-danger");
                div.find('.sucesso').show();
            } else {
                div.find('.sucesso').hide();
                div.find('.erro').show();
            }
            div.slideDown();
        });


    });

    $(".btn-status-salvar").bind("click", function (event) {
        event.preventDefault();
        $(".alert-geral").show();
        $(".alert-geral").html('Enviando ... ');
        $(".alert-geral").addClass("alert-success");


        var status = $('#situacao_id').find('option:selected');
        var idpedido = $("#" + campo + "hID");

        var banco = "";
        if (Number(status.val()) == 4)
            banco = $('#deposito_banco').find('option:selected').val();
        

        data = {
            idpedido: idpedido.val(),
            banco: banco,
            idloja: idloja,
            status: status.val()
        };


        $("#" + campo + "hStatus").val(status.val());

        $.post('wsPedidos.ashx?acao=alterastatus', data,
        function (msg) {
            $(".alert-geral").html(msg['message']);
            if (msg['response'] == "ok" && msg['message'].indexOf("Erro") == -1) {
                //$(".alert-geral").html('Salvo com sucesso!');
                $(".alert-geral").removeClass("alert-danger");
                $(".alert-geral").addClass("alert-success");
                setTimeout(function () { $(".alert").hide(); },20000);
            }
            else {
                $(".alert-geral").removeClass("alert-success");
                $(".alert-geral").addClass("alert-danger");
            }
        });

        return false;
    });

    $(".btn-observacao-salvar").bind("click", function (event) {
        event.preventDefault();
        $(".alert-observacao").show();
        $(".alert-observacao").html('Enviando ... ');
        $(".alert-observacao").addClass("alert-success");


        var observacao = $('#pedido-observacao');
        var idpedido = $("#" + campo + "hID");

        if (observacao.val() == '') {
            observacao.css('border', 'red');
            $(".alert-observacao").html('Favor preencher a Observação do Pedido!');
            $(".alert-observacao").addClass("alert-danger");
            return false;
        } else {
            observacao.css('border', 'none');
        }

        data = {
            idpedido: idpedido.val(),
            observacao: observacao.val()
        };


        $.post('wsPedidos.ashx?acao=observacao-pedido', data,
            function (msg) {
                $(".alert-geral").html(msg['message']);
                if (msg['response'] == "ok") {
                    $(".alert-observacao").html('Salvo com sucesso!');
                    $(".alert-observacao").removeClass("alert-danger");
                    $(".alert-observacao").addClass("alert-success");
                }
                else {
                    $(".alert-observacao").removeClass("alert-success");
                    $(".alert-observacao").addClass("alert-danger");
                }
            });

        return false;
    });

    
});


//

function DeleteItem(obj) {

    if (confirm('Deseja EXCLUIR este item?')) {
        var obj2 = $(obj).parent().parent();
        obj2.remove();

        //setTimeout(function () {
        //    $(".bt-add-item").focus();
        //}, 500);

    }
    return false;
}

function EditarItem(obj) {
    return false;

    //clearInterval(intervalAlertAgree);
    //$(".step-two").slideUp("slow");
    //$(".step-one").slideDown("slow", function () {
    //    $("html, body").animate({ scrollTop: $('.list-traveler').offset().top - 140 }, 1000);
    //});
}

function removerAcentos(newStringComAcento) {
    var string = newStringComAcento;
    var mapaAcentosHex = {
        a: /[\xE0-\xE6]/g,
        e: /[\xE8-\xEB]/g,
        i: /[\xEC-\xEF]/g,
        o: /[\xF2-\xF6]/g,
        u: /[\xF9-\xFC]/g,
        c: /\xE7/g,
        n: /\xF1/g
    };

    for (var letra in mapaAcentosHex) {
        var expressaoRegular = mapaAcentosHex[letra];
        string = string.replace(expressaoRegular, letra);
    }

    return string;
}
//use obj dinamico $(".test").live('click', function(){


